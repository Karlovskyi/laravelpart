<?php


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$userGroupData = ['namespace' => 'Rating\User', 'middleware' => 'last_modified'];

Route::group($userGroupData, function ()
{
    Route::get('/', 'MainPageController@main')
        ->name('user.main');

    Route::get('/reliability', 'ReliabilityPageController@reliability')
        ->name('user.reliability');

    Route::post('/compare', 'ComparePageController@comparePost')
        ->name('user.compare');
    Route::get('/compare', 'ComparePageController@compare')
        ->name('user.compare');


    Route::get('/compare-all', 'ComparePageController@compareAll')
        ->name('user.compare-all');

    Route::get('/how-to-choose', 'HowToChoosePageController@howToChoose')
        ->name('user.how-to-choose');

    Route::get('/about', 'AboutPageController@about')
        ->name('user.about');

    Route::get('/company/{company}/', 'CompanyPageController@company')
        ->name('user.company');

    Route::get('/{company}/company/{page?}', 'CompanyPageController@redirect')
        ->name('user.company.redirect');

    Route::get('/company/{company}/{page}', 'CompanyPageController@companyOld')
        ->name('user.company.old');


    Route::get('/account', 'AccountPageController@index')
        ->name('user.account');

    Route::get('/preview', 'PreviewPageController@index')
        ->name('user.preview');
});


/** Переадресация на групу admin/rating */
Route::get('admin/', function ()
{
    return redirect()->route('rating.admin');
});

$adminGroupData = ['namespace' => 'Rating\Admin', 'prefix' => 'admin/rating', 'middleware' => 'session'];

Route::group($adminGroupData, function ()
{
    Route::get('/auth', 'AuthController@auth')
        ->name('rating.admin.auth');
    Route::patch('/check', 'AuthController@check')
        ->name('rating.admin.check');

    Route::get('/', function ()
    {
        return view('rating.admin.index');
    })->name('rating.admin');

    $companiesAdminMethods = ['index', 'show', 'edit', 'update', 'create', 'store'];
    Route::resource('companies', 'RatingCompaniesController')
        ->names('rating.admin.companies')
        ->only($companiesAdminMethods);

    $reviewsAdminMethods = ['index', 'show', 'edit', 'update', 'destroy'];
    Route::resource('reviews', 'RatingReviewsController')
        ->names('rating.admin.reviews')
        ->only($reviewsAdminMethods);

    $imagesAdminMethods = ['index', 'edit', 'update'];
    Route::resource('images', 'RatingImagesController')
        ->names('rating.admin.images')
        ->only($imagesAdminMethods);

    Route::get('images/show-by-company/{company}', 'RatingImagesController@showByCompany')
        ->name('rating.admin.images.show-by-company');

    $pageSearchAttributesMethods = ['index', 'edit', 'update', 'create', 'store'];
    Route::resource('page-search-attributes', 'PageSearchAttributesController')
        ->names('rating.admin.page-search-attributes')
        ->only($pageSearchAttributesMethods);

    $pageHeadersMethods = ['index', 'edit', 'update', 'create', 'store'];
    Route::resource('headers', 'PageHeadersController')
        ->names('rating.admin.headers')
        ->only($pageHeadersMethods);

    $criteriaMethods = ['index', 'edit', 'update'];
    Route::resource('criteria', 'RatingCriteriaController')
        ->names('rating.admin.criteria')
        ->only($criteriaMethods);

    $constantsMethods = ['index', 'edit', 'update'];
    Route::resource('constants', 'ConstantsController')
        ->names('rating.admin.constants')
        ->only($constantsMethods);

    $pageRedirectsMethods = ['index', 'create', 'store', 'edit', 'update', 'destroy'];
    Route::resource('page-redirects', 'PageRedirectsController')
        ->names('rating.admin.page-redirects')
        ->only($pageRedirectsMethods);

    $pagesMethods = ['index', 'edit', 'update','create', 'store'];
    Route::resource('pages', 'PageController')
        ->names('rating.admin.pages')
        ->only($pagesMethods);

    Route::patch('pages/re-update/{page}', 'PageController@reUpdate')
        ->name('rating.admin.pages.re-update');

    Route::post('pages/update-all/', 'PageController@updateAll')
        ->name('rating.admin.pages.update-all');

});

$seoGroupData = ['namespace' => 'Seo'];

Route::group($seoGroupData, function () {
    Route::get('/sitemap', 'SitemapController@index')
        ->name('seo.sitemap.index');
    Route::get('/sitemap/pages', 'SitemapController@pages')
        ->name('seo.sitemap.pages');

    Route::get('/sitemap.xml', 'SitemapController@index');
});

//Auth::routes();


Route::get('login', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm'
]);
Route::post('login', [
    'as' => '',
    'uses' => 'Auth\LoginController@login'
]);
Route::post('logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
]);

// Восстановление пароля
Route::post('password/email', [
    'as' => 'password.email',
    'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
]);
Route::get('password/reset', [
    'as' => 'password.request',
    'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
]);
Route::post('password/reset', [
    'as' => 'password.update',
    'uses' => 'Auth\ResetPasswordController@reset'
]);
Route::get('password/reset/{token}', [
    'as' => 'password.reset',
    'uses' => 'Auth\ResetPasswordController@showResetForm'
]);

// Регистнация
Route::get('register', [
    'as' => 'register',
    'uses' => 'Auth\RegisterController@showRegistrationForm'
]);
Route::post('register', [
    'as' => '',
    'uses' => 'Auth\RegisterController@register'
]);

Route::get('/home', 'HomeController@index')->name('home');

Route::fallback('Exceptions\MovedPermanentlyController@index');
