<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$RatingApiGroup = [
    'namespace' => 'Api',
    'prefix' => '/rating'
];

Route::group($RatingApiGroup, function ()
{
    $companiesApiMethods = ['index'];
    Route::apiResource('companies', 'CompaniesController')
        ->names('rating.api.companies')
        ->only($companiesApiMethods);


    $reviewsApiMethods = ['store'];
    Route::apiResource('reviews', 'ReviewsController')
        ->names('rating.api.reviews')
        ->only($reviewsApiMethods);

    Route::get('companies/{company_id}/reviews', 'ReviewsController@getCompany_sReviews');

    Route::get('compare', 'ComparePageController@getDataForPage')->name('rating.api.compare');
});
