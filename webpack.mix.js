const mix = require('laravel-mix');

const version = '1.3.22'
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', `public/js/app.${version}.js`)
    .js('resources/js/admin.js', `public/js/admin.${version}.js`)
    .js('resources/js/reviews.js', `public/js/reviews.${version}.js`)
    .js('resources/js/about.js', `public/js/about.${version}.js`)
    .react('resources/js/compare_func/App', `public/js/compareList.${version}.js`)
    .copyDirectory('resources/logos', 'public/logos')
    .copyDirectory('resources/images', 'public/images')
    .copyDirectory('resources/fonts', 'public/fonts')
    .sass('resources/sass/admin.scss', `public/css/admin.${version}.css`)
    .sass('resources/sass/app.scss', `public/css/app.${version}.css`)
    .options({
        postCss: [
            require('autoprefixer')({
                grid: true,
            }),
        ],
    });


