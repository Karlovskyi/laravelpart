<?php


namespace App\Repositories\Exceptions\PageRedirects;


use App\Repositories\CoreRepository;
use App\Models\PageRedirects as Model;

class ExceptionPageRedirectsRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getRedirectByPath($path) {
        $column = ['id', 'path_from', 'path_to'];
        $result = $this->startConditions()
            ->select($column)
            ->where('path_from', $path)
            ->first();
        return $result;
    }

}
