<?php


namespace App\Repositories\User\Constants;


use App\Repositories\CoreRepository;
use App\Models\Constants as Model;

class UserConstantsRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getReviewsPerPage() {
        $response = $this->startConditions()
            ->select('constant_value')
            ->where('constant_key', 'reviews_per_page')
            ->first();
        return $response->constant_value;
    }

}
