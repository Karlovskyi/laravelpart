<?php


namespace App\Repositories\User\PageHeaders;


use App\Repositories\CoreRepository;
use App\Models\PageHeaders as Model;
use Illuminate\Support\Collection;

class UserPageHeadersRepository extends CoreRepository
{
    private $column = ['key', 'value'];

    protected function getModelClass()
    {
        return Model::class;
    }

    public function getHeadersByKeys (array $arr) {
        $res = $this->getHeadersByArray($arr)
            ->keyBy('key');

        return $res;
    }

    /**
     * @param array $arr
     * @return Collection
     */
    public function getHeadersList(array $arr) {
        return $this->getHeadersByArray($arr)
            ->keyBy('key')
            ->map(function ($item) {
                return $item->value;
            });
    }

    private function getHeadersByArray(array $arr) {
        $res = $this->startConditions()
            ->select($this->column)
            ->whereIn('key', $arr)
            ->toBase()
            ->get();

        return $res;
    }
}
