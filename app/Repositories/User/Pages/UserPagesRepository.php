<?php


namespace App\Repositories\User\Pages;


use App\Models\Pages;
use App\Repositories\CoreRepository;
use App\Models\Pages as Model;

class UserPagesRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * @param $str
     * @return Pages|null
     */

    public function getLastModifiedByIndex($str) {
        $column = ['page_id', 'page_str_index', 'last_modified'];

        $res = $this->startConditions()
            ->select($column)
            ->where('page_str_index', $str)
            ->first();

        return $res;
    }

}
