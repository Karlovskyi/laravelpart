<?php


namespace App\Repositories\User\RatingCriterion;


use App\Repositories\CoreRepository;
use App\Models\RatingCriterion as Model;

class UserRatingCriterionRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getAll()
    {
        $column = ['criteria_id', 'criteria_layout', 'criteria_name',
            'criteria_name_ru', 'criteria_description',
            'criteria_value_1', 'criteria_value_2', 'criteria_value_3'];
        return $this->startConditions()
            ->select($column)
            ->get()
            ->keyBy('criteria_name');
    }

}
