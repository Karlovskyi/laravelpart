<?php


namespace App\Repositories\User\RatingReviews;

use App\Models\RatingReviews as Model;
use App\Repositories\CoreRepository;

class UserRatingReviewsPaginatorRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getPageCount($company_id, $take)
    {
        $response = $this->startConditions()
            ->where('company_id', $company_id)
            ->where('is_published', 1)
            ->count();
        return (int)floor($response / $take);
    }

    public function getMainCompanyReviews($company_id, $page, $max_page, $take)
    {
        $column = ['review_text', 'review_mark', 'reviewer_name', 'review_link', 'review_date', 'review_id'];
        if ($page > $max_page)
        {
            $response = collect([]);
        } else if ($page > 2)
        {
            $response = $this->startConditions()
                ->select($column)
                ->with('images:image_id,image_path,review_id')
                ->where('is_published', 1)
                ->where('company_id', $company_id)
                ->orderBy('review_date', 'desc')
                ->skip($take * ($page - 1))
                ->take($take)
                ->get();

        } else
        {
            $response = $this->startConditions()
                ->select($column)
                ->with('images:image_id,image_path,review_id')
                ->where('is_published', 1)
                ->where('company_id', $company_id)
                ->whereIn('review_mark',
                    [4, 5])
                ->orderBy('review_date', 'desc')
                ->skip($take * ($page - 1))
                ->take($take)
                ->get();
        }
        return $response;
    }

    public function getStatistic($company_id)
    {
        $positiveReviews = $this->startConditions()
            ->where('company_id', $company_id)
            ->whereIn('review_mark', [4, 5])
            ->where('is_published', 1)
            ->count();
        $negativeReviews = $this->startConditions()
            ->where('company_id', $company_id)
            ->whereIn('review_mark', [1, 2, 3])
            ->where('is_published', 1)
            ->count();
        $result = [
            'positive' => $positiveReviews,
            'negative' => $negativeReviews
        ];
        return $result;
    }
    public function getCompanyReviews($page, $positive, $negative, $company_id, $take) {
        $column = ['review_text', 'review_mark', 'reviewer_name', 'review_link', 'review_date', 'review_id'];

        $response1 = $this->startConditions()->select($column)
            ->with('images:image_id,image_path,review_id')
            ->where('is_published', 1)
            ->where('company_id', $company_id)
            ->whereIn('review_mark', [4,5])
            ->orderBy('review_date', 'desc')
            ->skip(($take-2)*($page-1))
            ->take(($take-2))
            ->get();
        $response2 = $this->startConditions()->select($column)
            ->with('images:image_id,image_path,review_id')
            ->where('is_published', 1)
            ->where('company_id', $company_id)
            ->whereIn('review_mark', [1,2,3])
            ->orderBy('review_date', 'desc')
            ->skip(2*($page-1))
            ->take(2)
            ->get();
        $response2->each(function ($item) use ($response1) {
            $response1->push($item);
        });
        return $response1->shuffle();
    }
}
