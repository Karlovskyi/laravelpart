<?php


namespace App\Repositories\User\PageSearchAttributes;


use App\Repositories\CoreRepository;
use App\Models\PageSearchAttributes as Model;

class UserPageSearchRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getAttributes(string $title_index)
    {
        $result = $this->startConditions()
            ->select(['title', 'description', 'title_index', 'keywords'])
            ->where('title_index', $title_index)
            ->first();
        return $result;
    }

}
