<?php


namespace App\Repositories\User\RatingCompanies;

use App\Models\RatingCompanies as Model;
use App\Repositories\CoreRepository;

class UserRatingCompaniesPaginatorRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getCompanyIdAndNameBySlug(string $slug)
    {
        $column = ['company_id', 'company_slug', 'company_name'];
        $resonse = $this->startConditions()
            ->select($column)
            ->where('company_slug', $slug)
            ->first();
        return $resonse;
    }
}
