<?php


namespace App\Repositories\User\RatingCompanies;


use App\Repositories\CoreRepository;
use App\Models\RatingCompanies as Model;
use Illuminate\Database\Eloquent\Collection;

class UserRatingCompaniesRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getUserTopCompanies($count = 10)
    {
        $column = ['company_id', 'company_name', 'company_mark_difference', 'company_logo_path',
            'company_link', 'company_phone', 'company_mark_average', 'company_review_quantity',
            'company_slug', 'company_position'];
        return $this->startConditions()
            ->select($column)
            ->where('is_published', '=', 1)
            ->orderBy('company_mark_average', 'desc')
            ->get();
    }

    public function getBestCompany()
    {
        $column = ['company_mark_average', 'company_logo_path',
            'company_id', 'company_review_quantity', 'company_position',
            'company_name', 'company_slug'];
        $top1 = $this->startConditions()
            ->select($column)
            ->where('is_published', '=', 1)
            ->where('company_position', '=', 0)
            ->first();
        if ($top1)
        {
            return $top1;
        }
        return $this->startConditions()
            ->select($column)
            ->where('is_published', '=', 1)
            ->orderBy('company_mark_average', 'desc')
            ->first();
    }

    public function getForDropDown()
    {
        $column = ['company_id', 'company_name', 'company_slug', 'company_logo_path', 'company_mark_difference'];
        return $this->startConditions()
            ->select($column)
            ->where('is_published', '=', 1)
            ->orderBy('company_mark_average', 'desc')
            ->get();
    }

    /** @param string $slug
     * @return Model
     */

    public function getCompanyForCompaniesPage($slug)
    {
        $response = $this->startConditions()
            ->select('*')
            ->where('company_slug', '=', $slug)
            ->with(['images' => function ($query)
            {
                $query->select(['image_id', 'image_path'])
                    ->where('image_on_company_page', '=', 1);
            }])
            ->first();
        return $response;
    }

    public function getUserTopReliability($count = 10)
    {
        $column = ['company_id', 'company_name', 'company_logo_path', 'company_slug',
            'company_total_reliability', 'company_mark_average', 'company_term_of_work_clear',
            'company_staff_members_clear', 'company_income_dynamics', 'company_staff_members',
            'company_term_of_work', 'company_amount_of_credit_clear', 'company_reliability_position',
            'company_pay_attention_type', 'company_amount_of_credit', 'company_change_of_leader_clear',
            'company_change_of_leader', 'company_profitability_clear', 'company_profitability_clear',
            'company_financial_autonomy_clear', 'company_financial_autonomy', 'company_legal_address',
            'company_criteria_1', 'company_criteria_2', 'company_criteria_3', 'company_checks', 'company_enforcement_proceedings',
            'company_search_for_colleagues', 'company_payment_of_wages', 'company_bulk_address_register', 'company_relation'
        ];
        $res = $this->startConditions()
            ->select($column)
            ->where('is_published', '=', 1)
            ->orderBy('company_total_reliability', 'desc')
            ->orderBy('company_mark_average', 'desc')
            ->get();

        return $res->sortByDesc('TotalReliabilityPercent')->values(); // sort by accessor
    }

    /**
     * @return Collection;
     * @var Collection $clear
     */

    public function getAllSlugs()
    {

        $clear = $this->startConditions()
            ->select('company_slug', 'company_mark_average', 'company_total_reliability')
            ->where('is_published', '=', 1)
            ->orderBy('company_total_reliability', 'desc')
            ->orderBy('company_mark_average', 'desc')
            ->get();
        $ready = $clear->map(function ($item)
        {
            return $item->company_slug;
        });
        return $ready;
    }

    public function getDualTop($pairQuantity = 1) {
        $column = [
            'company_id', 'company_name', 'company_mark_difference', 'company_logo_path',
            'company_link', 'company_phone', 'company_mark_average', 'company_review_quantity',
            'company_slug', 'company_position', 'company_total_reliability', 'company_pay_attention_type',
            'company_pay_attention_type', 'company_criteria_1', 'company_criteria_2', 'company_criteria_3',
            'company_term_of_work_clear', 'company_staff_members_clear', 'company_amount_of_credit_clear', 'company_amount_of_credit',
            'company_change_of_leader_clear', 'company_change_of_leader', 'company_profitability_clear', 'company_profitability',
            'company_income_dynamics', 'company_staff_members', 'company_term_of_work', 'company_reliability_position',
            'company_financial_autonomy_clear', 'company_financial_autonomy', 'company_legal_address', 'company_checks',
            'company_enforcement_proceedings', 'company_search_for_colleagues', 'company_payment_of_wages', 'company_bulk_address_register',
            'company_relation'
            ];


        $res = $this->startConditions()
            ->select($column)
            ->where('is_published', '=', 1)
            ->orderBy('company_mark_average', 'desc')
            ->with(['reviews' => function($query) use ($pairQuantity) {
                return $query->select(['review_id', 'company_id', 'reviewer_name', 'review_mark', 'review_text', 'review_date', 'review_last_position'])
                    ->where('review_last_position', '<=', $pairQuantity)
                    ->orderBy('review_last_position', 'asc');
            }])
            ->get();

        return $res;
    }

}
