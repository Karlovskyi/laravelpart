<?php


namespace App\Repositories\Seeder\RatingCriterion;


use App\Repositories\CoreRepository;
use App\Models\RatingCriterion as Model;

class SeederRatingCriteriaRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function insertData($data)
    {
        return $this->startConditions()
            ->insert($data);
    }

}
