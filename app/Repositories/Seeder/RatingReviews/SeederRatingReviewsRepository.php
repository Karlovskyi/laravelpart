<?php


namespace App\Repositories\Seeder\RatingReviews;


use App\Repositories\CoreRepository;
use App\Models\RatingReviews as Model;
use Illuminate\Support\Collection;

class SeederRatingReviewsRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    /** @return Collection */

    public function seedHelper()
    {
        return $this->startConditions()
            ->join('rating_companies', 'rating_reviews.company_id', '=', 'rating_companies.company_id')
            ->select('rating_reviews.review_id', 'rating_reviews.reviewer_name', 'rating_reviews.review_date',
                'rating_companies.company_name', 'rating_reviews.review_mark')
            ->toBase()
            ->get()
            ->transform(function ($item)
            {
                return collect($item);
            });

    }
}
