<?php


namespace App\Repositories\Seeder\PageHeaders;


use App\Repositories\CoreRepository;
use App\Models\PageHeaders as Model;

class SeederPageHeaderRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function createHeader(array $data)
    {
        $result = $this->startConditions()
            ->create($data);

        if ($result)
        {
            return $result;
        } else
        {
            throw new \Exception('Не удалось создать заголовок');
        }
    }

    public function getKeys() {
        $column = ['id', 'key'];

        $res = $this->startConditions()
            ->select($column)
            ->toBase()
            ->get();

        return $res;
    }
}
