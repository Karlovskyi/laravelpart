<?php


namespace App\Repositories\Seeder\Constants;


use App\Repositories\CoreRepository;
use App\Models\Constants as Model;

class SeederConstantsRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function insertData($data)
    {
        return $this->startConditions()
            ->insert($data);
    }

}
