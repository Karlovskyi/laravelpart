<?php


namespace App\Repositories\Seeder\LinkingPageAndHeaders;


use App\Models\LinkingPageAndHeaders as Model;
use App\Repositories\CoreRepository;

class SeederLinkingPageAndHeadersRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function createLink(array $data)
    {
        $res = $this->startConditions()
            ->create($data);

        return $res;
    }

}
