<?php


namespace App\Repositories\Seeder\Pages;


use App\Models\Pages;
use App\Repositories\CoreRepository;
use App\Models\Pages as Model;
use Illuminate\Support\Collection;

class SeederPagesRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function createPage(array $data) {
        $res = $this->startConditions()
            ->create($data);
        return $res;
    }


    /**
     * @param array $links
     * @return Collection
     */
    public function getPagesByLinks(array $links) {
        $column = ['page_id', 'page_str_index', 'page_route_name'];
        $route = 'user.company';

        $res = $this->startConditions()
            ->select($column)
            ->toBase()
            ->where('page_route_name', $route)
            ->get();
        $res = $res->filter(function ($item) use ($links) {
            return (bool) in_array($item->page_str_index, $links);
        });
        return $res;

    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */

    public function getPagesForSetRel() {
        $column = ['page_id', 'page_route_name'];

        $res = $this->startConditions()
            ->select($column)
            ->get();

        return $res;
    }

    /** @return Collection */

    public function getPagesRoutes() {
        $column = ['page_id', 'page_route_name'];

        $res = $this->startConditions()
            ->select($column)
            ->toBase()
            ->get();
        return $res;
    }

    /**
     * @param Model $model
     * @param $id
     * @return bool
     */

    public function setSeoId(Pages $model, $id) {
        return $model->update(['seo_id' => $id]);
    }
}
