<?php


namespace App\Repositories\Seeder\RatingCompanies;


use App\Models\RatingCompanies;
use App\Repositories\CoreRepository;
use App\Models\RatingCompanies as Model;

class SeederRatingCompaniesRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function seedHelper()
    {
        return $this->startConditions()
            ->select('company_id', 'company_yandex_id', 'company_google_id',
                'company_otzovick_id')
            ->toBase()
            ->get()
            ->all();

    }

    public function getCompaniesQuantity()
    {
        return $this->startConditions()
            ->count();
    }

    public function getCompaniesSlug() {
        $column = ['company_slug'];
        $res = $this->startConditions()
            ->select($column)
            ->toBase()
            ->get();

        return $res;
    }

    public function getModelCompaniesSlug() {
        $column = ['company_slug', 'company_id'];
        $res = $this->startConditions()
            ->select($column)
            ->get();

        return $res;
    }

    /**
     * @param Model $model
     * @param $page_id
     * @return bool
     */

    public function setPageId(RatingCompanies $model, $page_id) {
        return $model->update(['page_id' => $page_id]);
    }

}
