<?php


namespace App\Repositories\Seeder\PageSearchAttributes;


use App\Repositories\CoreRepository;
use App\Models\PageSearchAttributes as Model;
use Illuminate\Support\Collection;

class SeederPageSearchRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function createElement(array $data)
    {
        return $this->startConditions()
            ->create($data);
    }

    /**
     * @return  Collection
     */

    public function getSeoIndexes() {
        $column = ['id', 'title_index'];

        $res = $this->startConditions()
            ->select($column)
            ->toBase()
            ->get();

        return $res;
    }

}
