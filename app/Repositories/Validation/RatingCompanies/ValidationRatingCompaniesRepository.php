<?php


namespace App\Repositories\Validation\RatingCompanies;


use App\Repositories\CoreRepository;
use App\Models\RatingCompanies as Model;

class ValidationRatingCompaniesRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getPublishedQuantity()
    {
        return $this->startConditions()
            ->where('is_published', '=', 1)
            ->count();
    }

}
