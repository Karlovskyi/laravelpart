<?php


namespace App\Repositories\Admin\Pages;


use App\Models\Pages;
use App\Repositories\CoreRepository;
use App\Models\Pages as Model;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;

class AdminPagesRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * @param int $count
     * @return LengthAwarePaginator
     */

    public function getPaginate($count = 25) {
        $column = ['page_id', 'page_str_index', 'page_route_name', 'last_modified'];
        $res = $this->startConditions()
            ->select($column)
            ->orderBy('page_id', 'desc')
            ->paginate($count);

        return $res;
    }

    /**
     * @param array $data
     * @return Pages|null
     */

    public function storePage(array $data) {
        $res = $this->startConditions()
            ->create($data);
        return $res;
    }

    /**
     * @param $id
     * @return Pages|null;
     */

    public function getPageById($id) {
        $column = ['page_id', 'page_str_index', 'page_route_name', 'last_modified', 'created_at', 'updated_at'];
        $res = $this->startConditions()
            ->select($column)
            ->where('page_id', $id)
            ->first();
        return $res;
    }

    /**
     * @param $id
     * @param array $data
     * @return boolean
     */

    public function updatePage($id, array $data) {
        $res = $this->startConditions()
            ->where('page_id', $id)
            ->first()
            ->update($data);

        return $res;
    }

    /**
     * @param $id
     * @return integer|null
     */

    public function reUpdatePage($id) {
        $res = $this->startConditions()
            ->where('page_id', $id)
            ->update(['last_modified' =>  Carbon::now()->format('Y-m-d H:i:s.u0')]);

        return $res;
    }

    public function updateAllPages() {
        $res = $this->startConditions()
            ->select('id')
            ->update(['last_modified' =>  Carbon::now()->format('Y-m-d H:i:s.u0')]);
        return $res;
    }
}
