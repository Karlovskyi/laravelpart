<?php


namespace App\Repositories\Admin\PageRedirectsRepositories;

use App\Models\PageRedirects as Model;
use App\Repositories\CoreRepository;

class AdminPageRedirectsRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getRedirectsPaginate($count = 20)
    {
        $column = ['id', 'path_from', 'path_to'];
        $result = $this->startConditions()
            ->select($column)
            ->orderBy('id', 'desc')
            ->paginate($count);

        return $result;
    }

    public function storeRedirect($data)
    {
        $result = $this->startConditions()
            ->create($data);
        return $result;
    }

    public function updateRedirect($data, $id) {
        $column = ['id', 'path_from', 'path_to'];
        $result = $this->startConditions()
            ->select($column)
            ->where('id', $id)
            ->first()
            ->update($data);
        return $result;
    }

    public function getRedirectById(int $id)
    {
        $column = ['id', 'path_from', 'path_to', 'created_at', 'updated_at'];
        $result = $this->startConditions()
            ->select($column)
            ->where('id', $id)
            ->first();
        return $result;
    }

    public function getNewRedirect()
    {
        return $this->startConditions();
    }

    public function delete($id) {
        $item = $this->startConditions()
            ->where('id', $id)
            ->first();
        if($item === null) {
            return true;
        }
        $result = $item->delete();
        return $result;
    }
}
