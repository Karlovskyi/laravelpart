<?php


namespace App\Repositories\Admin\RatingImages;


use App\Repositories\CoreRepository;
use App\Models\RatingImages as Model;

class AdminRatingImagesRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public
    function getWithPaginate($count = 50)
    {
        $column = ['image_id', 'image_path', 'is_published'];
        return $this->startConditions()
            ->select($column)
            ->orderBy('image_id', 'desc')
            ->paginate($count);
    }

    public
    function getEditOrShow($id)
    {
        $column = ['image_id', 'image_path', 'review_id', 'is_published', 'image_on_company_page'];
        return $this->startConditions()
            ->select($column)
            ->where('image_id', '=', $id)
            ->first();
    }

}
