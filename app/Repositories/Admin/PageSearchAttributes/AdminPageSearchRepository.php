<?php


namespace App\Repositories\Admin\PageSearchAttributes;


use App\Repositories\CoreRepository;
use App\Models\PageSearchAttributes as Model;

class AdminPageSearchRepository extends CoreRepository
{

    protected function getModelClass()
    {
        return Model::class;
    }

    public function getAllRowsAndCols()
    {
        $result = $this->startConditions()
            ->select(['title', 'title_index', 'id', 'description'])
            ->paginate(10);
        return $result;
    }

    public function getEdit(int $id)
    {
        $result = $this->startConditions()
            ->select(['title', 'title_index', 'id', 'description', 'keywords'])
            ->where('id', $id)
            ->first();
        return $result;
    }

    public function createModel() {
        return $this->startConditions();
    }

    public function createElement(array $data)
    {
        return $this->startConditions()
            ->create($data);
    }
}
