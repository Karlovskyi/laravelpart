<?php


namespace App\Repositories\Admin\PageHeaders;


use App\Repositories\CoreRepository;
use App\Models\PageHeaders as Model;

class AdminPageHeadersRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getAllHeaders()
    {
        $column = ['id', 'key', 'value', 'description'];
        $response = $this->startConditions()
            ->select($column)
            ->toBase()
            ->get();
        return $response;
    }

    public function getEditOrUpdate($id)
    {
        $response = $this->startConditions()
            ->select('*')
            ->where('id', $id)
            ->first();
        return $response;
    }

    public function createModel() {
        return $this->startConditions();
    }
    public function createHeader(array $data)
    {
        $result = $this->startConditions()
            ->create($data);

        if ($result)
        {
            return $result;
        } else
        {
            throw new \Exception('Не удалось создать заголовок');
        }
    }

}
