<?php


namespace App\Repositories\Admin\RatingReviews;


use App\Models\RatingReviews as Model;
use App\Repositories\CoreRepository;

class AdminRatingReviewsRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * @param int $id
     * @param int $count
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */

    public function getPaginateByCompanyId($id, $count = 20)
    {
        $column = ['review_id', 'reviewer_name', 'review_mark', 'review_date', 'is_published', 'positive'];
        $result = $this->startConditions()
            ->select($column)
            ->where('company_id', '=', $id)
            ->orderBy('review_id', 'desc')
            ->paginate($count);
        return $result;
    }

    /**
     * @param int $count
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */

    public function getPaginate($count = 30)
    {
        $column = ['review_id', 'reviewer_name', 'review_mark', 'review_date', 'is_published', 'positive'];
        $result = ($this->startConditions()
            ->select($column)
            ->orderBy('review_id', 'desc')
            ->paginate($count));
        return $result;
    }

    /**
     * @param int $count
     * @param string $search
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */

    public function getPaginateWithSearch($count, $search)
    {
        $column = ['review_id', 'reviewer_name', 'review_mark', 'review_date', 'is_published', 'positive'];
        $result = ($this->startConditions()
            ->select($column)
            ->where('reviewer_name', 'like', "%$search%")
            ->orWhere('review_text', 'like', "%$search%")
            ->orderBy('review_id', 'desc')
            ->paginate($count));
        return $result;
    }

    /**
     * @param int $id
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */

    public function getEditOrShow($id)
    {
        $column = [
            'review_id', 'company_id', 'resource_id', 'review_text',
            'review_mark', 'review_date', 'reviewer_name', 'review_link',
            'position', 'is_published', 'positive'
        ];
        return $this->startConditions()
            ->select($column)
            ->where('review_id', $id)
            ->with(['images:image_id,review_id,image_path', 'company:company_id,company_name'])
            ->first();
    }


    public function getUpdate($id)
    {
        $column = [
            'review_id', 'company_id', 'resource_id', 'review_text',
            'review_mark', 'review_date', 'reviewer_name', 'review_link',
            'position', 'is_published', 'positive'
        ];
        return $this->startConditions()
            ->select($column)
            ->where('review_id', '=', $id)
            ->with(['images:image_id,review_id,image_path',
                'company' => function ($query)
                {
                    $colomn = [
                        'company_id', 'company_name', 'company_mark_average', 'company_mark_average_otzovick',
                        'company_mark_average_google', 'company_mark_average_yandex', 'company_review_quantity',
                        'company_review_quantity_google', 'company_review_quantity_yandex',
                        'company_review_quantity_otzovick',
                        'company_mark_difference', 'company_positive', 'company_negative'
                    ];
                    $query->select($colomn);
                }])
            ->first();
    }

    public function getDelete(int $id)
    {
        $result = $this->startConditions()
            ->select(['review_id', 'company_id'])
            ->where('review_id', '=', $id)
            ->with('company:company_id')
            ->first();
        return $result;
    }
}
