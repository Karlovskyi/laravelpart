<?php


namespace App\Repositories\Admin\RatingCompanies;


use App\Repositories\CoreRepository;
use App\Models\RatingCompanies as Model;

class AdminRatingCompaniesRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }
    public function getPagination()
    {
        $column = ['company_name',
            'company_id',
            'company_mark_average', 'company_review_quantity', 'company_review_quantity_yandex',
            'company_review_quantity_google', 'company_review_quantity_otzovick', 'is_published'];
        return $this->startConditions()
            ->select($column)
            ->paginate(10);
    }

    public function getEditOrShow($id)
    {
        return $this->startConditions()
            ->where('company_id', '=', $id)
            ->with(['images' => function ($query)
            {
                $column = ['image_id', 'image_path', 'image_on_company_page'];
                $query->select($column)
                    ->where('image_on_company_page', '=', 1);
            }])
            ->first();
    }

    public function getAllCompaniesForComboBox()
    {
        return $this->startConditions()
            ->select(['company_name', 'company_id'])
            ->get();
    }

    public function getImagesPaginateByCompany($company_id, $count = 51)
    {

        return $this->startConditions()
            ->select('company_id')
            ->where('company_id', '=', $company_id)
            ->first();
    }

}
