<?php


namespace App\Repositories\Admin\RatingCriterion;


use App\Models\RatingCriterion as Model;
use App\Repositories\CoreRepository;

class AdminRatingCritetionRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getCriteriaNames()
    {
        return $this->startConditions()
            ->select('criteria_name', 'criteria_name_ru')
            ->get();
    }

    public function index()
    {
        $column = ['criteria_id', 'criteria_name', 'criteria_name_ru'];
        $response = $this->startConditions()
            ->select($column)
            ->orderBy('criteria_id')
            ->toBase()
            ->get();
        return $response;
    }

    public function getEdit($id)
    {
        $column = [
            'criteria_id', 'criteria_name', 'criteria_name_ru',
            'criteria_description', 'criteria_type', 'criteria_value_1',
            'criteria_value_2', 'criteria_value_3'
        ];
        $response = $this->startConditions()
            ->select($column)
            ->where('criteria_id', $id)
            ->toBase()
            ->first();

        return $response;
    }

    public function getUpdate($id)
    {
        $column = [
            'criteria_id', 'criteria_name_ru', 'criteria_description',
            'criteria_type', 'criteria_value_1',
            'criteria_value_2', 'criteria_value_3'
        ];
        $response = $this->startConditions()
            ->select($column)
            ->where('criteria_id', $id)
            ->first();
        return $response;
    }

}
