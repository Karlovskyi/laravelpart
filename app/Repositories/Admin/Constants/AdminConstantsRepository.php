<?php


namespace App\Repositories\Admin\Constants;


use App\Repositories\CoreRepository;
use App\Models\Constants as Model;

class AdminConstantsRepository extends CoreRepository
{

    protected function getModelClass()
    {
        return Model::class;
    }

    public function getIndex()
    {
        $response = $this->startConditions()
            ->select('*')
            ->paginate(10);
        return $response;
    }
    public function getEdit($id) {
        $response = $this->startConditions()
            ->select('*')
            ->where('constant_id', $id)
            ->first();
        return $response;
    }
}
