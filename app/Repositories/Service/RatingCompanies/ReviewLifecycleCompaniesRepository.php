<?php


namespace App\Repositories\Service\RatingCompanies;


use App\Models\RatingReviews;
use App\Repositories\CoreRepository;
use App\Models\RatingCompanies as Model;
use App\Repositories\Service\RatingReviews\LifecycleReviewsRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as BaseCollection;

class ReviewLifecycleCompaniesRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * @param int $company_id
     * @return RatingReviews
     */

    public function getCompanyWithReviewsOrderedByCreate(int $company_id) {
        $column = ['company_id'];
        $res = $this->startConditions()
            ->select($column)
            ->where('company_id', $company_id)
            ->with(['reviews' => function($query) {
                $column = ['review_id', 'company_id', 'created_at', 'is_published', 'review_last_position', 'review_mark'];
                return $query->select($column)
                    ->where('is_published', 1)
                    ->orderBy('created_at', 'desc')
                    ->orderBy('review_id', 'desc');
            }])->first();

        return $res;
    }

    /**
     * @return Collection
     */

    public function getAllCompaniesWithReviewsOrderedByCreate() {
        $column = ['company_id'];
        $res = $this->startConditions()
            ->select($column)
            ->with(['reviews' => function($query) {
                $column = ['review_id', 'company_id', 'created_at', 'is_published', 'review_last_position', 'review_mark'];
                return $query->select($column)
                    ->where('is_published', 1)
                    ->orderBy('created_at', 'desc')
                    ->orderBy('review_id', 'desc');
            }])->get();

        return $res;
    }


    public function updateReviewsLastPosition(BaseCollection $companies, int $quantity) {
        $reviewRepository = new LifecycleReviewsRepository();
        $updateData = collect();
        $deleteData = collect();
        $companies->each(function ($company) use ($quantity, $updateData, $deleteData) {
            $positiveReviews = $company->reviews->filter(function ($item) { return $item->review_mark > 3;})->values();
            $negativeReviews = $company->reviews->filter(function ($item) { return $item->review_mark <= 3;})->values();

            $this->fillLastPositionCollectionByNewIndexes($positiveReviews, $quantity, $updateData, $deleteData);
            $this->fillLastPositionCollectionByNewIndexes($negativeReviews, $quantity, $updateData, $deleteData);

        });

        foreach ($updateData->keys() as $position) {
            $ids = $updateData->get($position)->toArray();
            $reviewRepository->setLastPositionToReviews($ids, $position);
        }

        $reviewRepository->setLastPositionToReviews($deleteData->toArray(), null);
    }

    private function fillLastPositionCollectionByNewIndexes(&$collection, $quantity, &$updateData, &$deleteData) {
        $collection->each(function ($review, $index) use ($quantity, $updateData, $deleteData) {
            if($index < $quantity) {
                $put = $updateData->get($index+1);
                $put = $put === null ? collect([$review->review_id]) : $put->push($review->review_id);
                $updateData->put($index+1, $put);
            } else if($review->review_last_position !== null) {
                $deleteData->push($review->review_id);
            }
        });
    }

}
