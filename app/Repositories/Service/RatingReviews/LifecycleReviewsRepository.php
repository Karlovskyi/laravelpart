<?php


namespace App\Repositories\Service\RatingReviews;


use App\Repositories\CoreRepository;
use App\Models\RatingReviews as Model;

class LifecycleReviewsRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * @param array $ids
     * @param int|null $position
     * @return int|bool
     */

    public function setLastPositionToReviews(array $ids, $position) {
        $res = $this->startConditions()
            ->query()
            ->whereIn('review_id', $ids)
            ->update(['review_last_position' => $position]);
        return $res;
    }


}
