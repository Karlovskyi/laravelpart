<?php


namespace App\Repositories\Api\RatingReviews;

use App\Models\RatingReviews as Model;
use App\Repositories\CoreRepository;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;


class ApiRatingReviewsRepository extends CoreRepository
{

    protected function getModelClass()
    {
        return Model::class;
    }

    public function storeNewReview(array $review)
    {
        $review['review_must_be_published_at'] = Carbon::now()
            ->add(1, 'day');
        $review['is_published'] = 0;
        $review['positive'] = $review['review_mark'] > 3;
        $imgs = collect($review['imgs']);
        $this->storeTransaction($review, $imgs);
    }

    public function getExistingReviewsFromList(Collection $list)
    {
        $result = $this->startConditions()
            ->select('review_link')
            ->whereIn('review_link', $list)
            ->get();

        return $result;
    }

    private function storeTransaction(array $review, Collection $imgs)
    {
        DB::transaction(function () use ($review, $imgs)
        {
            /** @var Model $item */
            $item = $this->startConditions()
                ->create($review);
            if ($item)
            {
                $imgs->each(function ($img) use ($imgs, $item)
                {
                    $item->images()
                        ->create(['image_path' => $img]);
                });
            }
        });

    }

    public function getBadReviewsByCompanyId($company_id, $page)
    {
        $column =
            ['company_id', 'review_id', 'reviewer_name', 'review_link', 'review_text', 'review_mark', 'review_date'];
        return $this->startConditions()
            ->select($column)
            ->where('company_id', '=', $company_id)
            ->whereIn('review_mark', [1, 2, 3])
            ->with('images:image_id,image_path,review_id')
            ->take(2)
            ->skip(2 * $page - 1)
            ->orderBy('review_date', 'desc')
            ->get();
    }

    public function getGoodReviewsByCompanyId($company_id, $page)
    {
        $column =
            ['company_id', 'review_id', 'reviewer_name', 'review_link', 'review_text', 'review_mark', 'review_date'];

        return $this->startConditions()
            ->select($column)
            ->where('company_id', '=', $company_id)
            ->whereIn('review_mark', [4, 5])
            ->with('images:image_id,image_path,review_id')
            ->take(5)
            ->skip(5 * $page - 1)
            ->orderBy('review_date', 'desc')
            ->get();
    }

    public function getGoodSpecialReviewsByCompanyId($company_id, $page)
    {
        $column =
            ['company_id', 'review_id', 'reviewer_name', 'review_link', 'review_text', 'review_mark', 'review_date'];
        return $this->startConditions()
            ->select($column)
            ->where('company_id', '=', $company_id)
            ->whereIn('review_mark', [4, 5])
            ->with('images:image_id,image_path,review_id')
            ->take(5)
            ->skip(5 * $page - 2)
            ->orderBy('review_date', 'desc')
            ->get();
    }

    public function getBadSpecialReviewsByCompanyId($company_id, $page)
    {
        $column =
            ['company_id', 'review_id', 'reviewer_name', 'review_link', 'review_text', 'review_mark', 'review_date'];
        if ($page >= 3)
            return $this->startConditions()
                ->select($column)
                ->where('company_id', '=', $company_id)
                ->whereIn('review_mark', [1, 2, 3])
                ->with('images:image_id,image_path,review_id')
                ->take(1)
                ->skip(1 * $page - 3)
                ->orderBy('review_date', 'desc')
                ->get();
        return collect([]);
    }
}
