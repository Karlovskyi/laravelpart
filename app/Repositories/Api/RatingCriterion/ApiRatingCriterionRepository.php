<?php


namespace App\Repositories\Api\RatingCriterion;


use App\Repositories\CoreRepository;
use App\Models\RatingCriterion as Model;

class ApiRatingCriterionRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }
    public function getAllApi()
    {
        $column = ['criteria_id', 'criteria_name',
            'criteria_name_ru', 'criteria_description',
            'criteria_value_1', 'criteria_value_2', 'criteria_value_3'];
        return $this->startConditions()
            ->select($column)
            ->get()
            ->keyBy('criteria_name');
    }
}
