<?php


namespace App\Repositories\Api\RatingCompanies;

use App\Models\RatingCompanies as Model;
use App\Repositories\CoreRepository;

class ApiRatingCompaniesRepository extends CoreRepository
{


    protected function getModelClass()
    {
        return Model::class;
    }

    public function getCompaniesResourceId()
    {
        $column = ['company_id', 'company_yandex_id', 'company_google_id', 'company_otzovick_id'];
        $result = $this->startConditions()
            ->select($column)
            ->get();
        return $result;
    }

    public function getMainCompaniesId()
    {
        $result = $this->startConditions()
            ->select('company_id')
            ->where('company_name', '=', 'Студия Ремонтов')
            ->first()->company_id;
        return $result;
    }

    public function getPositiveForAllCompanies()
    {
        $result = $this->startConditions()
            ->select('company_id', 'company_positive', 'company_mark_difference')
            ->get()
            ->toBase();
        return $result;
    }

    public function getCompanyIds()
    {
        $result = $this->startConditions()
            ->select('company_id')
            ->toBase()
            ->get();
        return $result;
    }

    public function getEditOrShow($id)
    {
        return $this->startConditions()
            ->where('company_id', '=', $id)
            ->with(['images' => function ($query)
            {
                $column = ['image_id', 'image_path', 'image_on_company_page'];
                $query->select($column)
                    ->where('image_on_company_page', '=', 1);
            }])
            ->first();
    }

    public function getAllCompaniesWithAverageMark($id, $resourceId = null)
    {
        $column = ['company_id'];
        $result =
            $this->startConditions()
                ->select($column)
                ->where('company_id', '=',
                    $id)
                ->with(['reviews' => function ($query) use ($resourceId)
                {
                    if ($resourceId === null)
                    {
                        $query->select('review_id', 'review_mark', 'company_id')
                            ->where('is_published', '=', 1);
                    } else
                    {
                        $query->select('review_id', 'review_mark', 'company_id')
                            ->where('resource_id', '=', $resourceId)
                            ->where('is_published', '=', 1);
                    }

                }])
                ->first();
        return $result;
    }

    public function getPositiveQuantity($id, $resourceId = null)
    {
        $column = ['company_id'];
        $res =
            $this->startConditions()
                ->select($column)
                ->select($column)
                ->where('company_id', '=',
                    $id)
                ->with(['reviews' => function ($query) use ($resourceId)
                {
                    if ($resourceId === null)
                    {
                        $query->select('review_id', 'positive', 'company_id')
                            ->where('positive', '=', 1)
                            ->where('is_published', '=', 1);
                    } else
                    {
                        $query->select('review_id', 'positive', 'company_id')
                            ->where('resource_id', '=',
                                $resourceId)
                            ->where('positive', '=', 1)
                            ->where('is_published', '=', 1);
                    }

                }])
                ->first();
        return count($res->reviews);
    }

    public function getCompaniesAPI()
    {
        $column = ['company_id', 'company_name', 'company_slug', 'company_logo_path', 'company_mark_average',
            'company_review_quantity_yandex', 'company_review_quantity_google', 'company_review_quantity_otzovick',
            'company_mark_average_google', 'company_mark_average_yandex', 'company_mark_average_otzovick',
            'company_total_reliability', 'company_term_of_work', 'company_term_of_work_clear',
            'company_staff_members', 'company_staff_members_clear', 'company_amount_of_credit',
            'company_amount_of_credit_clear', 'company_change_of_leader', 'company_change_of_leader_clear',
            'company_payment_of_wages', 'company_profitability', 'company_profitability_clear',
            'company_legal_address', 'company_checks', 'company_income_dynamics',
            'company_enforcement_proceedings', 'company_bulk_address_register',
            'company_financial_autonomy_clear', 'company_financial_autonomy',
            'company_search_for_colleagues', 'company_mark_difference',
            'company_relation_clear', 'company_relation', 'company_link', 'company_pay_attention', 'company_pay_attention_type'];
        return $this->startConditions()
            ->select($column)
            ->where('is_published', '=', 1)
            ->orderBy('company_total_reliability', 'desc')
            ->orderBy('company_mark_average', 'desc')
            ->get();
    }

    public function getCompanyNameById($id)
    {
        return $this->startConditions()
            ->select('company_name')
            ->where('company_id', $id)
            ->first();
    }
}
