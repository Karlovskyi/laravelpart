<?php


namespace App\Repositories\Commands\RatingReviews;


use App\Repositories\CoreRepository;
use App\Models\RatingReviews as Model;

class CommandsRatingReviewsRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function publishReview(Model $review)
    {
        return $review->update(['is_published' => 1]);
    }

    public function getUnpublishedReviews()
    {
        $column = ['company_id', 'review_mark', 'is_published', "review_must_be_published_at",'review_id'];
        $result = $this->startConditions()
            ->select($column)
            ->whereNotNull('review_must_be_published_at')
            ->get();

        return $result;
    }

    public function getCompanyReviewDataById($companyId)
    {
        $result = [
            '1' => $this->startConditions()
                ->where('review_mark', '=', 1)
                ->where('company_id', '=', $companyId)
                ->where('is_published', '=', 1)
                ->count(),
            '2' => $this->startConditions()
                ->where('review_mark', '=', 2)
                ->where('company_id', '=', $companyId)
                ->where('is_published', '=', 1)
                ->count(),
            '3' => $this->startConditions()
                ->where('review_mark', '=', 3)
                ->where('company_id', '=', $companyId)
                ->where('is_published', '=', 1)
                ->count(),
        ];

        return $result;
    }
}
