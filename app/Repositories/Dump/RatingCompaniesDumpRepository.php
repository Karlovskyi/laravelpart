<?php


namespace App\Repositories\Dump;


use App\Repositories\CoreRepository;
use App\Models\RatingCompanies as Model;

class RatingCompaniesDumpRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getDump() {
        $response = $this->startConditions()
            ->select('*')
            ->orderBy('company_id')
            ->get();

        return $response;
    }
}
