<?php


namespace App\Repositories\Seo\Sitemap;


use App\Repositories\CoreRepository;
use App\Models\RatingCompanies as Model;

class CompaniesSitemapRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getCompaniesCollection() {
        $column = ['company_id', 'company_slug', 'updated_at', 'is_published'];

        $res = $this->startConditions()
            ->select($column)
            ->where('is_published', 1)
            ->with(['reviews' => function ($q) {
                $column = ['review_id', 'updated_at', 'company_id', 'is_published'];

                $q->select($column)
                    ->orderBy('updated_at', 'desc')
                    ->where('updated_at');
            }])
            ->get();


        return $res;
    }

}
