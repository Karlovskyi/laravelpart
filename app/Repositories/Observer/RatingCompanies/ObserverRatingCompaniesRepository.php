<?php


namespace App\Repositories\Observer\RatingCompanies;


use App\Repositories\CoreRepository;
use App\Models\RatingCompanies as Model;
use App\Repositories\Observer\Pages\ObserverPagesRepository;

class ObserverRatingCompaniesRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function refreshPageByCompanyId($id) {
        $column = ['company_id', 'page_id'];

        $res = $this->startConditions()
            ->select($column)
            ->where('company_id', $id)
            ->first();

        $rep = new ObserverPagesRepository();
        $res2 = $rep->refreshPage($res->page_id);
        return $res2;
    }
}
