<?php


namespace App\Repositories\Observer\LinkingPageAndHeaders;


use App\Repositories\CoreRepository;
use App\Models\LinkingPageAndHeaders as Model;
use App\Repositories\Observer\Pages\ObserverPagesRepository;

class ObserverLinkingPagesAndHeadersRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * @param $id
     * @return int
     */

    public function refreshPagesByHeaderId($id) {

        $column = ['id', 'page_id', 'header_id'];
        $pageIds = $this->startConditions()
            ->select($column)
            ->toBase()
            ->where('header_id', $id)
            ->get()
            ->map(function ($link) {
                return $link->page_id;
            })->toArray();
        $rep = new ObserverPagesRepository();
        $res = $rep->refreshPages($pageIds);
        return $res;
    }
}
