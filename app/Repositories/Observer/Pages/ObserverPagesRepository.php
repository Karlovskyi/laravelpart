<?php


namespace App\Repositories\Observer\Pages;


use App\Repositories\CoreRepository;
use App\Models\Pages as Model;
use Carbon\Carbon;

class ObserverPagesRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * @param $id
     * @return bool
     */

    public function refreshPage($id) {
        $column = ['page_id', 'last_modified'];
        $res = $this->startConditions()
            ->select($column)
            ->where('page_id', $id)
            ->first();
        if($res) {
            return $res->refreshLastModified();
        }
        return false;


    }

    /**
     * @param array $ids
     * @return integer
     */

    public function refreshPages(array $ids) {
        $res = $this->startConditions()
            ->whereIn('page_id', $ids)
            ->update(['last_modified' => Carbon::now()->format('Y-m-d H:i:s.u0')]);

        return $res;
    }
}
