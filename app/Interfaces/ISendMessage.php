<?php


namespace App\Interfaces;


interface ISendMessage
{
    public function sendMessage($message);
}
