<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Constants extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'constant_id';
    protected $fillable = [
        'constant_value'
    ];
}
