<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageRedirects extends Model
{
    protected $fillable = [
        'path_from',
        'path_to'
    ];
}
