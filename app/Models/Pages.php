<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $fillable = [
        'page_str_index',
        'page_route_name',
        'last_modified'
    ];

    protected $primaryKey = 'page_id';

    public function refreshLastModified()
    {
        return $this->update(['last_modified' => Carbon::now()->format('Y-m-d H:i:s.u0')]);
    }
}
