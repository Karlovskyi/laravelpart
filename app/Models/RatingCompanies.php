<?php

namespace App\Models;

use App\General\Constants\MainAppConstants;
use App\Models\Aggregates\PayAttentionConstants;
use App\Models\Interfaces\IPayAttentionConstants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RatingCompanies extends Model implements IPayAttentionConstants
{
    use SoftDeletes;

    protected $primaryKey = 'company_id';
    private $payAttentionConstants;

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->payAttentionConstants = new PayAttentionConstants();
    }

    protected $fillable = [
        "company_name",
        "company_slug",
        "company_link",
        "company_address",
        "company_logo_path",
        "company_phone",
        "company_google_maps_link",
        "company_inst",
        "company_fb",
        "company_you_tube",
        "company_vk",
        "company_description",
        "company_term_of_work",
        "company_staff_members",
        "company_amount_of_credit",
        "company_change_of_leader",
        "company_payment_of_wages",
        "company_profitability",
        "company_legal_address",
        "company_checks",
        "company_financial_autonomy",
        "company_income_dynamics",
        "company_enforcement_proceedings",
        "company_bulk_address_register",
        "company_search_for_colleagues",

        "company_term_of_work_clear",
        "company_staff_members_clear",
        "company_amount_of_credit_clear",
        "company_change_of_leader_clear",
        "company_profitability_clear",
        "company_financial_autonomy_clear",

        "company_total_reliability",

        "is_published",
        "company_position",
        "company_reliability_position",

        "company_criteria_1",
        "company_criteria_2",
        "company_criteria_3",

        "company_mark_average",
        "company_mark_average_yandex",
        "company_mark_average_google",
        "company_mark_average_otzovick",

        'company_review_quantity',
        'company_review_quantity_yandex',
        'company_review_quantity_google',
        'company_review_quantity_otzovick',

        'company_mark_difference',
        'company_positive',
        'company_negative',

        'company_rusprofile_link',
        'company_ogrn',
        'company_inn',
        'company_legal_address_value',
        'company_legal_address_comment',

        'company_relation_clear',
        'company_relation',
        'company_pay_attention',
        'company_pay_attention_type',

        'company_yandex_id',
        'company_google_id',
        'company_otzovick_id',

        'page_id'
    ];


    public function reviews()
    {
        return $this->hasMany(RatingReviews::class, 'company_id');
    }

    public function lastSomeReviews($count = 2) {
        return $this->reviews->slice(0, $count);
    }

    public function first_review()
    {
        return $this->reviews->first();
    }

    public function images()
    {
        return $this->hasManyThrough(RatingImages::class, RatingReviews::class,
            'company_id', 'review_id', 'company_id', 'review_id');
    }

    function getReviewTotalDataAttribute()
    {
        return "Всего: {$this->company_review_quantity}
        (Yandex: {$this->company_review_quantity_yandex},
        Google: {$this->company_review_quantity_google},
        Otzovick: {$this->company_review_quantity_otzovick})";
    }

    public function getCompanyLinkWithoutHTTPAttribute()
    {
        $stringToEdit = substr($this->company_link, 0, strripos($this->company_link, '?'));
        if(!$stringToEdit) {
            return '';
        }
        if ($stringToEdit === 'https://xn----itbbtigglchjdag0o.xn--p1ai/')
        {
            return 'ремонт-экспресс.рф';
        }
        $subResult = substr($stringToEdit, strpos($stringToEdit, '//') + 2);
        if ($subResult[strlen($subResult) - 1] === '/')
        {
            return substr($subResult, 0, -1);
        }
        return $subResult;
    }

    public function getCompanyPhoneWithNoSpacesAttribute()
    {
        return str_replace(' ', '', $this->company_phone);
    }

    public function getCompanyRusprofileLinkWithoutHTTPAttribute()
    {
        $result = substr($this->company_rusprofile_link,
            strpos($this->company_rusprofile_link, '//') + 2);
        return $result;
    }

    public function getLastModifiedPageAttribute()
    {
        $review_updated_at = $this->first_review()->updated_at === null ? $this->updated_at : $this->first_review()
            ->updated_at;

        return $this->updated_at->max($review_updated_at);
    }

    public function getPayAttentionDumpCollectionAttribute() {
        return $this->getPayAttentionDumpCollection();
    }

    public function getTotalReliabilityPercentAttribute() {
        $type = $this->company_pay_attention_type;
        $numerator = $this->company_total_reliability;
        $denominator = MainAppConstants::RELIABILITY_MAX;
        switch ($type) {
            case $this->getPayAttention('PAY_ATTENTION_BAD'): $denominator += 2;break;
            case $this->getPayAttention('PAY_ATTENTION_AVERAGE'): $denominator += 2; $numerator += 1;break;
            case $this->getPayAttention('PAY_ATTENTION_GOOD'): $denominator += 2;$numerator += 2;break;
            case $this->getPayAttention('PAY_ATTENTION_NOT_MORE_THAN_AVERAGE'): return min(80, round($numerator / $denominator * 100));
            case $this->getPayAttention('PAY_ATTENTION_ALL_IS_BAD'): return 0;
        }
        return round($numerator / $denominator * 100);
    }

    public function getTotalReliabilityTextStatusAttribute() {
        if($this->company_pay_attention_type === $this->getPayAttention('PAY_ATTENTION_CRITICAL') ||
            $this->company_pay_attention_type ===  $this->getPayAttention('PAY_ATTENTION_ALL_IS_BAD'))
           return 'Плохо';
        else if($this->TotalReliabilityPercent > 80)
            return 'Отлично!';
        else if($this->TotalReliabilityPercent  > 60)
            return 'Средне';
        else
            return 'Плохо';

    }

    public function getTotalReliabilityStyleStatusAttribute() {
        if($this->company_pay_attention_type === $this->getPayAttention('PAY_ATTENTION_CRITICAL') ||
            $this->company_pay_attention_type ===  $this->getPayAttention('PAY_ATTENTION_ALL_IS_BAD'))
            return 'danger';
        else if($this->TotalReliabilityPercent > 80)
            return 'success';
        else if($this->TotalReliabilityPercent  > 60)
            return 'warning';
        else
            return 'danger';
    }

    public function getAttantionTypeToNumberStatusAttribute()
    {
        return $this->payAttentionConstants->valueToNumberStatus($this->company_pay_attention_type);
    }

    public function getIsCriticalAttentionAttribute() {
        return $this->company_pay_attention_type === $this->getPayAttention('PAY_ATTENTION_CRITICAL');
    }

    public function getIsAllBadAttentionAttribute() {
        return $this->company_pay_attention_type === $this->getPayAttention('PAY_ATTENTION_ALL_IS_BAD');
    }

    public function getPayAttention(string $field)
    {
        return $this->payAttentionConstants->getPayAttention($field);
    }

    public function getPayAttentionDescription(string $field)
    {
        return $this->payAttentionConstants->getPayAttentionDescription($field);
    }

    public function getPayAttentionCollection()
    {
        return $this->payAttentionConstants->getPayAttentionCollection();
    }

    public function getPayAttentionDescriptionCollection()
    {
        return $this->payAttentionConstants->getPayAttentionDescriptionCollection();
    }

    public function getPayAttentionDumpCollection()
    {
        return $this->payAttentionConstants->getPayAttentionDumpCollection();
    }

    public function getStatusField(string $field) {
        return $this->company_pay_attention_type ===  $this->getPayAttention('PAY_ATTENTION_ALL_IS_BAD') ? 0 : $this[$field];
    }
}
