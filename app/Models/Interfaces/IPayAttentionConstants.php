<?php


namespace App\Models\Interfaces;


interface IPayAttentionConstants
{
    /**
     * @param string $field
     */
    public function getPayAttention(string $field);

    /**
     * @param string $field
     */
    public function getPayAttentionDescription(string $field);

    public function getPayAttentionCollection();
    public function getPayAttentionDescriptionCollection();
    public function getPayAttentionDumpCollection();
}
