<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RatingCriterion extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'criteria_id';

    protected $fillable = [
        'criteria_name_ru',
        'criteria_value_1',
        'criteria_value_2',
        'criteria_value_3',
        'criteria_description',

    ];

    public function replaceData($field, $data)
    {
        $strData = is_numeric($data) ? $this->addSplitterToNum($data) : $data;
        if ($this->criteria_name === 'company_amount_of_credit')
            return $this->amountOfCreditExaction($data, $strData);
        $str = $this->getAttribute($field);
        $withData = str_replace('(data)', $strData, $str);

        return $this->replaceYear($withData, $data);
    }

    private function amountOfCreditExaction($data, $strData)
    {
        $str = $this->getAttribute('criteria_value_1');
        $replace = $data >= 10000 ? "<span class=\"red-text\">$strData</span>" : $strData;
        return str_replace('(data)', $replace, $str);
    }

    private function addSplitterToNum($num)
    {
        $strNum = (string) $num;
        for($i = strlen($strNum) - 3; $i >= 1; $i -= 3) {
            $strNum = substr($strNum, 0, $i).' '.substr($strNum, $i);
        }
        return $strNum;
    }
    private function replaceYear($str, $data) {
        $int = (int) $data;
        if($int === 1) {
            $word = 'год';
        } else if($int > 1 && $int < 5) {
            $word = 'года';
        } else {
            $word = 'лет';
        }
        return str_replace('(year)', $word, $str);
    }
}
