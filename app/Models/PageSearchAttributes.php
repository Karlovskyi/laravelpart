<?php

namespace App\Models;

use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Model;

class PageSearchAttributes extends Model
{
    use Timestamp;
    protected $fillable = [
        'title', 'description', 'keywords', 'title_index'
    ];


    public function pages() {
        return $this->hasMany(Pages::class, 'seo_id');
    }
}
