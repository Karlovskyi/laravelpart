<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RatingImages extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'image_id';

    protected $fillable = [
        'is_published',
        'image_on_company_page',
        'image_path'
    ];

    public
    function review()
    {
        return $this->belongsTo(RatingReviews::class, 'review_id');
    }
}
