<?php

namespace App\Models;

use Carbon\Carbon;
use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RatingReviews extends Model
{
    use SoftDeletes, Timestamp;

    protected $fillable = [
        "reviewer_name",
        "review_link",
        "review_mark",
        "position",
        "resource_id",
        "company_id" ,
        "is_published",
        "review_text",
        "review_date",
        "positive",
        "review_must_be_published_at",
        "review_last_position"
    ];

    protected $primaryKey = 'review_id';

    public
    function images()
    {
        return $this->hasMany(RatingImages::class, 'review_id');
    }

    public
    function company()
    {
        return $this->belongsTo(RatingCompanies::class, 'company_id');
    }

    public
    function getReviewDateAttribute($date)
    {
        return (Carbon::parse($date)->isoFormat('DD.MM.YYYY'));
    }

    public
    function getResourceIdAttribute($resource_id)
    {
        if ($resource_id === 0)
        {
            return 'Yandex';
        } else if ($resource_id === 1)
        {
            return 'Google';
        } else if ($resource_id === 2)
        {
            return 'Otzovick';
        } else {
            return 'Что-то пошло те так';
        }
    }
    public function getReviewLinkAttribute($review_link)
    {
        if(!$review_link)
        {
         return null;
        }
        return $review_link;
    }

    public function getReviewerInitialsAttribute() {
        $initialsArray = explode(' ', $this->reviewer_name);
        $result = '';
        foreach ($initialsArray as $initial) {
            $result .= mb_substr($initial, 0, 1) ?? '';
        }

        return strtoupper($result);
    }

    public function getSlicedReviewTextAttribute() {
        $count = 120;
        if(mb_strlen($this->review_text) < $count) {
            return $this->review_text;
        }

        return trim(mb_substr($this->review_text, 0, $count)) . '...';
    }
}
