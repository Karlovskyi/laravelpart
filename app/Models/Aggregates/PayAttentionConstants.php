<?php


namespace App\Models\Aggregates;


use App\Models\Interfaces\IPayAttentionConstants;
use Illuminate\Support\Collection;

class PayAttentionConstants implements IPayAttentionConstants
{


    /**
     * @param string $field
     * @return int|null
     */

    public function getPayAttention(string $field)
    {
        return $this::PAY_ATTENTION_TYPES[$field]['value'] ?? null;
    }


    /**
     * @param string $field
     * @return int|null
     */

    public function getPayAttentionDescription(string $field)
    {
        return $this::PAY_ATTENTION_TYPES[$field]['description'] ?? null;
    }

    /**
     * @return Collection
     */

    public function getPayAttentionCollection()
    {
        return collect($this::PAY_ATTENTION_TYPES)
            ->map(function ($value)
            {
                return $value['value'];
            });
    }

    /**
     * @return Collection
     */

    public function getPayAttentionDescriptionCollection()
    {
        return collect($this::PAY_ATTENTION_TYPES)
            ->map(function ($value)
            {
                return $value['description'];
            });
    }

    /**
     * @return Collection
     */

    public function getPayAttentionDumpCollection()
    {
        return collect($this::PAY_ATTENTION_TYPES);
    }

    public function getEnum() {
        return $this->getPayAttentionCollection()
            ->values()
            ->toArray();
    }

    public function valueToNumberStatus($value) {
        return collect($this::PAY_ATTENTION_TYPES)->where('value', $value)
            ->first()['status-number'] ?? null;
    }


    private const PAY_ATTENTION_TYPES = [
        'PAY_ATTENTION_NO_EFFECT' => [
            'value' => 0,
            'status-number' => null,
            'description' => 'Поле не будет давать никаких эффектов на надежность'
        ],
        'PAY_ATTENTION_CRITICAL' => [
            'value' => 1,
            'status-number' => 0,
            'description' => 'Компания будет с плохой надёжностью независимо от рейтинга надежности'
        ],
        'PAY_ATTENTION_BAD' => [
            'value' => 2,
            'status-number' => 0,
            'description' => 'Поле будет трактовано как плохой критерий'
        ],
        'PAY_ATTENTION_AVERAGE' => [
            'value' => 3,
            'status-number' => 1,
            'description' => 'Поле будет трактовано как средний критерий'
        ],
        'PAY_ATTENTION_GOOD' => [
            'value' => 4,
            'status-number' => 2,
            'description' => 'Поле будет трактовано как хороший критерий'
        ],
        'PAY_ATTENTION_NOT_MORE_THAN_AVERAGE' => [
            'value' => 5,
            'status-number' => 0,
            'description' => 'Рейтинг компании с данным полем не может быть выше среднего'
        ],
        'PAY_ATTENTION_ALL_IS_BAD' => [
            'value' => 6,
            'status-number' => 0,
            'description' => 'С данным полем все поля компании станут плохими'
        ],
    ];
}
