<?php

namespace App\Models;

use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Model;

class PageHeaders extends Model
{
    use Timestamp;

    protected $fillable = [
        'value',
        'key',
        'description'
    ];

}
