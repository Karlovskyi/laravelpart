<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class InEnumRule implements Rule
{

    private $enum;
    private $isStrictMode;

    /**
     * Create a new rule instance.
     *
     * @param array $enum
     * @param bool $strict
     */
    public function __construct($enum = [], $strict = false)
    {
        $this->enum = collect($enum);
        $this->isStrictMode = $strict;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $res = false;

        $this->enum->each(function ($item) use (&$res, $value)
        {
            if ($this->isStrictMode)
            {
                if ($item === $value)
                {
                    $res = true;
                }
            } else {
                if($item == $value) {
                    $res = true;
                }
            }
        });

        return $res;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute is not in allowed range';
    }
}
