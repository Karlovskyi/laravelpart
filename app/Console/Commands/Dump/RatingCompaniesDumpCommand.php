<?php

namespace App\Console\Commands\Dump;

use App\Repositories\Dump\RatingCompaniesDumpRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class RatingCompaniesDumpCommand extends Command
{
    private $dump;

    /**
     * @var string
     */
    protected $signature = 'dump:rating-companies';

    /**
     * @var string
     */
    protected $description = 'Gives RatingCompanies table Dump';

    /**
     * @return void
     */
    public function __construct(RatingCompaniesDumpRepository $repository)
    {
        parent::__construct();

        $this->dump = $repository->getDump();
    }

    /**
     * @return int
     */
    public function handle()
    {
        Storage::disk('local')->put('RatingCompaniesDump.json', json_encode($this->dump, JSON_UNESCAPED_UNICODE));

        return 0;
    }
}
