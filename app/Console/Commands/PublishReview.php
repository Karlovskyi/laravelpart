<?php

namespace App\Console\Commands;

use App\Services\Messages\SendLogMessage;
use App\Services\ReviewPublishingService;
use Illuminate\Console\Command;

class PublishReview extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'review:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ReviewPublishingService $reviewPublishingService
     * @return int
     */
    public function handle(ReviewPublishingService $reviewPublishingService)
    {
        $SendLogMessage = new SendLogMessage();
        $reviewPublishingService->RegularPublishing($SendLogMessage);
        return 0;
    }
}
