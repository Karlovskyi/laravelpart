<?php

namespace App\Console\Commands\RatingReviews;

use App\Services\ReviewLastPositionService\ReviewLastPositionService;
use Illuminate\Console\Command;

class UpdateReviewState extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'review:updateState';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ReviewLastPositionService $reviewLastPositionService
     * @return int
     */
    public function handle(ReviewLastPositionService $reviewLastPositionService)
    {
        $reviewLastPositionService->run();
        return 0;
    }
}
