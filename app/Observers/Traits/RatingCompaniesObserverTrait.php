<?php


namespace App\Observers\Traits;


use App\Models\RatingCompanies;
use App\Services\ReliabilityDataConverter;
use Illuminate\Support\Str;

trait RatingCompaniesObserverTrait
{
    protected function setSlug(RatingCompanies $ratingCompanies)
    {
        if ($ratingCompanies->company_slug === '' || $ratingCompanies->company_slug === null)
        {
            $ratingCompanies->company_slug = Str::slug($ratingCompanies->company_name);
        }
    }

    protected function setRelation($clear)
    {
        if ($clear === '' || $clear === null)
        {
            return 1;
        }
        return 0;
    }

    protected
    function makeCalculation(RatingCompanies $rC, ReliabilityDataConverter $reliabilityDataConverter)
    {
        $rC['company_term_of_work'] = $reliabilityDataConverter->getTermOfWork($rC['company_term_of_work_clear']);
        $rC['company_staff_members'] = $reliabilityDataConverter->getStaffMembers($rC['company_staff_members_clear']);
        $rC['company_amount_of_credit'] = $reliabilityDataConverter
            ->getAmountOfCredit($rC['company_amount_of_credit_clear'], $rC['company_profitability_clear']);
        $rC['company_change_of_leader'] =
            $reliabilityDataConverter->getChangeOfLeader($rC['company_change_of_leader_clear']);
        $rC['company_profitability'] = $reliabilityDataConverter->getProfitability($rC['company_profitability_clear']);
        $rC['company_financial_autonomy'] =
            $reliabilityDataConverter->getFinancialAutonomy($rC['company_financial_autonomy_clear']);
    }

    protected function setTotalReliability(RatingCompanies $rC, ReliabilityDataConverter $reliabilityDataConverter)
    {
        $rC['company_total_reliability'] = $reliabilityDataConverter
            ->getTotalReliability(
                $rC['company_term_of_work_clear'],
                $rC['company_staff_members_clear'],
                $rC['company_amount_of_credit_clear'],
                $rC['company_profitability_clear'],
                $rC['company_change_of_leader_clear'],
                $rC['company_financial_autonomy_clear'],
                $rC['company_legal_address'],
                $rC['company_search_for_colleagues'],
                $rC['company_enforcement_proceedings'],
                $rC['company_payment_of_wages'],
                $rC['company_income_dynamics'],
                $rC['company_bulk_address_register'],
                $rC['company_relation']
            );
    }

}
