<?php

namespace App\Observers;

use App\Models\RatingReviews;
use App\Repositories\Observer\RatingCompanies\ObserverRatingCompaniesRepository;
use App\Services\ReviewLastPositionService\ReviewLastPositionService;
use App\Services\UpdateDBService;

class RatingReviewsObserver
{
    private $updateCompanyService;
    private $reviewLastPositionService;
    public function __construct(UpdateDBService $service, ReviewLastPositionService $reviewLastPositionService) {
        $this->updateCompanyService = $service;
        $this->reviewLastPositionService = $reviewLastPositionService;
    }

    /**
     * Handle the rating reviews "created" event.
     *
     * @param  \App\Models\RatingReviews  $ratingReviews
     * @return void
     */
    public function created(RatingReviews $ratingReviews)
    {
        $this->reviewLastPositionService->run($ratingReviews->company_id);
    }

    /**
     * Handle the rating reviews "updated" event.
     *
     * @param  \App\Models\RatingReviews  $ratingReviews
     * @return void
     */
    public function updating(RatingReviews $ratingReviews)
    {
        $this->setPositive($ratingReviews);

        if($ratingReviews['is_published'] + 0 === 1)
        {
            $ratingReviews['review_must_be_published_at'] = null;
        }
    }
    public function updated(RatingReviews $ratingReviews)
    {
        $company_id = $ratingReviews->company_id;
        $this->updateCompanyService->updateCompany_sReviewData($company_id);
        $ratingCompaniesRepository = new ObserverRatingCompaniesRepository();
        $ratingCompaniesRepository->refreshPageByCompanyId($company_id);

        $this->reviewLastPositionService->run($company_id);
    }

    /**
     * Handle the rating reviews "deleted" event.
     *
     * @param \App\Models\RatingReviews $ratingReviews
     * @return bool
     */
    public function deleting(RatingReviews $ratingReviews)
    {
        $delete_result = $ratingReviews->images()->delete();
        if($delete_result === null) {
            return false;
        }
    }
    public function deleted(RatingReviews $ratingReviews)
    {
        //
    }


    /**
     * Handle the rating reviews "restored" event.
     *
     * @param  \App\Models\RatingReviews  $ratingReviews
     * @return void
     */
    public function restored(RatingReviews $ratingReviews)
    {
        //
    }

    /**
     * Handle the rating reviews "force deleted" event.
     *
     * @param  \App\Models\RatingReviews  $ratingReviews
     * @return void
     */
    public function forceDeleted(RatingReviews $ratingReviews)
    {
        //
    }
    protected
    function setPositive(RatingReviews $ratingReviews) {
        if($ratingReviews->isDirty('review_mark')) {
            $ratingReviews->positive = $ratingReviews->review_mark > 3;
        }
    }
}
