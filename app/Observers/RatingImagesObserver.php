<?php

namespace App\Observers;

use App\Models\RatingImages;
use App\Repositories\Observer\RatingCompanies\ObserverRatingCompaniesRepository;

class RatingImagesObserver
{
    /**
     * Handle the rating logos "created" event.
     *
     * @param  \App\Models\RatingImages  $ratingImages
     * @return void
     */
    public function created(RatingImages $ratingImages)
    {
        //
    }

    /**
     * Handle the rating logos "updated" event.
     *
     * @param  \App\Models\RatingImages  $ratingImages
     * @return void
     */
    public function updated(RatingImages $ratingImages)
    {
        $company_id = $ratingImages->review->company_id;
        $ratingCompaniesRepository = new ObserverRatingCompaniesRepository();
        $ratingCompaniesRepository->refreshPageByCompanyId($company_id);
    }

    /**
     * Handle the rating logos "deleted" event.
     *
     * @param  \App\Models\RatingImages  $ratingImages
     * @return void
     */
    public function deleted(RatingImages $ratingImages)
    {
        //
    }

    /**
     * Handle the rating logos "restored" event.
     *
     * @param  \App\Models\RatingImages  $ratingImages
     * @return void
     */
    public function restored(RatingImages $ratingImages)
    {
        //
    }

    /**
     * Handle the rating logos "force deleted" event.
     *
     * @param  \App\Models\RatingImages  $ratingImages
     * @return void
     */
    public function forceDeleted(RatingImages $ratingImages)
    {
        //
    }
}
