<?php

namespace App\Observers;

use App\Models\PageHeaders;
use App\Repositories\Observer\LinkingPageAndHeaders\ObserverLinkingPagesAndHeadersRepository;

class PageHeadersObserver
{
    /**
     * Handle the page headers "created" event.
     *
     * @param PageHeaders $pageHeaders
     * @return void
     */
    public function created(PageHeaders $pageHeaders)
    {
        //
    }


    /**
     * Handle the page headers "updated" event.
     *
     * @param PageHeaders $pageHeaders
     * @return void
     */
    public function updated(PageHeaders $pageHeaders)
    {
        $rep = new ObserverLinkingPagesAndHeadersRepository();
        $rep->refreshPagesByHeaderId($pageHeaders->id);

    }

    /**
     * Handle the page headers "deleted" event.
     *
     * @param PageHeaders $pageHeaders
     * @return void
     */
    public function deleted(PageHeaders $pageHeaders)
    {
        //
    }

    /**
     * Handle the page headers "restored" event.
     *
     * @param PageHeaders $pageHeaders
     * @return void
     */
    public function restored(PageHeaders $pageHeaders)
    {
        //
    }

    /**
     * Handle the page headers "force deleted" event.
     *
     * @param PageHeaders $pageHeaders
     * @return void
     */
    public function forceDeleted(PageHeaders $pageHeaders)
    {
        //
    }
}
