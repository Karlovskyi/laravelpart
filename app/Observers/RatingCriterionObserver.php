<?php

namespace App\Observers;

use App\Models\RatingCriterion;

class RatingCriterionObserver
{
    public function updating(RatingCriterion $criterion) {
        $criterion->criteria_value_1 = $criterion->criteria_value_1 ?? '';
        $criterion->criteria_value_2 = $criterion->criteria_value_2 ?? '';
        $criterion->criteria_value_3 = $criterion->criteria_value_3 ?? '';

    }
}
