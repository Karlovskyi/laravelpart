<?php

namespace App\Observers;



use App\Models\Pages;
use Carbon\Carbon;

class PagesObserver
{
    /**
     * @param Pages $page
     * @return bool
     */

    public function creating(Pages $page)
    {
        $last_modified = Carbon::now()->format('Y-m-d H:i:s.u0');
        $page->setAttribute('last_modified', $last_modified);
        return true;
    }


    /**
     * @param Pages $page
     * @return bool
     */

    public function updating(Pages $page)
    {
        $last_modified = Carbon::now()->format('Y-m-d H:i:s.u0');
        $page->setAttribute('last_modified', $last_modified);
        return true;
    }
}
