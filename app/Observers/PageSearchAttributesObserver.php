<?php

namespace App\Observers;

use App\Models\PageSearchAttributes;
use App\Repositories\Observer\Pages\ObserverPagesRepository;

class PageSearchAttributesObserver
{
    /**
     * Handle the page search attributes "created" event.
     *
     * @param PageSearchAttributes $pageSearchAttributes
     * @return void
     */
    public function created(PageSearchAttributes $pageSearchAttributes)
    {
        //
    }

    /**
     * Handle the page search attributes "updated" event.
     *
     * @param PageSearchAttributes $pageSearchAttributes
     * @return void
     */
    public function updated(PageSearchAttributes $pageSearchAttributes)
    {
        $pageIds = $pageSearchAttributes->pages->map(function ($item) {
           return $item->page_id;
        })->toArray();

        $pagesRepository = new ObserverPagesRepository();
        $pagesRepository->refreshPages($pageIds);
    }

    /**
     * Handle the page search attributes "deleted" event.
     *
     * @param PageSearchAttributes $pageSearchAttributes
     * @return void
     */
    public function deleted(PageSearchAttributes $pageSearchAttributes)
    {
        //
    }

    /**
     * Handle the page search attributes "restored" event.
     *
     * @param PageSearchAttributes $pageSearchAttributes
     * @return void
     */
    public function restored(PageSearchAttributes $pageSearchAttributes)
    {
        //
    }

    /**
     * Handle the page search attributes "force deleted" event.
     *
     * @param PageSearchAttributes $pageSearchAttributes
     * @return void
     */
    public function forceDeleted(PageSearchAttributes $pageSearchAttributes)
    {
        //
    }
}
