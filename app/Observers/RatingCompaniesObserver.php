<?php

namespace App\Observers;

use App\Models\RatingCompanies;
use App\Observers\Traits\RatingCompaniesObserverTrait;
use App\Repositories\Observer\Pages\ObserverPagesRepository;
use App\Services\ReliabilityDataConverter;

class RatingCompaniesObserver
{
    use RatingCompaniesObserverTrait;

    protected $reliabilityDataConverter;

    public function __construct(ReliabilityDataConverter $reliabilityDataConverter)
    {
        $this->reliabilityDataConverter = $reliabilityDataConverter;
    }

    public function creating(RatingCompanies $ratingCompanies)
    {
        $this->setSlug($ratingCompanies);
    }

    /** @param RatingCompanies $ratingCompanies */

    public
    function updating(RatingCompanies $ratingCompanies)
    {
        if ($ratingCompanies['company_pay_attention'] === '')
        {
            $ratingCompanies['company_pay_attention'] = null;
        }
        if ($ratingCompanies->isDirty('company_relation_clear'))
        {
            $relation_clear = $ratingCompanies['company_relation_clear'];
            $ratingCompanies['company_relation'] = $this->setRelation($relation_clear);
        }
        $this->setSlug($ratingCompanies);
        if ($this->needToBeRecalculated(
            [
                'company_term_of_work_clear', 'company_staff_members_clear', 'company_amount_of_credit_clear',
                'company_profitability_clear', 'company_change_of_leader_clear', 'company_financial_autonomy_clear',
                'company_legal_address', 'company_search_for_colleagues', 'company_enforcement_proceedings',
                'company_payment_of_wages', 'company_income_dynamics', 'company_bulk_address_register', 'company_relation_clear'
            ],
            $ratingCompanies
        ))
        {
            $this->makeCalculation($ratingCompanies, $this->reliabilityDataConverter);
            $this->setTotalReliability($ratingCompanies, $this->reliabilityDataConverter);
        }
    }

    public
    function updated(RatingCompanies $ratingCompanies)
    {
        $pagesRepository = new ObserverPagesRepository();
        $pagesRepository->refreshPage($ratingCompanies->page_id);
    }

    /**
     * @param array $arr
     * @param RatingCompanies $ratingCompanies
     * @return bool ;
     */

    private function needToBeRecalculated(array $arr, RatingCompanies $ratingCompanies)
    {
        $res = false;
        foreach ($arr as $item) {
            if($ratingCompanies->isDirty($item)) {
                $res = true;
                break;
            }
        }
        return $res;
    }
}
