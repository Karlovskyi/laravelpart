<?php

namespace App\Jobs;

use App\Services\Messages\SendLogMessage;
use App\Services\ReviewPublishingService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PublishNewReviewsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @param ReviewPublishingService $reviewPublishingService
     * @return void
     */
    public function handle(ReviewPublishingService $reviewPublishingService)
    {
        $SendLogMessage = new SendLogMessage();
        $reviewPublishingService->RegularPublishing($SendLogMessage);
    }
}
