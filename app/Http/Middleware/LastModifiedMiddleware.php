<?php

namespace App\Http\Middleware;

use App\Repositories\User\PageHeaders\UserPageHeadersRepository;
use App\Repositories\User\Pages\UserPagesRepository;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LastModifiedMiddleware
{

    protected $pageHeadersRepository;
    protected $pagesRepository;
    private $page;

    public function __construct(
        UserPageHeadersRepository $pageHeadersRepository,
        UserPagesRepository $pagesRepository
    )
    {
        $this->page = request()->path() === '/' ? '/' :'/' . request()->path();
        $this->pageHeadersRepository = $pageHeadersRepository;
        $this->pagesRepository = $pagesRepository;
    }
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data = null;
        $pageModel = $this->pagesRepository->getLastModifiedByIndex($this->page);
        if($pageModel) {
            $ifLastModified = $_SERVER['HTTP_IF_MODIFIED_SINCE'] ??  null;
            $envLastModified = $_ENV['HTTP_IF_MODIFIED_SINCE'] ?? null;
            $data = Carbon::parse($pageModel->last_modified)->format('D, d M Y H:i:s \G\M\T');
            if($ifLastModified) {
                $ifData = Carbon::parse($ifLastModified)->format('D, d M Y H:i:s \G\M\T');
                if($data <= $ifData) {
                    header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
                    exit;
                }
            }

            if($envLastModified) {
                $envData = Carbon::parse($envLastModified)->format('D, d M Y H:i:s \G\M\T');
                if($data <= $envData) {
                    header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
                    exit;
                }
            }

        }

        $response = $next($request);
        if($data) {
            $response->header('Last-Modified', $data);
        }

        return $response;
    }
}
