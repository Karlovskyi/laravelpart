<?php

namespace App\Http\Middleware;

use App\Services\LoginPasswordCheckService;
use Closure;

class SessionMiddleware
{
    private $loginPasswordCheckService;

    public function __construct(LoginPasswordCheckService $loginPasswordCheckService)
    {
        $this->loginPasswordCheckService = $loginPasswordCheckService;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public
    function handle($request, Closure $next)
    {
        $login = session('login') ?? '';
        $password =  session('password') ?? '';

        $correctLoginAndPassword = $this
            ->loginPasswordCheckService
            ->checkLoginAndPassword($login, $password);

        $path = $request->path();
        if (!$correctLoginAndPassword
            && $path !== 'admin/rating/auth'
            && $path !== 'admin/rating/check')
        {

            return redirect()
                ->route('rating.admin.auth')
                ->with(['path' => $path]);
        }
        return $next($request);
    }
}
