<?php

namespace App\Http\Controllers\Api;

use App\General\Constants\MainAppConstants;
use App\Http\Controllers\Controller;
use App\Models\Aggregates\PayAttentionConstants;
use App\Repositories\Api\RatingCompanies\ApiRatingCompaniesRepository;
use App\Repositories\Api\RatingCriterion\ApiRatingCriterionRepository;

class ComparePageController extends Controller
{
    public function getDataForPage(
        ApiRatingCompaniesRepository $ratingCompaniesRepository,
        ApiRatingCriterionRepository $ratingCriteriaRepository,
        PayAttentionConstants $payAttentionConstants,
        MainAppConstants $appConstants
    )
    {
        $companies = $ratingCompaniesRepository->getCompaniesAPI();
        $criterion = $ratingCriteriaRepository->getAllApi();
        return [
            "companies" => $companies,
            "criterion" => $criterion,
            "app_constants" => ["RELIABILITY_MAX" => $appConstants::RELIABILITY_MAX],
            "pay_attention_constants" => $payAttentionConstants->getPayAttentionDumpCollection()
        ];
    }
}
