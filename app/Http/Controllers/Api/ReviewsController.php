<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ApiReviewStoreRequest;
use App\Jobs\PublishNewReviewsJob;
use App\Repositories\Api\RatingCompanies\ApiRatingCompaniesRepository;
use App\Repositories\Api\RatingReviews\ApiRatingReviewsRepository;
use App\Repositories\RatingCompaniesRepository;
use App\Repositories\RatingReviewsRepository;
use App\Services\ApiStoreService;
use Illuminate\Http\Request;


class ReviewsController extends Controller
{
    /**
     * @param ApiReviewStoreRequest $request
     * @return string
     */
    public function store(ApiReviewStoreRequest $request, ApiStoreService $apiStoreService)
    {
        $data = collect($request->reviews ?? []);

        $response = $apiStoreService->addNewReviews($data);

        return $response;
    }

    public function getCompany_sReviews(
        $company_id,
        ApiRatingReviewsRepository $ratingReviewsRepository,
        ApiRatingCompaniesRepository $ratingCompaniesRepository,
        Request $request
    )
    {
        $page = $request->page ?? 1;
        $name = $ratingCompaniesRepository->getCompanyNameById($company_id);
        if($name === null) {
            abort(403);
        }
        if ($name->company_name === 'Студия Ремонтов')
        {
            $goodReviews = $ratingReviewsRepository->getGoodSpecialReviewsByCompanyId($company_id, $page);
            $badReviews = $ratingReviewsRepository->getBadSpecialReviewsByCompanyId($company_id, $page);
        } else {
            $badReviews = $ratingReviewsRepository->getBadReviewsByCompanyId($company_id, $page);
            $goodReviews = $ratingReviewsRepository->getGoodReviewsByCompanyId($company_id, $page);

        }

        $reviews = $goodReviews->concat($badReviews)->shuffle();
        return (['data' => $reviews]);
}
}
