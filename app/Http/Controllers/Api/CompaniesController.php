<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Api\RatingCompanies\ApiRatingCompaniesRepository;
use App\Repositories\RatingCompaniesRepository;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{
    /**
     * @param ApiRatingCompaniesRepository $ratingCompaniesRepository
     * @return string[]
     */
    public function index(ApiRatingCompaniesRepository $ratingCompaniesRepository)
    {
        $companies = $ratingCompaniesRepository->getCompaniesAPI();
        return $companies;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
