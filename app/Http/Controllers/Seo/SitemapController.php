<?php

namespace App\Http\Controllers\Seo;

use App\Http\Controllers\Controller;
use App\Repositories\Seo\Sitemap\CompaniesSitemapRepository;
use Illuminate\Http\Request;

class SitemapController extends Controller
{
    public function index() {
        return response()
            ->view('seo.sitemap.index')
            ->header('Content-Type', 'text/xml');
    }

    public function pages(CompaniesSitemapRepository $sitemapRepository) {
        $companies = $sitemapRepository->getCompaniesCollection();

        return response()
            ->view('seo.sitemap.pages', [
                'companies' => $companies
            ])
            ->header('Content-Type', 'text/xml');
    }
}
