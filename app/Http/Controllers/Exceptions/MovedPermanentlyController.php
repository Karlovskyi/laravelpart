<?php

namespace App\Http\Controllers\Exceptions;

use App\Http\Controllers\Controller;
use App\Repositories\Exceptions\PageRedirects\ExceptionPageRedirectsRepository;
use Illuminate\Http\Request;

class MovedPermanentlyController extends Controller
{
    public function index(Request $request, ExceptionPageRedirectsRepository $repository) {
        $redirect = $repository->getRedirectByPath($request->path());

        if($redirect === null)
        {
            $request->isUnredirectable = true;
            abort(404);
        }
        return redirect($redirect->path_to);
    }
}
