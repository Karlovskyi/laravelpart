<?php

namespace App\Http\Controllers\Exceptions;

use App\Http\Controllers\Controller;
use App\Repositories\User\PageSearchAttributes\UserPageSearchRepository;

class NotFoundController extends Controller
{
    public function index()
    {

        $seo = (new UserPageSearchRepository())->getAttributes('not-found');
        return response()->view('errors.404',
            [
                'err' => '404',
                'seo' => $seo], 404);
    }
}
