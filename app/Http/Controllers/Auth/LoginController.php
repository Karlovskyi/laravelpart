<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Repositories\PageHeadersRepository;
use App\Repositories\PageSearchAttributesRepository;
use App\Repositories\User\PageHeaders\UserPageHeadersRepository;
use App\Repositories\User\PageSearchAttributes\UserPageSearchRepository;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(
        UserPageSearchRepository $pageSearchAttributesRepository,
        UserPageHeadersRepository $headersRepository
    )
    {
        $seo = $pageSearchAttributesRepository->getAttributes('login');
        $headers = $headersRepository->getHeadersByKeys(['login']);

        return view('auth.login', ['seo' => $seo, 'headers' => $headers]);
    }

    public function username()
    {
        return $this->findUsername();
    }

    public function findUsername() {
        $login = request()->input('login');

        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'login';

        request()->merge([$fieldType => $login]);

        return $fieldType;
    }

}
