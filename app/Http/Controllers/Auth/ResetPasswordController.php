<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Repositories\User\PageHeaders\UserPageHeadersRepository;
use App\Repositories\User\PageSearchAttributes\UserPageSearchRepository;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function showResetForm(
        Request $request,
        UserPageSearchRepository $pageSearchAttributesRepository,
        UserPageHeadersRepository $headersRepository,
        $token = null
    )
    {
        $seo = $pageSearchAttributesRepository->getAttributes('password-reset');
        $headers = $headersRepository->getHeadersByKeys(['password-reset']);

        return view('auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email, 'seo' => $seo, 'headers' => $headers]
        );
    }

    protected function validationErrorMessages()
    {
        return [
            "token.required" => "Ключ, это необходимый параметр",
            "email.required" => "Это обязательное поле",
            "email.email" => "Поле должно быть почтой",
            "password.required" => "Это обязательное поле",
            "password.confirmed" => "Поля должны совпадать",
            "password.min" => "Минимальная длина пароля :min символов"
        ];
    }
}
