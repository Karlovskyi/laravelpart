<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Repositories\User\PageHeaders\UserPageHeadersRepository;
use App\Repositories\User\PageSearchAttributes\UserPageSearchRepository;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255', 'min:3'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:user'],
            'password' => ['required', 'string', 'min:8', 'same:password-confirm'],
            'login' => ['required', 'string', 'min:5', 'max:255', 'unique:user'],
            'surname' => ['required', 'string', 'min:3', 'max:255']
        ], [
            '*.required' => 'Это обязательное поле',
            '*.string' => 'Это поле должно быть строкой',
            '*.min' => 'Минимальная длина поля: :min символов',
            '*.max' => 'Максимальная длина поля: :max символов',
            '*.email' => 'Поле должно быть адресом электронной почты',
            '*.same' => 'Поля должны совпадать',
            '*.unique' => 'К сожалению, это значение уже занято'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'login' => $data['login'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function showRegistrationForm(
        UserPageSearchRepository $pageSearchAttributesRepository,
        UserPageHeadersRepository $headersRepository
    )
    {
        $seo = $pageSearchAttributesRepository->getAttributes('register');
        $headers = $headersRepository->getHeadersByKeys(['register']);

        return view('auth.register', ['seo' => $seo, 'headers' => $headers]);
    }
}
