<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\User\PageHeaders\UserPageHeadersRepository;
use App\Repositories\User\PageSearchAttributes\UserPageSearchRepository;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function showLinkRequestForm(UserPageSearchRepository $pageSearchAttributesRepository,
        UserPageHeadersRepository $headersRepository) {
        $seo = $pageSearchAttributesRepository->getAttributes('password-forgot');
        $headers = $headersRepository->getHeadersByKeys(['password-reset']);

        return view('auth.passwords.email', ['seo' => $seo, 'headers' => $headers]);
    }
}
