<?php

namespace App\Http\Controllers\Rating\Admin;
use App\Http\Controllers\Rating\Controller as BaseController;

abstract
class Controller extends BaseController
{
    public function __construct()
    {
    }

    protected function deleteRequestFields($request, $fields) {
        foreach ($fields as $field) {
            if(isset($request[$field])) {
                unset($request[$field]);
            }
        }
    }
}
