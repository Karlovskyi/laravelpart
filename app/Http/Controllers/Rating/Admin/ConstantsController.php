<?php

namespace App\Http\Controllers\Rating\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Admin\Constants\AdminConstantsRepository;
use Illuminate\Http\Request;

class ConstantsController extends Controller
{
    private $repository;

    public function __construct(AdminConstantsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $constants = $this->repository->getIndex();

        return view('rating.admin.constants.index', ['constants' => $constants]);
    }


    public function edit($id)
    {
        $constant = $this->repository->getEdit($id);
        if (!$constant)
        {
            abort(404);
        }
        return view('rating.admin.constants.edit', ['constant' => $constant]);
    }

    public function update(Request $request, $id)
    {
        $constant = $this->repository->getEdit($id);
        if (!$constant)
        {
            abort(404);
        }

        $result = $constant->update($request->all());

        if ($result)
        {
            return redirect()
                ->route('rating.admin.constants.edit', $id)
                ->with(['success' => 'База успешно обновлена']);
        } else
        {
            return back()
                ->withInput()
                ->withErrors(['msg' => 'Что-то пошло не так']);
        }
    }
}
