<?php

namespace App\Http\Controllers\Rating\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CritariaUpdateRequest;
use App\Repositories\Admin\RatingCriterion\AdminRatingCritetionRepository;

class RatingCriteriaController extends Controller
{
    private $repository;

    public function __construct(AdminRatingCritetionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $criteria = $this->repository->index();
        return view('rating.admin.criteria.index', ['criteria' => $criteria]);
    }


    public function edit($id)
    {
        $criteria = $this->repository->getEdit($id);
        if (!$criteria)
        {
            abort(404);
        }
        return view('rating.admin.criteria.edit', ['criteria' => $criteria]);
    }


    public function update(CritariaUpdateRequest $request, $id)
    {
        $data = $request->all();
        $criteria = $this->repository->getUpdate($id);

        $result = $criteria->update($data);
        if ($result)
        {
            return redirect()
                ->route('rating.admin.criteria.edit', $criteria->criteria_id)
                ->with(['success' => 'База успешно обновлена!']);
        } else
        {
            return back()
                ->withInput()
                ->withErrors(['msg' => 'Что-то пошло не так']);
        }
    }
}
