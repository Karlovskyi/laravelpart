<?php

namespace App\Http\Controllers\Rating\Admin;

use App\Http\Requests\PageHeaderStoreRequest;
use App\Http\Requests\PageHeaderUpdateRequest;
use App\Repositories\Admin\PageHeaders\AdminPageHeadersRepository;

class PageHeadersController extends Controller
{
    private $headersRepository;

    public function __construct(AdminPageHeadersRepository $headersRepository)
    {
        parent::__construct();
        $this->headersRepository = $headersRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $headers = $this->headersRepository->getAllHeaders();

        return view('rating.admin.page_headers.index', [
            'headers' => $headers
        ]);
    }

    /**
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $header = $this->headersRepository->getEditOrUpdate($id);
        return view('rating.admin.page_headers.edit', [
            'header' => $header
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PageHeaderUpdateRequest $request, $id)
    {
        $this->deleteRequestFields($request, ['key', 'description']);
        $header = $this->headersRepository->getEditOrUpdate($id);
        if ($header === null)
        {
            abort(404);
        }
        $data = $request->all();
        $result = $header->update($data);

        if ($result)
        {
            return redirect()
                ->route('rating.admin.headers.edit', $id)
                ->with(['success' => 'База успешно обновлена']);
        } else
        {
            return back()
                ->withErrors(['msg' => 'Не удалось обновить запись'])
                ->withInput();
        }
    }

    public function create() {
        $header = $this->headersRepository->createModel();
        return view('rating.admin.page_headers.create', [
            'header' => $header
        ]);
    }

    public function store(PageHeaderStoreRequest $request) {
        $result = $this->headersRepository->createHeader($request->all());
        if ($result)
        {
            return redirect()
                ->route('rating.admin.headers.edit', $result->id)
                ->with(['success' => 'Запись успешно создана']);
        } else
        {
            return back()
                ->withErrors(['msg' => 'Не удалось создать запись'])
                ->withInput();
        }

    }
}
