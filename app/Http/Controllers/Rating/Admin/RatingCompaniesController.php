<?php

namespace App\Http\Controllers\Rating\Admin;

use App\Http\Requests\RatingCompaniesStoreRequest;
use App\Http\Requests\RatingCompaniesUpdateRequest;
use App\Models\RatingCompanies;
use App\Repositories\Admin\RatingCompanies\AdminRatingCompaniesRepository;
use App\Repositories\Admin\RatingCriterion\AdminRatingCritetionRepository;
use App\Repositories\Admin\RatingReviews\AdminRatingReviewsRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class RatingCompaniesController extends Controller
{

    private $ratingCompaniesRepository;
    private $ratingReviewsRepository;
    private $ratingCriteriaRepository;

    public function __construct(AdminRatingCompaniesRepository $ratingCompaniesRepository,
        AdminRatingReviewsRepository $ratingReviewsRepository,
        AdminRatingCritetionRepository $ratingCriteriaRepository)
    {
        parent::__construct();

        $this->ratingCompaniesRepository = $ratingCompaniesRepository;
        $this->ratingReviewsRepository = $ratingReviewsRepository;
        $this->ratingCriteriaRepository = $ratingCriteriaRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $companies = $this->ratingCompaniesRepository->getPagination();

        return view('rating.admin.companies.index')->with('companies', $companies);
    }

    /**
     * @param $id
     * @return View
     */
    public function show($id)
    {
        $company = $this->ratingCompaniesRepository->getEditOrShow($id);
        $reviews = $this->ratingReviewsRepository->getPaginateByCompanyId($id);
        if (!$company)
        {
            abort(404);
        }
        $review_active = strripos(url()->full(), 'page') !== false;
        return view('rating.admin.companies.show',
            ['company' => $company, 'reviews' => $reviews,
                'review_active' => $review_active]);

    }

    /**
     * @param $id
     * @return View
     */

    public function edit($id)
    {
        $company = $this->ratingCompaniesRepository->getEditOrShow($id);
        $criterion = $this->ratingCriteriaRepository->getCriteriaNames();

        if (!$company)
        {
            abort(404);
        }

        return view('rating.admin.companies.edit', ['company' => $company, 'criterion' => $criterion]);
    }

    /**
     * @param RatingCompaniesUpdateRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(RatingCompaniesUpdateRequest $request, $id)
    {

        $item = $this->ratingCompaniesRepository->getEditOrShow($id);
        if (!$item)
        {
            abort(404);
        }
        $data = $request->all();
        $result = $item->update($data);

        if ($result)
        {
            return redirect()
                ->route('rating.admin.companies.edit', $item->company_id)
                ->with(['success' => 'База успешно обновлена']);
        } else
        {
            return back()
                ->withErrors(['msg' => 'С обновлением базы что-то пошло не так'])
                ->withInput();
        }
    }

    public function create()
    {
        $company = new RatingCompanies();
        $criterion = $this->ratingCriteriaRepository->getCriteriaNames();

        return view('rating.admin.companies.create', ['company' => $company, 'criterion' => $criterion]);
    }

    public function store(RatingCompaniesStoreRequest $request)
    {
        /** @var RatingCompanies $company */
        $company = new RatingCompanies();
        $result = $company->create($request->all());

        if ($result)
        {
            return redirect()
                ->route('rating.admin.companies.edit', $result->company_id)
                ->with(['success' => 'Компания успешно создана']);
        } else
        {
            return back()
                ->withErrors(['msg' => 'Что-то пошло не так'])
                ->withInput();
        }
    }
}
