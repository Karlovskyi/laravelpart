<?php

namespace App\Http\Controllers\Rating\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PageRedirects\PageRedirectsStoreRequest;
use App\Http\Requests\PageRedirects\PageRedirectsUpdateRequest;
use App\Repositories\Admin\PageRedirectsRepositories\AdminPageRedirectsRepository;

class PageRedirectsController extends Controller
{

    private $repository;

    public function __construct(AdminPageRedirectsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $redirects = $this->repository->getRedirectsPaginate();
        return view('rating.admin.page_redirects.index', ['redirects' => $redirects]);
    }

    public function create()
    {
        $redirect = $this->repository->getNewRedirect();
        return view('rating.admin.page_redirects.create', ['redirect' => $redirect]);
    }

    public function store(PageRedirectsStoreRequest $request)
    {
        $result = $this->repository->storeRedirect($request->all());
        return $this->redirectAfterCU($result, $result->id);
    }

    public function edit($id)
    {
        $redirect = $this->repository->getRedirectById($id);

        if($redirect === null) {
            abort(404);
        }

        return view('rating.admin.page_redirects.edit', ['redirect' => $redirect]);
    }

    public function update(PageRedirectsUpdateRequest $request, $id)
    {
        $result = $this->repository->updateRedirect($request->all(), $id);

        return $this->redirectAfterCU($result, $id);
    }

    public function destroy($id)
    {
        $result = $this->repository->delete($id);
        if($result) {
            return redirect()->route('rating.admin.page-redirects.index')
                ->with(['success' => "Операция прошла успешно"]);
        } else {
            return back()->withInput()
                ->withErrors(['msg' => 'Что-то пошло не так']);
        }
    }

    private function redirectAfterCU($result, $id)
    {
        if ($result)
        {
            return redirect()
                ->route('rating.admin.page-redirects.edit', $id)
                ->with(['success' => "Операция прошла успешно"]);
        } else
        {
            return back()
                ->withInput()
                ->withErrors(['msg' => 'Что-то пошло не так']);
        }
    }
}
