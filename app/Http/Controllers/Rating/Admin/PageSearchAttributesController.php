<?php

namespace App\Http\Controllers\Rating\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PageSearchAttributesStoreRequest;
use App\Repositories\Admin\PageSearchAttributes\AdminPageSearchRepository;
use Illuminate\Http\Request;

class PageSearchAttributesController extends Controller
{
    protected $pageSearchAttributesRepository;

    public function __construct(AdminPageSearchRepository $pageSearchAttributesRepository)
    {
        $this->pageSearchAttributesRepository = $pageSearchAttributesRepository;
    }

    public function index()
    {
        $attributes = $this->pageSearchAttributesRepository->getAllRowsAndCols();
        return view('rating.admin.page_search_attributes.index')->with('attributes', $attributes);
    }

    public function edit($id)
    {
        $attribute = $this->pageSearchAttributesRepository->getEdit($id);
        return view('rating.admin.page_search_attributes.edit')->with('attribute', $attribute);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $attribute = $this->pageSearchAttributesRepository->getEdit($id);
        if (!$attribute)
        {
            abort(404);
        }
        $result = $attribute->update($request->all());
        if ($result)
        {
            return redirect()
                ->route('rating.admin.page-search-attributes.edit', $attribute->id)
                ->with(['success' => 'База успешно обновлена']);
        } else
        {
            return back()
                ->withInput()
                ->withErrors(['msg' => 'Что-то пошло не так']);
        }
    }

    public function create() {
        $attribute = $this->pageSearchAttributesRepository->createModel();
        return view('rating.admin.page_search_attributes.create', ['attribute' => $attribute]);
    }

    public function store(PageSearchAttributesStoreRequest $request) {
        $result = $this->pageSearchAttributesRepository->createElement($request->all());

        if ($result)
        {
            return redirect()
                ->route('rating.admin.page-search-attributes.edit', $result->id)
                ->with(['success' => 'База успешно обновлена']);
        } else
        {
            return back()
                ->withInput()
                ->withErrors(['msg' => 'Что-то пошло не так']);
        }

    }
}
