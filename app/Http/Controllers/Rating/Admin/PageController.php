<?php

namespace App\Http\Controllers\Rating\Admin;

use App\Http\Requests\Admin\Pages\AdminPageReUpdateRequest;
use App\Http\Requests\Admin\Pages\AdminPageStoreRequest;
use App\Http\Requests\Admin\Pages\AdminPageUpdateRequest;
use App\Repositories\Admin\Pages\AdminPagesRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class PageController extends Controller
{
    /**
     * @param AdminPagesRepository $adminPagesRepository
     * @return View
     */

    public function index(AdminPagesRepository $adminPagesRepository)
    {
        return view('rating.admin.pages.index', ['pagination' => $adminPagesRepository->getPaginate()]);
    }

    /**
     * @return View
     */

    public function create()
    {
        return view('rating.admin.pages.create');
    }

    /**
     * @param AdminPageStoreRequest $request
     * @param AdminPagesRepository $adminPagesRepository
     * @return RedirectResponse
     */

    public function store(AdminPageStoreRequest $request, AdminPagesRepository $adminPagesRepository)
    {
        $res = $adminPagesRepository->storePage($request->all());
        if($res) {
            return redirect()
                ->route('rating.admin.pages.edit', $res->page_id)
                ->with(['success' => 'Запись успешно создана']);
        } else {
            return back()
                ->withInput()
                ->withErrors(['msg' => 'Что-то пошло не так']);
        }
    }

    /**
     * @param $id
     * @param AdminPagesRepository $adminPagesRepository
     * @return View
     */

    public function edit($id, AdminPagesRepository $adminPagesRepository) {
        $page = $adminPagesRepository->getPageById($id);
        if(!$page)
            abort(404);
        return view('rating.admin.pages.edit', ['page' => $page]);
    }

    /**
     * @param $id
     * @param AdminPageUpdateRequest $request
     * @param AdminPagesRepository $adminPagesRepository
     * @return RedirectResponse
     */

    public function update($id, AdminPageUpdateRequest $request, AdminPagesRepository $adminPagesRepository) {
        $res = $adminPagesRepository->updatePage($id, $request->all());

        if($res) {
            return redirect()
                ->route('rating.admin.pages.edit', $id)
                ->with(['success' => 'Запись успешно обновлена']);
        } else {
            return back()
                ->withInput()
                ->withErrors(['msg' => 'Что-то пошло не так']);
        }
    }

    public function reUpdate($id, AdminPagesRepository $adminPagesRepository, AdminPageReUpdateRequest $request) {
        $res = $adminPagesRepository->reUpdatePage($id);


        if($res) {
            return redirect()
                ->route('rating.admin.pages.index', ['page' => $request->get('page')])
                ->with(['success' => 'Запись успешно обновлена']);
        } else {
            return back()
                ->withInput()
                ->withErrors(['msg' => 'Что-то пошло не так']);
        }
    }

    public function updateAll(AdminPagesRepository $adminPagesRepository) {
        $res = $adminPagesRepository->updateAllPages();

        if($res) {
            return redirect()
                ->route('rating.admin.pages.index')
                ->with(['success' => 'Все записи успешно обновлены']);
        } else {
            return back()
                ->withInput()
                ->withErrors(['msg' => 'Что-то пошло не так']);
        }
    }
}
