<?php

namespace App\Http\Controllers\Rating\Admin;


use App\Http\Requests\RatingImagesUpdateRequest;
use App\Repositories\Admin\RatingCompanies\AdminRatingCompaniesRepository;
use App\Repositories\Admin\RatingImages\AdminRatingImagesRepository;
use App\Repositories\RatingCompaniesRepository;
use Illuminate\Http\Request;

class RatingImagesController extends Controller
{
    private $ratingImagesRepository;

    public
    function __construct(AdminRatingImagesRepository $ratingImagesRepository)
    {
        parent::__construct();
        $this->ratingImagesRepository = $ratingImagesRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public
    function index()
    {
//        dd(__METHOD__);
        $images = $this->ratingImagesRepository
            ->getWithPaginate();
        return view('rating.admin.images.index',
            ['images' => $images]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function show($id)
    {
        dd(__METHOD__, $id);
    }

    /**
     * @param int $id
     * @return \Illuminate\View\View
     */
    public
    function edit($id)
    {
        $image = $this->ratingImagesRepository->getEditOrShow($id);
        return view('rating.admin.images.edit', [
            'image' => $image,
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public
    function update(RatingImagesUpdateRequest $request, $id)
    {
        /** @var \App\Models\RatingImages $image */
        $image = $this->ratingImagesRepository->getEditOrShow($id);
        if (!$image)
        {
            abort(404);
        }
        $data = $request->all();
        $result = $image->update($data);

        if ($result)
        {
            return redirect()
                ->route('rating.admin.images.edit', $image->image_id)
                ->with(['success' => 'Картинка успешно изменена']);
        } else
        {
            return back()
                ->withInput()
                ->withErrors(['msg' => 'С обновлением что-то пошло не так']);
        }

    }

    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        dd(__METHOD__, $id);
    }

    /**
     * @param $company
     * @param AdminRatingCompaniesRepository $ratingCompaniesRepository
     * @return \Illuminate\View\View
     */

    public function showByCompany($company, AdminRatingCompaniesRepository $ratingCompaniesRepository)
    {
        $images = $ratingCompaniesRepository->getImagesPaginateByCompany($company)->images()->paginate(51);
        return view('rating.admin.images.show_by_company', ['images' => $images]);
    }
}
