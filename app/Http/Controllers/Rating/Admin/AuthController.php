<?php

namespace App\Http\Controllers\Rating\Admin;


use App\Services\LoginPasswordCheckService;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public
    function auth()
    {
        $path = session('path') ?? '/admin/rating';
        return view('rating.admin.auth', ['path' => $path]);
    }

    public
    function check(Request $request, LoginPasswordCheckService $loginPasswordCheckService)
    {
        $login = $request->all()['login'] ?? '';
        $password = $request->all()['password'] ?? '';
        $path = $request->all()['path'] ?? null;
        if ($loginPasswordCheckService->checkLoginAndPassword($login, $password) &&
            $path !== null)
        {
            $request->session()->put(['password' => $password, 'login' => $login]);
            return redirect($path);
        } else
        {
            return back()->with(['err' => true]);
        }
    }
}
