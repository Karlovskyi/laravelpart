<?php

namespace App\Http\Controllers\Rating\Admin;


use App\Http\Requests\RatingReviewsUpdateRequest;
use App\Models\RatingReviews;
use App\Repositories\Admin\RatingCompanies\AdminRatingCompaniesRepository;
use App\Repositories\Admin\RatingReviews\AdminRatingReviewsRepository;
use App\Services\DeleteService;
use App\Services\ReviewsAdminService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class RatingReviewsController extends Controller
{

    private $ratingReviewsRepository;
    private $ratingCompaniesRepository;

    public
    function __construct(
        AdminRatingCompaniesRepository $ratingCompaniesRepository,
        AdminRatingReviewsRepository $ratingReviewsRepository
    )
    {
        parent::__construct();
        $this->ratingReviewsRepository = $ratingReviewsRepository;
        $this->ratingCompaniesRepository = $ratingCompaniesRepository;
    }

    /**
     * @param Request $request
     * @return View
     */
    public
    function index(Request $request)
    {
        $search = $request->get('search');
        if($search) {
            $reviews = $this->ratingReviewsRepository->getPaginateWithSearch(50, $search);
        } else {
            $reviews = $this->ratingReviewsRepository->getPaginate(50);
        }

        return view('rating.admin.reviews.index', ['reviews' => $reviews, 'search' => $search]);
    }

    /**
     * @param int $id
     * @return View
     */

    public
    function show($id)
    {
        $review = $this->ratingReviewsRepository->getEditOrShow($id);
        if (!$review)
        {
            abort(404);
        }
        return view('rating.admin.reviews.show',
            ['review' => $review]);
    }

    /**
     * @param int $id
     * @return View
     */
    public
    function edit($id)
    {
        $review = $this->ratingReviewsRepository->getEditOrShow($id);
        $companies = $this->ratingCompaniesRepository->getAllCompaniesForComboBox();
        if (!$review)
        {
            abort(404);
        }
        return view('rating.admin.reviews.edit',
            ['review' => $review, 'companies' => $companies]);
    }

    /**
     * @param RatingReviewsUpdateRequest $request
     * @param int $id
     * @param ReviewsAdminService $reviewsAdminService
     * @return RedirectResponse
     */
    public
    function update(RatingReviewsUpdateRequest $request, $id, ReviewsAdminService $reviewsAdminService)
    {
        /** @var RatingReviews $item */
        // Обновление компании в Observer

        $item = $this->ratingReviewsRepository->getUpdate($id);

        if (!$item)
        {
            abort(404);
        }
        $data = $request->all();
        return $reviewsAdminService->recalculateTransaction($item, $data);
    }

    /**
     * @param int $id
     * @param DeleteService $deleteService
     * @return RedirectResponse
     */
    public
    function destroy($id, DeleteService $deleteService)
    {
        return $deleteService->deleteReview($id);
    }
}
