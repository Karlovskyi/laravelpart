<?php

namespace App\Http\Controllers\Rating\User;

use App\Repositories\User\PageSearchAttributes\UserPageSearchRepository;
use App\Repositories\User\RatingCompanies\UserRatingCompaniesRepository;
use App\Repositories\User\RatingCriterion\UserRatingCriterionRepository;
use App\Services\Pagination\ReviewPaginationService;

class CompanyPageController extends UserController
{


    /**
     * @param $company_slug
     * @param UserRatingCompaniesRepository $ratingCompaniesRepository
     * @param UserPageSearchRepository $pageSearchAttributesRepository
     * @param UserRatingCriterionRepository $ratingCriteriaRepository
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function company(
        $company_slug,
        UserRatingCompaniesRepository $ratingCompaniesRepository,
        UserPageSearchRepository $pageSearchAttributesRepository,
        UserRatingCriterionRepository $ratingCriteriaRepository
    )
    {

        $page = request()->get('page') ?? 1;
        $company = $ratingCompaniesRepository->getCompanyForCompaniesPage($company_slug);

        if ($company === null || $this->isPageIncorrect($page))
        {
            $route = route('user.company', ['company' => $company_slug]);
            return redirect($route, 301);
        }
        $paginate = (new ReviewPaginationService($company_slug, $page))->main();

        if ($paginate['reviews']->isEmpty() || $page <= 0)
        {
            $route = route('user.company', ['company' => $company_slug]);
            return redirect($route, 301);
        }

        $searchAttributes = $pageSearchAttributesRepository->getAttributes('company');
        $headers = $this->pageHeadersRepository
            ->getHeadersList(['contacts', 'portfolio', 'rely', 'rating', 'reviews', 'company']);
        $companies = $ratingCompaniesRepository->getForDropDown();
        $best = $ratingCompaniesRepository->getBestCompany();

        $criterion = $ratingCriteriaRepository->getAll();

        $searchAttributes->title = str_replace('(name)', $company->company_name, $searchAttributes->title);
        $searchAttributes->description = str_replace('(name)', $company->company_name, $searchAttributes->description);
        $this->replaceHeaders($headers, $company->company_name);

        return view('rating.user.company',
            ['company' => $company,
                'seo' => $searchAttributes,
                'companies' => $companies,
                'best' => $best,
                'headers' => $headers,
                'paginate' => $paginate,
                'criterion' => $criterion]);
    }

    public function companyOld($company_slug, $page)
    {
        $route = route('user.company', ['company' => $company_slug, 'page' => $page]);
        return redirect($route, 301);
    }

    public function redirect($company_slug, $page = null)
    {
        $route = $page ?
            route('user.company', $company_slug) . "/$page" :
            route('user.company', $company_slug);

        return redirect($route, 301);
    }

    private function replaceHeaders(&$headers, $companyName)
    {
        foreach ($headers as $k => $v)
        {
            $headers[$k] = str_replace('(name)', $companyName, $v);
        }
    }

    private function isPageIncorrect($page)
    {
        $match = [];
        preg_match('/[^0-9]/', $page, $match);

        return isset($match[0]);
    }
}
