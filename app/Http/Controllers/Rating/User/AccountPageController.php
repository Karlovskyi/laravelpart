<?php

namespace App\Http\Controllers\Rating\User;

class AccountPageController extends UserController
{
    public function index()
    {
        $seo = [
            'title' => 'Аккаунт',
            'description' => 'Аккаунт пользователя',
            'keywords' => null
        ];

        return view('rating.user.account', ['seo' => $seo]);
    }
}
