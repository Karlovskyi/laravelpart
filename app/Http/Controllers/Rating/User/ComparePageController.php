<?php

namespace App\Http\Controllers\Rating\User;

use App\Repositories\User\PageSearchAttributes\UserPageSearchRepository;
use App\Repositories\User\RatingCompanies\UserRatingCompaniesRepository;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ComparePageController extends UserController
{

    /**
     * @param UserPageSearchRepository $pageSearchAttributesRepository
     * @return View
     */

    public function compare(UserPageSearchRepository $pageSearchAttributesRepository)
    {
        $searchAttributes = $pageSearchAttributesRepository->getAttributes('compare');

        $headers = $this->pageHeadersRepository->getHeadersList(['compare']);

        return view('rating.user.compare', [
            'seo' => $searchAttributes,
            'slug' => [],
            'headers' => $headers]);
    }

    /**
     * @param Request $request
     * @param UserPageSearchRepository $pageSearchAttributesRepository
     * @return View
     */

    public function comparePost(
        Request $request,
        UserPageSearchRepository $pageSearchAttributesRepository
    )
    {
        $searchAttributes = $pageSearchAttributesRepository->getAttributes('compare');

        $headers = $this->pageHeadersRepository->getHeadersList(['compare']);

        $slug = $request->all()['slug'] ?? [];

        return view('rating.user.compare',
            [
                'seo' => $searchAttributes,
                'slug' => $slug,
                'headers' => $headers]);
    }

    /**
     * @param UserRatingCompaniesRepository $ratingCompaniesRepository
     * @param UserPageSearchRepository $pageSearchAttributesRepository
     * @return View
     */

    public function compareAll(
        UserRatingCompaniesRepository $ratingCompaniesRepository,
        UserPageSearchRepository $pageSearchAttributesRepository
    )
    {

        $searchAttributes = $pageSearchAttributesRepository->getAttributes('compareAll');

        $headers = $this->pageHeadersRepository->getHeadersList(['compare']);

        $slug = $ratingCompaniesRepository->getAllSlugs();
        return view('rating.user.compare',
            [
                'seo' => $searchAttributes,
                'slug' => $slug,
                'headers' => $headers]);
    }
}
