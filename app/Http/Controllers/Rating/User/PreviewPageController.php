<?php

namespace App\Http\Controllers\Rating\User;


use App\Repositories\User\PageSearchAttributes\UserPageSearchRepository;
use App\Repositories\User\RatingCompanies\UserRatingCompaniesRepository;
use App\Repositories\User\RatingCriterion\UserRatingCriterionRepository;
use App\Services\SortCompanies;

class PreviewPageController extends UserController
{
    public function index(
        UserRatingCompaniesRepository $ratingCompaniesRepository,
        UserPageSearchRepository $pageSearchAttributesRepository,
        UserRatingCriterionRepository $ratingCriteriaRepository,
        SortCompanies $sortCompanies
    )
    {
        $criterion = $ratingCriteriaRepository->getAll();
        $companies = $ratingCompaniesRepository->getDualTop();
        $companies = $sortCompanies->sortByPosition($companies, 'company_position');
        $best = $ratingCompaniesRepository->getBestCompany();
        $searchAttributes = $pageSearchAttributesRepository->getAttributes('main');

        $headers = $this->pageHeadersRepository
            ->getHeadersList(['rating-companies', 'how-to-choose', 'remember', 'about-site']);

        return view('rating.user.preview',
            [
                'companies' => $companies, 'best' => $best,
                'seo' => $searchAttributes,
                'headers' => $headers,
                'criterion' => $criterion
            ]);
    }
}
