<?php


namespace App\Http\Controllers\Rating\User;


use App\Http\Controllers\Rating\Controller;
use App\Repositories\User\PageHeaders\UserPageHeadersRepository;
use App\Repositories\User\Pages\UserPagesRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

abstract class UserController extends Controller
{
    protected $pageHeadersRepository;
    protected $pagesRepository;
    protected $last_modified = null;

    public function __construct(
        UserPageHeadersRepository $pageHeadersRepository,
        UserPagesRepository $pagesRepository
    )
    {
        $this->pageHeadersRepository = $pageHeadersRepository;
        $this->pagesRepository = $pagesRepository;
    }
}
