<?php

namespace App\Http\Controllers\Rating\User;

use App\Models\Constants;
use App\Repositories\User\PageSearchAttributes\UserPageSearchRepository;
use App\Repositories\User\RatingCompanies\UserRatingCompaniesRepository;
use App\Repositories\User\RatingCriterion\UserRatingCriterionRepository;
use App\Services\SortCompanies;
use Illuminate\View\View;

class ReliabilityPageController extends UserController
{
    /**
     * @param UserRatingCompaniesRepository $ratingCompaniesRepository
     * @param UserRatingCriterionRepository $ratingCriteriaRepository
     * @param UserPageSearchRepository $pageSearchAttributesRepository
     * @param SortCompanies $sortCompanies
     * @return View
     */

    public function reliability(
        UserRatingCompaniesRepository $ratingCompaniesRepository,
        UserRatingCriterionRepository $ratingCriteriaRepository,
        UserPageSearchRepository $pageSearchAttributesRepository,
        SortCompanies $sortCompanies
    )
    {
        $companies = $ratingCompaniesRepository->getUserTopReliability();
        $companies = $sortCompanies->sortByPosition($companies, 'company_reliability_position');
        $best = $ratingCompaniesRepository->getBestCompany();
        $criterion = $ratingCriteriaRepository->getAll();
        $searchAttributes = $pageSearchAttributesRepository->getAttributes('reliability');

        $headers = $this->pageHeadersRepository
            ->getHeadersList(['about-site-rely', 'rating-reliability']);

        return view('rating.user.reliability',
            [
                'companies' => $companies,
                'best' => $best,
                'criterion' => $criterion,
                'seo' => $searchAttributes,
                'headers' => $headers
            ]);
    }
}
