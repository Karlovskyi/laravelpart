<?php

namespace App\Http\Controllers\Rating\User;

use App\Repositories\User\PageSearchAttributes\UserPageSearchRepository;
use App\Repositories\User\RatingCompanies\UserRatingCompaniesRepository;

class HowToChoosePageController extends UserController
{

    /**
     * @param UserRatingCompaniesRepository $ratingCompaniesRepository
     * @param UserPageSearchRepository $pageSearchAttributesRepository
     * @return \Illuminate\View\View
     */

    public function howToChoose(
        UserRatingCompaniesRepository $ratingCompaniesRepository,
        UserPageSearchRepository $pageSearchAttributesRepository
    )
    {
        $searchAttributes = $pageSearchAttributesRepository->getAttributes('howToChoose');

        $best = $ratingCompaniesRepository->getBestCompany();

        $headers = $this->pageHeadersRepository
            ->getHeadersList(['how-calculate', 'how-calculate-rely', 'how-to-choose', 'remember']);

        return view('rating.user.how-to-choose', [
            'seo' => $searchAttributes,
            'best' => $best,
            'headers' => $headers]);
    }
}
