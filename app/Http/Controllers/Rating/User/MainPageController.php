<?php

namespace App\Http\Controllers\Rating\User;

use App\Repositories\User\PageSearchAttributes\UserPageSearchRepository;
use App\Repositories\User\RatingCompanies\UserRatingCompaniesRepository;
use App\Services\SortCompanies;
use Illuminate\View\View;

class MainPageController extends UserController
{

    /**
     * @param UserRatingCompaniesRepository $ratingCompaniesRepository
     * @param UserPageSearchRepository $pageSearchAttributesRepository
     * @param SortCompanies $sortCompanies
     * @return View
     */

    public function main(
        UserRatingCompaniesRepository $ratingCompaniesRepository,
        UserPageSearchRepository $pageSearchAttributesRepository,
        SortCompanies $sortCompanies
    )
    {
        $companies = $ratingCompaniesRepository->getUserTopCompanies();
        $companies = $sortCompanies->sortByPosition($companies, 'company_position');
        $best = $ratingCompaniesRepository->getBestCompany();
        $searchAttributes = $pageSearchAttributesRepository->getAttributes('main');

        $headers = $this->pageHeadersRepository
            ->getHeadersList(['rating-companies', 'how-to-choose', 'remember', 'about-site']);


        return view('rating.user.main',
            ['companies' => $companies, 'best' => $best,
                'seo' => $searchAttributes,
                'headers' => $headers]);
    }
}
