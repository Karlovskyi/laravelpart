<?php

namespace App\Http\Controllers\Rating\User;

use App\Repositories\User\PageSearchAttributes\UserPageSearchRepository;
use App\Repositories\User\RatingCompanies\UserRatingCompaniesRepository;
use Illuminate\View\View;

class AboutPageController extends UserController
{
    /**
     * @param UserRatingCompaniesRepository $ratingCompaniesRepository
     * @param UserPageSearchRepository $pageSearchAttributesRepository
     * @return View
     */

    public function about(
        UserRatingCompaniesRepository $ratingCompaniesRepository,
        UserPageSearchRepository $pageSearchAttributesRepository
    )
    {
        $searchAttributes = $pageSearchAttributesRepository->getAttributes('about');
        $best = $ratingCompaniesRepository->getBestCompany();
        $headers = $this->pageHeadersRepository
            ->getHeadersList(['about']);

        return view('rating.user.about',
            [
                'seo' => $searchAttributes,
                'best' => $best,
                'headers' => $headers]);
    }
}
