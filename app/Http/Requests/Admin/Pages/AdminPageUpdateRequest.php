<?php

namespace App\Http\Requests\Admin\Pages;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AdminPageUpdateRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page_str_index' => ['required', 'string', 'min:3','max:191',
                Rule::unique('pages')->ignore($this->pageId(), 'page_id')],
            'page_route_name' => 'required|string|min:3|max:191',
            'page_id' => 'required|numeric|exists:pages,page_id'
        ];
    }

    /** @return array */

    public function validationData()
    {
        return array_merge(parent::validationData(), ['page_id' => $this->route('page')]);
    }

    /** @return int */

    private function pageId() {
        return (integer) $this->route('page');
    }
}
