<?php

namespace App\Http\Requests;

use App\Models\Aggregates\PayAttentionConstants;
use App\Repositories\Validation\RatingCompanies\ValidationRatingCompaniesRepository;
use App\Rules\InEnum;
use App\Rules\InEnumRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RatingCompaniesUpdateRequest extends FormRequest
{

    protected $company_id;
    protected $max_position_value;
    protected $pay_attention_enum;

    /**
     * @param Request $request
     * @param ValidationRatingCompaniesRepository $ratingCompaniesRepository
     * @param PayAttentionConstants $payAttentionConstants
     */

    public function __construct(
        Request $request,
        ValidationRatingCompaniesRepository $ratingCompaniesRepository,
        PayAttentionConstants $payAttentionConstants
    )
    {
        parent::__construct();
        $this->company_id = (integer) $request->route()->company;
        $this->max_position_value = $ratingCompaniesRepository->getPublishedQuantity() - 1;
        $this->pay_attention_enum = $payAttentionConstants->getEnum();
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required|string|min:3|max:100',
            'company_slug' => 'max:100',
            'company_link' => 'url|nullable',
            'company_address' => 'string|nullable',
            'company_number' => 'numeric|nullable',
            'company_google_maps_link' => 'url|nullable',
            'company_inst' => 'url|nullable',
            'company_fb' => 'url|nullable',
            'company_you_tube' => 'url|nullable',

            'company_description' => 'string|nullable',

            "company_term_of_work" => "numeric|min:0|max:2",
            "company_staff_members" => "numeric|min:0|max:2",
            "company_amount_of_credit" => "numeric|min:0|max:2",
            "company_change_of_leader" => "numeric|min:0|max:2",
            "company_profitability" => "numeric|min:0|max:2",
            "company_legal_address" => "numeric|min:0|max:2",
            "company_checks" => "numeric|min:0|max:2",
            "company_financial_autonomy" => "numeric|min:0|max:2",
            "company_enforcement_proceedings" => "numeric|min:0|max:2",


            "company_payment_of_wages" => "numeric|min:0|max:1",
            "company_income_dynamics" => "numeric|min:0|max:1",
            "company_bulk_address_register" => "numeric|min:0|max:1",

            'company_amount_of_credit_clear'  => 'numeric|nullable',
            'company_change_of_leader_clear' => 'numeric|nullable',
            'company_profitability_clear' => 'numeric|nullable',
            'company_financial_autonomy_clear' => 'numeric|nullable',

            'company_position' => ['numeric', 'nullable',
                Rule::unique('rating_companies')->ignore($this->company_id, 'company_id'),
                "max:$this->max_position_value", 'min:0'],

            'company_reliability_position' => ['numeric', 'nullable',
                Rule::unique('rating_companies')->ignore($this->company_id, 'company_id'),
                "max:$this->max_position_value", 'min:0'],


            "company_criteria_1" => ['string'],
            "company_criteria_2" => ['string'],
            "company_criteria_3" => ['string'],

            'company_rusprofile_link' => 'url|nullable',
            'company_ogrn' => 'string|nullable',
            'company_inn' => 'string|nullable',
            'company_legal_address_value' => 'string|nullable',
            'company_legal_address_comment' => 'string|nullable',

            'company_pay_attention' => 'string|nullable|max:191',
            'company_pay_attention_type' => ['required', new InEnumRule($this->pay_attention_enum)],
        ];
    }
}
