<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RatingReviewsUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reviewer_name' => 'required|string|min:2|max:100',
            'review_link' => 'url',
            'review_mark' => 'numeric|min:1|max:5',
            'resource_id' => 'numeric|min:0|max:2',
            'company_id' => 'integer|exists:rating_companies,company_id',
            'is_published' => 'boolean',
            'position' => 'integer',
            'review_text' => 'string'
        ];
    }
}
