<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RatingImagesUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_published' => 'integer|min:0|max:1',
            'image_on_company_page' => 'integer|min:0|max:1',
        ];
    }
}
