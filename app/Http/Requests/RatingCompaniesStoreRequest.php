<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RatingCompaniesStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // todo make normal validation
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required|unique:rating_companies|min:3|max:190',
            'company_logo_path' => 'required|unique:rating_companies|string',
            'company_yandex_id' => 'required|unique:rating_companies|string',
            'company_google_id' => 'required|unique:rating_companies|string',
            'company_otzovick_id' => 'required|unique:rating_companies|string',
        ];
    }
}
