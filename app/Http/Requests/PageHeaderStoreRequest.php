<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageHeaderStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "value" => 'required|string|min:3|max:191',
            "key" => 'required|string|min:3|max:191|unique:page_headers',
            "description" => 'required|string|min:3|max:20000'
        ];
    }
}
