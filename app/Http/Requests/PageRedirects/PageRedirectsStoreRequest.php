<?php

namespace App\Http\Requests\PageRedirects;

use Illuminate\Foundation\Http\FormRequest;

class PageRedirectsStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "path_from" => "required|string|max:191|unique:page_redirects",
            "path_to" => "required|string|max:191"
        ];
    }
}
