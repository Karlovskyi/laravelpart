<?php

namespace App\Http\Requests\PageRedirects;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PageRedirectsUpdateRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "path_from" => ["required", "string", "max:191", Rule::unique('page_redirects')
                ->ignore($this->getId())],
            "path_to" => "required|string|max:191"
        ];
    }

    private function getId()
    {
        return Request::route()->parameter('page_redirect');
    }
}
