<?php

namespace App\Http\Requests;

use App\Services\LoginPasswordCheckService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ApiReviewStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request, LoginPasswordCheckService $loginPasswordCheckService)
    {
        $metaData = $request->meta_data ?? [];
        $login = $metaData['login'] ?? '';
        $password = $metaData['password'] ?? '';
        return $loginPasswordCheckService->checkLoginAndPassword($login, $password);

    }

    /**
     * This validation method do NOT validate uniqueness of review
     * To see validation of uniqueness check App\Services\ApiStoreService
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reviews' => 'array|required',
            'reviews.*' => 'array',
            'reviews.*.reviewer_name' => 'required|string',
            'reviews.*.resource_id' => 'required|int|min:0|max:2',
            'reviews.*.company_id' => 'required|string',
            'reviews.*.review_date' => 'required|date',
            'reviews.*.review_mark' => 'required|int|min:1|max:5',
            'reviews.*.review_text' => 'required|string',
            'reviews.*.review_link' => 'required|url',

            'reviews.*.imgs' => 'array',
            'reviews.*.imgs.*' => 'url',
        ];
    }
}
