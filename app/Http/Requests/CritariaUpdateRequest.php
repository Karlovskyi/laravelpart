<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CritariaUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'criteria_name_ru' => 'required|string|max:100',
            'criteria_description' => 'string|nullable',
            'criteria_value_1' => 'string|nullable',
            'criteria_value_2' => 'string|nullable',
            'criteria_value_3' => 'string|nullable'
        ];
    }
}
