<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageSearchAttributesStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|string|min:3|max:191",
            "description" => "nullable|string|min:3|max:20000",
            "keywords" => "nullable|string|min:3|max:20000",
            "title_index" => "required|string|min:3|max:191|unique:page_search_attributes"
        ];
    }
}
