<?php
if(! function_exists('is_current_path')) {
    function is_current_path($path) {
        $current_path = request()->root() . '/' . request()->path();
        return $current_path === $path;
    }
}
