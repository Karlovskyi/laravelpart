<?php

if(! function_exists('getAppConstants')) {
    function getAppConstants() {
        return new \App\General\Constants\MainAppConstants();
    }
}
