<?php

namespace App\Providers;

use App\Models\PageHeaders;
use App\Models\Pages;
use App\Models\PageSearchAttributes;
use App\Models\RatingCompanies;
use App\Models\RatingCriterion;
use App\Models\RatingImages;
use App\Models\RatingReviews;
use App\Observers\PageHeadersObserver;
use App\Observers\PageSearchAttributesObserver;
use App\Observers\PagesObserver;
use App\Observers\RatingCompaniesObserver;
use App\Observers\RatingCriterionObserver;
use App\Observers\RatingImagesObserver;
use App\Observers\RatingReviewsObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        RatingCompanies::observe(RatingCompaniesObserver::class);
        RatingReviews::observe(RatingReviewsObserver::class);
        RatingImages::observe(RatingImagesObserver::class);
        RatingCriterion::observe(RatingCriterionObserver::class);
        Pages::observe(PagesObserver::class);
        PageSearchAttributes::observe(PageSearchAttributesObserver::class);
        PageHeaders::observe(PageHeadersObserver::class);
    }
}
