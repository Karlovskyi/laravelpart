<?php


namespace App\Services;


use App\Repositories\Api\RatingCompanies\ApiRatingCompaniesRepository;
use App\Repositories\Api\RatingReviews\ApiRatingReviewsRepository;
use Exception;
use Illuminate\Support\Collection;


class ApiStoreService
{
    private $apiRatingCompaniesRepository;
    private $apiRatingReviewsRepository;
    private $reviewPublishingService;

    public function __construct(
        ApiRatingCompaniesRepository $apiRatingCompaniesRepository,
        ApiRatingReviewsRepository $apiRatingReviewsRepository,
        ReviewPublishingService $reviewPublishingService
    )
    {
        $this->apiRatingCompaniesRepository = $apiRatingCompaniesRepository;
        $this->apiRatingReviewsRepository = $apiRatingReviewsRepository;
        $this->reviewPublishingService = $reviewPublishingService;
    }

    public function addNewReviews(Collection $data)
    {
        $data = $this->deleteSameReviews($data);
        $data = $this->setCompanyId($data);
        $data = $this->uniquenessFilter($data);
        $data->each(function ($item)
        {
            $this->apiRatingReviewsRepository->storeNewReview($item);

        });
        return "Request was accepted. Thank You for pulling";
    }

    private function getFindField(int $resource_id)
    {
        if ($resource_id === 0)
        {
            return 'company_yandex_id';
        } else if ($resource_id === 1)
        {
            return 'company_google_id';
        } else if ($resource_id === 2)
        {
            return 'company_otzovick_id';
        }
        throw new Exception("resource id must be in [0, 1, 2]");
    }

    private function setCompanyId(Collection $data)
    {
        $resourceIdList = $this->apiRatingCompaniesRepository->getCompaniesResourceId();
        $data = $data->map(function ($item) use ($resourceIdList)
        {
            $resourceField = $this->getFindField($item['resource_id']);

            $realCompanyId = $resourceIdList
                ->where($resourceField, '=', $item['company_id'])
                ->first()['company_id'];


            if ($realCompanyId)
            {
                $item['company_id'] = $realCompanyId;
                return $item;
            } else
            {
                return null;
            }

        })
            ->filter();
        return $data;
    }

    private function uniquenessFilter(Collection $data)
    {
        $arrayOfLinks = $data->map(function ($item)
        {
            return $item['review_link'];
        });
        $existing = $this->apiRatingReviewsRepository->getExistingReviewsFromList($arrayOfLinks);
        $data = $data->filter(function ($review) use ($existing)
        {
            if ($existing->where('review_link', '=', $review['review_link'])
                    ->count() === 0)
            {
                return true;
            } else return false;
        });

        return $data;
    }

    private function deleteSameReviews(Collection $data)
    {
        $data = $data->each(function ($item, $index) use ($data)
        {
            if($data->where('review_link', '=', $item['review_link'])->count() > 1) {
                $data[$index] = null;
            }
        })->filter();
        return $data;
    }
}
