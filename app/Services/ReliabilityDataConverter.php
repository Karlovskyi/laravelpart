<?php


namespace App\Services;


class ReliabilityDataConverter
{
    public function getTermOfWork($term_of_work)
    {
        if ($term_of_work === null)
        {
            return null;
        }
        if ($term_of_work >= 4)
        {
            return 2;
        } else if ($term_of_work >= 2)
        {
            return 1;
        } else
        {
            return 0;
        }
    }

    public function getStaffMembers($stuff_members)
    {
        if ($stuff_members === null)
        {
            return null;
        }
        if ($stuff_members >= 15)
        {
            return 2;
        } else if ($stuff_members >= 5)
        {
            return 1;
        } else
        {
            return 0;
        }
    }

    public function getAmountOfCredit($amount_of_credit, $profitability)
    {
        if ($amount_of_credit === null)
        {
            return null;
        }

        if ($amount_of_credit <= $profitability * 0.1)
        {
            return 2;
        } else if ($amount_of_credit <= $profitability * 0.2)
        {
            return 1;
        } else
        {
            return 0;
        }
    }

    public function getChangeOfLeader($change_of_leader)
    {
        if ($change_of_leader === null)
        {
            return null;
        }
        if ($change_of_leader >= 2)
        {
            return 2;
        } else if ($change_of_leader >= 1)
        {
            return 1;
        } else
        {
            return 0;
        }
    }

    public function getProfitability($profitability)
    {
        if ($profitability === null)
        {
            return null;
        }
        if ($profitability >= 2000000)
        {
            return 2;
        } else if ($profitability >= 0)
        {
            return 1;
        } else
        {
            return 0;
        }
    }

    public function getFinancialAutonomy($financialAutonomy)
    {
        if($financialAutonomy === null) {
            return null;
        }

        if($financialAutonomy >= 50) {
            return 2;
        } else if ($financialAutonomy >= 5) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getIsPublished($item)
    {
        return $item['term_of_work'] !== null &&
            $item['staff_members'] !== null &&
            $item['amount_of_credit'] !== null &&
            $item['change_of_leader'] !== null &&
            $item['staff_members'] !== null &&
            $item['legal_address'] !== null;
    }

    public function getTotalReliability(
        $term_of_work,
        $staff_members,
        $amount_of_credit,
        $profitability,
        $change_of_leader,
        $financial_autonomy,
        $legal_address,
        $search_for_colleagues,
        $enforcement_proceedings,
        $payment_of_wages,
        $income_dynamics,
        $bulk_address_register,
        $company_relation = 1)
    {

        $answer =
            $this->getTermOfWork($term_of_work) +
            $this->getStaffMembers($staff_members) +
            $this->getAmountOfCredit($amount_of_credit, $profitability) +
            $this->getChangeOfLeader($change_of_leader) +
            $this->getProfitability($profitability) +
            $this->getFinancialAutonomy($financial_autonomy) +
            $legal_address +
//            $checks + не используем
            $search_for_colleagues +
            $enforcement_proceedings +
            $payment_of_wages * 2 +   //boolean
            $income_dynamics * 2 +    //boolean
            $bulk_address_register * 2 + //boolean
            $company_relation + 1; // boolean
        return $answer === 0 ? null : $answer;
    }
}
