<?php


namespace App\Services;


use App\Interfaces\ISendMessage;
use App\Repositories\Api\RatingCompanies\ApiRatingCompaniesRepository;
use App\Repositories\Commands\RatingReviews\CommandsRatingReviewsRepository;


class ReviewPublishingService
{
    private $published = 0;

    private $unpublishedReviews;
    private $positiveForAllCompanies;

    private $mainCompanyId;
    private $mainCompanyData;
    private $mainAllReviews;

    private $updateDBService;
    private $apiRatingCompaniesRepository;

    private $ratingReviewsRepository;


    // TODO separate API and Commands Repository

    /**
     * @param CommandsRatingReviewsRepository $ratingReviewsRepository
     * @param ApiRatingCompaniesRepository $apiRatingCompaniesRepository
     * @param UpdateDBService $updateDBService
     */

    public function __construct(
        CommandsRatingReviewsRepository $ratingReviewsRepository,
        ApiRatingCompaniesRepository $apiRatingCompaniesRepository, UpdateDBService $updateDBService)
    {
        $this->updateDBService = $updateDBService;
        $this->apiRatingCompaniesRepository = $apiRatingCompaniesRepository;

        $this->mainCompanyId = $apiRatingCompaniesRepository->getMainCompaniesId();
        $this->positiveForAllCompanies = $apiRatingCompaniesRepository->getPositiveForAllCompanies();

        $this->unpublishedReviews = $ratingReviewsRepository->getUnpublishedReviews();
        $this->mainCompanyData = $ratingReviewsRepository->getCompanyReviewDataById($this->mainCompanyId);

        $this->mainAllReviews = $this->mainCompanyData[1] + $this->mainCompanyData[2] + $this->mainCompanyData[3] +
            $this->positiveForAllCompanies->where('company_id', '=', $this->mainCompanyId)
                ->first()->company_positive;

        $this->ratingReviewsRepository = $ratingReviewsRepository;
    }

    public function RegularPublishing(ISendMessage $ISendMessage)
    {
        if (!$this->mainCompanyId || !$this->mainCompanyData || !$this->mainAllReviews
            || !$this->positiveForAllCompanies || !$this->ratingReviewsRepository)
        {
            return false;
        }
        $unpublishedAtStart = $this->unpublishedReviews->count();

        $this->publishMainReviews();
        $this->publishOtherReviews();


        $this->apiRatingCompaniesRepository->getCompanyIds()
            ->each(function ($item)
            {
                $this->updateDBService->updateCompany_sReviewData($item->company_id);
            });


        $ISendMessage->sendMessage("В базе данных было $unpublishedAtStart отзывов, из них $this->published опубликовано");
        return true;

    }

    private function publishMainReviews()
    {
        $this->unpublishedReviews->each(function ($review, $index)
        {
            if ($review->company_id === $this->mainCompanyId)
            {
                switch ($review->review_mark)
                {
                    case 5:
                    case 4:
                        $this->ratingReviewsRepository->publishReview($review) ? $this->published++ : null;
                    break;
                    case 3:
                        if ($this->mainCompanyData[3] / $this->mainAllReviews < 0.01)
                        {
                            $this->ratingReviewsRepository->publishReview($review) ? $this->published++ : null;
                        }
                    break;
                    case 2:
                        if ($this->mainCompanyData[2] / $this->mainAllReviews < 0.02)
                        {
                            $this->ratingReviewsRepository->publishReview($review) ? $this->published++ : null;
                        }
                    break;
                    case 1:
                        if ($this->mainCompanyData[1] / $this->mainAllReviews < 0.04)
                        {
                            $this->ratingReviewsRepository->publishReview($review) ? $this->published++ : null;
                        }
                    break;
                }
                $this->unpublishedReviews[$index] = null;
            }
        });
        $this->unpublishedReviews = $this->unpublishedReviews->filter();
    }

    private function publishOtherReviews()
    {
        $this->unpublishedReviews->each(function ($review, $index)
        {
            $companyMarkDifference = $this->positiveForAllCompanies->where('company_id', $review->company_id)
                ->first()
                ->company_mark_difference;
            $mainCompanyMarkDifference = $this->positiveForAllCompanies->where('company_id', $this->mainCompanyId)
                ->first()
                ->company_mark_difference;
            switch ($review->review_mark)
            {
                case 4:
                case 5:
                    if ($mainCompanyMarkDifference - $companyMarkDifference >= 100)
                    {
                        $this->ratingReviewsRepository->publishReview($review) ? $this->published++ : null;
                    }
                break;
                case 3:
                case 2:
                case 1:
                    $this->ratingReviewsRepository->publishReview($review) ? $this->published++ : null;
                break;
            }
            $this->unpublishedReviews[$index] = null;

        });
        $this->unpublishedReviews = $this->unpublishedReviews->filter();
    }
}
