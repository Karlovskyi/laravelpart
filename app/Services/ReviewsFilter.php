<?php


namespace App\Services;

class ReviewsFilter
{
    public function setIsPublished($review, &$memoryArr)
    {
        $id = $review['url_base'];
        $mark = $review['mark'];
        if ($id === '1704435823' ||
            $id === '086e7be38ac4003d72c21f02fff7fcb9953af7c7' ||
            $id === 'https://otzovik.com/reviews/kompaniya_studiya_remontov_russia_moscow/')
        {
            switch ($mark)
            {
                case 4:
                case 5: {
                $memoryArr['all']++;
                return 1;
                }
                break;
                case 3:
                    {
                        if($memoryArr['negative3'] / $memoryArr['all'] > 0.01)
                            return 0;
                        $memoryArr['negative3']++;
                        $memoryArr['all']++;
                        return 1;
                    }
                break;
                case 2:
                    {
                        if($memoryArr['negative2'] / $memoryArr['all'] > 0.01)
                            return 0;
                        $memoryArr['negative2']++;
                        $memoryArr['all']++;
                        return 1;
                    }
                break;
                case 1:
                    if($memoryArr['negative1'] / $memoryArr['all'] > 0.01)
                        return 0;
                    $memoryArr['negative1']++;
                    $memoryArr['all']++;
                    return 1;
                break;
            }
        }
        return 1;
    }
}
