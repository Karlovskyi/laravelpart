<?php


namespace App\Services;

use App\Repositories\Admin\RatingReviews\AdminRatingReviewsRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;

class DeleteService
{
    private $ratingReviewsRepository;
    private $updateDBService;

    public function __construct(
        AdminRatingReviewsRepository $ratingReviewsRepository,
        UpdateDBService $updateDBService
    )
    {
        $this->ratingReviewsRepository = $ratingReviewsRepository;
        $this->updateDBService = $updateDBService;
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */

    public function deleteReview(int $id)
    {

        $review = $this->ratingReviewsRepository->getDelete($id);
        if ($review)
        {

            DB::beginTransaction();
            $result1 = $review->delete();
            $result2 = $this->updateDBService->updateCompany_sReviewData($review->company->company_id);
            if ($result1 && $result2)
            {
                DB::commit();
                return redirect()
                    ->route('rating.admin.reviews.index')
                    ->with(['success' => 'Отзыв был удален!']);
            } else
            {
                DB::rollBack();
                return back()
                    ->withInput()
                    ->withErrors(["msg' => 'Нет отзыва с id={$id}"]);
            }
        } else
        {
            return back()
                ->withInput()
                ->withErrors(["msg' => 'Нет отзыва с id={$id}"]);
        }
    }
}
