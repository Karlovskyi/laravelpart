<?php


namespace App\Services;


class SortCompanies
{
    public function sortByPosition($companies, $field)
    {
        $newCompanies = collect([]);
        for ($i = 0; $i < $companies->count(); $i++)
        {
            if ($companies[$i][$field] !== null)
            {
                $newCompanies[$companies[$i][$field]] = $companies[$i];
                $companies[$i] = null;
            }
        }
        for ($i = 0; $i < $companies->count(); $i++)
        {
            if ($companies[$i] !== null)
            {
                if($newCompanies->has($i)) {
                    $this->findPlace($newCompanies, $i, $companies[$i]);
                } else {
                    $newCompanies[$i] = $companies[$i];
                }
            }
        }
        return $newCompanies->sortKeys();
    }

    private function findPlace(&$newCompanies, $i, $company)
    {
        $i++;
        if($newCompanies->has($i)) {
            $this->findPlace($newCompanies, $i, $company);
        } else {
            $newCompanies[$i] = $company;
            return;
        }
    }
}
