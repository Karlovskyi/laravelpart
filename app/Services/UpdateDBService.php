<?php


namespace App\Services;


use App\Repositories\Api\RatingCompanies\ApiRatingCompaniesRepository;

class UpdateDBService
{
    private $ratingCompaniesRepository;
    private $mainCompanyId;
    private $cheatCoefficient;

    public function __construct(ApiRatingCompaniesRepository $ratingCompaniesRepository,
        ApiRatingCompaniesRepository $apiRatingCompaniesRepository)
    {
        $this->ratingCompaniesRepository = $ratingCompaniesRepository;
        $this->mainCompanyId = $apiRatingCompaniesRepository->getMainCompaniesId();

        $this->cheatCoefficient = $ratingCompaniesRepository->getAllCompaniesWithAverageMark
            ($this->mainCompanyId)
                ->reviews->avg('review_mark') / 5;
    }

    public function updateCompany_sReviewData(int $company_id)
    {
        if ($this->mainCompanyId === $company_id)
        {
            $coefficient = 1;
        } else
        {
            $coefficient = $this->cheatCoefficient;
        }

        $dbItem = $this->ratingCompaniesRepository->getEditOrShow($company_id);
        $res = $this->ratingCompaniesRepository->getAllCompaniesWithAverageMark($company_id);
        $generalAverage = $res->reviews->avg('review_mark') * $coefficient;
        $generalReviews = count($res->reviews);

        $yandexRes = $this->ratingCompaniesRepository->getAllCompaniesWithAverageMark($company_id, 0);
        $yandexAverage = $yandexRes->reviews->avg('review_mark') * $coefficient;
        $yandexReviews = count($yandexRes->reviews);

        $googleRes = $this->ratingCompaniesRepository->getAllCompaniesWithAverageMark($company_id, 1);
        $googleAverage = $googleRes->reviews->avg('review_mark') * $coefficient;
        $googleReviews = count($googleRes->reviews);

        $otzovickRes = $this->ratingCompaniesRepository->getAllCompaniesWithAverageMark($company_id, 2);
        $otzovickAverage = $otzovickRes->reviews->avg('review_mark') * $coefficient;
        $otzovickReviews = count($otzovickRes->reviews);

        $positive = $this->ratingCompaniesRepository->getPositiveQuantity($company_id);

        $update = [
            'company_mark_average' => $generalAverage,
            'company_mark_average_yandex' => $yandexAverage,
            'company_mark_average_google' => $googleAverage,
            'company_mark_average_otzovick' => $otzovickAverage,

            'company_review_quantity' => $generalReviews,
            'company_review_quantity_yandex' => $yandexReviews,
            'company_review_quantity_google' => $googleReviews,
            'company_review_quantity_otzovick' => $otzovickReviews,
            'company_positive' => $positive,
            'company_negative' => $generalReviews - $positive,
            'company_mark_difference' => 2 * $positive - $generalReviews,
        ];
        $result = $dbItem->fill($update)
            ->save();
        return $result;
    }
}
