<?php


namespace App\Services;


use Illuminate\Support\Facades\DB;

class ReviewsAdminService
{
    private $updateDBService;

    public function __construct(UpdateDBService $updateDBService)
    {
        $this->updateDBService = $updateDBService;
    }
    public function recalculateTransaction($item, $data)
    {

        return DB::transaction(function () use ($item, $data)
        {
            $company_id = $item->company->company_id;

            $result = $item->update($data);
            if($result) {
                $result = $this->updateDBService->updateCompany_sReviewData($company_id);
            }
            if ($result)
            {
                return redirect()
                    ->route('rating.admin.reviews.edit', $item->review_id)
                    ->with(['success' => 'Отзыв успешно изменен']);
            } else
            {
                return back()
                    ->withInput()
                    ->withErrors(['msg' => 'С обновлением что-то пошло не так']);
            }
        });
    }
}
