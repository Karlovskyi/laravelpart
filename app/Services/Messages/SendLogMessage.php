<?php


namespace App\Services\Messages;


use App\Interfaces\ISendMessage;
use Illuminate\Support\Facades\Log;

class SendLogMessage implements ISendMessage
{
    public function sendMessage($message)
    {

        logs()->info($message);
    }
}
