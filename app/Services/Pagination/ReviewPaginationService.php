<?php


namespace App\Services\Pagination;

use App\Repositories\User\Constants\UserConstantsRepository;
use App\Repositories\User\RatingCompanies\UserRatingCompaniesPaginatorRepository;
use App\Repositories\User\RatingReviews\UserRatingReviewsPaginatorRepository;

class ReviewPaginationService
{
    private $mainIdentifier = 'Студия Ремонтов';

    private $slug;
    private $page;
    private $company;
    private $main;

    private $take;

    private $repository;

    public function __construct(string $slug, int $page) {
        $take = ((int) (new UserConstantsRepository())->getReviewsPerPage());

        $this->take = $take && $take > 6 && $take <= 20 ? $take : 6;

        $this->repository = new UserRatingReviewsPaginatorRepository();
        $repository = new UserRatingCompaniesPaginatorRepository();
        $this->page = $page;
        $this->slug = $slug;
        $this->company = $repository->getCompanyIdAndNameBySlug($slug);

        if($this->company->company_name === $this->mainIdentifier) {
            $this->main = true;
        } else {
            $this->main = false;
        }
    }

    public function main() {
        $company_id = $this->company->company_id;
        $pageCount = $this->repository->getPageCount($company_id, $this->take);
        $stat = $this->repository->getStatistic($company_id);

        if($this->main) {
            $reviews = $this->repository->getMainCompanyReviews($company_id, $this->page, $pageCount, $this->take);

        } else {
            $reviews = $this->repository->getCompanyReviews($this->page, $stat['positive'], $stat['negative'],
                $company_id, $this->take);
        }
        return [
            'max' => $pageCount - 1,
            'current' => $this->page,
            'reviews' => $reviews,
            'slug' => $this->slug
        ];

    }
}
