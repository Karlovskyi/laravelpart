<?php


namespace App\Services\ReviewLastPositionService;


use App\Repositories\Service\RatingCompanies\ReviewLifecycleCompaniesRepository;

class ReviewLastPositionService
{
    private $reviewQuantity = 10;
    private $companiesRepository;
    public function __construct(ReviewLifecycleCompaniesRepository $companiesRepository) {
        $this->companiesRepository = $companiesRepository;
    }

    public function run($company_id = null) {
        if($company_id) {
            $company = $this->companiesRepository->getCompanyWithReviewsOrderedByCreate($company_id);
            $companies = collect([$company]);
        } else {
            $companies = $this->companiesRepository->getAllCompaniesWithReviewsOrderedByCreate();
        }

        $this->companiesRepository->updateReviewsLastPosition($companies, $this->reviewQuantity);
        return 0;
    }
}
