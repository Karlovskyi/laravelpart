<?php

use App\Repositories\PageSearchAttributesRepository;
use App\Repositories\Seeder\PageSearchAttributes\SeederPageSearchRepository;
use Illuminate\Database\Seeder;

class PageSearchAttributesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param SeederPageSearchRepository $pageSearchAttributesRepository
     * @return void
     */
    public function run(SeederPageSearchRepository $pageSearchAttributesRepository)
    {
        $collection = collect([
            [
                'title_index' => 'main',
                'title' => 'Главная',
                'description' => null
            ],
            [
                'title_index' => 'reliability',
                'title' => 'Топ по надежности',
                'description' => null
            ],
            [
                'title_index' => 'compare',
                'title' => 'Сравнить компании',
                'description' => null
            ],
            [
                'title_index' => 'compareAll',
                'title' => 'Сравнить все компании',
                'description' => null
            ],
            [
                'title_index' => 'howToChoose',
                'title' => 'Как выбрать',
                'description' => null
            ],
            [
                'title_index' => 'about',
                'title' => 'О нас',
                'description' => null
            ],
            [
                'title_index' => 'company',
                'title' => '(name) | Рейтинг',
                'description' => null
            ],
            [
                'title_index' => 'login',
                'title' => 'Вход в аккаунт',
                'description' => 'Форма входа в аккаунт на сайте remont-rate.ru'
            ],
            [
                'title_index' => 'register',
                'title' => 'Регистрация',
                'description' => 'Форма регистрации на сайте remont-rate.ru'
            ],
            [
                'title_index' => 'password-forgot',
                'title' => 'Забыли пароль',
                'description' => 'Форма восстановления пароля на сайте remont-rate.ru'
            ],
            [
                'title_index' => 'password-reset',
                'title' => 'Восставить пароль',
                'description' => 'Форма восстановления пароля на сайте remont-rate.ru'
            ],
            [
                'title_index' => 'not-found',
                'title' => 'Ошибка | Страница не найдена',
                'description' => 'Страница не найдена'
            ]
        ]);
        $collection->each(function ($item) use ($pageSearchAttributesRepository)
        {
            $pageSearchAttributesRepository->createElement($item);
        });

    }
}
