<?php

use Database\Seeders\PageHeadersSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RatingCompaniesSeeder::class);
        $this->call(RatingReviewsSeeder::class);
        $this->call(RatingImagesSeeder::class);
        $this->call(RatingAverageValuesSeeder::class);
        $this->call(ConstantsSeeder::class);
        $this->call(RatingCriteriaSeeder::class);
        $this->call(PageSearchAttributesSeeder::class);
        $this->call(PageHeadersSeeder::class);
        $this->call(PagesSeeder::class);
        $this->call(RatingCompaniesPageRelationSeeder::class);
        $this->call(PagesRelationSeeder::class);
        $this->call(LinkingPageAndHeadersSeeder::class);
    }
}
