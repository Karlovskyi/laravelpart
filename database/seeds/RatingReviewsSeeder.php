<?php

use App\Repositories\Seeder\RatingCompanies\SeederRatingCompaniesRepository;
use App\Services\ReviewLastPositionService\ReviewLastPositionService;
use App\Services\ReviewsFilter;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RatingReviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param SeederRatingCompaniesRepository $ratingCompaniesRepository
     * @param ReviewsFilter $reviewsFilter
     * @param ReviewLastPositionService $reviewLastPositionService
     * @return void
     *
     * Данный seeder отвечает за наполнение таблицы rating_reviews для того,
     * чтобы связать отзывы и компании, мы достаем из базы компании и проходимся двойным циклом.
     * Из-за того, что mySQL не может принять 6000 отзывов за один insert (выдает ошибку "MySQL server has gone away"),
     * seeder разбивает insert-ы на 30 частей по 200-е отзывов.
     */
    public
    function run(SeederRatingCompaniesRepository $ratingCompaniesRepository, ReviewsFilter $reviewsFilter, ReviewLastPositionService $reviewLastPositionService)
    {
        $memoryArr = [
            'all' => 0,
            'negative1' => 0,
            'negative2' => 0,
            'negative3' => 0,
        ];


        $companies = $ratingCompaniesRepository->seedHelper();
        $file = json_decode(file_get_contents('./resources/files_for_db/final.json'), true);

        $PULL_ON_EACH_INSERT = 200;

        if (count($file) % $PULL_ON_EACH_INSERT !== 0)
        {
            $limit = intval(count($file) / $PULL_ON_EACH_INSERT) + 1;
        } else
        {
            $limit = count($file) / $PULL_ON_EACH_INSERT;
        }

        for ($k = 1; $k <= $limit; $k++)
        {
            $to = $k === $limit ? count($file) : $k * $PULL_ON_EACH_INSERT;
            $from = ($k - 1) * $PULL_ON_EACH_INSERT;
            $this->pushThousandReviews($from, $to, $companies, $file, $reviewsFilter, $memoryArr);
        }
//        dd($memoryArr);
        $reviewLastPositionService->run();
    }

    /**
     * @param int $from
     * @param int $to
     * @param $companies
     * @param array $file
     * @param ReviewsFilter $reviewFilter
     * @param array $memoryArr
     */
    private
    function pushThousandReviews($from, $to, &$companies, &$file, $reviewFilter, &$memoryArr)
    {
        $toInsert = [];
        for ($j = $from; $j < $to; $j++)
        {
            $item = $file[$j];
            // 0 - yandex, 1 - google, 2 - otzovick
            if ($item['sourse_id'] === 0)
            {
                $key = 'company_yandex_id';
            } else if ($item['sourse_id'] === 1)
            {
                $key = 'company_google_id';
            } else
            {
                $key = 'company_otzovick_id';
            }
            for ($i = 0; $i < count($companies); $i++)
            {
                $company = (array)$companies[$i];
                if ($company["$key"] === $item['url_base'])
                {
                    $toInsert[] = [
                        'company_id' => $company['company_id'],
                        'resource_id' => $item['sourse_id'],
                        'review_text' => $item['text'] ?? '',
                        'review_mark' => $item['mark'],
                        'review_date' => $item['date'],
                        'reviewer_name' => $item['name'],
                        'review_link' => $item['link'],
                        'positive' => $item['mark'] > 3,
                        'is_published' => $reviewFilter->setIsPublished($item, $memoryArr)
                    ];
                    break;
                }
            }
        }
        DB::table('rating_reviews')
            ->insert($toInsert);
    }
}
