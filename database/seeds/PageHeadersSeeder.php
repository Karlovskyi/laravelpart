<?php

namespace Database\Seeders;

use App\Repositories\PageHeadersRepository;
use App\Repositories\Seeder\PageHeaders\SeederPageHeaderRepository;
use Illuminate\Database\Seeder;

class PageHeadersSeeder extends Seeder
{
    public function __construct(SeederPageHeaderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        $this->data = collect($this->data);

        $this->data->each(function ($item)
        {
            $this->repository->createHeader($item);
        });
    }

    private $repository;
    private $data = [
        [
            'key' => 'about-site',
            'value' => 'Рейтинг компаний по ремонту квартир в Москве',
            'description' => 'Этот заголовок есть в блоке, который описывает сайт'
        ],
        [
            'key' => 'about-site-rely',
            'value' => 'Рейтинг надёжности компаний по ремонту квартир в Москве',
            'description' => 'Главный заголовок на странице надежности'
        ],
        [
            'key' => 'rating-companies',
            'value' => 'Рейтинг компаний',
            'description' => 'Этот заголовок стоит перед топом по отзывам'
        ],
        [
            'key' => 'rating-reliability',
            'value' => 'ТОП по надёжности',
            'description' => 'Этот заголовок стоит перед топом по надежности'
        ],
        [
            'key' => 'compare',
            'value' => 'Сравнить компании',
            'description' => 'Заголовок на странице сравнить'
        ],
        [
            'key' => 'contacts',
            'value' => 'Контакты',
            'description' => 'Блок с контактами компании'
        ],
        [
            'key' => 'portfolio',
            'value' => 'Портфолио',
            'description' => 'Блок с портфолио компании'
        ],
        [
            'key' => 'rely',
            'value' => 'Надёжность',
            'description' => 'Блок где показывается надежность компании'
        ],
        [
            'key' => 'rating',
            'value' => 'Рейтинг',
            'description' => 'Блок где показывается статистика по отзывам компании'
        ],
        [
            'key' => 'reviews',
            'value' => 'Отзывы о компании (name)',
            'description' => 'Блок с отзывами'
        ],
        [
            'key' => 'how-calculate',
            'value' => 'Как рассчитывается рейтинг',
            'description' => 'Как рассчитывается рейтинг'
        ],
        [
            'key' => 'how-calculate-rely',
            'value' => 'Как рассчитывается надёжность',
            'description' => 'Как рассчитывается надёжность'
        ],
        [
            'key' => 'how-to-choose',
            'value' => 'Как выбрать компанию по ремонту',
            'description' => 'Как выбрать компанию по ремонту'
        ],
        [
            'key' => 'remember',
            'value' => 'Запомнить',
            'description' => 'Блок запомнить'
        ],
        [
            'key' => 'about',
            'value' => 'О нас',
            'description' => 'Главный заголовок странички about'
        ],
        [
            'key' => 'login',
            'value' => 'Вход в аккаунт',
            'description' => 'Заголовок страницы Входа в аккаунт'
        ],
        [
            'key' => 'register',
            'value' => 'Регистрация',
            'description' => 'Заголовок страницы Регистрации'
        ],
        [
            'key' => 'password-reset',
            'value' => 'Восстановить пароль',
            'description' => 'Заголовок страницы "Восстановить пароль"'
        ],
        [
            'key' => 'company',
            'value' => 'Юридическая информация и отзывы компании ООО «(name)»',
            'description' => 'Заголовок страницы компании'
        ],
    ];
}
