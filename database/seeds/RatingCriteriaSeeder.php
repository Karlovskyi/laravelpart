<?php

use App\Repositories\Seeder\RatingCriterion\SeederRatingCriteriaRepository;
use Illuminate\Database\Seeder;

class RatingCriteriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param SeederRatingCriteriaRepository $ratingCriteriaRepository
     * @return void
     */
    public function run(SeederRatingCriteriaRepository $ratingCriteriaRepository)
    {
        $path = base_path().'\resources\files_for_db\rating_criteria.json';
        $data = json_decode(file_get_contents($path), true);

        $ratingCriteriaRepository->insertData($data);
    }
}
