<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use \App\Services\ReliabilityDataConverter;

class RatingCompaniesSeeder extends Seeder
{
    /**
     * @return void
     */
    public function run(ReliabilityDataConverter $reliabilityDataConverter)
    {
        $file = json_decode(file_get_contents('./resources/files_for_db/rating_companies.json'), true);

        $dataToInsert = [];

        foreach ($file as $item)
        {
            $dataToInsert[] = [
                'company_name' => $item['name'],
                'company_slug' => Str::slug($item['name']),
                'company_address' => $item['address'],
                'company_logo_path' => $item['logo_path'],

                'company_link' => $item['link'],

                'company_yandex_id' => $item['yandex_id'],
                'company_google_id' => $item['google_id'],
                'company_otzovick_id' => $item['otzovick_id'],

                'company_term_of_work_clear' => $item['term_of_work'],
                'company_term_of_work' => $reliabilityDataConverter->getTermOfWork($item['term_of_work']),

                'company_staff_members_clear' => $item['staff_members'],
                'company_staff_members' => $reliabilityDataConverter->getStaffMembers($item['staff_members']),

                'company_amount_of_credit_clear' => $item['amount_of_credit'],
                'company_amount_of_credit' => $reliabilityDataConverter
                    ->getAmountOfCredit($item['amount_of_credit'], $item['profitability']),

                'company_change_of_leader_clear' => $item['change_of_leader'],
                'company_change_of_leader' => $reliabilityDataConverter->getChangeOfLeader($item['change_of_leader']),


                'company_profitability_clear' => $item['profitability'],
                'company_profitability' => $reliabilityDataConverter->getProfitability($item['profitability']),

                'company_financial_autonomy_clear' => $item['financial_autonomy'],
                'company_financial_autonomy' => $reliabilityDataConverter->getFinancialAutonomy($item['financial_autonomy']),

                'company_legal_address' => $item['legal_address'],
                'company_checks' => $item['checks'],
                'company_search_for_colleagues' => $item['search_for_colleagues'],
                'company_enforcement_proceedings' => $item['enforcement_proceedings'],


                'company_payment_of_wages' => $item['payment_of_wages'],
                'company_income_dynamics' => $item['income_dynamics'],
                'company_bulk_address_register' => $item['bulk_address_register'],

                'company_rusprofile_link' => $item['rusprofile_link'],
                'company_pay_attention' => $item['pay_attention'],
                'company_pay_attention_type' => $item['pay_attention_type'],

                'company_total_reliability' => $reliabilityDataConverter
                    ->getTotalReliability(
                        $item['term_of_work'],
                        $item['staff_members'],
                        $item['amount_of_credit'],
                        $item['profitability'],
                        $item['change_of_leader'],
                        $item['financial_autonomy'],
                        $item['legal_address'],
                        $item['search_for_colleagues'],
                        $item['enforcement_proceedings'],
                        $item['payment_of_wages'],
                        $item['income_dynamics'],
                        $item['bulk_address_register']
                    ),

                'is_published' => $reliabilityDataConverter->getIsPublished($item)
            ];
        }
        DB::table('rating_companies')
            ->insert($dataToInsert);
    }
}
