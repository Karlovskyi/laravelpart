<?php

use App\Repositories\Seeder\Pages\SeederPagesRepository;
use App\Repositories\Seeder\RatingCompanies\SeederRatingCompaniesRepository;
use Illuminate\Database\Seeder;

class RatingCompaniesPageRelationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param SeederRatingCompaniesRepository $ratingCompaniesRepository
     * @param SeederPagesRepository $pagesRepository
     * @return void
     */
    public function run(
        SeederRatingCompaniesRepository $ratingCompaniesRepository,
        SeederPagesRepository $pagesRepository
    )
    {
        $companies = $ratingCompaniesRepository->getModelCompaniesSlug();

        $links = $companies->map(function ($item) {
            return route('user.company', $item->company_slug, false);
        })->toArray();
        $pages = $pagesRepository->getPagesByLinks($links);

        $companies->each(function ($item) use ($pages, $ratingCompaniesRepository) {
            $page = $pages
                ->where('page_str_index', route('user.company', $item->company_slug, false))
                ->first();
            if($page) {
                $ratingCompaniesRepository->setPageId($item, $page->page_id);
            } else {
                throw new Exception("Не найдена станица компании" . $item->company_slug);
            }

        });
    }
}
