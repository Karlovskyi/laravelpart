<?php

use App\Repositories\Seeder\LinkingPageAndHeaders\SeederLinkingPageAndHeadersRepository;
use App\Repositories\Seeder\PageHeaders\SeederPageHeaderRepository;
use App\Repositories\Seeder\Pages\SeederPagesRepository;
use Illuminate\Database\Seeder;

class LinkingPageAndHeadersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param SeederPagesRepository $pagesRepository
     * @param SeederPageHeaderRepository $headerRepository
     * @param SeederLinkingPageAndHeadersRepository $linkingPageAndHeadersRepository
     * @return void
     */
    public function run(
        SeederPagesRepository $pagesRepository,
        SeederPageHeaderRepository $headerRepository,
        SeederLinkingPageAndHeadersRepository $linkingPageAndHeadersRepository
    )
    {
        $pages = $pagesRepository->getPagesRoutes();
        $headers = $headerRepository->getKeys();

        $pages->each(function ($page) use ($headers, $linkingPageAndHeadersRepository)
        {
            $headers->each(function ($header) use ($page, $linkingPageAndHeadersRepository)
            {
                if (in_array($header->key, $this->data[$page->page_route_name]))
                {
                    $linkingPageAndHeadersRepository->createLink([
                        'page_id' => $page->page_id,
                        'header_id' => $header->id
                    ]);
                }

            });
        });
    }

    private $data = [
        'user.main' => ['rating-companies', 'how-to-choose', 'remember', 'about-site'],
        'user.reliability' => ['about-site-rely', 'rating-reliability'],
        'user.compare' => ['compare'],
        'user.compare-all' => ['compare'],
        'user.how-to-choose' => ['how-calculate', 'how-calculate-rely', 'how-to-choose', 'remember'],
        'user.about' => ['about'],
        'user.company' => ['contacts', 'portfolio', 'rely', 'rating', 'reviews', 'company']
    ];
}
