<?php

use App\Repositories\Seeder\RatingCompanies\SeederRatingCompaniesRepository;
use App\Repositories\RatingCompaniesRepository;
use App\Services\UpdateDBService;
use Illuminate\Database\Seeder;

class RatingAverageValuesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param UpdateDBService $updateDBService
     * @param SeederRatingCompaniesRepository $ratingCompaniesRepository
     * @return void
     */
    public
    function run(UpdateDBService $updateDBService, SeederRatingCompaniesRepository $ratingCompaniesRepository)
    {
        $COUNT = $ratingCompaniesRepository->getCompaniesQuantity();
    for ($i = 1; $i <= $COUNT; $i++)
        {
            /** @var App\Models\RatingCompanies $dbItem */
            $result = $updateDBService->updateCompany_sReviewData($i);
            if(!$result) {
                echo "Что-то пошло те так, при вычеслении средних показателей компании с id = $i\n";
            }
        }

    }
}
