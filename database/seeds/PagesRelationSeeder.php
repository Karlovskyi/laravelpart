<?php

use App\Repositories\Seeder\Pages\SeederPagesRepository;
use App\Repositories\Seeder\PageSearchAttributes\SeederPageSearchRepository;
use Illuminate\Database\Seeder;

class PagesRelationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param SeederPagesRepository $pagesRepository
     * @param SeederPageSearchRepository $searchRepository
     * @return void
     */
    public function run(
        SeederPagesRepository $pagesRepository,
        SeederPageSearchRepository $searchRepository
    )
    {
        $pages = $pagesRepository->getPagesForSetRel();
        $seo = $searchRepository->getSeoIndexes()->keyBy('title_index');

        $pages->each(function ($item) use ($pagesRepository, $seo) {
            $index = $this->schema[$item->page_route_name];
            $seo_id = $seo[$index]->id;
            $pagesRepository->setSeoId($item, $seo_id);
        });
    }


    private $schema = [
        'user.main' => 'main',
        'user.reliability' => 'reliability',
        'user.compare' => 'compare',
        'user.compare-all' => 'compareAll',
        'user.how-to-choose' => 'howToChoose',
        'user.about' => 'about',
        'user.company' => 'company',
    ];
}
