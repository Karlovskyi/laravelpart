<?php

use App\Repositories\Seeder\RatingReviews\SeederRatingReviewsRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RatingImagesSeeder extends Seeder
{
    private $limmit = [

    ];

    private function setImageOnCompanyPage($item)
    {
        if (!isset($this->limmit[$item['company_name']]))
        {
            $this->limmit[$item['company_name']] = 0;
        }
        if ($item['company_name'] === 'Студия Ремонтов' && $item['review_mark'] === 5 &&
            $this->limmit[$item['company_name']] < 20)
        {
            $this->limmit[$item['company_name']]++;
            return 1;
        }
        if ($item['review_mark'] <= 3 &&
            $this->limmit[$item['company_name']] < 20)
        {
            $this->limmit[$item['company_name']]++;
            return 1;
        }

        return 0;
    }

    /**
     *
     * @param SeederRatingReviewsRepository $ratingReviewsRepository
     * тот Seeder отвечает за заполнение и связывание элементов таблицы rating_images
     */
    public
    function run(SeederRatingReviewsRepository $ratingReviewsRepository)
    {
        $file = json_decode(file_get_contents('./resources/files_for_db/final.json'), true);
        $reviewsWithImages = [];
        foreach ($file as $review)
        {
            if (count($review['imgs']) !== 0)
            {
                $reviewsWithImages[] = $review;
            }
        }
        $data = $ratingReviewsRepository->seedHelper();
        $toInsert = [];
        for ($i = 0; $i < count($reviewsWithImages); $i++)
        {
            $reviewItem = $reviewsWithImages[$i];
            for ($j = 0; $j < count($data); $j++)
            {
                $dataItem = $data[$j];
                if ($reviewItem['name'] === $dataItem['reviewer_name'] &&
                    $reviewItem['date'] === $dataItem['review_date'])
                {
                    $imgs = $reviewItem['imgs'];
                    foreach ($imgs as $link)
                    {
                        $toInsert[] = [
                            'review_id' => $dataItem['review_id'],
                            'image_path' => $link,
                            'image_on_company_page' => $this->setImageOnCompanyPage($dataItem)
                        ];
                    }
                    break;
                }
            }
        }
        DB::table('rating_images')
            ->insert($toInsert);
    }
}
