<?php

use Illuminate\Database\Seeder;
use App\Repositories\Seeder\Constants\SeederConstantsRepository;

class ConstantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param SeederConstantsRepository $constantsRepository
     * @return void
     */
    public function run(SeederConstantsRepository $constantsRepository)
    {
        $data = [
            [
                'constant_key' => 'reviews_per_page',
                'constant_value' => '6'
            ],
        ];

        $constantsRepository->insertData($data);
    }
}
