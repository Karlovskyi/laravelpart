<?php

use App\Repositories\Seeder\Pages\SeederPagesRepository;
use App\Repositories\Seeder\RatingCompanies\SeederRatingCompaniesRepository;
use Illuminate\Database\Seeder;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param SeederRatingCompaniesRepository $ratingCompaniesRepository
     * @param SeederPagesRepository $pagesRepository
     * @return void
     */
    public function run(
        SeederRatingCompaniesRepository $ratingCompaniesRepository,
        SeederPagesRepository $pagesRepository
    )
    {

        $companies = $ratingCompaniesRepository->getCompaniesSlug()->map(function ($item) {
            return [
                'page_str_index' => route('user.company', $item->company_slug, false),
                'page_route_name' => 'user.company'
            ];
        })->toArray();

        $data = array_merge($this->data, $companies);

        foreach ($data as $v) {
            $pagesRepository->createPage($v);
        }
    }

    private $data = [
        [
            'page_str_index' => '/',
            'page_route_name' => 'user.main'
        ], [
            'page_str_index' => '/reliability',
            'page_route_name' => 'user.reliability'
        ], [
            'page_str_index' => '/compare',
            'page_route_name' => 'user.compare'
        ], [
            'page_str_index' => '/compare-all',
            'page_route_name' => 'user.compare-all'
        ], [
            'page_str_index' => '/how-to-choose',
            'page_route_name' => 'user.how-to-choose'
        ], [
            'page_str_index' => '/about',
            'page_route_name' => 'user.about'
        ]
    ];
}
