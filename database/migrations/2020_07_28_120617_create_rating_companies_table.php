<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatingCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rating_companies', function (Blueprint $table) {
            $table->increments('company_id');
            $table->string('company_name')->unique()->index();
            $table->string('company_slug')->unique()->index();
            $table->string('company_link')->nullable()->default(null);
            $table->string('company_address')->nullable();
            $table->string('company_logo_path')->nullable();
            $table->string('company_phone')->nullable()->default(null);
            $table->string('company_google_maps_link', 1024)->nullable()->default(null);
            $table->string('company_inst')->nullable()->default(null);
            $table->string('company_fb')->nullable()->default(null);
            $table->string('company_you_tube')->nullable()->default(null);

            $table->string('company_criteria_1')->default('company_staff_members');
            $table->string('company_criteria_2')->default('company_term_of_work');
            $table->string('company_criteria_3')->default('company_income_dynamics');

            $table->text('company_description')->nullable()->default(null);

            $table->boolean('is_published')->default(true);

            $table->string('company_yandex_id')->unique();
            $table->string('company_google_id')->unique();
            $table->string('company_otzovick_id')->unique();

            $table->integer('company_positive')->default(0);
            $table->integer('company_negative')->default(0);

            $table->integer('company_reliability_position')->nullable()->unique()->default(null);
            $table->integer('company_position')->nullable()->unique()->default(null);

            $table->integer('company_mark_difference')->default(0);

            $table->unsignedFloat('company_mark_average')->default(0);
            $table->unsignedFloat('company_mark_average_yandex')->default(0);
            $table->unsignedFloat('company_mark_average_google')->default(0);
            $table->unsignedFloat('company_mark_average_otzovick')->default(0);

            $table->integer('company_review_quantity')->default(0);
            $table->integer('company_review_quantity_yandex')->default(0);
            $table->integer('company_review_quantity_google')->default(0);
            $table->integer('company_review_quantity_otzovick')->default(0);


            $table->integer('company_amount_of_credit')->nullable();
            $table->integer('company_amount_of_credit_clear')->nullable();

            $table->integer('company_change_of_leader')->nullable();
            $table->integer('company_change_of_leader_clear')->nullable();

            $table->integer('company_profitability')->nullable();
            $table->integer('company_profitability_clear')->nullable();

            $table->integer('company_financial_autonomy')->nullable();
            $table->integer('company_financial_autonomy_clear')->nullable();

            $table->integer('company_term_of_work')->nullable();
            $table->integer('company_term_of_work_clear')->nullable();

            $table->integer('company_staff_members')->nullable();
            $table->integer('company_staff_members_clear')->nullable();

            $table->integer('company_legal_address')->nullable();
            $table->integer('company_checks')->nullable();
            $table->integer('company_enforcement_proceedings')->nullable();
            $table->integer('company_search_for_colleagues')->nullable();


            $table->boolean('company_payment_of_wages')->nullable();
            $table->boolean('company_income_dynamics')->nullable();
            $table->boolean('company_bulk_address_register')->nullable();



            $table->string('company_rusprofile_link')->nullable();

            $table->integer('company_total_reliability')->nullable();


            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rating_companies');
    }
}
