<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddToCritToCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rating_companies', function (Blueprint $table) {
            $table->boolean('company_relation')->nullable();
            $table->string('company_relation_clear')->nullable();
            $table->string('company_pay_attention')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rating_companies', function (Blueprint $table) {

        });
    }
}
