<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('surname');
            $table->string('login')->unique();
            $table->string('email')->unique();
            $table->string('password');

            $table->boolean('is_admin')->default(0);
            $table->boolean('is_company_agent')->default(0);
            $table->integer('company_id')->unsigned()->nullable();
            $table->foreign('company_id')->references('company_id')->on('rating_companies');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
