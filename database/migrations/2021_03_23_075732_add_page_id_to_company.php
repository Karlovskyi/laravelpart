<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPageIdToCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rating_companies', function (Blueprint $table) {
            $table->unsignedInteger('page_id')->index()->nullable();
            $table->foreign('page_id')->references('page_id')->on('pages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rating_company', function (Blueprint $table) {
            //
        });
    }
}
