<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCriterionFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rating_criteria', function (Blueprint $table)
        {
            $table->text('criteria_description')->nullable()->default(null);
            $table->integer('criteria_type')->default(1);
            $table->string('criteria_value_1')->default('');
            $table->string('criteria_value_2')->default('');
            $table->string('criteria_value_3')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
