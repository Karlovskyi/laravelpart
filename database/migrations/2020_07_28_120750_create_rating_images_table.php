<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatingImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rating_images', function (Blueprint $table) {

            $table->increments('image_id');

            $table->integer('review_id')->unsigned()->index();

            $table->boolean('is_published')->default(true);
            $table->boolean('image_on_company_page')->default(false);


            $table->string('image_path');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('review_id')->references('review_id')->on('rating_reviews');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rating_images');
    }
}
