<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatingReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rating_reviews', function (Blueprint $table) {
            $table->increments('review_id');

            $table->integer('company_id')->unsigned()->index();
            $table->integer('resource_id')->index(); // 0 - yandex, 1 - google, 2 - otzovick

            $table->text('review_text');
            $table->integer('review_mark');
            $table->date('review_date');
            $table->string('reviewer_name');
            $table->string('review_link', 512)->unique();

            $table->date('review_must_be_published_at')->default(null)->nullable();

            $table->integer('position')->default(0);
            $table->boolean('is_published')->default(true);
            $table->boolean('positive');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('company_id')->references('company_id')->on('rating_companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rating_reviews');
    }
}
