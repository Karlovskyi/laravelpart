<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CompanyLegalInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rating_companies', function (Blueprint $table)
        {
            $table->string('company_ogrn')->nullable()->default(null);
            $table->string('company_inn')->nullable()->default(null);
            $table->string('company_legal_address_value', 256)->nullable()->default(null);
            $table->string('company_legal_address_comment', 1024)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
