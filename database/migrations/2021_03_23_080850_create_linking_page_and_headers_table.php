<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkingPageAndHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linking_page_and_headers', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('page_id')->index();
            $table->unsignedBigInteger('header_id')->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('page_id')->references('page_id')->on('pages');
            $table->foreign('header_id')->references('id')->on('page_headers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('linking_page_and_headers');
    }
}
