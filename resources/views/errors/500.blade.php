@extends('errors::minimal')

@section('title', __('Server ErrorIndicator'))
@section('code', '500')
@section('message', __('Server ErrorIndicator'))
