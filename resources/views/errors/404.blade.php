@extends('layouts.user')

@section('content')
    <section class="error">
        <h1 class="header header-main ta-c">404</h1>
        <p class="error__paragraph header header-sub ta-c">Упс, что-то пошло не так</p>
    </section>

@endsection
