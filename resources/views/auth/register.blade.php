@extends('layouts.user')

@section('content')
        <article class="form">
            <div class="form__wrapper">
                <h1 class="header header-main">{{ __($headers['register']->value) }}</h1>

                <section class="form__body">
                    <form method="POST" action="{{ route('register') }}" class="form__element">
                        @csrf

                        <div class="form__group">
                            <label for="name" class="form__label">{{ __('Имя') }}</label>

                            <div class="form__input-group">
                                <input id="name" type="text" class="form__input @error('name') form__input_error @enderror"
                                       name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="form__error-tip" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form__group">
                            <label for="surname" class="form__label">{{ __('Фамилия') }}</label>

                            <div class="form__input-group">
                                <input id="surname" type="text" class="form__input @error('surname') form__input_error @enderror"
                                       name="surname" value="{{ old('surname') }}" required autocomplete="surname" autofocus>

                                @error('surname')
                                    <span class="form__error-tip" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form__group">
                            <label for="login" class="form__label">{{ __('Логин') }}</label>

                            <div class="form__input-group">
                                <input id="login" type="text" class="form__input @error('login') form__input_error @enderror"
                                       name="login" value="{{ old('login') }}" required autocomplete="login" autofocus>

                                @error('login')
                                <span class="form__error-tip" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form__group">
                            <label for="email" class="form__label">{{ __('E-Mail') }}</label>

                            <div class="form__input-group">
                                <input id="email" type="email" class="form__input @error('email') form__input_error @enderror"
                                       name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="form__error-tip" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form__group">
                            <label for="password" class="form__label">{{ __('Пароль') }}</label>

                            <div class="form__input-group">
                                <input id="password" type="password" class="form__input @error('password') form__input_error @enderror"
                                       name="password" value="{{ old('password') }}" required autocomplete="password" autofocus>

                                @error('password')
                                    <span class="form__error-tip" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form__group">
                            <label for="password-confirm" class="form__label">{{ __('Подтверждение пароля') }}</label>

                            <div class="form__input-group">
                                <input id="password-confirm" type="password" class="form__input @error('password-confirm') form__input_error @enderror"
                                       name="password-confirm" value="{{ old('password-confirm') }}" required autocomplete="password-confirm" autofocus>
                            </div>
                        </div>

                        <div class="form__submit-wrapper">
                            <button type="submit" class="btn btn_submit">
                                {{ __('Зарегестрироваться') }}
                            </button>
                        </div>
                    </form>
                </section>
            </div>
        </article>
@endsection

