@extends('layouts.user')

@section('content')
    <article class="form">
        <div class="form__wrapper">
            <h1 class="header header-main">{{ __($headers['password-reset']->value) }}</h1>

            <div class="form__body">


                <form method="POST" action="{{ route('password.email') }}" class="form__element">
                    @csrf

                    <div class="form__group">
                        <label for="email" class="form__label">{{ __('E-Mail') }}</label>

                        <div class="form__input-group">
                            <input id="email" type="email" class="form__input @error('email') form__input_error @enderror"
                                   name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="form__error-tip" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            @if (session('status'))
                                <span class="form__success-tip" role="alert">
                                    <strong>{{ session('status') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form__submit-wrapper">
                        <button type="submit" class="btn btn_submit">
                            {{ __('Восстановить пароль') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </article>
@endsection
