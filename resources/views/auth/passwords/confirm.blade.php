@extends('layouts.user')

@section('content')
    <article class="form">
        <div class="form__wrapper">
            <h1 class="header header-main">{{ __('Подтвердить пароль') }}</h1>

            <section class="form__body">
                <form method="POST" action="{{ route('password.confirm') }}">
                    @csrf

                    <div class="form__group">
                        <label for="password" class="form__label">{{ __('Пароль') }}</label>

                        <div class="form__input-group">
                            <input id="password" type="password"
                                   class="form__input @error('password') form__input_error @enderror" name="password" required
                                   autocomplete="current-password">

                            @error('password')
                                <span class="form__error-tip" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form__submit-wrapper">
                        <button type="submit" class="btn btn_submit">
                            {{ __('Восстановить пароль') }}
                        </button>
                        @if (Route::has('password.request'))
                            <a class="form__link" href="{{ route('password.request') }}">
                                {{ __('Забыли пароль?') }}
                            </a>
                        @endif
                    </div>
                </form>
            </section>
        </div>
    </article>
@endsection
