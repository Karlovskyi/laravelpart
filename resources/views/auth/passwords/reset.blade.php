@extends('layouts.user')

@section('content')
    <article class="form">
        <div class="form__wrapper">
            <h1 class="header header-main">{{ __($headers['password-reset']->value) }}</h1>

            <section class="form__body">
                <form method="POST" action="{{ route('password.update') }}" class="form__element">
                    @csrf

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form__group">
                        <label for="email"
                               class="form__label">{{ __('E-Mail') }}</label>

                        <div class="form__input-group">
                            <input id="email" type="email"
                                   class="form__input @error('email') form__input_error @enderror" name="email"
                                   value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="form__error-tip" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form__group">
                        <label for="password"
                               class="form__label">{{ __('Пароль') }}</label>

                        <div class="form__input-group">
                            <input id="password" type="password"
                                   class="form__input @error('password') is-invalid @enderror" name="password"
                                   required autocomplete="new-password">

                            @error('password')
                                <span class="form__error-tip" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form__group">
                        <label for="password-confirm"
                               class="form__label">{{ __('Подтверждение пароля') }}</label>

                        <div class="form__input-group">
                            <input id="password-confirm" type="password" class="form__input"
                                   name="password_confirmation" required autocomplete="new-password">
                        </div>
                    </div>

                    <div class="form__submit-wrapper">
                        <button type="submit" class="btn btn_submit">
                            {{ __('Восстановить пароль') }}
                        </button>
                    </div>
                </form>
            </section>
        </div>
    </article>
@endsection
