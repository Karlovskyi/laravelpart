@extends('layouts.user')

@section('content')
    <article class="form">
        <div class="form__wrapper">
            <h1 class="header header-main">{{ __($headers['login']->value) }}</h1>

            <section class="form__body">
                <form method="POST" action="{{ route('login') }}" class="form__element">
                    @csrf

                    <div class="form__group">
                        <label for="email" class="form__label">{{ __('E-Mail или Логин') }}</label>

                        <div class="form__input-group">
                            <input id="email" class="form__input {{ $errors->has('login') || $errors->has('email') ? 'form__input_error' : '' }}"
                                   name="login" value="{{ old('email') ?? old('login') }}" required autocomplete="email" autofocus>

                            @if ($errors->has('login') || $errors->has('email'))
                                <span class="form__error-tip" role="alert">
                                    <strong>{{ $errors->first('login') ?: $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form__group">
                        <label for="password" class="form__label">{{ __('Пароль') }}</label>

                        <div class="form__input-group">
                            <input id="password" type="password" name="password"
                                   class="form__input @error('password') form__input_error @enderror" required
                                   autocomplete="current-password">

                            @error('password')
                                <span class="form__error-tip" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form__group">
                        <div class="form__input-group form__input-group_checkbox">
                            <input type="checkbox" name="remember"
                                   id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form__label" for="remember">
                                {{ __('Запомнить меня') }}
                            </label>
                        </div>
                    </div>

                    <div class="form__submit-wrapper">
                        <button type="submit" class="btn btn_submit">
                            {{ __('Войти') }}
                        </button>
                        @if (Route::has('password.request'))
                        <a class="form__link" href="{{ route('password.request') }}">
                            {{ __('Забыли пароль?') }}
                        </a>
                        @endif
                        <a class="form__link" href="{{ route('register') }}">
                            {{ __('Регестрация') }}
                        </a>
                    </div>
                </form>
            </section>
        </div>
    </article>
@endsection
