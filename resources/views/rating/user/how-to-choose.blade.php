@extends('layouts.user')

@section('content')
    @component('rating.user.includes.best-choice.best_choice',
        ['best' => $best])
        <div class="stars_best-choice">
            @include('rating.user.includes.stars', ['company_star' => $best])
        </div>
    @endcomponent

    @include('rating.user.includes.cover',  ['extraStyle' => 'cover_first-position'])

    @include('rating.user.includes.main_info',
       [
           'howCalculate'  => $headers['how-calculate'],
           'howCalculateRely' => $headers['how-calculate-rely'],
           'howToChoose' => $headers['how-to-choose'],
           'remember' => $headers['remember']
       ])
@endsection
