<section class="company-data">
    <form action="{{route('user.compare')}}" method="POST" name="compare">
        @csrf
    </form>
    <div class="company-data__name header-company">
        <h1>{{$company_header}}</h1>
    </div>
    <div class="company-data__general">

        <img src="/logos/{{$company->company_logo_path}}"
             alt="Логотип Компании {{$company->company_name}}"
             title="Логотип Компании {{$company->company_name}}"
             class="company-data__logo"
             width="150" height="150"
             loading="lazy">
        <div class="company-data__flex-wrapper">
            <div class="company-data__rate-part">
                <div>
                    @if($company->company_link)
                        <a href="{{$company->company_link}}" class="company-data__link"
                           target="_blank">{{$company->companyLinkWithoutHTTP}}</a>
                    @else
                        <span class="text">Ссылка отсутствует</span>
                    @endif
                </div>
                <div class="stars_company-data">
                    @include('rating.user.includes.stars', ['company_star' => $company])
                </div>
                <div class="company-data__total-reviews">
                    Количество отзывов: {{$company->company_review_quantity}}
                </div>
            </div>
            <div class="company-data__compare-part">
                <div class="company-data__compare-button">
                    @include('rating.user.includes.dropdown',
                ['company_dropdown' => $company, 'dropdown_index' => $company->company_id, 'dropdown__class' => 'btn_company'])
                </div>
            </div>


        </div>
    </div>
    @include('rating.user.includes.company.contacts')
    @include('rating.user.includes.portfolio', ['portfolio' => $portfolio])
    @include('rating.user.includes.reliability-full', ['rely' => $rely, 'criterion' => $criterion])
    @include('rating.user.includes.company_rating', ['rating' => $rating])
</section>
