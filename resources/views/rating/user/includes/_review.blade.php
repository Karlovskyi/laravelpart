@php $isImages = (bool) $review->images->count(); @endphp
<li class="company-reviews__item" @if($isImages)itemscope itemtype="https://schema.org/Review" @endif id="reviews-{{$index}}">
    <div @if($isImages)itemprop="itemReviewed" itemscope itemtype="https://schema.org/LocalBusiness"@endif>
        @if($isImages)<meta itemprop="name" content="{{$company->company_name}}">@endif
        <div class="company-reviews__item-info-line">
            <div class="company-reviews__item-mark">
                <a href="{{$review->review_link}}" class="company-reviews__item-source"
                   rel="nofollow"
                   target="_blank">
                    <span>{{$review->reviewer_name}}</span>
                </a>
                @include('rating.user.includes.stars.stars_review', ['mark' => $review->review_mark, 'review' => $isImages])
            </div>
            <div class="company-reviews__item-date">
                {{$review->review_date}}
            </div>
        </div>
        <div class="company-reviews__item-text" @if($isImages)itemprop="reviewBody"@endif>{{$review->review_text}}</div>
        <div class="company-reviews__item-image-wrapper">
            @foreach($review->images as $img)
                <img src="{{$img->image_path}}"
                     alt="Фото из отзыва-{{$counter['v']}}"
                     title="Фото из отзыва-{{$counter['v']}}"
                     class="company-reviews__item-image"
                     itemprop="image"
                     height="180" width="234"
                     loading="lazy">
                @php $counter['v'] = $counter['v'] + 1 @endphp
            @endforeach
        </div>
    </div>
    @if($isImages)
        <span itemprop="author" itemscope itemtype="https://schema.org/Person">
            <meta itemprop="name" content="{{$review->reviewer_name}}">
        </span>
    @endif
</li>
