<section class="company-reviews" id="reviews">
    <div class="header-sub">
        <h2>{{$header}}</h2>
    </div>
    <ul class="company-reviews__container" data-company_id="{{$company->company_id}}">
        @php $counter = collect(['v' => 1]) @endphp
        @foreach($paginate['reviews'] as $i => $review)
            @include('rating.user.includes._review', ['review' => $review, 'counter' => $counter, 'index' => $i])
        @endforeach
    </ul>
    @include('rating.user.includes.paginator', ['paginate' => $paginate])
</section>
