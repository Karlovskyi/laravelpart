<div class="spinner">
    <div class="spinner__arc">
        <div></div>
    </div>
</div>
<style type="text/css">
    @keyframes spin {
        0% {
            transform: rotate(0deg)
        }
        50% {
            transform: rotate(180deg)
        }
        100% {
            transform: rotate(360deg)
        }
    }

    .spinner__arc div {
        position: absolute;
        animation: spin 1.45s linear infinite;
        width: 64px;
        height: 64px;
        top: 18px;
        left: 18px;
        border-radius: 50%;
        box-shadow: 0 3.6px 0 0 #3c63c4;
        transform-origin: 32px 33.8px;
    }

    .spinner {
        width: 91px;
        height: 91px;
        display: inline-block;
        overflow: hidden;
        background: none;
    }

    .spinner__arc {
        width: 100%;
        height: 100%;
        position: relative;
        transform: translateZ(0) scale(0.91);
        backface-visibility: hidden;
        transform-origin: 0 0;
    }

    .spinner__arc div {
        box-sizing: content-box;
    }
</style>
