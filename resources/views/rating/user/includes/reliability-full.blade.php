<div class="reliability" id="rely">
    <h2>{{$rely}}</h2>
    @include('rating.user.includes._reliability_content', ['criterion' => $criterion])
    <div class="reliability__links reliability__links_rely">
        <a id="more-1" class="reliability__link-more" aria-label="Развернуть блок">Больше</a>
        <a id="less-1" class="reliability__link-less" aria-label="Свернуть блок">Скрыть</a>
    </div>
</div>
