<div class="graphic-line" data-division="{{$company_rely->TotalReliabilityPercent}}"
     data-attention="{{$company_rely->IsCriticalAttention}}">
    <div class="graphic-line__percent">
        {{$company_rely->TotalReliabilityPercent}}%
    </div>
    <canvas class="graphic-line__canvas" width="162" height="3"></canvas>
    <div class="graphic-line__text">
        {{$company_rely->TotalReliabilityTextStatus}}
    </div>
</div>
