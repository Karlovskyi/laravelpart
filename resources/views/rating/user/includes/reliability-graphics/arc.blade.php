<div class="graphic-arc" data-division="{{$company_rely->TotalReliabilityPercent}}"
     data-attention="{{$company_rely->IsCriticalAttention}}">
    <canvas class="graphic-arc__canvas" width="100" height="100"></canvas>
    <div class="graphic-arc__percent">
        {{$company_rely->TotalReliabilityPercent}}%
    </div>
    <div class="graphic-arc__text">
        {{$company_rely->TotalReliabilityTextStatus}}
    </div>
</div>
