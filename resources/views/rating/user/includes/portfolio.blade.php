<div class="portfolio">
    <h2>{{$portfolio}}</h2>
    @if($company->images->isNotEmpty())
        <div class="portfolio__img-wrapper">
            <img src="{{$company->images[0]->image_path}}"
                 alt="Выбраная картинка"
                 title="Выбраная картинка"
                 class="portfolio__img"
                 width="744" height="471"
                 loading="lazy">
            <img src="{{$company->images[0]->image_path}}"
                 alt="Выбраная картинка"
                 title="Выбраная картинка"
                 class="portfolio__img-bg"
                 width="744" height="471"
                 loading="lazy">
            <button class="portfolio__controls portfolio__controls_left"  aria-label="Предыдущая фотография">
            </button>
            <button class="portfolio__controls portfolio__controls_right" aria-label="Следующая фотография">
            </button>
        </div>

    @endif
    <div class="portfolio__watermark">
        Фото из отзывов
    </div>
    <div class="portfolio__overflow">
        <button class="portfolio__arrow portfolio__left" aria-label="Листать гарелею влево">
        </button>
        <div class="portfolio__carousel">
            @foreach($company->images as $i => $image)
                <img src="{{$image->image_path}}"
                     alt="Фото из отзыва {{$i}}"
                     title="Фото из отзыва {{$i}}"
                     class="portfolio__item @if($i === 0) portfolio__item_selected @endif"
                     width="160" height="160"
                     loading="lazy">
            @endforeach
        </div>
        <button class="portfolio__arrow portfolio__right" aria-label="Листать гарелею вправо">
        </button>
    </div>
    @include('rating.user.includes._portfolio_about')
</div>
