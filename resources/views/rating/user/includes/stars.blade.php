{{--
    Чтобы использовать данный компонент,
    передайте параметр $compamy__star
--}}
@include('rating.user.includes.stars.stars_base', ['star' => $company_star->company_mark_average, 'type' => $type ?? ''])
