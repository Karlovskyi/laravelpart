<table class="how-calculate__alter-table">
    <tr class="how-calculate__alter-table-header">
        <td class="how-calculate__alter-table-item-side-h">Категория</td>
        <td class="how-calculate__alter-table-item-main-h">
            Хорошо
            <span class="how-calculate__circle" data-content="2"></span>
        </td>
    </tr>

    @include('rating.user.includes._alter-table-group', ['names' => [
    'Больше 4 лет', '15 и более', 'Менее 10% от прибыли', 'Более 2 лет назад', 'Есть', 'Более 2 млн',
    'Не менялся более 2 лет', 'Более 50%', 'Положительная', 'Нет', 'Не состоит', 'Есть аккаунты в HH и других подобных площадках.'
    ]])

    <tr class="how-calculate__alter-table-header">
        <td class="how-calculate__alter-table-item-side-h">Категория</td>
        <td class="how-calculate__alter-table-item-main-h">
            Средне
            <span class="how-calculate__circle" data-content="1"></span>
        </td>
    </tr>

    @include('rating.user.includes._alter-table-group', ['names' =>
    ['Больше 2 лет', 'Менее 15', 'Менее 20% от прибыли','Менее 2 лет назад','-',
    'Положительная', 'Менялся в течение последних двух лет', '5-50%', '-', 'Есть', '-',
    'Есть информация о наборе сотрудников только на сайте.']])

    <tr class="how-calculate__alter-table-header">
        <td class="how-calculate__alter-table-item-side-h">Категория</td>
        <td class="how-calculate__alter-table-item-main-h">
            Плохо
            <span class="how-calculate__circle" data-content="0"></span>
        </td>
    </tr>

    @include('rating.user.includes._alter-table-group', ['names' =>
    ['Год и меньше', 'Менее 5', 'Более 21% от прибыли', 'Год и меньше', 'Нет',
    'Отрицательная', 'Менялся менее чем год назад', 'Менее 5%', 'Отрицательная',
    'Есть завершённые', 'Состоит','Не ищет сотрудников.']])
</table>
