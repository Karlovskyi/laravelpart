<section class="info" id="info">
    <div class="header_info header">
        <h1>{{$howCalculate}}</h1>
    </div>
    <p class="info__paragraph info__paragraph_offset">Мы собираем отзывы с оценками из самых проверенных
        сайтов отзывов. Высшую позицию в рейтинге занимает тот, у кого разница
        положительных и отрицательных отзывов выше.
    </p>
    <p class="info__paragraph info__paragraph_offset">
        Мы вручную проверяем отзывы. Это не значит, что всем отзывам можно доверять,
        потому что хороший купленный отзыв
        сложно отличить от честного. Помимо этого, проверить большое количество отзывов,
        не совершив ошибок – невозможно. По возможности самостоятельно проверяйте отзывы.
    </p>
    @include('rating.user.includes.how_calculate', ['howCalculateRely' => $howCalculateRely])
    @include('rating.user.includes.how-to-choose.long-text', [
    'howToChoose' => $howToChoose,
    'remember' => $remember,
    'extraClass' => 'header_info'
    ])
</section>
