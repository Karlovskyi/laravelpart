<aside class="photo-zoom photo-zoom_disable">
    <button class="photo-zoom__close" aria-label="Закрыть просмотр фотографии">
        <span class="photo-zoom__line photo-zoom__line_first">
        </span>
        <span class="photo-zoom__line photo-zoom__line_second">
        </span>
    </button>
    <div class="photo-zoom__img-wrapper">
        <img class="photo-zoom__img photo-zoom__img_disable" src="{{asset('/images/loading-screen.svg')}}"
             width="700" height="700"
             alt="Увеличеная фотография из отзывов"
             title="Увеличеная фотография из отзывов">
    </div>
</aside>
