<ul class="rating-list">
    @foreach($companies as $index => $company)
        <li class="rating-list__li">
            <a href="{{ route('user.company', $company->company_slug) }}"
               class="rating-card rating-card_col"
               data-link="{{$company->company_slug}}"
               style="z-index: {{100-$index}}; position: relative">

                <div class="rating-card__line">
                    <div class="rating-card__line rating-card__line_center flex-basis-120 h-5">
                        <div class="flex-basis-30">
                            {{$index+1}}
                        </div>
                        <div class="flex-basis-90">
                            <img src="logos/{{$company->company_logo_path}}"
                                 alt="Логотип компании {{$company->company_name}}"
                                 title="Логотип компании {{$company->company_name}}"
                                 class="rating-card__img"
                                 width="67" height="81"
                                 loading="lazy">
                        </div>
                    </div>
                    <div class="rating-card__col flex-basis-170">
                        <h3 class="text-overflow text-normal text-size-4 max-w-10">
                            {{$company->company_name}}
                        </h3>
                        <div class="text-overflow rating-card__appeared-link max-w-10"
                             data-content="{{$company->CompanyLinkWithoutHTTP}}">
                            @if($company->company_link){{$company->CompanyLinkWithoutHTTP}}@else Ссылки нет @endif
                        </div>
                        <div>@if($company->company_phone){{$company->company_phone}}@else Номера нет @endif</div>
                        <div>mail@mail.com</div>
                    </div>
                    <div class="rating-card__col flex-basis-230">
                        <div>Средняя оценка</div>
                        @include('rating.user.includes.stars', ['company_star' => $company, 'type' => 'no-margin'])
                        <div>Количество отзывов: {{$company->company_review_quantity}}</div>
                        <div class="text-dark-link"
                             data-type="link"
                             data-direction="reviews">Узнать об отзывах</div>
                    </div>
                    <div class="rating-card__col">
                        <div>Надёжность</div>
                        @include('rating.user.includes.reliability-graphics.line', ['company_rely' => $company])
                        <div class="text-dark-link"
                             data-type="link"
                             data-direction="rely">Узнать о надежности</div>
                    </div>
                </div>
                <div class="rating-card_hover-appear">
                    <div class="rating-card__col rating-card__additional-info mt-5">
                        <h4 class="text-bold text-color-gray-dark">Надежность:</h4>
                        <ul class="flex flex-jc-between mt-3 reset-list">
                            <li class="flex-grow-1">
                                @foreach($criterion as $criteria)
                                    @if($criteria->criteria_name === $company->company_criteria_1)
                                        @include($criteria->criteria_layout, ['criteria' => $criteria, 'tip' => 0])
                                        @break
                                    @endif
                                @endforeach
                            </li>
                            <li class="flex-grow-1">
                                @foreach($criterion as $criteria)
                                    @if($criteria->criteria_name === $company->company_criteria_2)
                                        @include($criteria->criteria_layout, ['criteria' => $criteria, 'tip' => 0])
                                        @break
                                    @endif
                                @endforeach
                            </li>
                            <li class="flex-grow-1">
                                @foreach($criterion as $criteria)
                                    @if($criteria->criteria_name === $company->company_criteria_3)
                                        @include($criteria->criteria_layout, ['criteria' => $criteria, 'tip' => 0])
                                        @break
                                    @endif
                                @endforeach
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="rating-card_hover-appear">
                    <div class="rating-card__col rating-card__additional-info mt-3">
                    <h4 class="text-bold text-color-gray-dark">Отзывы:</h4>
                    <ul class="flex flex-jc-between mt-3 reset-list">
                        @foreach($company->lastSomeReviews() as $i => $review)
                            <li class="rating-card__line @if($i === 0) mr-4 @elseif($company->lastSomeReviews()->count() === $i + 1) ml-4 @endif">
                                <div class="flex-basis-70">
                                    <div class="rating-card__user-icon" data-random="{{random_int(1, 9)}}">
                                        {{$review->ReviewerInitials}}
                                    </div>
                                </div>
                                <div class="rating-card__col flex-basis-150 flex-grow-1">
                                    <div class="text-color-gray-dark text-lh-15">{{$review->reviewer_name}}</div>
                                    @include('rating.user.includes.stars.stars_review', ['mark' => $review->review_mark, 'type' => 'no-margin'])
                                    <div class="text-size-35 mt-1">{{$review->SlicedReviewText}}</div>
{{--                                    <div class="text-dark-link text-size-3"--}}
{{--                                         data-type="link"--}}
{{--                                         data-direction="reviews-{{$i}}">Прочитать полный отзыв</div>--}}
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
                </div>

                <div class="rating-card__line mt-6 flex-jc-center">
                    @include('rating.user.includes.dropdown',
                    [
                        'company_dropdown' => $company,
                        'dropdown_index' => $company->company_id,
                        'dropdown__class' => 'btn_sharp-angle btn_little-padding text-lh-15',
                        'dropdown_overflow_class' => 'dropdown__wrapper-overflow_sharp-angle'])
                </div>
            </a>
        </li>
    @endforeach
</ul>
