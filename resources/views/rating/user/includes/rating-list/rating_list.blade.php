<ul class="rating-list">
    @foreach($companies as $index => $company)
    <li class="rating-list__li">
        <a href="{{ route('user.company', $company->company_slug) }}" class="rating-list__item
            @if($list_type === 'rely') rating-list__item_rely @endif"
             data-link="{{$company->company_slug}}"
             style="z-index: {{100-$index}}; position: relative">

            <div class="rating-list__item-index">
                {{$index+1}}
            </div>

            @if($list_type === 'star')
                <span class="rating-list__item-img-wrapper">
                    <img src="logos/{{$company->company_logo_path}}"
                         alt="Логотип компании {{$company->company_name}}"
                         title="Логотип компании {{$company->company_name}}"
                         class="rating-list__item-img"
                         width="50" height="70"
                         loading="lazy">
                </span>
                <div class="rating-list__item-data">
                    <h3 class="rating-list__item-data-value">
                        {{$company->company_name}}
                    </h3>
                    <div class="rating-list__item-data-value rating-list__item-data-value_link"
                         data-content="{{$company->CompanyLinkWithoutHTTP}}">
                        @if($company->company_link){{$company->CompanyLinkWithoutHTTP}}@else Ссылки нет @endif
                    </div>
                    <div class="rating-list__item-data-value">
                        @if($company->company_phone){{$company->company_phone}}@else Номера нет @endif
                    </div>
                </div>
                <div class="rating-list__item-rate">
                    <div class="rating-list__item-rate-value">Средняя оценка</div>
                    @include('rating.user.includes.stars', ['company_star' => $company])
                    <div class="rating-list__item-rate-value">Количество отзывов: {{$company->company_review_quantity}}</div>
                </div>


            @elseif($list_type === 'rely')
                <h3 class="rating-list__item-title">{{$company->company_name}}</h3>
                @include('rating.user.includes._rely-rating-data')

                <div class="rating-list__item-arc">
                    @include('rating.user.includes.reliability-graphics.arc', ['company_rely' => $company])
                </div>
            @endif
            @include('rating.user.includes.dropdown',
                ['company_dropdown' => $company,
                'dropdown_index' => $company->company_id,
                'extraClass' => 'rating-list__item-btn'])
        </a>
    </li>
    @endforeach
</ul>
