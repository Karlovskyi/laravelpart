<div class="paginator">
    @if($paginate['current'] - 1 > 0)
    <a href="{{route('user.company', $paginate['slug'])}}" class="paginator__arrow paginator__arrow_double">
        <span></span>
        <span></span>
    </a>
    <a href="{{route('user.company', ['company' => $paginate['slug'], 'page' => $paginate['current'] - 1])}}#reviews"
       class="paginator__arrow">
    </a>
    @endif
    @if($paginate['current'] - 4 > 0)
        @if($paginate['current'] - 4 === 1)
            <a href="{{route('user.company', ['company' => $paginate['slug'], 'page' => $paginate['current'] - 4])}}#reviews" class="paginator__item">
                {{$paginate['current'] - 4}}
            </a>
        @else
            <div class="paginator__item paginator__item_dots">...</div>
        @endif
    @endif
    @if($paginate['current'] - 3 > 0)
        <a href="{{route('user.company', ['company' => $paginate['slug'], 'page' => $paginate['current'] - 3])}}#reviews" class="paginator__item">
            {{$paginate['current'] - 3}}
        </a>
    @endif
    @if($paginate['current'] - 2 > 0)
        <a href="{{route('user.company', ['company' => $paginate['slug'], 'page' => $paginate['current'] - 2])}}#reviews" class="paginator__item">
            {{$paginate['current'] - 2}}
        </a>
    @endif
    @if($paginate['current'] - 1 > 0)
        <a href="{{route('user.company', ['company' => $paginate['slug'], 'page' => $paginate['current'] - 1])}}#reviews" class="paginator__item">
            {{$paginate['current'] - 1}}
        </a>
    @endif
    <span class="paginator__item paginator__current">
        {{$paginate['current']}}
    </span>
    @if($paginate['current'] + 1 <= $paginate['max'])
        <a href="{{route('user.company', ['company' => $paginate['slug'], 'page' => $paginate['current'] + 1])}}#reviews" class="paginator__item">
            {{$paginate['current'] + 1}}
        </a>
    @endif
    @if($paginate['current'] + 2 <= $paginate['max'])
        <a href="{{route('user.company', ['company' => $paginate['slug'], 'page' => $paginate['current'] + 2])}}#reviews" class="paginator__item">
            {{$paginate['current'] + 2}}
        </a>
    @endif
    @if($paginate['current'] + 3 <= $paginate['max'])
        <a href="{{route('user.company', ['company' => $paginate['slug'], 'page' => $paginate['current'] + 3])}}#reviews" class="paginator__item">
            {{$paginate['current'] + 3}}
        </a>
    @endif
    @if($paginate['current'] + 4 <= $paginate['max'])
        @if($paginate['current'] + 4 === $paginate['max'])
            <a href="{{route('user.company', ['company' => $paginate['slug'], 'page' => $paginate['current'] + 4])}}#reviews" class="paginator__item">
                {{$paginate['current'] + 4}}
            </a>
        @else
            <div class="paginator__item paginator__item_dots">...</div>
        @endif
    @endif
    @if($paginate['current'] + 1 <= $paginate['max'])
    <a href="{{ route('user.company',['company' => $paginate['slug'], 'page' => min($paginate['current'] + 1, $paginate['max'])])}}#reviews"
       class="paginator__arrow paginator__arrow_right">
    </a>
    <a href="{{ route('user.company',['company' => $paginate['slug'], 'page' => $paginate['max']]) }}#reviews"
       class="paginator__arrow  paginator__arrow_right paginator__arrow_double">
        <span></span>
        <span></span>
    </a>
    @endif
</div>
