<div class="company-data__rating">
    <h2>{{$rating}}</h2>
    <div class="company-data__rating-wrapper">
        <div class="company-data__different-rate">
            <div class="company-data__res-line">
                <div class="company-data__res-wrapper">
                    <img src="{{ asset('/images/yandex-logo.png') }}"
                         alt="Логотип компании Яндекс"
                         width="61" height="24"
                         title="Логотип компании Яндекс">
                </div>
                <div class="company-data__average-data">{{$company->company_mark_average_yandex}}</div>
                <div class="company-data__review-count">({{$company->company_review_quantity_yandex}} отзывов)</div>
            </div>
            <div class="company-data__res-line">
                <div class="company-data__res-wrapper">
                    <img src="{{ asset('/images/google-logo.png') }}"
                         alt="Логотип компании Google"
                         width="73" height="24"
                         title="Логотип компании Google">
                </div>
                <div class="company-data__average-data">{{$company->company_mark_average_google}}</div>
                <div class="company-data__review-count">({{$company->company_review_quantity_google}} отзывов)</div>
            </div>
            <div class="company-data__res-line">
                <div class="company-data__res-wrapper">
                    <img src="{{ asset('/images/otzovick-logo.png') }}"
                         alt="Логотип компании Отзовик"
                         width="98" height="24"
                         title="Логотип компании Отзовик">
                </div>
                <div class="company-data__average-data">{{$company->company_mark_average_otzovick}}</div>
                <div class="company-data__review-count">({{$company->company_review_quantity_otzovick}} отзывов)</div>
            </div>
        </div>
        <div class="company-data__total-rate">
            <div>Средняя оценка</div>
            @include('rating.user.includes.stars', ['company_star' => $company])
            <div>Количество отзывов: {{$company->company_review_quantity}}</div>
        </div>
    </div>
</div>
