@php
    if(!isset($names))    {
    $names = ['', '', '','','','','','','','','',''];
}
@endphp


@include('rating.user.includes._alter-table-item', ['side' => 'Срок работы', 'main' => $names[0]])
@include('rating.user.includes._alter-table-item', ['side' => 'Описание',
'main' => 'Если компания работает больше 4 лет это значит,
что она вышла на некий уровень стабильности и прибыльности.
Большинство компаний закрывается не дожив до 3 лет.'])

@include('rating.user.includes._alter-table-item', ['side' => 'Сотрудников в штате', 'main' => $names[1]])
@include('rating.user.includes._alter-table-item', ['side' => 'Описание',
'main' => 'Чем больше сотрудников в штате, и крупнее компания, и тем больше к ней доверия'])

@include('rating.user.includes._alter-table-item', ['side' => 'Сумма долгов', 'main' => $names[2]])
@include('rating.user.includes._alter-table-item', ['side' => 'Описание',
'main' => 'Если долговые обязательства составляют большой процент от прибыли, то велик шанс закрытия или банкроства компании.'])

@include('rating.user.includes._alter-table-item', ['side' => 'Смена руководителя', 'main' => $names[3]])
@include('rating.user.includes._alter-table-item', ['side' => 'Описание',
'main' => 'Если недавно поменялся руководитель, то есть шанс, что предыдущий принимал неэффективные решения.
Также высок шанс финансовых манипуляций, например, объявление банкротства через подставное лицо.'])

@include('rating.user.includes._alter-table-item', ['side' => 'Выплата зарабатной платы', 'main' => $names[4]])
@include('rating.user.includes._alter-table-item', ['side' => 'Описание',
'main' => 'Если компания не платит налоги на зарплаты сотрудников, то есть шанс,
 что за это она будет привлечена к ответственности.'])

@include('rating.user.includes._alter-table-item', ['side' => 'Прибыльность', 'main' => $names[5]])

@include('rating.user.includes._alter-table-item', ['side' => 'Юридический адрес', 'main' => $names[6]])
@include('rating.user.includes._alter-table-item', ['side' => 'Описание',
'main' => 'Постоянство юр. адреса косвенно указывает на стабильность компании'])

@include('rating.user.includes._alter-table-item', ['side' => 'Коэффициент финансовой автономии', 'main' => $names[7]])
@include('rating.user.includes._alter-table-item', ['side' => 'Описание',
'main' => 'Процент средств оборота, обеспеченных компанией. Указывает на финансовую стабильность.'])

@include('rating.user.includes._alter-table-item', ['side' => 'Динамика доходов', 'main' => $names[8]])
@include('rating.user.includes._alter-table-item', ['side' => 'Описание',
'main' => 'Компания, доходы которой растут, с большей вероятностью в полной мере выполнит условия контракта.'])

@include('rating.user.includes._alter-table-item', ['side' => 'Исполнительные производства', 'main' => $names[9]])
@include('rating.user.includes._alter-table-item', ['side' => 'Описание', 'main' => 'Стоит изучить предмет текущих производств.'])

@include('rating.user.includes._alter-table-item', ['side' => 'Реестр массовых адресов', 'main' => $names[10]])
@include('rating.user.includes._alter-table-item', ['side' => 'Описание',
'main' => 'В реестр массовых адресов попадают компании,
которые зарегистрированы в одном офисе/компанте с другими компаниями.
Это может означать, что компания очень маленькая, либо это не её настоящий адрес.'])

@include('rating.user.includes._alter-table-item', ['side' => 'Поиск сотрудников', 'main' => $names[11]])
@include('rating.user.includes._alter-table-item', ['side' => 'Описание',
'main' => 'Если компания активно ищет сотрудников это означает,
 что она развивается, не находится на грани банкротства и не является посредником.'])
