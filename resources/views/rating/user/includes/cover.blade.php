<aside class="cover {{$extraStyle ?? ''}}">
    <span class="cover__item cover__item-1"></span>
    <span class="cover__item cover__item-2"></span>
</aside>
