<section class="how-calculate">
    <div class="how-calculate__wrapper">
        <div class="header_how-calculate">
            <h2>{{$howCalculateRely}}</h2>
        </div>

        <p class="how-calculate__paragraph ">
            Чтобы рассчитать надёжность компании,
            мы автоматически собираем из открытых источников юридическую информацию.
            Какую информацию мы берём и как оцениваем смотри в таблице ниже:
        </p>
        <table class="how-calculate__table">
            <thead class="how-calculate__table-head">
            <tr class="how-calculate__table-head-line">
                <th class="how-calculate__table-head-item">Категория</th>
                <th class="how-calculate__table-head-item">Хорошо
                    <span class="how-calculate__circle" data-content="2"></span>
                </th>
                <th class="how-calculate__table-head-item">Средне
                    <span class="how-calculate__circle" data-content="1"></span>
                </th>
                <th class="how-calculate__table-head-item">Плохо
                    <span class="how-calculate__circle" data-content="0"></span>
                </th>
            </tr>
            </thead>
            <tbody class="how-calculate__table-body">
            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Срок работы</td>
                <td class="how-calculate__table-body-item">Больше 4 лет</td>
                <td class="how-calculate__table-body-item">Больше 2 лет</td>
                <td class="how-calculate__table-body-item">Год и меньше</td>
            </tr>
            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Описание</td>
                <th colspan="3" class="how-calculate__table-body-item">
                    Если компания работает больше 4 лет это значит,
                    что она вышла на некий уровень стабильности и прибыльности.
                    Большинство компаний закрывается не дожив до 3 лет.
                </th>
            </tr>
            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Сотрудников в штате</td>
                <td class="how-calculate__table-body-item">15 и более</td>
                <td class="how-calculate__table-body-item">Менее 15</td>
                <td class="how-calculate__table-body-item">Менее 5</td>
            </tr>
            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Описание</td>
                <td class="how-calculate__table-body-item" colspan="3">
                    Чем больше сотрудников в штате, и крупнее компания, и тем больше к ней доверия
                </td>
            </tr>

            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Сумма долгов</td>
                <td class="how-calculate__table-body-item">Менее 10% от прибыли</td>
                <td class="how-calculate__table-body-item">Менее 20% от прибыли</td>
                <td class="how-calculate__table-body-item">Более 21% от прибыли</td>
            </tr>
            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Описание</td>
                <td class="how-calculate__table-body-item" colspan="3">
                    Если долговые обязательства составляют большой процент от прибыли,
                    то велик шанс закрытия или банкротства компании.
                </td>
            </tr>

            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Смена руководителя</td>
                <td class="how-calculate__table-body-item">Более 2 лет назад</td>
                <td class="how-calculate__table-body-item">Менее 2 лет назад</td>
                <td class="how-calculate__table-body-item">Год и меньше</td>
            </tr>
            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Описание</td>
                <td class="how-calculate__table-body-item" colspan="3">
                    Если недавно поменялся руководитель, то есть шанс, что предыдущий принимал неэффективные решения.
                    Также высок шанс финансовых манипуляций, например, объявление банкротства через подставное лицо.
                </td>
            </tr>
            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Выплата заработной платы</td>
                <td class="how-calculate__table-body-item">Есть</td>
                <td class="how-calculate__table-body-item">-</td>
                <td class="how-calculate__table-body-item">Нет</td>
            </tr>
            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Описание</td>
                <td class="how-calculate__table-body-item" colspan="3">
                    Если компания не платит налоги на зарплаты сотрудников,
                    то есть шанс, что за это она будет привлечена к ответственности.
                </td>
            </tr>

            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Прибыльность</td>
                <td class="how-calculate__table-body-item">Более 2 млн</td>
                <td class="how-calculate__table-body-item">Положительная</td>
                <td class="how-calculate__table-body-item">Отрицательная</td>
            </tr>

            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Юридический адрес</td>
                <td class="how-calculate__table-body-item">Не менялся более 2 лет</td>
                <td class="how-calculate__table-body-item">Менялся в течение последних двух лет</td>
                <td class="how-calculate__table-body-item">Менялся менее чем год назад</td>
            </tr>
            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Описание</td>
                <td class="how-calculate__table-body-item" colspan="3">
                    Постоянство юр. адреса косвенно указывает на стабильность компании
                </td>
            </tr>

            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Коэффициент финансовой автономии</td>
                <td class="how-calculate__table-body-item">Более 50%</td>
                <td class="how-calculate__table-body-item">5-50%</td>
                <td class="how-calculate__table-body-item">Менее 5%</td>
            </tr>
            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Описание</td>
                <td class="how-calculate__table-body-item" colspan="3">
                    Процент средств оборота, обеспеченных компанией. Указывает на финансовую стабильность.
                </td>
            </tr>

            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Динамика доходов</td>
                <td class="how-calculate__table-body-item">Положительная</td>
                <td class="how-calculate__table-body-item">-</td>
                <td class="how-calculate__table-body-item">Отрицательная</td>
            </tr>
            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Описание</td>
                <td class="how-calculate__table-body-item" colspan="3">
                    Компания, доходы которой растут, с большей вероятностью в полной мере выполнит условия контракта.
                </td>
            </tr>

            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Исполнительные производства</td>
                <td class="how-calculate__table-body-item">Нет</td>
                <td class="how-calculate__table-body-item">Есть</td>
                <td class="how-calculate__table-body-item">Есть завершённые</td>
            </tr>
            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Описание</td>
                <td class="how-calculate__table-body-item" colspan="3">
                    Стоит изучить предмет текущих производств.
                </td>
            </tr>

            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Реестр массовых адресов</td>
                <td class="how-calculate__table-body-item">Не состоит</td>
                <td class="how-calculate__table-body-item">-</td>
                <td class="how-calculate__table-body-item">Состоит</td>
            </tr>
            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Описание</td>
                <td class="how-calculate__table-body-item" colspan="3">
                    В реестр массовых адресов попадают компании,
                    которые зарегистрированы в одном офисе/комнате
                    с другими компаниями. Это может означать, что компания
                    очень маленькая, либо это не её настоящий адрес.
                </td>
            </tr>

            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Поиск сотрудников</td>
                <td class="how-calculate__table-body-item">Есть аккаунты в HH и других подобных площадках. </td>
                <td class="how-calculate__table-body-item">Есть информация о наборе сотрудников только на сайте. </td>
                <td class="how-calculate__table-body-item">Не ищет сотрудников. </td>
            </tr>
            <tr class="how-calculate__table-body-line">
                <td class="how-calculate__table-body-item">Описание</td>
                <td class="how-calculate__table-body-item" colspan="3">
                    Если компания активно ищет сотрудников это означает, что она развивается,
                    не находится на грани банкротства и не является посредником.
                </td>
            </tr>

            </tbody>
        </table>

        @include('rating.user.includes._alter-table')
    </div>
</section>
