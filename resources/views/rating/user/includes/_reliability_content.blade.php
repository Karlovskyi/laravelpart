<ul class="reliability__content reliability__content_with-state "
    data-content="{{$company->TotalReliabilityStyleStatus}}">

    <li class="reliability__group">
        <span class="reliability__label">{{$criterion['company_amount_of_credit']->criteria_name_ru}}</span>
        <span class="reliability__value">
            {!! $criterion['company_amount_of_credit']->replaceData('criteria_value_1', $company->company_amount_of_credit_clear) !!}
        </span>

        @if($criterion['company_amount_of_credit']->criteria_description)
        <div class="tip tip_disable tip_company">
            <a class="tip__question" aria-label="Развернуть подсказку">
            </a>
            <p class="tip__content">
                {{$criterion['company_amount_of_credit']->criteria_description}}
            </p>
        </div>
        @endif
        <i class="circle circle_list" data-content="{{$company->getStatusField('company_amount_of_credit')}}"></i>

    </li>

    <li class="reliability__group">
        <span class="reliability__label">{{$criterion['company_profitability']->criteria_name_ru}}</span>
        <span class="reliability__value">
            {{$criterion['company_profitability']->replaceData('criteria_value_1', $company->company_profitability_clear)}}
        </span>
        @if($criterion['company_profitability']->criteria_description)
            <div class="tip tip_disable tip_company">
                <a class="tip__question" aria-label="Развернуть подсказку">
                </a>
                <p class="tip__content">
                    {{$criterion['company_profitability']->criteria_description}}
                </p>
            </div>
        @endif
        <i class="circle circle_list" data-content="{{$company->getStatusField('company_profitability')}}"></i>
    </li>

    <li class="reliability__group">
        <span class="reliability__label">{{$criterion['company_financial_autonomy']->criteria_name_ru}}</span>
        <span class="reliability__value">
            {{$criterion['company_financial_autonomy']->replaceData('criteria_value_1', $company->company_financial_autonomy_clear)}}
        </span>
        @if($criterion['company_financial_autonomy']->criteria_description)
        <div class="tip tip_disable tip_company">
            <a class="tip__question" aria-label="Развернуть подсказку">
            </a>
            <p class="tip__content">
                {{$criterion['company_financial_autonomy']->criteria_description}}
            </p>
        </div>
        @endif
        <i class="circle circle_list" data-content="{{$company->getStatusField('company_financial_autonomy')}}"></i>
    </li>

    <li class="reliability__group">
        <span class="reliability__label">{{$criterion['company_staff_members']->criteria_name_ru}}</span>
        <span class="reliability__value">
            {{$criterion['company_staff_members']->replaceData('criteria_value_1', $company->company_staff_members_clear)}}
        </span>
        @if($criterion['company_staff_members']->criteria_description)
            <div class="tip tip_disable tip_company">
                <a class="tip__question" aria-label="Развернуть подсказку">
                </a>
                <p class="tip__content">
                    {{$criterion['company_staff_members']->criteria_description}}
                </p>
            </div>
        @endif
        <i class="circle circle_list" data-content="{{$company->getStatusField('company_staff_members')}}"></i>
    </li>

    <li class="reliability__group">
        <span class="reliability__label">{{$criterion['company_term_of_work']->criteria_name_ru}}</span>
        <span class="reliability__value">
          {{$criterion['company_term_of_work']->replaceData('criteria_value_1', $company->company_term_of_work_clear)}}
        </span>
        @if($criterion['company_term_of_work']->criteria_description)
        <div class="tip tip_disable tip_company">
            <a class="tip__question" aria-label="Развернуть подсказку">
            </a>
            <p class="tip__content">
                {{$criterion['company_term_of_work']->criteria_description}}
            </p>
        </div>
        @endif
        <i class="circle circle_list" data-content="{{$company->getStatusField('company_term_of_work')}}"></i>
    </li>


</ul>

<ul class="reliability__content" id="resizeable">
    <li class="reliability__group">
        <span class="reliability__label">{{$criterion['company_change_of_leader']->criteria_name_ru}}</span>
        <span class="reliability__value">
            @if($company->company_change_of_leader_clear === 100)
                Не менялся
            @elseif($company->company_change_of_leader === 2)
                {{$criterion['company_change_of_leader']->replaceData('criteria_value_1', $company->company_change_of_leader_clear)}}
            @elseif($company->company_change_of_leader === 1)
                {{$criterion['company_change_of_leader']->replaceData('criteria_value_2', $company->company_change_of_leader_clear)}}
            @else
                {{$criterion['company_change_of_leader']->replaceData('criteria_value_3', $company->company_change_of_leader_clear)}}
            @endif
        </span>
        @if($criterion['company_change_of_leader']->criteria_description)
        <div class="tip tip_disable tip_company">
            <a class="tip__question" aria-label="Развернуть подсказку">
            </a>
            <p class="tip__content">
                {{$criterion['company_change_of_leader']->criteria_description}}
            </p>
        </div>
        @endif
        <i class="circle circle_list" data-content="{{$company->getStatusField('company_change_of_leader')}}"></i>
    </li>

    <li class="reliability__group">
        <span class="reliability__label">{{$criterion['company_legal_address']->criteria_name_ru}}</span>
        <span class="reliability__value">
            @if($company->company_legal_address)
                {{$criterion['company_legal_address']->replaceData('criteria_value_1', $company->company_legal_address)}}
            @else
                {{$criterion['company_legal_address']->replaceData('criteria_value_2', $company->company_legal_address)}}
            @endif
        </span>
        @if($criterion['company_legal_address']->criteria_description)
        <div class="tip tip_disable tip_company">
            <a class="tip__question" aria-label="Развернуть подсказку">
            </a>
            <p class="tip__content">
                {{$criterion['company_legal_address']->criteria_description}}
            </p>
        </div>
        @endif
        <i class="circle circle_list" data-content="{{$company->getStatusField('company_legal_address')}}"></i>
    </li>

    <li class="reliability__group">
        <span class="reliability__label">{{$criterion['company_checks']->criteria_name_ru}}</span>
        <span class="reliability__value">
           @if($company->company_checks === 0)
                {{$criterion['company_checks']->replaceData('criteria_value_1', $company->company_checks)}}
            @else
                {{$criterion['company_checks']->replaceData('criteria_value_2', $company->company_checks)}}
            @endif
        </span>

        @if($criterion['company_checks']->criteria_description)
            <div class="tip tip_disable tip_company">
                <a class="tip__question" aria-label="Развернуть подсказку">
                </a>
                <p class="tip__content">
                    {{$criterion['company_checks']->criteria_description}}
                </p>
            </div>
        @endif
    </li>

    <li class="reliability__group">
        <span class="reliability__label">{{$criterion['company_enforcement_proceedings']->criteria_name_ru}}</span>
        <span class="reliability__value">
           @if($company->company_enforcement_proceedings)
                {{$criterion['company_enforcement_proceedings']->replaceData('criteria_value_1', $company->company_enforcement_proceedings)}}
           @else
                {{$criterion['company_enforcement_proceedings']->replaceData('criteria_value_2', $company->company_enforcement_proceedings)}}
           @endif
        </span>

        @if($criterion['company_enforcement_proceedings']->criteria_description)
        <div class="tip tip_disable tip_company">
            <a class="tip__question" aria-label="Развернуть подсказку">
            </a>
            <p class="tip__content">
                {{$criterion['company_enforcement_proceedings']->criteria_description}}.
            </p>
        </div>
        @endif
        <i class="circle circle_list" data-content="{{$company->getStatusField('company_enforcement_proceedings')}}"></i>
    </li>

    <li class="reliability__group">
        <span class="reliability__label">{{$criterion['company_search_for_colleagues']->criteria_name_ru}}</span>
        <span class="reliability__value">
            @if($company->company_search_for_colleagues === 2)
                {{$criterion['company_search_for_colleagues']->replaceData('criteria_value_1', $company->company_search_for_colleagues)}}
            @elseif($company->company_search_for_colleagues === 1)
                {{$criterion['company_search_for_colleagues']->replaceData('criteria_value_2', $company->company_search_for_colleagues)}}
            @else
                {{$criterion['company_search_for_colleagues']->replaceData('criteria_value_3', $company->company_search_for_colleagues)}}
            @endif
        </span>

        @if($criterion['company_search_for_colleagues']->criteria_description)
        <div class="tip tip_disable tip_company">
            <a class="tip__question" aria-label="Развернуть подсказку">
            </a>
            <p class="tip__content">
                {{$criterion['company_search_for_colleagues']->criteria_description}}
            </p>
        </div>
        @endif
        <i class="circle circle_list" data-content="{{$company->getStatusField('company_search_for_colleagues')}}"></i>
    </li>

    <li class="reliability__group">
        <span class="reliability__label">{{$criterion['company_payment_of_wages']->criteria_name_ru}}</span>
        <span class="reliability__value">
            @if($company->company_payment_of_wages)
                {{$criterion['company_payment_of_wages']->replaceData('criteria_value_1', $company->company_payment_of_wages)}}
            @else
                {{$criterion['company_payment_of_wages']->replaceData('criteria_value_2', $company->company_payment_of_wages)}}
            @endif
        </span>
        @if($criterion['company_payment_of_wages']->criteria_description)
        <div class="tip tip_disable tip_company">
            <a class="tip__question" aria-label="Развернуть подсказку">
            </a>
            <p class="tip__content">
                {{$criterion['company_payment_of_wages']->criteria_description}}
            </p>
        </div>
        @endif

        <i class="circle circle_list" data-content="{{$company->getStatusField('company_payment_of_wages') * 2}}"></i>
    </li>

    <li class="reliability__group">
        <span class="reliability__label">{{$criterion['company_income_dynamics']->criteria_name_ru}}</span>
        <span class="reliability__value">
            @if($company->company_income_dynamics)
                {{$criterion['company_income_dynamics']->replaceData('criteria_value_1', $company->company_income_dynamics)}}
            @else
                {{$criterion['company_income_dynamics']->replaceData('criteria_value_2', $company->company_income_dynamics)}}
            @endif
        </span>

        @if($criterion['company_income_dynamics']->criteria_description)
            <div class="tip tip_disable tip_company">
                <a class="tip__question" aria-label="Развернуть подсказку">
                </a>
                <p class="tip__content">
                    {{$criterion['company_income_dynamics']->criteria_description}}
                </p>
            </div>
        @endif

        <i class="circle circle_list" data-content="{{$company->getStatusField('company_income_dynamics') * 2}}"></i>
    </li>
    <li class="reliability__group">
        <span class="reliability__label">{{$criterion['company_bulk_address_register']->criteria_name_ru}}</span>
        <span class="reliability__value">
            @if($company->company_bulk_address_register)
                {{$criterion['company_bulk_address_register']->replaceData('criteria_value_1', $company->company_bulk_address_register)}}
            @else
                {{$criterion['company_bulk_address_register']->replaceData('criteria_value_2', $company->company_bulk_address_register)}}
            @endif
        </span>
        @if($criterion['company_bulk_address_register']->criteria_description)
        <div class="tip tip_disable tip_company">
            <a class="tip__question" aria-label="Развернуть подсказку">
            </a>
            <p class="tip__content">
                {{$criterion['company_bulk_address_register']->criteria_description}}
            </p>
        </div>
        @endif
        <i class="circle circle_list" data-content="{{$company->getStatusField('company_bulk_address_register') *2}}"></i>
    </li>

    <li class="reliability__group">
        <span class="reliability__label">{{$criterion['company_relation']->criteria_name_ru}}</span>
        <span class="reliability__value">
            @php
                $cr1 = $criterion['company_relation']->replaceData('criteria_value_1', $company->company_relation_clear);
                $cr2 = $criterion['company_relation']->replaceData('criteria_value_2', $company->company_relation_clear);
            @endphp
            @if($company->company_relation)
                {{$cr1}}
            @else
                {{$cr2}}
            @endif
        </span>
        @if($criterion['company_relation']->criteria_description && $company->company_relation)
            <div class="tip tip_disable tip_company">
                <a class="tip__question" aria-label="Развернуть подсказку">
                </a>
                <p class="tip__content">
                    {{$criterion['company_relation']->criteria_description}}
                </p>
            </div>
        @endif
        <i class="circle circle_list" data-content="{{$company->getStatusField('company_relation') + 1}}"></i>
    </li>
    @if($company->company_pay_attention)
        <li class="reliability__group">
            <span class="reliability__label">Обратите внимание:</span>
            <span class="reliability__value">{{$company->company_pay_attention}}</span>
            @if($company->AttantionTypeToNumberStatus !== null)
                <i class="circle circle_list" data-content="{{$company->AttantionTypeToNumberStatus}}"></i>
            @endif
        </li>
    @endif
</ul>
