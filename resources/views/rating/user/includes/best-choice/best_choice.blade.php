{{--
    Чтобы использовать данный компонент,
    передайте параметр $best
--}}
@php $aside_best_choice_href = route('user.company', $best->company_slug); @endphp
<aside class="best-choice {{$extraStyle ?? ''}}">
    @if(!is_current_path($aside_best_choice_href))
        <a class="best-choice__img-wrapper" href="{{ $aside_best_choice_href }}">
            @include('rating.user.includes.best-choice.best-choice-content')
        </a>
    @else
        <span class="best-choice__img-wrapper">
            @include('rating.user.includes.best-choice.best-choice-content')
        </span>
    @endif

    <div class="btn_best-choice">
        <a href="{{ route('user.how-to-choose') }}" class="btn btn_light btn__link btn__link_dark btn__link_best-choice">
            Как формируется рейтинг
        </a>
    </div>
</aside>
