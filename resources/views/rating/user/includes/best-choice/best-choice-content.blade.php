<img src="/logos/{{$best->company_logo_path}}"
     alt="Логотип компании {{$best->company_name}}"
     title="Логотип компании {{$best->company_name}}"
     class="best-choice__sub-img"
     height="100" width="100"
     loading="lazy" >
<div class="best-choice__wrapper">
    <div class="header ta-c header-small best-choice__header">
        Лучший выбор
    </div>
    <img src="/logos/{{$best->company_logo_path}}"
         alt="Логотип компании {{$best->company_name}}"
         title="Логотип компании {{$best->company_name}}"
         class="best-choice__img"
         height="160" width="160"
         loading="lazy">
    {{ $slot }}
    <div class="header ta-c header-small best-choice__header best-choice__name">
        {{$best->company_name}}
    </div>
    <div class="best-choice__text ta-c">
        На основании рейтинга и надёжности компаний
    </div>
</div>
