<section class="about-site">
    <h1>{{$header}}</h1>

    <p class="about-site__paragraph_little">
        Рейтинг ремонтных компаний включает себя отзывы из достоверных источников: Яндекс, Гугл и Отзовик
        <span class="tip tip_disable">
            <span class="about-site__link tip__question tip__question_span">(почему они?)</span>.
            <small class="tip__content tip__content_big">
                Мы выбрали эти площадки для сравнения ремонтных компаний, потому что Яндекс,
                Гугл и Отзовик лучше всех сайтов отзывов могут проверять подлинность отзыва.
                Например, у Яндекса есть возможность определить как себя ведёт пользователь:
                в каких местах он бывал, какие ещё отзывы оставляет и т.п.
            </small>
        </span>
        Чтобы пользователь мог оценить компанию максимально объективно в одном месте.
    </p>
    <p class="about-site__paragraph">
        У каждого отзыва есть ссылка на источник, чтобы вы могли проверить его сами.
        Также Вы можете сравнить ремонтные компании между собой по отзывам и надёжности
        <span class="tip tip_disable tip__question_span">
            <span class="tip__question tip__question_inline"></span>
            <small class="tip__content tip__content_info">
                Показатель надёжности формируется исходя из общедоступных проверенных данных.
                Мы собрали факторы, по которым можно отличить надёжную компанию от сомнительной.
                Оценка составляется при суммировании всех факторов.
                Вы можете не соглашаться с нашим видением и проверить всё самостоятельно.
                Ссылки и данные для этого указаны на странице компаний.
            </small>
        </span>
    </p>
    <form action="{{route('user.compare')}}" method="POST" name="compare">
        @csrf
        <button class="btn btn_dark btn_bold btn__about-site" type="submit">
            <span class="btn__link btn__link_about-site" id="form-link">
                Сравнить компании
            </span>
        </button>
    </form>
</section>
