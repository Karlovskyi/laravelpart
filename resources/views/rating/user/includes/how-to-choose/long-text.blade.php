<h2 class="{{$extraClass ?? ''}}">{{$howToChoose}}</h2>
<p class="info__paragraph info__paragraph_offset">
    Ремонт квартиры – дорого, долго и часто с нервами.
    Поэтому выделите время и осознанно изучите
    любую имеющуюся в интернете информацию.
    В этом вам поможет наш сайт и эта статья.
</p>
<div class="header_info header-sub header ta-c">
    <h3>I. Читайте только проверенные отзывы</h3>
</div>

<p class="info__paragraph info__paragraph_offset">
    В интернете очень распространена публикация купленных отзывов,
    что очень сильно затрудняет выбор достойной ремонтной компании.
</p>
<p class="info__paragraph info__paragraph_offset">
    Однако есть сервисы, которые могут бороться с купленными отзывами
    и которым можно доверять - Яндекс, Гугл и Отзовик. Мы решили собрать
    их в одном месте, чтобы вы могли без лишних телодвижений сравнивать
    разные компании. Также мы удаляем одинаковые отзывы с разных площадок
    и слишком подозрительные отзывы.
</p>
<picture>
    <source media="(max-width: 576px)"
            srcset="{{ asset('/images/how-to-choose/verified-review-sm.jpg') }}">
    <source media="(max-width: 992px)"
             srcset="{{ asset('/images/how-to-choose/verified-review-md.jpg') }}">
    <source media="(max-width: 1200px)"
            srcset="{{ asset('/images/how-to-choose/verified-review-lg.jpg') }}">
    <img src="{{ asset('/images/how-to-choose/verified-review-xl.jpg') }}"
         alt="Отзывы о ремонтной компании на Яндекс.Картах"
         title="Отзывы о ремонтной компании на Яндекс.Картах"
         class="info__img"
         width="741" height="376"
         loading="lazy">
</picture>
<div class="header header-sub ta-c header_info">
    <h3 >II. Научитесь определять купленные отзывы самостоятельно</h3>
</div>

<p class="info__paragraph info__paragraph_offset">
    Да, Яндекс и Гугл хорошо определяются большую часть купленных отзывов.
    Но даже они ошибается и пропускают написанные хорошим копирайтером отзывы.
    Чтобы ещё больше снизить вероятность поверить купленному отзыву - научитесь самостоятельно определять фейки.
</p>
<p class="info__paragraph info__paragraph_offset">
    Что может выдать купленные отзывы:
</p>
<p class="info__paragraph info__paragraph_offset-little">
    1.  <span class="info__blue">Несколько похожих отзывов рядом.</span> Это происходит из-за одного и того же ТЗ на написание отзыва.
    Особое внимание обратите на дату публикации – аномально много отзывов за один день признак накрутки.
</p>
<p class="info__paragraph info__paragraph_offset">
    2.	<span class="info__blue">Ошибки.</span> Копирайтер, который пишет отзыв, скорее всего не пользовался услугами ремонтной компании.
    Например, он может написать, что ремонт всей квартиры выполнял только один мастер и за короткий срок.
    Очевидно, что сроки, в таком случае будет сильно затянуты, а качество будет не на пять звёзд.
</p>
<p class="info__paragraph info__paragraph_offset">
    3.	<span class="info__blue">Отсутствие подробностей.</span> Купленный отзыв будет содержать общие формулировки,
    которые подходят ко всем товарам: данная модель, эта компания, низкие цены и т.п.
</p>
<p class="info__paragraph info__paragraph_offset">
    <span class="info__blue">Купленный. </span>Очень довольна работой данной компании.
    Работа вся в срок, ответственный прораб, который помог подобрать материалы.
    Соотношение цена/качество соответствует. Очень быстро, слаженно работают мастера,
    смотреть за их работой одно удовольствие. Рекомендую данную фирму к сотрудничеству.
</p>
<p class="info__paragraph info__paragraph_offset">
    <span class="info__blue">Скорее всего, честный.</span> Договор № КС1-24690.
    Делали ремонт двухкомнатной старой квартиры в жутком состоянии. Результатом довольны).
    Получилось все так, как и хотели. Было продление сроков - но у нас увеличился объем работ,
    плюс наше родное ЖКХ чинило нам препятствия. Радует наличие скидки,
    и то, что получаешь обратную связь стабильно.
    Был момент, что некоторые вещи фирма правила, но в итоге, все получилось хорошо.
</p>
<p class="info__paragraph info__paragraph_offset">
    4.	<span class="info__blue">Неправильный факты.</span> Когда копирайтер хочет добавить подробностей,
    то он скорее придумает их из головы, чем посмотрит на сайте.
    Так быстрее и проще. Поэтому не стесняйтесь проверять факты.
    Например, копирайтер говорит, что воспользовался рассрочкой на ремонт,
    при том, что компания её не предоставляет.
</p>
<p class="info__paragraph info__paragraph_offset">
    5.	<span class="info__blue">Слишком много деталей.</span> Все поля заполнены,
    очень много подробностей и незначительные недостатки.
    Честный покупатель скорее потратит силы, чтобы поругать продавца, чем чтобы похвалить.
    Если, конечно, пользователю за этот отзыв продавец не даст скидку или что-нибудь ещё.
</p>
<picture>
    <source media="(max-width: 576px)"
            srcset="{{ asset('/images/how-to-choose/detailed-review-sm.jpg') }}">
    <source media="(max-width: 992px)"
            srcset="{{ asset('/images/how-to-choose/detailed-review-md.jpg') }}">
    <source media="(max-width: 1200px)"
            srcset="{{ asset('/images/how-to-choose/detailed-review-lg.jpg') }}">
    <img src="{{ asset('/images/how-to-choose/detailed-review-xl.jpg') }}"
         alt="Отзыв о ремонтной компании со слишком большим количеством деталей"
         title="Отзыв о ремонтной компании со слишком большим количеством деталей"
         class="info__img"
         width="700" height="469"
         loading="lazy">
</picture>

<p class="info__paragraph info__paragraph_offset">
    6.	<span class="info__blue">Натянутый негатив.</span>
    Пользователь пишет негатив о компании, но тут же ему находится оправдание.
    Например, затянули со сроками ремонта, зато оплатили неустойку.
    Честный клиент вряд ли станет оправдывать недостатки услуги, за которую заплатил.
</p>
<picture>
    <source media="(max-width: 576px)"
            srcset="{{ asset('/images/how-to-choose/negative-review-sm.jpg') }}">
    <source media="(max-width: 992px)"
            srcset="{{ asset('/images/how-to-choose/negative-review-md.jpg') }}">
    <source media="(max-width: 1200px)"
            srcset="{{ asset('/images/how-to-choose/negative-review-lg.jpg') }}">
    <img src="{{ asset('/images/how-to-choose/negative-review-xl.jpg') }}"
         alt="Натянутый негатив в отзыве у ремонтной компании"
         title="Натянутый негатив в отзыве у ремонтной компании"
         class="info__img"
         width="728" height="443"
         loading="lazy">
</picture>
<p class="info__paragraph info__paragraph_offset">
    7.	<span class="info__blue">Фотографии из интернета.</span>
    Копирайтеру негде взять фотографии настоящего ремонта,
    особенно в больших количествах, поэтому он начинает использовать фотографии из интернета.
    Проверить это легко – воспользуйтесь поиском по картинкам Яндекса или Гугла.
</p>
<p class="info__paragraph info__paragraph_offset-little">
    Как проверить отзыв:
</p>
<p class="info__paragraph info__paragraph_offset">
    Ремонт – дорогое удовольствие, поэтому перед покупкой можно потратить время на дополнительную проверку отзывов:
</p>
<p class="info__paragraph info__paragraph_offset-little">
    1. <span class="info__blue">Свяжитесь с автором отзыва.</span> Если есть возможность связаться с пользователем напрямую – воспользуйтесь ей. Не стесняйтесь спросить контакты в комментариях к отзыву
</p>
<p class="info__paragraph info__paragraph_offset-little">
    2. <span class="info__blue">Обратите внимание на то, как компания</span> стимулирует клиентов оставлять отзывы. Если у компании есть акции, связанные с оставлением отзыва, то положительных отзывов будет больше. Возможно, это будет честный отзыв, а может слегка приукрашенный.
</p>
<h3 class="header header-sub ta-c header_info">
    III. Проверьте надёжность ремонтной компании
</h3>
<p class="info__paragraph info__paragraph_offset">
    Помимо отзывов есть множество доказательств добропорядочности компании. Например, все компании обязаны фиксировать прибыль, количество сотрудников, делопроизводства в суде и т.п.
</p>
<p class="info__paragraph info__paragraph_offset">
    Напрямую на качество ремонта это не влияет, но согласитесь, что желания заказывать дорогостоящий ремонт у компании, в которой зарегистрировано два сотрудника будет меньше, чем у такой же компании с 50+ сотрудниками.
    <span class="d-block info__blue">Как проверить надёжность:</span>
</p>
<p class="info__paragraph info__paragraph_offset-little">
    1. Найдите на сайте ИНН/ОГРН. Если у компании на сайте нет этих данных – вполне возможно, что компании есть, что скрывать.
</p>
<picture>
    <source media="(max-width: 576px)"
            srcset="{{ asset('/images/how-to-choose/low-info-about-company-sm.jpg') }}">
    <source media="(max-width: 992px)"
            srcset="{{ asset('/images/how-to-choose/low-info-about-company-md.jpg') }}">
    <source media="(max-width: 1200px)"
            srcset="{{ asset('/images/how-to-choose/low-info-about-company-lg.jpg') }}">
    <img src="{{ asset('/images/how-to-choose/low-info-about-company-xl.jpg') }}"
         alt="Юридическая информация с сайта ремонтной компании"
         title="Юридическая информация с сайта ремонтной компании"
         class="info__img"
         loading="lazy">
</picture>
<p class="info__paragraph info__paragraph_offset-little">
    2. Введите эти данные на сайте <a href="https://www.rusprofile.ru/" class="info__link">https://www.rusprofile.ru/</a>  и проанализируйте имеющуюся информацию:
</p>
<picture>
    <source media="(max-width: 576px)"
            srcset="{{ asset('/images/how-to-choose/data-about-company-on-rusprofile-sm.jpg') }}">
    <source media="(max-width: 992px)"
            srcset="{{ asset('/images/how-to-choose/data-about-company-on-rusprofile-md.jpg') }}">
    <source media="(max-width: 1200px)"
            srcset="{{ asset('/images/how-to-choose/data-about-company-on-rusprofile-lg.jpg') }}">
    <img src="{{ asset('/images/how-to-choose/data-about-company-on-rusprofile-xl.jpg') }}"
         alt="Данные о ремонтной компании на RusProfile"
         title="Данные о ремонтной компании на RusProfile"
         class="info__img"
         loading="lazy">
</picture>
<p class="info__paragraph info__paragraph_offset-little">
    •&nbsp;&nbsp;&nbsp;	дата создания;
</p>
<p class="info__paragraph info__paragraph_offset-little">
    •&nbsp;&nbsp;&nbsp;	количество сотрудников – не всегда правильно отражает истинную ситуацию,
    например, здесь могут не учитываться наёмные рабочие;
</p>
<p class="info__paragraph info__paragraph_offset-little">
    •&nbsp;&nbsp;&nbsp;	а также изучите раздел «Надёжность». Помимо средней оценки, советуем обратить внимание
    на пункт «Отрицательные факты». Возможно, увидев наличие судопроизводств на крупную сумму – дважды подумаете,
    прежде чем начинать сотрудничать с этой компанией.
</p>
<picture>
    <source media="(max-width: 576px)"
            srcset="{{ asset('/images/how-to-choose/company-reliability-on-rusprofile-sm.jpg') }}">
    <img src="{{ asset('/images/how-to-choose/company-reliability-on-rusprofile.jpg') }}"
         alt="Надёжность ремонтной компании на RusProfile"
         title="Надёжность ремонтной компании на RusProfile"
         class="info__img"
         loading="lazy">
</picture>
<p class="info__paragraph info__paragraph_offset-little">
    3. Сравните компании между собой.

    Прежде чем делать вывод о какой-либо компании – сравните её с остальными. Вполне возможно, что факты, которые rusprofile считает отрицательными – норма для строительной отрасли.

    Для вашего удобства, самые важные данные о компаниях находятся на нашем сайте. Компании можно добавить в сравнение, чтобы понять какая компания подходит больше.
</p>

<div class="header header-sub ta-c header_info">
    <h3>IV. Не доверяйте площадкам, которые говорят, что сами проверяют отзывы</h3>
</div>

<p class="info__paragraph info__paragraph_offset">
    Для проверки достоверности отзыва крупные сервисы используют множество факторов, в том числе IP,
    местоположение пользователя и компании, фотографии на дубли и т.п. Если это небольшой новый сайт (как наш),
    то ему просто невыгодно реализовывать такой функционал.
    В большинстве случаев, такие сайты созданы конкретной ремонтной компанией.
</p>
<p class="info__paragraph info__paragraph_offset">
    В свою очередь, мы берём отзывы сразу с проверенных источников.
    Автоматически проверяем на дубли и вручную удаляем ярко выраженные купленные отзывы.
    Например, если на Яндексе у одной компании на один негативный отзыв
    появилось пять положительных за один день, то такие отзывы
    автоматически отправляются на проверку модератору и при обнаружении нарушения – удаляются.
</p>
<p class="info__paragraph info__paragraph_offset">
    В остальном наш сервис рассчитывает на алгоритмы и модерацию крупных агрегаторов отзывов
</p>

<div class="header ta-c header_info">
    <h3>{{$remember}}</h3>
</div>

<p class="info__paragraph info__paragraph_offset">

    1. <span class="info__blue">Отличайте купленные отзывы от честных.</span> Учитесь их распознавать и пользуйтесь проверенными сервисами.
</p>
<p class="info__paragraph info__paragraph_offset">
    2. <span class="info__blue">Копайте глубже.</span> Не стесняйтесь спрашивать подробности о компании у тех, кто оставлял отзыв.
    Он знает какой это сложный выбор и с радостью поможет. А если не знает – значит его отзыву нельзя доверять.
</p>
<p class="info__paragraph info__paragraph_offset">
    3. <span class="info__blue">Используйте не только отзывы.</span> Не ленитесь заходить на rusprofile и похожие сайты,
    чтобы проверить дополнительную информацию о компании.
    Такую информацию компания не может подделать.
</p>
<p class="info__paragraph info__paragraph_offset">
    4. <span class="info__blue">Проверяйте наличие документов.</span>
    Если на сайте компании есть сканы документов – это хороший знак.

</p>
