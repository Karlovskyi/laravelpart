<ul class="rating-list__item-info">
    <li class="rating-list__item-label rating-list__item-label_first">Надёжность:</li>

    <li class="rating-list__item-characteristic">
        @foreach($criterion as $criteria)
            @if($criteria->criteria_name === $company->company_criteria_1)
                @include($criteria->criteria_layout, ['criteria' => $criteria])
                @break
            @endif
        @endforeach
    </li>
    <li class="rating-list__item-characteristic">
        @foreach($criterion as $criteria)
            @if($criteria->criteria_name === $company->company_criteria_2)
                @include($criteria->criteria_layout, ['criteria' => $criteria])
                @break
            @endif
        @endforeach
    </li>
    <li class="rating-list__item-characteristic">
        @foreach($criterion as $criteria)
            @if($criteria->criteria_name === $company->company_criteria_3)
                @include($criteria->criteria_layout, ['criteria' => $criteria])
                @break
            @endif
        @endforeach
    </li>
    <li class="rating-list__item-link">
        Больше
    </li>
</ul>
