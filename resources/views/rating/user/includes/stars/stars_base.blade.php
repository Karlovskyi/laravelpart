<div class="stars @if($type ?? '' === 'no-margin') m-0 @endif" @if($review ?? false)
    temprop="reviewRating" itemscope itemtype="https://schema.org/Rating" @endif>
    <div class="stars__gray" data-content="{{$star}}">
        <i class="stars__item stars__item_gray"></i>
        <i class="stars__item stars__item_gray"></i>
        <i class="stars__item stars__item_gray"></i>
        <i class="stars__item stars__item_gray"></i>
        <i class="stars__item stars__item_gray"></i>
        <span class="stars__yellow" data-content="{{$star}}">
            <i class="stars__item stars__item_yellow"></i>
            <i class="stars__item stars__item_yellow"></i>
            <i class="stars__item stars__item_yellow"></i>
            <i class="stars__item stars__item_yellow"></i>
            <i class="stars__item stars__item_yellow"></i>
        </span>
    </div>
    <div class="stars__mark-average" @if($review ?? false) itemprop="ratingValue" @endif>
        {{$star}}
    </div>
</div>
