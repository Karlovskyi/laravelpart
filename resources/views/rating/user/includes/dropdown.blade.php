{{--
    Чтобы использовать данный компонент,
    передайте параметры $company_dropdown и $dropdown_index
--}}

<div class="dropdown {{isset($dropdown__class) ? 'dropdown_company' : ''}} {{$extraClass ?? ''}}">
    <div class="dropdown__btn btn btn_light btn_compare {{$dropdown__class ?? ''}}"
            data-content="{{$company_dropdown->company_slug}}">
            Сравнить с...
    </div>
    <div class="dropdown__wrapper-overflow {{$dropdown_overflow_class ?? ''}}">
        <ul class="dropdown__main dropdown__main_disable" data-base="{{$company_dropdown->company_slug}}">
            @foreach($companies as $i => $company_for_list)
                @if($company_for_list->company_id !== $dropdown_index)
                    <li class="dropdown__compare-item" data-base="{{$company_for_list->company_slug}}">
                        <img src="/logos/{{$companies[$i]->company_logo_path}}"
                             alt="Логотип компании {{$companies[$i]->company_name}}"
                             title="Логотип компании {{$companies[$i]->company_name}}"
                             width="25" height="20"
                             class="dropdown__compare-img">
                        <div class="dropdown__compare-text">{{$companies[$i]->company_name}}</div>
                    </li>
                @endif
            @endforeach
        </ul>
        <i class="dropdown__close">
            <span>Скрыть</span>
        </i>
    </div>
</div>
