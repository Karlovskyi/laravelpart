<div class="portfolio__about">
    <div class="portfolio__about-title">Описание</div>
    <div class="portfolio__description">
        @if($company->company_description)
        {!! $company->company_description !!}
        @else
        <span class="text">Описание отсутствует</span>
        @endif
    </div>
    <div class="reliability__links reliability__links_description">
        <a id="more-2" class="reliability__link-more reliability__link-more_portfolio" aria-label="Свернуть блок">Скрыть</a>
        <a id="less-2" class="reliability__link-less reliability__link-less_portfolio" aria-label="Развернуть блок">Больше</a>
    </div>
</div>
