<section class="about">

    <div class="header_about-big header_left header_big">
        <h1>{{$about}}</h1>
    </div>
    <div class="header header_weight-medium header_left mt-10 mt-md-5">
        <h2>Что мы делаем?</h2>
    </div>

    <div class="header_about-little header_weight-medium header_left header_black mt-5 mt-md-3">
        <h3>Отзывы</h3>
    </div>

    <p class="about__paragraph mt-4 mt-md-2">
        Собираем отзывы с оценками из самых проверенных сайтов отзывов.
        Высшую позицию в рейтинге занимает тот,
        у кого разница положительных и отрицательных отзывов выше.
    </p>

    <p class="about__paragraph mt-4 mt-md-2">
        Мы вручную проверяем отзывы. Это не значит, что всем отзывам можно доверять,
        потому что хороший купленный отзыв сложно отличить от честного.
        Помимо этого, проверить большое количество отзывов,
        не совершив ошибок – невозможно.
        По возможности самостоятельно проверяйте отзывы.
    </p>
    <div class="header_about-little header_weight-medium header_left header_black mt-5">
        <h3>Юридическая информация о компании</h3>
    </div>

    <p class="about__paragraph mt-4 mt-md-2">
        Помимо отзывов, мы ищем любую проверенную информацию о компании: дата основания,
        количество сотрудников, наличие судебных разбирательств и т.п.
    </p>

    <p class="about__paragraph mt-4 mt-md-2">
        Мы не зарабатываем на рекламе конкретных компаний по ремонту.
        Единственный источник дохода сайта – реклама.
        Поэтому, чтобы поддержать проект и помочь ему развиваться отключите
        AdBlock и посоветуйте сайт друзьям, которые собираются делать ремонт в квартире.
    </p>

    <div class="header header_weight-medium header_left mt-7">
        <h2>Популярные вопросы</h2>
    </div>

    <div class="card card_no-overflow card_touchable mt-4">
        <h3 class="card__header header_with-arrow" data-content="q">
            Как мне поможет этот сайт?
            <i class="about__toggle-arrow">
            </i>
        </h3>

        <p class="about__paragraph mt-4 mt-md-2" data-content="a">
            Сайт создан для тех, кто хочет не прогадать с ремонтом.
            Мы предлагаем инструменты для сравнения разных ремонтных компаний.
            Главная идея – дать пользователю самому выбрать компанию, опираясь на данные, которые мы собрали.
        </p>
    </div>

    <div class="card card_no-overflow card_touchable mt-5">
        <h3 class="card__header header_with-arrow" data-content="q">
            Почему нельзя просто выбрать компанию из ТОП-3?
            <i class="about__toggle-arrow">
            </i>
        </h3>

        <p class="about__paragraph mt-4 mt-md-2" data-content="a">
            Топ основан на отзывах, которые не всегда отражают истинную ситуацию.
            Нужно помнить, что негативные отзывы оставляют чаще, чем положительные.
            Зачем писать, если и так всё хорошо.
            Также, многие компании поощряют публикацию отзывов с помощью скидки или подарков.
            В таком случае количество положительных отзывов увеличивается в разы.
            Ну и не нужно забывать про купленные отзывы.
        </p>
    </div>

    <div class="card card_no-overflow card_touchable mt-5">
        <h3 class="card__header header_with-arrow" data-content="q">
            Можно ли доверять отзывам на этом сайте?
            <i class="about__toggle-arrow">
            </i>
        </h3>

        <p class="about__paragraph mt-4 mt-md-2" data-content="a">
            Можно, но не всем и обязательно проверяйте.
        </p>
    </div>

    <div class="card card_no-overflow card_touchable mt-5">
        <h3 class="card__header header_with-arrow" data-content="q">
            Можно ли доверять юридической информации о компании?
            <i class="about__toggle-arrow">
            </i>
        </h3>

        <p class="about__paragraph mt-4 mt-md-2" data-content="a">
            Скорее да, чем нет. Эта информация берётся из официальных источников.
            Компания может умалчивать о своих проблемах, платить налоги только частично и т.п.
            Но эта самая проверенная информация из всей имеющейся в интернете о компании.
        </p>
    </div>

    <div class="card card_no-overflow card_touchable mt-5">
        <h3 class="card__header header_with-arrow" data-content="q">
            Откуда отзывы?
            <i class="about__toggle-arrow">
            </i>
        </h3>

        <p class="about__paragraph mt-4 mt-md-2" data-content="a">
            Из Яндекса, Гугла и Отзовика. Отфильтровали только максимально похожие на купленные отзывы и дубли.
        </p>
    </div>

    <div class="card card_no-overflow card_touchable mt-5 mb-5">
        <h3 class="card__header header_with-arrow" data-content="q">
            Можно ли добавить сюда свою компанию?
            <i class="about__toggle-arrow">
            </i>
        </h3>

        <p class="about__paragraph mt-4 mt-md-2" data-content="a">
            Нет. Компания автоматически попадает к нам в рейтинг,
            если набирается более 100 отзывов в сумме на Яндексе,
            в Гугле или Отзовике, не считая дублей.
        </p>
    </div>
</section>
