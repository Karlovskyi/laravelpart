<div class="company-data__contacts">
    <h2>{{$contacts}}</h2>
    <ul class="company-data__contacts-wrapper">
        @if($company->company_link)
            <li>
                <div class="company-data__label">Официальный сайт:</div>
                <div class="company-data__value">
                    <a href="{{$company->company_link}}" target="_blank">
                        {{$company->companyLinkWithoutHTTP}}
                    </a>
                </div>
            </li>
        @endif
        @if($company->company_phone)
            <li>
                <div class="company-data__label">Телефон:</div>
                <div class="company-data__value">
                    <a href="tel:{{$company->CompanyPhoneWithNoSpaces}}">{{$company->company_phone}}</a>
                </div>
            </li>
        @endif


        @if($company->company_address)
            <li>
                <div class="company-data__label">Адрес:</div>
                <div class="company-data__value">
                    {{$company->company_address}}
                </div>
            </li>
        @endif
        @if($company->company_fb || $company->company_inst || $company->company_you_tube)
            <li>
                <div class="company-data__label">Социальные сети:</div>
                <div class="company-data__value_social-networks">
                    @if($company->company_fb)
                        <a href="{{$company->company_fb}}" target="_blank"
                           class="company-data__social-logo"
                           data-content="fb">
                        </a>
                    @endif
                    @if($company->company_inst)
                        <a href="{{$company->company_inst}}" target="_blank"
                           class="company-data__social-logo"
                           data-content="inst">
                        </a>
                    @endif
                    @if($company->company_you_tube)
                        <a href="{{$company->company_you_tube}}" target="_blank"
                           class="company-data__social-logo"
                           data-content="yt">
                        </a>
                    @endif
                    @if($company->company_vk)
                        <a href="{{$company->company_vk}}" target="_blank"
                           class="company-data__social-logo"
                           data-content="vk">
                        </a>
                    @endif
                </div>
            </li>
        @endif
        @if($company->company_rusprofile_link)
            <li>
                <div class="company-data__label">Ссылка на Rusprofile:</div>
                <div class="company-data__value">
                    <a href="{{$company->company_rusprofile_link}}" target="_blank">
                        {{$company->companyRusprofileLinkWithoutHTTP}}
                    </a>
                </div>
            </li>
        @endif
        @if($company->company_ogrn)
            <li>
                <div class="company-data__label">ОГРН:</div>
                <div class="company-data__value">
                    {{$company->company_ogrn}}
                </div>
            </li>
        @endif
        @if($company->company_inn)
            <li>
                <div class="company-data__label">ИНН:</div>
                <div class="company-data__value">
                    {{$company->company_inn}}
                </div>
            </li>
        @endif
        @if($company->company_legal_address_value)
            <li>
                <div class="company-data__label">Юридический адрес:</div>
                <div class="company-data__value">
                    {{$company->company_legal_address_value}}
                </div>
            </li>
        @endif
    </ul>
</div>
