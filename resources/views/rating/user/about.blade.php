@extends('layouts.user')

@section('content')
    @component('rating.user.includes.best-choice.best_choice',
        ['best' => $best])
        <div class="stars_best-choice">
            @include('rating.user.includes.stars', ['company_star' => $best])
        </div>
    @endcomponent

    @include('rating.user.includes.cover', ['extraStyle' => 'cover_first-position'])
    @include('rating.user.includes.about-content', ['about' => $headers['about']])
@endsection

@section('scripts')
    @parent
    <script defer src="{{ asset('/js/about.1.3.22.js') }}"></script>
@endsection
