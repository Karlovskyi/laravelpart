@extends('layouts.user')

@section('content')
       <section class="compare">
           <div class="compare__header-wrapper">
               <h1 class="header header_main ta-c compare__header">{{$headers['compare']}}</h1>
               <p class="compare__paragraph">
                   В таблице есть все данные, которые могут понадобится для осознанного выбора компании. Все данные можно проверить
                   самостоятельно. Подробную информацию о компании можно получить кликнув по названию компании.
               </p>
           </div>

           <div class="compare__main-wrapper" id="compare" data-slug="@foreach($slug as $i => $s){{$s}};@endforeach"></div>

       </section>
@endsection

@section('scripts')
    @parent
    <script defer src=" {{ asset('/js/compareList.1.3.22.js') }}"></script>
@endsection

@section('footer')
    footer_compare
@endsection

@section('nav')
    nav_compare
@endsection

@section('main-class') container_compare @endsection

@section('seo_content')
    <link rel="canonical" href="{{route('user.compare-all')}}">
@endsection
