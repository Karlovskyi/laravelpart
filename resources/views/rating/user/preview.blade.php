@extends('layouts.user')

@section('content')
    @include('rating.user.includes.side_form_submit')

    @include('rating.user.includes.about_site', ['header' => $headers['about-site']])

    @component('rating.user.includes.best-choice.best_choice',
        ['best' => $best, 'extraStyle' => 'best-choice_second-position'])
        <div class="stars_best-choice">
            @include('rating.user.includes.stars', ['company_star' => $companies->first()])
        </div>
    @endcomponent

    @include('rating.user.includes.cover')

    <section class="rating-companies">
        <h2>{{$headers['rating-companies']}}</h2>
        @include('rating.user.includes.rating-list.rating_list_dual')
    </section>
    <section class="info" id="info">
        @include('rating.user.includes.how-to-choose.long-text', [
    'howToChoose' => $headers['how-to-choose'],
    'remember' => $headers['remember']
    ])
    </section>
@endsection

@section('seo_content')
    <link rel="canonical" href="{{route('user.main')}}">
@endsection
