@extends('layouts.user')

@section('content')

    @component('rating.user.includes.best-choice.best_choice',
        ['best' => $best])
        <div class="stars_best-choice">
            @include('rating.user.includes.stars', ['company_star' => $best])
        </div>
    @endcomponent

    @include('rating.user.includes.cover', ['extraStyle' => 'cover_first-position'])

    @include('rating.user.includes.company_data',
    [
        'company_header' => $headers['company'],
        'contacts' => $headers['contacts'],
        'portfolio' => $headers['portfolio'],
        'rely' => $headers['rely'],
        'rating' => $headers['rating'],
        'criterion' => $criterion
        ])

        @include('rating.user.includes.company_reviews', ['header' => $headers['reviews'], 'paginate' => $paginate])
        @include('rating.user.includes.review-photo-zoom.main')
@endsection

@section('scripts')
    @parent
    <script defer src="{{ asset('/js/reviews.1.3.22.js') }}"></script>
@endsection

@section('seo_content')
    <link rel="canonical" href="{{route('user.company' , $paginate['slug'])}}">
    @if($paginate['current'] !== 1)
    <link rel="prev" href="{{route('user.company' , ['company' => $paginate['slug'], 'page' => $paginate['current'] - 1])}}">
    @endif
    @if($paginate['current'] !== $paginate['max'])
    <link rel="next" href="{{route('user.company' , ['company' => $paginate['slug'], 'page' => $paginate['current'] + 1])}}">
    @endif
@endsection
