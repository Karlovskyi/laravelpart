@extends('layouts.user')

@section('content')
    <form action="{{ route('logout') }}" method="post">
        @csrf
        @method("POST")
        <input type="submit" value="Выйти из аккаунта">
    </form>
@endsection
