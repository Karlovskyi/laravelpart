<div class="criteria" data-tip="{{$tip ?? 1}}">
        <span class="criteria__key">{{$criteria->criteria_name_ru}}</span>
    @if($company->company_bulk_address_register)
        <span class="criteria__value">{{$criteria->replaceData('criteria_value_1', $company->company_bulk_address_register)}}</span>
    @else
        <span class="criteria__value">{{$criteria->replaceData('criteria_value_2', $company->company_bulk_address_register)}}</span>
    @endif
    <span class="criteria__tip">{{$criteria->criteria_description}}</span>
    <i class="circle circle_list" data-content="{{$company->getStatusField('company_bulk_address_register') *2}}"></i>
</div>

