<div class="criteria" data-tip="{{$tip ?? 1}}">
    <span class="criteria__key">{{$criteria->criteria_name_ru}}</span>
    @if($company->company_legal_address)
        <span class="criteria__value">
            {{$criterion['company_legal_address']->replaceData('criteria_value_1', $company->company_legal_address)}}
        </span>
    @else
        <span class="criteria__value">
            {{$criterion['company_legal_address']->replaceData('criteria_value_2', $company->company_legal_address)}}
        </span>
    @endif
    <span class="criteria__tip">{{$criteria->criteria_description}}</span>
    <i class="circle circle_list" data-content="{{$company->getStatusField('company_legal_address')}}"></i>
</div>
