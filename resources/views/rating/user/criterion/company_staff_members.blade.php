<div class="criteria" data-tip="{{$tip ?? 1}}">
    <span class="criteria__key">{{$criteria->criteria_name_ru}}</span>
    <span class="criteria__value">
        {{$criterion['company_staff_members']->replaceData('criteria_value_1', $company->company_staff_members_clear)}}
    </span>
    <span class="criteria__tip">{{$criteria->criteria_description}}</span>
    <i class="circle circle_list" data-content="{{$company->getStatusField('company_staff_members')}}"></i>
</div>

