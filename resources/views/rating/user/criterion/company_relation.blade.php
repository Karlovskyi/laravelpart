<div class="criteria" data-tip="{{$tip ?? 1}}">
    <span class="criteria__key">{{$criteria->criteria_name_ru}}</span>
    <span class="criteria__value">
        @if(!$company->company_relation)
            {!! $criteria->replaceData('criteria_value_1', $company->company_relation_clear) !!}
        @elseif($company->company_relation)
            {!! $criteria->replaceData('criteria_value_2', $company->company_relation_clear) !!}
        @endif
    </span>
    @if($criteria->criteria_description)
        <span class="criteria__tip">{{$criteria->criteria_description}}</span>
    @endif
    <i class="circle circle_list" data-content="{{$company->getStatusField('company_relation') + 1}}"></i>
</div>
