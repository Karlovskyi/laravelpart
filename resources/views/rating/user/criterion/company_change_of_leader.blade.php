<div class="criteria" data-tip="{{$tip ?? 1}}">
    <span class="criteria__key">{{$criteria->criteria_name_ru}}</span>
    @if($company->company_change_of_leader_clear === 100)
            <span class="criteria__value">Не менялся</span>
    @elseif($company->company_change_of_leader === 2)
        <span class="criteria__value">
            {{$criterion['company_change_of_leader']->replaceData('criteria_value_1', $company->company_change_of_leader_clear)}}
        </span>
    @elseif($company->company_change_of_leader === 1)
        <span class="criteria__value">
            {{$criterion['company_change_of_leader']->replaceData('criteria_value_2', $company->company_change_of_leader_clear)}}
        </span>
    @else
        <span class="criteria__value">
            {{$criterion['company_change_of_leader']->replaceData('criteria_value_3', $company->company_change_of_leader_clear)}}
        </span>
    @endif
    <span class="criteria__tip">{{$criteria->criteria_description}}</span>
    <i class="circle circle_list" data-content="{{$company->getStatusField('company_change_of_leader')}}"></i>
</div>

