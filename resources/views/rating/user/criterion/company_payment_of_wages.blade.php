<div class="criteria" data-tip="{{$tip ?? 1}}">
    <span class="criteria__key">{{$criteria->criteria_name_ru}}</span>
    @if($company->company_payment_of_wages)
        <span class="criteria__value">
            {{$criterion['company_payment_of_wages']->replaceData('criteria_value_1', $company->company_payment_of_wages)}}
        </span>
    @else
        <span class="criteria__value">
            {{$criterion['company_payment_of_wages']->replaceData('criteria_value_2', $company->company_payment_of_wages)}}
        </span>
    @endif
    <span class="criteria__tip">{{$criteria->criteria_description}}</span>
    <i class="circle circle_list" data-content="{{$company->getStatusField('company_payment_of_wages') * 2}}"></i>
</div>
