<div class="criteria" data-tip="{{$tip ?? 1}}">
    <span class="criteria__key">{{$criteria->criteria_name_ru}}</span>
    @if($company->company_income_dynamics)
        <span class="criteria__value">
            {{$criterion['company_income_dynamics']->replaceData('criteria_value_1', $company->company_income_dynamics)}}
        </span>
    @else
        <span class="criteria__value">
            {{$criterion['company_income_dynamics']->replaceData('criteria_value_2', $company->company_income_dynamics)}}
        </span>
    @endif
    <span class="criteria__tip">{{$criteria->criteria_description}}</span>
    <i class="circle circle_list" data-content="{{$company->getStatusField('company_income_dynamics')*2}}"></i>
</div>
