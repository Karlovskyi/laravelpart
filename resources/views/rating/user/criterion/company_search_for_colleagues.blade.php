<div class="criteria" data-tip="{{$tip ?? 1}}">
    <span class="criteria__key">{{$criteria->criteria_name_ru}}</span>
    @if($company->company_search_for_colleagues === 2)
        <span class="criteria__value">
            {{$criterion['company_search_for_colleagues']->replaceData('criteria_value_1', $company->company_search_for_colleagues)}}
        </span>
    @elseif($company->company_search_for_colleagues === 1)
        <span class="criteria__value">
            {{$criterion['company_search_for_colleagues']->replaceData('criteria_value_2', $company->company_search_for_colleagues)}}
        </span>
    @else
        <span class="criteria__value">
            {{$criterion['company_search_for_colleagues']->replaceData('criteria_value_3', $company->company_search_for_colleagues)}}
        </span>
    @endif
    <span class="criteria__tip">{{$criteria->criteria_description}}</span>
    <i class="circle circle_list" data-content="{{$company->getStatusField('company_search_for_colleagues')}}"></i>
</div>
