<div class="criteria" data-tip="{{$tip ?? 1}}">
    <span class="criteria__key">{{$criteria->criteria_name_ru}}</span>
    @if($company->company_checks === 0)
        <span class="criteria__value">
            {{$criterion['company_checks']->replaceData('criteria_value_1', $company->company_checks)}}
        </span>
    @else
        <span class="criteria__value">
            {{$criterion['company_checks']->replaceData('criteria_value_2', $company->company_checks)}}
        </span>
    @endif
    <span class="criteria__tip" data-content="none">{{$criteria->criteria_description}}</span>
</div>
