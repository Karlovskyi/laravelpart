<div class="criteria" data-tip="{{$tip ?? 1}}">
    <span class="criteria__key">{{$criteria->criteria_name_ru}}</span>
    <span class="criteria__value">
        {{$criterion['company_term_of_work']->replaceData('criteria_value_1', $company->company_term_of_work_clear)}}
    </span>
    <span class="criteria__tip">{{$criteria->criteria_description}}</span>
    <i class="circle circle_list" data-content="{{$company->getStatusField('company_term_of_work')}}"></i>
</div>
