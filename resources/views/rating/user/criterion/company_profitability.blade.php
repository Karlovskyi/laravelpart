<div class="criteria" data-tip="{{$tip ?? 1}}">
    <span class="criteria__key">{{$criteria->criteria_name_ru}}</span>
    <span class="criteria__value">
        {{$criterion['company_profitability']->replaceData('criteria_value_1', $company->company_profitability_clear)}}
    </span>
    <span class="criteria__tip" data-content="none">{{$criteria->criteria_description}}</span>
    <i class="circle circle_list" data-content="{{$company->getStatusField('company_profitability')}}"></i>
</div>
