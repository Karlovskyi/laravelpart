@extends('layouts.user')

@section('content')
       @include('rating.user.includes.side_form_submit')

       @include('rating.user.includes.about_site', ['header' => $headers['about-site-rely']])

       @component('rating.user.includes.best-choice.best_choice',
           ['best' => $best, 'extraStyle' => 'best-choice_second-position'])
           <div class="stars_best-choice">
               @include('rating.user.includes.stars', ['company_star' => $companies->first()])
           </div>
       @endcomponent

       @include('rating.user.includes.cover')

       <section class="rating-reliability">
           <h2>{{$headers['rating-reliability']}}</h2>
           @include('rating.user.includes.rating-list.rating_list', ['list_type' => 'rely'])
       </section>
@endsection
