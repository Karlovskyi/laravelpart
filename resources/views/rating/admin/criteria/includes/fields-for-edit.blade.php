<div class="card p-3">
    <div class="form-group">
        <label for="criteria_name_ru">Название</label>
        <input type="text" name="criteria_name_ru"
               value="{{old('criteria_name_ru', $criteria->criteria_name_ru)}}"
               class="form-control"
               id="criteria_name_ru">

    </div>

    <div class="form-group">
        <label for="criteria_type">Тип критения</label>
        <select name="criteria_type" id="criteria_type" class="form-control" disabled>
            <option value="1" @if($criteria->criteria_type === 1)selected @endif>
                Унарный
            </option>
            <option value="2" @if($criteria->criteria_type === 2)selected @endif>
                Бинарный
            </option>
            <option value="3" @if($criteria->criteria_type === 3)selected @endif>
                Тернарный
            </option>
        </select>
    </div>
    @if($criteria->criteria_type >= 1)
    <div class="form-group">
        <label for="criteria_value_1">Значение 1</label>
        <input type="text" name="criteria_value_1"
               value="{{old('criteria_value_1', $criteria->criteria_value_1)}}"
               class="form-control"
               id="criteria_value_1">

    </div>
    @endif

    @if($criteria->criteria_type >= 2)
        <div class="form-group">
            <label for="criteria_value_2">Значение 2</label>
            <input type="text" name="criteria_value_2"
                   value="{{old('criteria_value_2', $criteria->criteria_value_2)}}"
                   class="form-control"
                   id="criteria_value_2">

        </div>
    @endif

    @if($criteria->criteria_type >= 3)
        <div class="form-group">
            <label for="criteria_value_3">Значение 3</label>
            <input type="text" name="criteria_value_3"
                   value="{{old('criteria_value_3', $criteria->criteria_value_3)}}"
                   class="form-control"
                   id="criteria_value_3">

        </div>
    @endif

    <div class="form-group">
        <label for="criteria_description">Описание</label>
        <textarea name="criteria_description" id="criteria_description" rows="10"
                  class="form-control"
        >{{old('criteria_description', $criteria->criteria_description)}}</textarea>

    </div>
</div>

