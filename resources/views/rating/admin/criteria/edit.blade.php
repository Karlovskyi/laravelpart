@extends('layouts.admin')

@section('content')
    <form action="{{route('rating.admin.criteria.update', $criteria->criteria_id)}}" method="post">
        @method('PATCH')
        @csrf
        <div class="container mt-3">
            @include('rating.admin._includes.message_indicator')
            <div class="d-flex justify-content-center flex-row">
                <div class="col-8">
                    @include('rating.admin.criteria.includes.fields-for-edit')

                </div>
                <div class="col-3 offset-1">
                    <div class="card p-3">
                        <button class="btn btn-success" type="submit">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
