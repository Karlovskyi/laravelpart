@extends('layouts.admin')

@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Строковый идентификтор</th>
            <th scope="col">Название</th>
            <th scope="col">Изменить</th>
        </tr>
        </thead>
        <tbody>
        @foreach($criteria as $crit)
            <tr>
                <th scope="row">{{$crit->criteria_id}}</th>
                <td>{{$crit->criteria_name}}</td>
                <td>{{$crit->criteria_name_ru}}</td>
                <td><a href="{{route('rating.admin.criteria.edit', $crit->criteria_id)}}"  class="badge bg-dark">Изменить</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
