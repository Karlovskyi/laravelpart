@extends('layouts.admin')

@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Ключ</th>
            <th scope="col">Значение</th>
            <th scope="col">Ссылки</th>
        </tr>
        </thead>
        <tbody>
        @foreach($constants as $constant)
            <tr >
                <th scope="row">{{$constant->constant_id}}</th>
                <td>{{$constant->constant_key}}</td>
                <td>{{$constant->constant_value}}</td>
                <td>
                    <a href="{{route('rating.admin.constants.edit', $constant->constant_id)}}"  class="badge bg-dark">Изменить</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="justify-content-center d-flex mt-3">
        {{$constants->links()}}
    </div>
@endsection
