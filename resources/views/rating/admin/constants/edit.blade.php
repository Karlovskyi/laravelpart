@extends('layouts.admin')

@section('content')
    @include('rating.admin._includes.message_indicator')
    <form action="{{ route('rating.admin.constants.update', $constant->constant_id) }}" method="POST">
        @method('PATCH')
        @csrf
        <div class="d-flex">
            <div class="col-8">
                <div class="card card-body p-3 m-3">
                    <div class="form-group">
                        <label for="constant_value">
                            Значение константы(После изменения проверьте измененную работу сайта, возможно, вы ввели не вадидние данные)
                        </label>
                        <input type="text" class="form-control"
                               name="constant_value"
                               id="constant_value"
                               value="{{old('constant_value', $constant->constant_value)}}">
                    </div>
                </div>
            </div>
            <div class="col-3 offset-1">
                <div class="card card-body p-3 m-3">
                    <div class="form-group">
                        <label for="constant_key">Ключ константы</label>
                        <input type="text" disabled class="form-control"
                               name="constant_key"
                               id="constant_key"
                               value="{{old('constant_key',$constant->constant_key)}}">
                    </div>
                    <button class="btn btn-success mt-3">Сохранить</button>
                </div>
            </div>
        </div>

    </form>
@endsection
