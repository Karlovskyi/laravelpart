@extends('layouts.admin')

@section('content')
    <form action="{{route('rating.admin.page-search-attributes.store')}}" method="POST">
        @csrf
        @include('rating.admin.page_search_attributes.includes.formBody')
    </form>
@endsection
