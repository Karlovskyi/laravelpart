@extends('layouts.admin')

@section('content')
    <form action="{{route('rating.admin.page-search-attributes.update', $attribute->id)}}" method="POST">
        @method("PATCH")
        @csrf
        @include('rating.admin.page_search_attributes.includes.formBody')
    </form>
@endsection
