@include('rating.admin._includes.message_indicator')
<div class="container mt-3 d-flex justify-content-center flex-row">
    <div class="col-8 card p-3">
        <div class="form-group">
            <label for="title">Title @if($attribute->title_index === 'company')<br>Вместо (name) подставиться
                название компании@endif</label>
            <input type="text" id="title" name="title" class="form-control"
                   value="{{ old('title', $attribute->title) }}">
        </div>
        <div class="form_group">
            <label for="description">Description</label>
            <textarea name="description" id="description" rows="10"
                      class="form-control">{{ old('description', $attribute->description) }}</textarea>
        </div>
        <div class="form_group">
            <label for="keywords">Keywords</label>
            <textarea name="keywords" id="keywords" rows="10"
                      class="form-control">{{ old('keywords', $attribute->keywords) }}</textarea>
        </div>
    </div>
    <div class="col-3 offset-1">
        <div class="card p-3">
            <button class="btn btn-success">Сохранить</button>
            <div class="form-group">
                <label for="id">ID</label>
                <input type="text" id="id" class="form-control" name="id" disabled
                       value="{{ $attribute->id}}">
            </div>
            <div class="form-group">
                <label for="title_index">Текстовый идентификатор</label>
                <input type="text" id="title_index" class="form-control" name="title_index" @if($attribute->exists) disabled @endif
                       value="{{ $attribute->title_index }}">
            </div>
        </div>

    </div>
</div>
