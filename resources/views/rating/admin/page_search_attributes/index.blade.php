@extends('layouts.admin')

@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Строковой идентификатор</th>
            <th scope="col">Title</th>
            <th scope="col">Description</th>
            <th scope="col">Изменить</th>
        </tr>
        </thead>
        <tbody>
        @foreach($attributes as $attribute)
            <tr >
                <th scope="row">{{$attribute->id}}</th>
                <td>{{$attribute->title_index}}</td>
                <td>{{$attribute->title}}</td>
                <td>{{$attribute->description}}</td>
                <td><a href="{{route('rating.admin.page-search-attributes.edit', $attribute->id)}}"  class="badge bg-dark">Изменить</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <a href="{{ route('rating.admin.page-search-attributes.create') }}" class="btn btn-success ml-3">Создать</a>
    <div class="justify-content-center d-flex mt-3">
        {{$attributes->links()}}
    </div>
@endsection
