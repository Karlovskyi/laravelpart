@extends('layouts.admin')

@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Внутрений идентификатов</th>
            <th scope="col">Заголовок</th>
            <th scope="col">Описание</th>
            <th scope="col">Изменить</th>
        </tr>
        </thead>
        <tbody>
        @foreach($headers as $header)
            <tr>
                <th scope="row">{{$header->id}}</th>
                <td>{{$header->key}}</td>
                <td>{{$header->value}}</td>
                <td>{{$header->description}}</td>
                <td><a href="{{route('rating.admin.headers.edit', $header->id)}}"  class="badge bg-dark">Изменить</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <a href="{{ route('rating.admin.headers.create') }}" class="btn btn-success ml-3">
        Создать
    </a>
@endsection
