@extends('layouts.admin')

@section('content')
    <form action="{{route('rating.admin.headers.update', $header->id)}}" method="POST">
        @method('PATCH')
        @csrf
        @include('rating.admin.page_headers.includes.formBase')
    </form>
@endsection
