<div class="container m-3 p-3 m-auto">
    @include('rating.admin._includes.message_indicator')
    <div class="d-flex justify-content-center m-3">
        <div class="col-8">
            <div class="card p-3">
                <div class="form-group">
                    <label for="value">Значение заголовка</label>
                    <input type="text" class="form-control"
                           name="value"
                           id="value"
                           value="{{old('value', $header->value)}}">
                </div>
                <div class="form-group mt-2">
                    <label for="key">Ключ для поиска</label>
                    <input type="text" class="form-control"
                           name="key"
                           id="key"
                           value="{{old('key', $header->key)}}"
                           @if($header->exists) disabled @endif>
                </div>
                <div class="form-group mt-2">
                    <label for="description">Описание</label>
                    <textarea
                        name="description"
                        id="description"
                        rows="5"
                        class="form-control"
                        @if($header->exists) disabled @endif
                    >{{old('description', $header->description)}}</textarea>
                </div>
            </div>
        </div>
        <div class="col-3 offset-1">
            <div class="card p-3">
                <button class="btn btn-success" type="submit">Сохранить</button>
                <div class="form-group mt-2">
                    <label for="updated_at">Последнее изменения</label>
                    <input type="text" class="form-control"
                           name="updated_at"
                           id="updated_at"
                           value="{{old('updated_at', $header->updated_at)}}"
                           disabled>
                </div>
                <div class="form-group mt-2">
                    <label for="created_at">Создано</label>
                    <input type="text" class="form-control"
                           name="created_at"
                           id="created_at"
                           value="{{old('created_at', $header->created_at)}}"
                           disabled>
                </div>
            </div>
        </div>
    </div>
</div>
