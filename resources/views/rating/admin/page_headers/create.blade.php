@extends('layouts.admin')

@section('content')
    <form action="{{route('rating.admin.headers.store')}}" method="POST">
        @csrf
        @include('rating.admin.page_headers.includes.formBase')
    </form>
@endsection
