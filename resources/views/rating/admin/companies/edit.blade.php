@extends('layouts.admin')

@section('content')
    <div class="container mt-1">
        @include('rating.admin.companies.includes.message_indicator')
        <form action="{{route('rating.admin.companies.update', $company->company_id)}}" method="POST">
            @method("PATCH")
            @csrf
            <div class="d-flex justify-content-center">
                <div class="col-8">
                    @include('rating.admin.companies.includes.edit_main')
                </div>
                <div class="col-3 ml-3">
                    @include('rating.admin.companies.includes.edit_side')
                </div>
            </div>

        </form>
    </div>
@endsection
