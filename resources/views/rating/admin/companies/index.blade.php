@extends('layouts.admin')

@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Название</th>
            <th scope="col">Отзывы</th>
            <th scope="col">Средний балл</th>
            <th scope="col">Ссылки</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($companies as $company)
            <tr @if(!$company->is_published) style="background-color: #ccc" @endif>
                <th scope="row">{{$company->company_id}}</th>
                <td>{{$company->company_name}}</td>
                <td>{{$company->reviewTotalData}}</td>
                <td>{{$company->company_mark_average}}</td>
                <td><a href="{{route('rating.admin.companies.show', $company->company_id)}}"  class="badge bg-dark">Показать полностью</a></td>
                <td><a href="{{route('rating.admin.companies.edit', $company->company_id)}}"  class="badge bg-dark">Изменить</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="d-flex justify-content-around align-items-center">
        <a href="{{route('rating.admin.companies.create')}}" class=" btn btn-primary text-light d-block">Создать</a>
        <div class="justify-content-center d-flex mt-3">

            {{$companies->links()}}
        </div>
    </div>



@endsection
