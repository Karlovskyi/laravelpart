@extends('layouts.admin')

@section('content')
    <div class="container mt-4">
        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item ">
                        <a class="nav-link @if(!$review_active) active @endif" id="about-tab" data-toggle="tab" href="#about" role="tab"
                           aria-controls="general" aria-selected="@if(!$review_active) true @else false @endif">Общее</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tech-tab" data-toggle="tab" href="#tech" role="tab"
                           aria-controls="profile" aria-selected="false">Технические переменные</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="compare-tab" data-toggle="tab" href="#compare" role="tab"
                           aria-controls="contact" aria-selected="false">Критерии сравнения</a>
                    </li>
                    <li class="nav-item">
                        <a href="#review" class="nav-link @if($review_active) active @endif" id="review-tab"  data-toggle="tab" role="tab"
                           aria-controls="review" aria-selected="@if($review_active) true @else false @endif">Отзывы</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="active_images-tab" data-toggle="tab" href="#active_images" role="tab"
                           aria-controls="active_images" aria-selected="false">Картинки в портфолио</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade @if(!$review_active) show active @endif" id="about" role="tabpanel" aria-labelledby="home-tab">
                        @include('rating.admin.companies.includes.show_general_tab')
                    </div>
                    <div class="tab-pane fade" id="tech" role="tabpanel" aria-labelledby="profile-tab">
                        @include('rating.admin.companies.includes.show_tech_tab')
                    </div>
                    <div class="tab-pane fade" id="compare" role="tabpanel" aria-labelledby="contact-tab">
                        @include('rating.admin.companies.includes.show_compare_tab')
                    </div>
                    <div class="tab-pane fade @if($review_active) show active @endif" id="review" role="tabpanel" aria-labelledby="review-tab">
                        @include('rating.admin.companies.includes.show_review_tab')
                    </div>
                    <div class="tab-pane fade" id="active_images" role="tabpanel" aria-labelledby="contact-tab">
                        @include('rating.admin.companies.includes.show_active_images')
                    </div>
                </div>
            </div>

        </div>
        <a href="{{route('rating.admin.companies.edit', $company->company_id)}}">
            <button type="button" class="btn btn-primary mt-3 ml-3">Изменить</button>
        </a>
    </div>
@endsection
