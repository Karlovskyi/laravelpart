<div class="jumbotron">
    <div class="alert alert-light font-weight-light p-1" role="alert">
        Адресный идентификатор: {{$company->company_slug}}
    </div>
    <div class="alert alert-light font-weight-light p-1" role="alert">
        GoogleID: {{$company->company_google_id}}
    </div>
    <div class="alert alert-light font-weight-light p-1" role="alert">
        YandexID: {{$company->company_yandex_id}}
    </div>
    <div class="alert alert-light font-weight-light p-1" role="alert">
        OtzovickID: {{$company->company_otzovick_id}}
    </div>
    <div class="alert alert-light font-weight-light p-1" role="alert">
        Позиция по отзывам:
        @if($company->company_position)
            {{$company->company_position}}
        @else
            По показателю N.
        @endif
    </div>
    <div class="alert alert-light font-weight-light p-1" role="alert">
        Позиция по надежности:
        @if($company->company_reliability_position)
            {{$company->company_reliability_position}}
        @else
            В соотвествием с критериями надежности.
        @endif
    </div>
    <div class="alert alert-light font-weight-light p-1" role="alert">
        N = {{$company->company_mark_difference}}
    </div>
    <div class="alert alert-light font-weight-light p-1" role="alert">
        Средняя оценка: {{$company->company_mark_average}}
        (Google: {{$company->company_mark_average_google}}, Yandex: {{$company->company_mark_average_yandex}},
        Otzovick: {{$company->company_mark_average_otzovick}})
    </div>
    <div class="alert alert-light font-weight-light p-1" role="alert">
        Количество отзывов: {{$company->company_review_quantity}}
        (Google: {{$company->company_review_quantity_google}},
        Yandex: {{$company->company_review_quantity_yandex}},
        Otzovick: {{$company->company_review_quantity_otzovick}})
    </div>
</div>
