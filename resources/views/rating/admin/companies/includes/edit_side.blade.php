<div class="card">
    <div class="card-body">
        <button type="submit" class="btn btn-success">Сохранить</button>
        <div class="form-group mt-1">
            <label for="yandex_id">Yandex ID</label>
            <input type="text" class="form-control" id="yandex_id" value="{{$company->company_yandex_id}}" disabled>
        </div>
        <div class="form-group mt-1">
            <label for="google_id">Google ID</label>
            <input type="text" class="form-control" id="google_id" value="{{$company->company_google_id}}" disabled>
        </div>
        <div class="form-group mt-1">
            <label for="otzovick_id">Otzovick ID</label>
            <input type="text" class="form-control" id="otzovick_id" value="{{$company->company_otzovick_id}}" disabled>
        </div>
        <div class="form-group mt-1">
            <label for="mark_difference">Показатель N</label>
            <input type="text" class="form-control" id="mark_difference" value="{{$company->company_mark_difference}}" disabled>
        </div>
        <div class="form-group mt-1">
            <label for="mark_average">Средняя оценка</label>
            <input type="text" class="form-control" id="mark_average" value="{{$company->company_mark_average}}" disabled>
        </div>
        <div class="form-group mt-1">
            <label for="review_quantity">Количество отзывов</label>
            <input type="text" class="form-control" id="review_quantity" value="{{$company->company_review_quantity}}" disabled>
        </div>
        <div class="form-group mt-1">
            <label for="updated_at">Изменино</label>
            <input type="text" class="form-control" id="updated_at" value="{{$company->updated_at}}" disabled>
        </div>
        <div class="form-group mt-1">
            <label for="deleted_at">Удалено</label>
            <input type="text" class="form-control" id="deleted_at" value="{{$company->deleted_at}}" disabled>
        </div>
        <div class="form-group mt-1">
            <label for="company_total_reliability">Баллы надежности</label>
            <input type="text" class="form-control" id="company_total_reliability" value="{{$company->company_total_reliability}}" disabled>
        </div>
        <div class="form-group mt-1">
            <label for="is_published">Публикация на сайте</label>
            <input type="text" class="form-control" id="is_published" value="{{$company->is_published ? 'Опубликовано' : 'Не опубликовано'}}" disabled>
        </div>
    </div>
</div>
