@php
    $fields = [
    ['company_legal_address', 'Юридический адрес'],
    ['company_checks', 'Проверки'],
    ['company_enforcement_proceedings', 'Исполнительные производства'],
    ['company_search_for_colleagues', 'Поиск сотрудников'],
    ['company_change_of_leader', 'Смена руководителя (хорошо - не менялся, плохо сменился менее года, средне - количество лет указать ниже)'],
    ];
    $inputFields = [
        ['company_term_of_work_clear', 'Срок работы'],
        ['company_staff_members_clear', 'Сотрудников в штате'],
        ['company_amount_of_credit_clear', 'Сумма долгов'],
        ['company_change_of_leader_clear', 'Смена руководителя'],
        ['company_profitability_clear', 'Прибыльность'],
        ['company_financial_autonomy_clear', 'Коэффициент финансовой автономии'],
    ];
    $boolFields = [
       ['company_payment_of_wages', 'Выплата зарабатной платы'],
       ['company_income_dynamics', 'Динамика доходов'],
       ['company_bulk_address_register', 'Реестр массовых адресов']
    ]
@endphp

@foreach($fields as $field)
    <div class="form-group m-3">
        <label for="{{$field[0]}}">{{$field[1]}}</label>
        <select class="form-control" name="{{$field[0]}}"
                id="{{$field[0]}}">
            <option value="0" @if(old($field[0], $company[$field[0]]) + 0 === 0) selected @endif>Плохо</option>
            <option value="1" @if(old($field[0], $company[$field[0]]) + 0 === 1) selected @endif>Средне</option>
            <option value="2" @if(old($field[0], $company[$field[0]]) + 0 === 2) selected @endif>Хорошо</option>
        </select>
    </div>
@endforeach
<hr>
@foreach($boolFields as $field)
    <div class="form-group m-3">
        <label for="{{$field[0]}}">{{$field[1]}}</label>
        <select class="form-control" name="{{$field[0]}}"
                id="{{$field[0]}}">
            <option value="0" @if(old($field[0], $company[$field[0]]) + 0 === 0) selected @endif>Плохо</option>
            <option value="1" @if(old($field[0], $company[$field[0]]) + 0 === 1) selected @endif>Хорошо</option>
        </select>
    </div>
@endforeach
<hr>
@foreach($inputFields as $field)
    <div class="form-group m-3">
        <label for="{{$field[0]}}">{{$field[1]}}</label>
        <input type="text" class="form-control"
               name="{{$field[0]}}"
               id="{{$field[0]}}"
               value="{{old($field[0], $company[$field[0]])}}">
    </div>
@endforeach
<hr>
    <h5 class="pl-3">Дополнительные критерии</h5>
    <div class="form-group m-3">
        <input type="hidden" name="company_relation_clear" value="">
        <label for="company_relation_clear">Связи компании (в случае отсутствия связей &#8212; оставить пустую строку)</label>
        <input type="text"
               name="company_relation_clear"
               id="company_relation_clear"
               value="{{old('company_relation_clear', $company->company_relation_clear)}}"
               class="form-control">
    </div>
    <div class="form-group m-3">
        <label for="company_pay_attention">Обратить внимание</label>
        <input type="text"
               name="company_pay_attention"
               id="company_pay_attention"
               value="{{old('company_pay_attention', $company->company_pay_attention)}}"
               class="form-control">
    </div>
    <div class="form-group m-3">
        <label for="company_pay_attention_type">Тип сообщения "Обратите внимания"</label>
        <select class="form-control" name="company_pay_attention_type">
            @foreach($company->PayAttentionDumpCollection as $pa)

                <option value="{{$pa['value']}}"
                        @if(old('company_pay_attention_type', $company['company_pay_attention_type']) == $pa['value']) selected @endif
                >{{$pa['value'] . '.' . $pa['description']}}</option>
            @endforeach
        </select>
    </div>


