<div class="jumbotron d-flex pb-3 pt-3">
    <img src="/logos/{{$company->company_logo_path}}" alt="logo"
         class="m-4" style="object-fit: contain; max-height: 200px; max-width: 200px;">
    <div class="ml-3 d-flex flex-column justify-content-around">
        <h1 class="display-4 ">{{$company->company_name}}</h1>


        <div>
            @if($company->company_link)
            <a href="{{$company->company_link}}"
                class="badge bg-info">Сайт: {{$company->company_link}}</a>
            @else
                <span class="text">Сайт: Отсутствует</span>
            @endif
        </div>

        <div>
            @if($company->company_google_maps_link)
            <a href="{{$company->company_google_maps_link}}"
                class="badge bg-info">Карта: {{$company->company_google_maps_link}}</a>
            @else
                <span class="text">Карта: Отсутствует</span>
            @endif
        </div>
        <div>Адресс:
            @if($company->company_address)
                {{$company->company_address}}
            @else
                <span class="text">Отсутствует</span>
            @endif

        </div>

        <div>Телефон:
            @if($company->company_number)
            {{$company->company_number}}
            @else
                <span class="text">Отсутствует</span>
            @endif
        </div>

        <div>Instagram:
            @if($company->company_inst)
            <a href="{{$company->company_inst}}"
                           class="badge bg-info">{{$company->company_inst}}</a>
            @else
                <span class="text">Отсутствует</span>
            @endif
        </div>

        <div>
            Facebook:
            @if($company->company_inst)
            <a href="{{$company->company_fb}}"
                          class="badge bg-info">{{$company->company_fb}}</a>
            @else
                <span class="text">Отсутствует</span>
            @endif
        </div>

        <div>YouTube:
            @if($company->company_inst)
            <a href="{{$company->company_you_tube}}"
                         class="badge bg-info">{{$company->company_you_tube}}</a>
            @else
                <span class="text">Отсутствует</span>
            @endif
        </div>
        <div>VK:
            @if($company->company_vk)
                <a href="{{$company->company_vk}}"
                   class="badge bg-info">{{$company->company_vk}}</a>
            @else
                <span class="text">Отсутствует</span>
            @endif
        </div>
    </div>
</div>
<div class="display-6 mb-3 mt-3">О компании:</div>
<div class="jumbotron">
    @if($company->company_description)
    {{$company->company_description}}
    @else
        <span class="text">Отсутствует описание</span>
    @endif
</div>
