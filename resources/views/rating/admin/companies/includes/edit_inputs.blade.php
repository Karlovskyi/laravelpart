<div class="card-body">
    <div class="form-group">
        <label for="company_name">Название Компании</label>
        <input type="text"
               value="{{old('company_name', $company->company_name)}}"
               required
               minlength="3"
               class="form-control"
               id="company_name"
               name="company_name">
    </div>
    <div class="form-group mt-1">
        <label for="company_slug">Адресный идентификатор (url)</label>
        <input type="text"
               value="{{old('company_slug', $company->company_slug)}}"
               class="form-control"
               id="company_slug"
               name="company_slug">
    </div>
    <div class="form-group mt-1">
        <label for="company_link">Ссылка на сайт</label>
        <input type="text"
               value="{{old('company_link', $company->company_link)}}"
               minlength="3"
               class="form-control"
               id="company_link"
               name="company_link">
    </div>

    <div class="form-group mt-1">
        <label for="company_logo_path">Путь к логотипу</label>
        <input type="text"
               value="{{old('company_logo_path', $company->company_logo_path)}}"
               required
               minlength="3"
               class="form-control"
               id="company_logo_path"
               name="company_logo_path"
               aria-describedby="logo_path_help">

        <small id="logo_path_help" class="form-text text-muted">Менять очень осторожно!</small>
    </div>
    <div class="form-group mt-1">
        <label for="company_position">Строго заданная позиция (отзывы). Счет начитается с нуля</label>
        <input type="text"
               value="{{old('company_position', $company->company_position)}}"
               class="form-control"
               id="company_position"
               name="company_position">
    </div>

    <div class="form-group mt-1">
        <label for="company_reliability_position">Спрого заданная позиция (надежность). Счет начитается с нуля</label>
        <input type="text"
               value="{{old('company_reliability_position', $company->company_reliability_position)}}"
               class="form-control"
               id="company_reliability_position"
               name="company_reliability_position">
    </div>


    <div class="form-group mt-1">
        <label for="company_criteria_1">Критерий для показа в топ по надежности</label>
        <select class="form-control" name="company_criteria_1"
                id="company_criteria_1">
            @foreach($criterion as $criteria)
            <option value="{{$criteria->criteria_name}}"
                    @if(old('company_criteria_1', $company->company_criteria_1) === $criteria->criteria_name) selected @endif
            >
                {{$criteria->criteria_name_ru}}
            </option>
            @endforeach
        </select>
    </div>

    <div class="form-group mt-1">
        <label for="company_criteria_2">Критерий для показа в топ по надежности</label>
        <select class="form-control" name="company_criteria_2"
                id="company_criteria_2">
            @foreach($criterion as $criteria)
                <option value="{{$criteria->criteria_name}}"
                        @if(old('company_criteria_2', $company->company_criteria_2) === $criteria->criteria_name) selected @endif
                >
                    {{$criteria->criteria_name_ru}}
                </option>
            @endforeach
        </select>
    </div>

    <div class="form-group mt-1">
        <label for="company_criteria_3">Критерий для показа в топ по надежности</label>
        <select class="form-control" name="company_criteria_3"
                id="company_criteria_3">
            @foreach($criterion as $criteria)
                <option value="{{$criteria->criteria_name}}"
                        @if(old('company_criteria_3', $company->company_criteria_3) === $criteria->criteria_name) selected @endif
                >
                    {{$criteria->criteria_name_ru}}
                </option>
            @endforeach
        </select>
    </div>

    <div class="form-group mt-3 ml-2">
        <input type="hidden" value="0" name="is_published">

            <input type="checkbox" name="is_published" id="is_published" value="1" @if(old('is_published', $company->is_published)) checked @endif>
            <label for="is_published">Опубликовать</label>
    </div>
</div>
