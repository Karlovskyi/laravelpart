<div class="jumbotron">
    @if($company->images->isNotEmpty())
        <div class="d-flex flex-wrap">
            @foreach($company->images as $image)
                <a href="{{route('rating.admin.images.edit', $image->image_id)}}" class="d-block w-25 m-3 ml-5 mr-5">
                    <img src="{{$image->image_path}}" alt="{{$image->image_path}}" class="img-thumbnail h-100 w-100 rounded d-block" style="object-fit: cover">
                </a>
            @endforeach
        </div>
    @else
        <div class="display-6 text-center p-3">
            У комнапии нет портфолио
        </div>
    @endif
</div>
<a href="{{ route('rating.admin.images.show-by-company', $company->company_id)}}">
    <button class="btn btn-primary">
        Добавить в портфолио
    </button>
</a>
