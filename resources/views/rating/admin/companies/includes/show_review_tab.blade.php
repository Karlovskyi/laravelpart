<div class="jumbotron">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#ID В общем списке</th>
            <th scope="col">Имя оставившего</th>
            <th scope="col">Балл</th>
            <th scope="col">Дата отзыва</th>
            <th scope="col">Ссылки</th>
        </tr>
        </thead>
        <tbody>
            @foreach($reviews as $review)
                <tr @if(!$review->is_published) style="background-color: #ccc;" @endif>
                    <th scope="row">{{$review->review_id}}</th>
                    <td>{{$review->reviewer_name}}</td>
                    <td>
                        <span class="badge @if($review->positive) bg-success @else bg-danger @endif">
                            {{$review->review_mark}}
                        </span>
                    </td>
                    <td>{{$review->review_date}}</td>
                    <td><a href="{{route('rating.admin.reviews.show', $review->review_id)}}" class="badge bg-dark">Смотреть детально</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="d-flex justify-content-center">
        {{$reviews->links()}}
    </div>
</div>
