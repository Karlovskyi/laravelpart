<div class="card">
    <div class="card-body p-3 m-3">
        <div>
            <button class="btn btn-success">Сохранить</button>
        </div>
        <ul class="nav nav-tabs mt-3" id="tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="main-tab" data-toggle="tab" href="#main" role="tab" aria-controls="main" aria-selected="true">Общее</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="false">Описание</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="other-tab" data-toggle="tab" href="#other" role="tab" aria-controls="other" aria-selected="false">Критерии сравнения</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contacts" role="tab" aria-controls="contacts" aria-selected="false">Контактные данные</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="other-tab" data-toggle="tab" href="#api-data" role="tab" aria-controls="contacts" aria-selected="false">Данние для API</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="main-tab">
                @include('rating.admin.companies.includes.edit_inputs')
            </div>
            <div class="tab-pane fade" id="description" role="tabpanel" aria-labelledby="description-tab">
                <div class="form-group m-3">
                    <label for="company_description">Описание Компании</label>
                    <textarea name="company_description" id="company_description" rows="10" class="form-control"
                    >{{old('company_description', $company->company_description)}}</textarea>
                </div>
            </div>
            <div class="tab-pane fade" id="other" role="tabpanel" aria-labelledby="other-tab">
                @include('rating.admin.companies.includes.edit_selects')
            </div>
            <div class="tab-pane fade" id="contacts">
                @include('rating.admin.companies.includes.edit_contacts')
            </div>
            <div class="tab-pane fade" id="api-data">
                @include('rating.admin.companies.includes.create_api_data')
            </div>

        </div>
    </div>
</div>
