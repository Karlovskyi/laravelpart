<div class="card-body">
    <div class="form-group mt-1">
        <label for="company_phone">Телефонный номер</label>
        <input type="text"
               value="{{old('company_phone', $company->company_phone)}}"
               minlength="5"
               class="form-control"
               id="company_phone"
               name="company_phone">
    </div>
    <div class="form-group mt-1">
        <label for="company_google_maps_link">Ссылка на Google карту</label>
        <input type="text"
               value="{{old('company_google_maps_link', $company->company_google_maps_link)}}"
               minlength="5"
               class="form-control"
               id="company_google_maps_link"
               name="company_google_maps_link">
    </div>
    <div class="form-group mt-1">
        <label for="company_inst">Ссылка на Instagram</label>
        <input type="text"
               value="{{old('company_inst',$company->company_inst)}}"
               minlength="3"
               class="form-control"
               id="company_inst"
               name="company_inst">
    </div>
    <div class="form-group mt-1">
        <label for="company_vk">Ссылка на VK</label>
        <input type="text"
               value="{{old('company_vk',$company->company_vk)}}"
               minlength="3"
               class="form-control"
               id="company_vk"
               name="company_vk">
    </div>
    <div class="form-group mt-1">
        <label for="company_fb">Ссылка на Facebook</label>
        <input type="text"
               value="{{old('company_fb', $company->company_fb)}}"
               minlength="3"
               class="form-control"
               id="company_fb"
               name="company_fb">
    </div>
    <div class="form-group mt-1">
        <label for="company_you_tube">Ссылка на YouTube</label>
        <input type="text"
               value="{{old('company_you_tube', $company->company_you_tube)}}"
               minlength="3"
               class="form-control"
               id="company_you_tube"
               name="company_you_tube">
    </div>
    <div class="form-group mt-1">
        <label for="company_rusprofile_link">Ссылка на Rusprofile</label>
        <input type="text"
               value="{{old('company_rusprofile_link', $company->company_rusprofile_link)}}"
               minlength="3"
               class="form-control"
               id="company_rusprofile_link"
               name="company_rusprofile_link">
    </div>

    <div class="form-group mt-1">
        <label for="company_ogrn">ОГРН</label>
        <input type="text"
               value="{{old('company_ogrn', $company->company_ogrn)}}"
               minlength="3"
               class="form-control"
               id="company_ogrn"
               name="company_ogrn">
    </div>

    <div class="form-group mt-1">
        <label for="company_inn">ИНН</label>
        <input type="text"
               value="{{old('company_inn', $company->company_inn)}}"
               minlength="3"
               class="form-control"
               id="company_inn"
               name="company_inn">
    </div>

    <div class="form-group mt-1">
        <label for="company_address">Адрес компании</label>
        <input type="text"
               value="{{old('company_address', $company->company_address)}}"
               minlength="3"
               class="form-control"
               id="company_address"
               name="company_address">
    </div>

    <div class="form-group mt-1">
        <label for="company_legal_address_value">Юридический адрес компании</label>
        <input type="text"
               value="{{old('company_legal_address_value', $company->company_legal_address_value)}}"
               minlength="3"
               class="form-control"
               id="company_legal_address_value"
               name="company_legal_address_value">
    </div>

    <div class="form-group mt-1">
        <label for="company_legal_address_comment">Коментарий к адресу</label>
        <input type="text"
               value="{{old('company_legal_address_comment', $company->company_legal_address_comment)}}"
               minlength="3"
               class="form-control"
               id="company_legal_address_comment"
               name="company_legal_address_comment">
    </div>

</div>
