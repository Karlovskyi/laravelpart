<div class="jumbotron">
    <div class="alert alert-light font-weight-light p-0">Срок
        работы: {{$company->company_term_of_work}}</div>
    <div class="alert alert-light font-weight-light p-0">Сотрудников в
        штате: {{$company->company_staff_members}}</div>
    <div class="alert alert-light font-weight-light p-0">Сумма
        долгов: {{$company->company_amount_of_credit}}</div>
    <div class="alert alert-light font-weight-light p-0">Смена
        руководителя: {{$company->company_change_of_leader}}</div>
    <div class="alert alert-light font-weight-light p-0">Выплата зарабатной
        платы: {{$company->company_payment_of_wages}}</div>
    <div class="alert alert-light font-weight-light p-0">
        Прибыльность: {{$company->company_profitability}}</div>
    <div class="alert alert-light font-weight-light p-0">Юридический
        адрес: {{$company->company_legal_address}}</div>
    <div class="alert alert-light font-weight-light p-0">Проверки: {{$company->company_checks}}</div>
    <div class="alert alert-light font-weight-light p-0">Коэффициент финансовой
        автономии: {{$company->company_financial_autonomy}}</div>
    <div class="alert alert-light font-weight-light p-0">Динамика
        доходов: {{$company->company_income_dynamics}}</div>
    <div class="alert alert-light font-weight-light p-0">Исполнительные
        производства: {{$company->company_enforcement_proceedings}}</div>
    <div class="alert alert-light font-weight-light p-0">Реестр массовых
        адресов: {{$company->company_bulk_address_register}}</div>
    <div class="alert alert-light font-weight-light p-0">Поиск
        сотрудников: {{$company->company_search_for_colleagues}}</div>
</div>
