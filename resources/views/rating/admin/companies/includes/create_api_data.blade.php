<div class="card-body">
    <div class="form-group">
        <label for="company_yandex_id">Яндекс ID</label>
        <input type="text"
               id="company_yandex_id"
               name="company_yandex_id"
               class="form-control"
               value="{{old('company_yandex_id', $company->company_yandex_id)}}">
    </div>
    <div class="form-group">
        <label for="company_google_id">Google ID</label>
        <input type="text"
               id="company_google_id"
               name="company_google_id"
               class="form-control"
               value="{{old('company_google_id', $company->company_google_id)}}">
    </div>
    <div class="form-group">
        <label for="company_otzovick_id">Отзовик ID</label>
        <input type="text"
               id="company_otzovick_id"
               name="company_otzovick_id"
               class="form-control"
               value="{{old('company_otzovick_id', $company->company_otzovick_id)}}">
    </div>
</div>
