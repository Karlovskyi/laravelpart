@extends('layouts.admin')

@section('content')
    <form action="{{route('rating.admin.page-redirects.update', $redirect->id)}}" method="POST">
        @csrf
        @method('PATCH')
        @include('rating.admin.page_redirects.includes.formBase')
        <div class="d-flex justify-content-center">
            <a class="btn btn-danger w-25" id="delete">Удалить</a>
        </div>
    </form>
@endsection
