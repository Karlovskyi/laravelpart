@extends('layouts.admin')

@section('content')
    <form action="{{route('rating.admin.page-redirects.store', $redirect->id)}}" method="POST">
        @csrf
        @include('rating.admin.page_redirects.includes.formBase')
    </form>
@endsection
