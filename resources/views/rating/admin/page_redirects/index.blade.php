@extends('layouts.admin')

@section('content')
    @include('rating.admin._includes.message_indicator')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Начальный путь</th>
            <th scope="col">Путь перенаправления</th>
            <th scope="col">Изменить</th>
        </tr>
        </thead>
        <tbody>
        @foreach($redirects as $redirect)
            <tr>
                <th scope="row">{{$redirect->id}}</th>
                <td>{{$redirect->path_from}}</td>
                <td>{{$redirect->path_to}}</td>
                <td><a href="{{route('rating.admin.page-redirects.edit', $redirect->id)}}"  class="badge bg-dark">Изменить</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="d-flex justify-content-around align-items-center mt-3">
        {{$redirects->links()}}
        <a href="{{ route('rating.admin.page-redirects.create') }}" class="btn btn-success">
            Создать
        </a>
    </div>
@endsection
