<div class="container m-3 p-3 m-auto">
    @include('rating.admin._includes.message_indicator')
    <div class="d-flex justify-content-center m-3">
        <div class="col-8">
            <div class="card p-3">
                <div class="form-group">
                    <label for="path_from">Перенаправлять с</label>
                    <input type="text" class="form-control"
                           name="path_from"
                           id="path_from"
                           value="{{old('path_from', $redirect->path_from)}}">
                </div>
                <div class="form-group mt-2">
                    <label for="path_to">Перенаправлять на</label>
                    <input type="text" class="form-control"
                           name="path_to"
                           id="path_to"
                           value="{{old('path_to', $redirect->path_to)}}">
                </div>
            </div>
        </div>
        @include('rating.admin._includes.timestamp_sidebar', ['obj' => $redirect])
    </div>
</div>
