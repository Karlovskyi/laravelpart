@extends('layouts.admin')

@section('content')
    @include('rating.admin.reviews.includes.message_indicator')
    <nav class="navbar navbar-light bg-light">
        <form class="d-flex col-6">
            <input class="form-control my-2 col-9" type="search"
                   placeholder="Поиск по автору и содержанию отзыва" aria-label="Search"
                   name="search"
                   value="{{$search}}">
            <button class="btn btn-outline-success my-2 offset-1 col-2" type="submit">Поиск</button>
        </form>
    </nav>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Имя оставившего</th>
            <th scope="col">Балл</th>
            <th scope="col">Дата отзыва</th>
            <th scope="col">Ссылки</th>
        </tr>
        </thead>
        <tbody>
        @foreach($reviews as $review)
            <tr @if(!$review->is_published) style="background-color: #ccc" @endif>
                <th scope="row">{{$review->review_id}}</th>
                <td>{{$review->reviewer_name}}</td>
                <td><span class="badge @if($review->positive) bg-success @else bg-danger @endif">{{$review->review_mark}}</span></td>
                <td>{{$review->review_date}}</td>
                <td>
                    <a href="{{route('rating.admin.reviews.show', $review->review_id)}}" class="badge bg-dark">Смотреть детально</a>
                    <a href="{{route('rating.admin.reviews.edit', $review->review_id)}}" class="badge bg-dark">Изменить</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="d-flex justify-content-center mt-3">
        {{$reviews->links()}}
    </div>
@endsection
