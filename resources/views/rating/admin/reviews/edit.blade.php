@extends('layouts.admin')

@section('content')
    <form action="{{route('rating.admin.reviews.update', $review->review_id)}}" method="POST">
        @method("PATCH")
        @csrf
        <div class="container mt-3 d-flex">
            <div class="col-8">
                @include('rating.admin.reviews.includes.edit_main_bar')
            </div>
            <div class="col-3 offset-1">
                @include('rating.admin.reviews.includes.edit_side_bar')
            </div>
        </div>
    </form>
@endsection
