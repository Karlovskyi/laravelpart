@extends('layouts.admin')

@section('content')
    <div class="container mt-3">
        <div class="card">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="main-tab" data-toggle="tab" href="#main" role="tab"
                       aria-controls="nav-main" aria-selected="true">Общее</a>
                    <a class="nav-item nav-link" id="text-tab" data-toggle="tab" href="#text" role="tab"
                       aria-controls="text" aria-selected="false">Текст</a>
                    <a class="nav-item nav-link" id="images-tab" data-toggle="tab" href="#images" role="tab"
                       aria-controls=imagest" aria-selected="false">Картинки</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="main-tab">
                    @include('rating.admin.reviews.includes.show_main')
                </div>
                <div class="tab-pane fade" id="text" role="tabpanel" aria-labelledby="text-tab">
                    <div class="p-3">{{$review->review_text}}</div>
                </div>
                <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
                    @include('rating.admin.reviews.includes.show_images')
                </div>
            </div>
        </div>
        <a href="{{route('rating.admin.reviews.edit', $review->review_id)}}" class="d-block m-3">
            <button class="btn btn-primary">Изменить</button>
        </a>
    </div>

@endsection
