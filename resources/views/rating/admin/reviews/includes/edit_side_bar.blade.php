<div class="card p-3">
    <button class="btn btn-danger" type="button" id="delete">
        Удалить
    </button>
    <button class="btn btn-success mt-3" type="submit">
        Сохранить
    </button>
    <div class="jumbotron">
        <div class="form-group mt-1">
            <label for="review_id">ID отзыва</label>
            <input type="text" disabled
                   value="{{$review->review_id}}"
                   class="form-control"
                   name="review_id"
                   id="review_id">
        </div>
        <div class="form-group mt-1">
            <label for="review_date">Дата отзыва</label>
            <input type="text" disabled
                   value="{{$review->review_date}}"
                   class="form-control"
                   name="review_date"
                   id="review_date">
        </div>
        <div class="form-group mt-1">
            <label for="company_name">Отзыв о компании</label>
            <input type="text" disabled
                   value="{{$review->company->company_name}}"
                   class="form-control"
                   name="company_name"
                   id="company_name">
        </div>
    </div>
</div>
