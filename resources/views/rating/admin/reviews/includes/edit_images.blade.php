<div class="d-flex flex-wrap m-3">
    @if($review->images->isEmpty())
        <div class="display-6 m-4 d-block w-100 text-center">
            У данного отзыва нет фото
        </div>
    @else
        @foreach($review->images as $image)
           <div class="m-1">
               Смотреть картинку: <a href="{{route('rating.admin.images.edit', $image->image_id)}}" class="badge bg-secondary">{{$image->image_path}}</a>
           </div>
        @endforeach

    @endif
</div>
