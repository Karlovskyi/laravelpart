<div class="jumbotron m-3">
        <div class="alert alert-light m-1 p-1">Отзыв оставил(а) <strong>{{$review->reviewer_name}}</strong></div>
        <div class="alert alert-light m-1 p-1">На платформе: {{$review->resource_id}}</div>
        <div class="alert alert-light m-1 p-1">Ссылка на отзыв:
            <a href="{{$review->review_link ?? '#'}}"
               class="badge bg-dark">{{$review->review_link ?? 'Ссылка отсутствует'}}</a>
        </div>
        <div class="alert alert-light m-1 p-1">Балл:
            <span class="badge @if($review->positive) bg-success @else bg-danger @endif">
                {{$review->review_mark}}
            </span>
        </div>
        <div class="alert alert-light m-1 p-1">
            Дата оставления: {{$review->review_date}}
        </div>
        <div class="alert alert-light m-1 p-1">
            Отзыв о компании: <a href="{{route('rating.admin.companies.show', $review->company_id)}}" class="badge bg-warning">
                {{$review->company->company_name}}
            </a>
        </div>
        <div class="alert alert-light m-1 p-1">
            Строго закрепленная позиция: @if($review->position === 0) Отсутствует @else {{$review->position}} @endif
        </div>
        <div class="alert alert-light m-1 p-1">
            @if($review->is_published)
                <span class="badge bg-success">Опубликовано</span>
            @else
                <span class="badge bg-dark">Не опубликовано</span>
            @endif
        </div>
</div>
