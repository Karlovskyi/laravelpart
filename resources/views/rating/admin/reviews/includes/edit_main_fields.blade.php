<div class="jumbotron p-3">
    <div class="form-group mt-1">
        <label for="reviewer_name">Имя оставившего отзыв</label>
        <input type="text"
               value="{{old('reviewer_name', $review->reviewer_name)}}"
               class="form-control"
               name="reviewer_name"
               id="reviewer_name">
    </div>
    <div class="form-group mt-1">
        <label for="review_link">Ссылка на отзыв</label>
        <input type="text"
               value="{{old('review_link', $review->review_link)}}"
               class="form-control"
               name="review_link"
               id="review_link">
    </div>
    <div class="form-group mt-1">
        <label for="review_mark">Балл</label>
        <input type="text"
               value="{{old('review_mark', $review->review_mark)}}"
               class="form-control"
               name="review_mark"
               id="review_mark">
    </div>
    <div class="form-group mt-1">
        <label for="position">Закреплен за местом</label>
        <input type="text"
               value="{{old('position', $review->position)}}"
               class="form-control"
               name="position"
               id="position">
    </div>

    <div class="form-group mt-1">
        <label for="resource_id">Ресурс</label>
        <select name="resource_id" id="resource_id" class="form-control">
            <option value="0" @if(old('resource_id', $review->resource_id) === 'Yandex') selected @endif>Yandex</option>
            <option value="1" @if(old('resource_id', $review->resource_id) === 'Google') selected @endif>Google</option>
            <option value="2" @if(old('resource_id', $review->resource_id) === 'Otzovick') selected @endif>Otzovick</option>
        </select>
    </div>
    <div class="form-group mt-1">
    <label for="company_id">Компания</label>
    <select name="company_id" id="company_id" class="form-control">
        /** @var \Illuminate\Database\Eloquent\Collection $companies */
        @foreach($companies as $company)
            <option value="{{$company->company_id}}"
            @if($company->company_id === old('company_id', $review->company_id)) selected @endif>{{$company->company_name}}</option>
        @endforeach
    </select>

    <div class="form-group mt-3 ml-2">
        <input type="hidden" value="0" name="is_published">

        <input type="checkbox" name="is_published" id="is_published" value="1" @if(old('is_published',$review->is_published)) checked @endif>
        <label for="is_published">Опубликовать</label>
    </div>
</div>
</div>
