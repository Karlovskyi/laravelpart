@include('rating.admin.reviews.includes.message_indicator')

<div class="card">
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="main-tab" data-toggle="tab" href="#main" role="tab"
               aria-controls="nav-main" aria-selected="true">Общее</a>
            <a class="nav-item nav-link" id="text-tab" data-toggle="tab" href="#text" role="tab"
               aria-controls="text" aria-selected="false">Текст</a>
            <a class="nav-item nav-link" id="images-tab" data-toggle="tab" href="#images" role="tab"
               aria-controls=imagest" aria-selected="false">Картинки</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="main-tab">
                @include('rating.admin.reviews.includes.edit_main_fields')
        </div>
        <div class="tab-pane fade" id="text" role="tabpanel" aria-labelledby="text-tab">
            <div class="form-group m-3">
                <label for="review_text">Текст отзыва</label>
                <textarea name="review_text" id="review_text" rows="10" class="form-control">{{$review->review_text}}</textarea>
            </div>
        </div>
        <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
            @include('rating.admin.reviews.includes.edit_images')
        </div>
    </div>
</div>
