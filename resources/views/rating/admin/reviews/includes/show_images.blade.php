<div class="d-flex flex-wrap">
    @if($review->images->isEmpty())
        <div class="display-6 m-4 d-block w-100 text-center">
            У данного отзыва нет фото
        </div>
    @else
        @foreach($review->images as $image)
            <img src="{{$image->image_path}}" alt="{{$review->reviewer_name}}" class="img-thumbnail m-3 ml-5 mr-5 rounded d-block w-25">
        @endforeach

    @endif
</div>
