<div class="col-3 offset-1">
    <div class="card p-3">
        <button class="btn btn-success" type="submit">Сохранить</button>
        <div class="form-group mt-2">
            <label for="updated_at">Последнее изменения</label>
            <input type="text" class="form-control"
                   name="updated_at"
                   id="updated_at"
                   value="{{old('updated_at', $obj->updated_at)}}"
                   disabled>
        </div>
        <div class="form-group mt-2">
            <label for="created_at">Создано</label>
            <input type="text" class="form-control"
                   name="created_at"
                   id="created_at"
                   value="{{old('created_at', $obj->created_at)}}"
                   disabled>
        </div>
    </div>
</div>
