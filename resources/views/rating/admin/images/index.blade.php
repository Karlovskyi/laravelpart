@extends('layouts.admin')

@section('content')
    <div class="d-flex flex-wrap">
            @foreach($images as $image)
            <a href="{{route('rating.admin.images.edit', $image->image_id)}}" class="d-block w-25 m-3 ml-5 mr-5">
                <img src="{{$image->image_path}}" alt="{{$image->image_path}}" class="img-thumbnail h-100 w-100 rounded d-block" style="object-fit: cover">
            </a>
            @endforeach
    </div>
    <div class="d-flex justify-content-center">
        {{$images->links()}}
    </div>
@endsection



