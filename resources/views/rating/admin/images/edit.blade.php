@extends('layouts.admin')

@section('content')
    <form action="{{route('rating.admin.images.update', $image->image_id)}}" method="POST">
        @method("PATCH")
        @csrf
        @include('rating.admin.images.includes.message_indicator')
        <div class="container card mt-3">

            <div class="card-body ">
                <img src="{{$image->image_path}}" alt="{{$image->image_path}}" class="img-thumbnail rounded d-block w-25 m-3 ml-5 mr-5 float-right">
                <button class="btn btn-success float-right mt-3" type="submit">Сохранить</button>
                <div class="form-group">
                    <label for="image_path">Ссылка</label>
                    <input type="text" name="image_path"
                           id="image_path" disabled
                           value="{{$image->image_path}}"
                           class="form-control w-50">
                </div>
                <div class="form-group mt-3 ml-2">
                    <input type="hidden" value="0" name="is_published">
                    <input type="checkbox" name="is_published"
                           id="is_published"
                           value="1"
                           @if(old('is_published',$image->is_published)) checked @endif>
                    <label for="is_published">Опубликовать</label>
                </div>
                <div class="form-group mt-3 ml-2">
                    <input type="hidden" value="0" name="image_on_company_page">

                    <input type="checkbox" name="image_on_company_page"
                           id="image_on_company_page"
                           value="1"
                           @if(old('image_on_company_page',$image->image_on_company_page)) checked @endif>
                    <label for="image_on_company_page">Добавить в список картинок опубликованых на странице о компании</label>
                </div>
            </div>
        </div>

    </form>
@endsection
