@extends('layouts.admin')

@section('content')
    @include('rating.admin._includes.message_indicator')
    <div class="card card-body m-3">
        <form action="{{route('rating.admin.pages.update-all')}}" method="post">
            @csrf
            <button class="btn btn-primary">Обновить все страницы</button>
        </form>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Строковой идентификатор</th>
            <th scope="col">Маршрут</th>
            <th scope="col">Последнее изменение</th>
            <th scope="col">Изменить</th>
            <th scope="col">Обновить</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pagination as $pageRow)
            <tr >
                <th scope="row">{{$pageRow->page_id}}</th>
                <td>{{$pageRow->page_str_index}}</td>
                <td>{{$pageRow->page_route_name}}</td>
                <td>{{$pageRow->last_modified}}</td>
                <td><a href="{{route('rating.admin.pages.edit',$pageRow->page_id)}}"  class="badge bg-dark">Изменить</a></td>
                <td>
                    <form action="{{route('rating.admin.pages.re-update',$pageRow->page_id)}}"  class="badge bg-dark" method="post">
                        @method('PATCH')
                        @csrf
                        <input name="page" type="hidden" value="{{ $pagination->currentPage() }}">
                        <button class="badge bg-dark border-0 text-white">Обновить</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <a href="{{ route('rating.admin.pages.create') }}" class="btn btn-success ml-3">Создать</a>
    <div class="justify-content-center d-flex mt-3">
        {{$pagination->links()}}
    </div>
@endsection
