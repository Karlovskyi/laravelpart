@extends('layouts.admin')

@section('content')
    @include('rating.admin._includes.message_indicator')
    <div class="container mt-1">
        <form action="{{route('rating.admin.pages.update', $page->page_id)}}" method="POST">
            @method("PATCH")
            @csrf
            <div class="d-flex justify-content-center">
                <div class="col-12">
                    @include('rating.admin.pages.includes.edit_main')
                </div>
            </div>

        </form>
    </div>
@endsection
