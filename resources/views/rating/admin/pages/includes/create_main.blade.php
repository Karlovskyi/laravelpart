<div class="card">
    <div class="card-body p-3 m-3">
        <div>
            <button class="btn btn-success">Сохранить</button>
        </div>
        <div class="mt-3">
            <div class="form-group">
                <label for="page_str_index">Название Сраницы</label>
                <input type="text"
                       value="{{old('page_str_index')}}"
                       required
                       minlength="3"
                       class="form-control"
                       id="page_str_index"
                       name="page_str_index">
            </div>
            <div class="form-group">
                <label for="page_route_name">Название Маршрута</label>
                <input type="text"
                       value="{{old('page_route_name')}}"
                       required
                       minlength="3"
                       class="form-control"
                       id="page_route_name"
                       name="page_route_name">
            </div>
        </div>
    </div>
</div>
