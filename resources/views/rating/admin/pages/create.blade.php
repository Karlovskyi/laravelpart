@extends('layouts.admin')

@section('content')
    @include('rating.admin._includes.message_indicator')
    <div class="container mt-1">
        <form action="{{route('rating.admin.pages.store')}}" method="POST">
            @method("POST")
            @csrf
            <div class="d-flex justify-content-center">
                <div class="col-12">
                    @include('rating.admin.pages.includes.create_main')
                </div>
            </div>

        </form>
    </div>
@endsection
