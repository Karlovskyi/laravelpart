<?php echo '<?xml version="1.0" encoding="UTF-8"?>' ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>{{ route('user.main') }}</loc>
        <lastmod>2021-03-17</lastmod>
        <changefreq>monthly</changefreq>
        <priority>1</priority>
    </url>
    <url>
        <loc>{{ route('user.reliability') }}</loc>
        <lastmod>2021-03-17</lastmod>
        <changefreq>monthly</changefreq>
        <priority>0.9</priority>
    </url>
    <url>
        <loc>{{ route('user.how-to-choose') }}</loc>
        <lastmod>2021-03-17</lastmod>
        <changefreq>monthly</changefreq>
        <priority>0.7</priority>
    </url>
    <url>
        <loc>{{ route('user.about') }}</loc>
        <lastmod>2021-03-17</lastmod>
        <changefreq>monthly</changefreq>
        <priority>0.7</priority>
    </url>
    @foreach($companies as $company)
        <url>
            <loc>{{ route('user.company', $company->company_slug) }}</loc>
            <lastmod>{{$company->LastModifiedPage->toAtomString()}}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach
</urlset>
