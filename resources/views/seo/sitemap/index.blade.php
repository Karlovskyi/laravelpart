<?php echo '<?xml version="1.0" encoding="UTF-8"?>' ?>

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <sitemap>
        <loc>{{ route('seo.sitemap.pages') }}</loc>
        <lastmod>2021-03-12</lastmod>
    </sitemap>
</sitemapindex>
