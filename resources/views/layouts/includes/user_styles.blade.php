@php
use Jenssegers\Agent\Agent;
$agent = new Agent();
@endphp
<link rel="preload" href="/fonts/Roboto-Regular.ttf'" as="font" type="font/truetype" crossorigin>
<link rel="preload" href="/fonts/Roboto-Bold.ttf'" as="font" type="font/truetype" crossorigin>
@if($agent->isMobile())
    <link rel="stylesheet" href="{{ asset('css/app.1.3.22.css') }}" media="none" onload="if(media!='all')media='all'">
    <noscript><link rel="stylesheet" href="{{ asset('css/app.1.3.22.css') }}"></noscript>
@else
    <link rel="stylesheet" href="{{ asset('css/app.1.3.22.css') }}">
@endif
