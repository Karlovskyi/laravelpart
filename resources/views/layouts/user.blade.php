<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="<?php echo csrf_token() ?>">
    <title>{{$seo['title'] ?? ''}}</title>
    <meta name="description"
          content="{{$seo['description'] ?? ''}}">
    @if($seo['keywords'] ?? null)
        <meta name="keywords" content="{{$seo['keywords'] ?? ''}}" />
    @endif
    @include('layouts.includes.user_styles')

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon-16x16.png')}}">
    <link rel="manifest" href="{{ asset('images/site.webmanifest')}}">
    <link rel="mask-icon" href="{{ asset('images/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    @section('seo_content') @show

    @include('counters.google-analytics')
</head>
<body>
    <header class="nav @section('nav') @show">
        <nav class="nav__main">
            <a href="{{ route('user.main') }}" class="nav__link nav__link_main">
                <img src="{{asset('images/logo.svg')}}"
                     alt="Логотип Сайта remont-rate.ru"
                     title="Логотип Сайта remont-rate.ru"
                     class="nav__logo"
                     width="45" height="40"
                     loading="lazy">
                <div class="header header-sub italic">Remont-rate</div>
            </a>
            <div class="nav__list-wrapper">
                <ul class="nav__list">
                    <li class="nav__list-item">
                        <a href="{{ route('user.compare-all') }}" class="nav__link">Сравнить все</a>
                    </li>
                    <li class="nav__list-item">
                        <a href="{{ route('user.main') }}" class="nav__link">Топ по отзывам</a>
                    </li>
                    <li class="nav__list-item">
                        <a href="{{ route('user.reliability') }}" class="nav__link">Топ по надёжности</a>
                    </li>
                    <li class="nav__list-item">
                        <a href="{{ route('user.how-to-choose') }}" class="nav__link">Как выбрать</a>
                    </li>
                    <li class="nav__list-item">
                        <a href="{{ route('user.about') }}" class="nav__link">О нас</a>
                    </li>
                    @if(false)
                        <li class="nav__list-separator"></li>
                        @if(Auth::guest())
                            <li class="nav__list-item">
                                <a href="{{ route('login') }}" class="nav__link nav__user-link" data-content="login" aria-label="Войти в аккаунт">
                                    <span class="nav__user-text">Войти в аккаунт</span>
                                </a>
                            </li>
                        @else
                            <li class="nav__list-item">
                                <a href="{{ route('user.account') }}" class="nav__link nav__user-link" data-content="account" aria-label="Управление аккаунтом">
                                    <span class="nav__user-text">Управление аккаунтом</span>
                                </a>
                            </li>
                        @endif
                    @endif
                </ul>
            </div>
            <div class="nav__burger">
                <span class="nav__burger-line"></span>
                <span class="nav__burger-line"></span>
                <span class="nav__burger-line"></span>
            </div>
        </nav>
    </header>
    <main class="container @section('main-class') @show">
        @yield('content')
    </main>
    <footer class="footer container @section('footer') @show">
        <nav>
            <ul class="footer__list">
                <li class="footer__list-item">
                    <a href="{{ route('user.compare-all') }}" class="footer__link">Сравнить все</a>
                </li>
                <li class="footer__list-item">
                    <a href="{{ route('user.main') }}" class="footer__link">Топ по отзывам</a>
                </li>
                <li class="footer__list-item">
                    <a href="{{ route('user.reliability') }}" class="footer__link">Топ по надёжности</a>
                </li>
                <li class="footer__list-item">
                    <a href="{{ route('user.how-to-choose') }}" class="footer__link">Как выбрать</a>
                </li>
                <li class="footer__list-item">
                    <a href="{{ route('user.about') }}" class="footer__link">О нас</a>
                </li>
            </ul>
        </nav>

        <address class="footer__date ta-c">
            <a href="mailto:" class="footer__link">info@remont-rate.ru</a>
        </address>
    </footer>
    @section('scripts')
        <script defer src="{{ asset('js/app.1.3.22.js') }}"></script>
    @show
    @include('counters.yandex-metrika')
    @include('counters.roistat-counter')
</body>
</html>
