<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AdminPage</title>
    <link href="{{ asset('css/admin.1.3.22.css') }}" rel="stylesheet">

</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark pl-3 pr-3">
        <a class="navbar-brand" href="{{route('rating.admin')}}">Админка</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('rating.admin.companies.index')}}">Компании</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('rating.admin.reviews.index')}}">Отзывы</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('rating.admin.images.index')}}">Картинки</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('rating.admin.page-search-attributes.index')}}">SEO параметры</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('rating.admin.headers.index')}}">Заголовки на страницах</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('rating.admin.criteria.index')}}">Критерии сравнения</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('rating.admin.constants.index')}}">Константы</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('rating.admin.page-redirects.index')}}">301-е Редиректы</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('rating.admin.pages.index')}}">Страницы</a>
                </li>
            </ul>
        </div>
    </nav>
    <main>
        @yield('content')
    </main>
    <script defer src="{{ asset('js/admin.1.3.22.js') }}"></script>
</body>
</html>
