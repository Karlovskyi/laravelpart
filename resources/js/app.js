const {addQuestionToggling, hideOnBodyClick} = require('./src/tips_control/addQuestionToggling')
import footerToBottom from "./src/footer";
import {dropdown} from "./src/dropdown/dropdown";
import {arcDrow, lineDrow} from "./src/graphic";

document.addEventListener('DOMContentLoaded', () => {
    require('./src/burger_toggle');
    require('./src/star_painting').starPainting();
    require('./src/redirect');
    arcDrow();
    lineDrow();
    require('./src/tips_show');
    require('./src/print_circle');
    require('./src/main_form_submit');
    dropdown();
    require('./src/how_calculate');
    require('./src/side_form_appear');
    addQuestionToggling();
    hideOnBodyClick();
    footerToBottom();
})
