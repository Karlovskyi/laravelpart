const photoZoom = require('./src/photo_zoom/index')
const scrollToRely = require('./src/scrollToRely')

document.addEventListener('DOMContentLoaded', () => {
    // require('./api/reviews'); Было принято решение перейти на пагинацию
    require('./src/carousel').carousel();
    require('./src/question_toggle');
    require('./src/more-less')
    photoZoom();
    // scrollToRely(); Удалено в версии 1.0.2
})
