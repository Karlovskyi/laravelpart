const print_circle = () => {
    const circles = document.querySelectorAll('.circle')
    circles.forEach(item => {
        const grade = Number(item.dataset.content);
        let color;
        if (grade === 2) {
            color = '#138700';
        } else if(grade === 1) {
            color = '#ffb800';
        } else {
            color = '#ff0000';
        }
        item.style.background = color;
    })
}

print_circle();
