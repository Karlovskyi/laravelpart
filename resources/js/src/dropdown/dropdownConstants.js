const dropdownConstants = {
    disableClass: 'dropdown__main_disable',
    dropdowns: []
}
const initializeConstants = () => {
    dropdownConstants.dropdowns = document.querySelectorAll('.dropdown')
}

export {
    dropdownConstants,
    initializeConstants
}
