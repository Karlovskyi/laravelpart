import {
    dropdownConstants
} from "./dropdownConstants";

const closeDropdown = (dropdown_main) => {
    const {disableClass} = dropdownConstants;
    dropdown_main.classList.add(disableClass)
}

const toggleDropdown = (dropdown_main) => {
    const {disableClass} = dropdownConstants;
    dropdown_main.classList.toggle(disableClass)
}

const onBodyClick = (e) => {
    const {dropdowns} = dropdownConstants
    dropdowns.forEach(dropdown => {
        if(e.path.indexOf(dropdown) === -1) {
            const dropdown_main = dropdown.querySelector('.dropdown__main');
            closeDropdown(dropdown_main)
        }
    });
}

const toggleButtonColor = (button) => {
    button.textContent = button.textContent.trim() === 'Сравнить с...' ? 'Добавлено к сравнению' : 'Сравнить с...'
    button.classList.toggle('dropdown__btn_selected')
    button.classList.toggle('btn_light');
    button.classList.toggle('btn_dark');
}

export {
    closeDropdown,
    toggleDropdown,
    onBodyClick,
    toggleButtonColor
}
