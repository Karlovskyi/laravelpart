const formSubmit = (dropdown_main) => {
    const compareArr = dropdown_main.querySelectorAll('.dropdown__compare-item');

    compareArr.forEach(item => {
        item.addEventListener('click', (e) => {
            e.preventDefault(); // For safari
            const form = document.forms.compare;
            form.insertAdjacentHTML('beforeend',
                `<input value="${item.dataset.base}" name="slug[2]" type="hidden">`);
            form.insertAdjacentHTML('beforeend',
                `<input value="${dropdown_main.dataset.base}" name="slug[1]" type="hidden">`);
            form.submit()
        })
    })
}

export {
    formSubmit
}
