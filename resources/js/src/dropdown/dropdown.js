import {fixDropdownOverflow} from "./dropdownOverflow";
import {toggleDropdown, onBodyClick, closeDropdown, toggleButtonColor} from "./hiddingDropdown";
import {formSubmit} from "./dropdownFormSubmit";

import {
    dropdownConstants,
    initializeConstants
} from "./dropdownConstants";

const dropdown = () => {
    initializeConstants();
    const {dropdowns} = dropdownConstants;

        dropdowns.forEach(dropdown => {
            const button = dropdown.querySelector('.dropdown__btn');
            const dropdown_main = dropdown.querySelector('.dropdown__main');
            const close = dropdown.querySelector('.dropdown__close');

            button.addEventListener('click', (e) => {
                e.preventDefault(); // For safari
                if(dropdowns.length > 1) {
                    toggleButtonColor(button);
                    fixDropdownOverflow(dropdown, dropdowns.length)
                }
                toggleDropdown(dropdown_main)
            });
            close.addEventListener('click', (e) => {
                e.preventDefault();
                closeDropdown(dropdown_main)
            })
            formSubmit(dropdown_main);
        })

    document.body.addEventListener('click', onBodyClick);
}


export {
    dropdown
}
