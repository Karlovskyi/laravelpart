const fixDropdownOverflow = (dropdown, itemsCount) => {
    const offsetTop = dropdown.getBoundingClientRect().top + window.scrollY;
    const docHeight = document.body.clientHeight;
    const itemHeight = dropdown.querySelector('.dropdown__compare-item').clientHeight;
    const backlash = 200;
    const footerHeight = document.querySelector('footer').clientHeight;
    if(docHeight - (offsetTop + itemHeight * itemsCount + backlash) < 0) {
        //add overflow to dropdown if it's lower that page height
        const mainList = dropdown.querySelector('.dropdown__main');
        const overflowList = dropdown.querySelector('.dropdown__wrapper-overflow ');
        const maxHeight = docHeight - offsetTop - footerHeight - dropdown.clientHeight;
        overflowList.style.maxHeight = `${maxHeight}px`;
        mainList.style.overflowY = 'scroll';

    }
}

export {
    fixDropdownOverflow
}
