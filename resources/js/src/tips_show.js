const tipsShow = () => {
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        return
    }
    const blocks = document.querySelectorAll('.criteria')
    blocks.forEach(block => {
        const needTip = Number(block.dataset.tip || 0);
        const tip = block.querySelector('.criteria__tip');
        if(tip && needTip) {
            block.addEventListener('mouseenter', () => {
                tip.classList.add('criteria__tip_active');
            })
            block.addEventListener('mouseleave', () => {
                tip.classList.remove('criteria__tip_active');
            })
        }
    })
}

tipsShow();
