const scrollToRely = () => {
    const offset = document.querySelector('#rely').offsetTop;
    console.log(offset)
    if(!(window.location.href.indexOf('#reviews') + 1)) {
        window.scrollTo({
            top: offset,
            behavior: "smooth"
        });
    }
}

module.exports = scrollToRely;
