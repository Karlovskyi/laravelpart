const sideFormAppear = () => {
    const secondBlock = document.querySelectorAll('.rating-list__li')[2] // 3-блок
    const form = document.querySelector('.side-form-submit__btn');
    if(secondBlock && form) {

        window.addEventListener('scroll', () => {
            const offsetToAppear = secondBlock.getBoundingClientRect().top -  window.innerHeight;
            if(offsetToAppear < 0) {
                form.classList.add('side-form-submit__btn_appear')
            } else {
                form.classList.remove('side-form-submit__btn_appear')
            }
        })
    }

}

sideFormAppear();
