const addQuestionToggling = () => {
    const questions = document.querySelectorAll('.tip')
    questions.forEach(q => {
        q.addEventListener('click', () => {
            q.classList.toggle('tip_disable')
        })
    })
}


const hideOnBodyClick = () => {

    const body = document.querySelector('body');
    const questions = document.querySelectorAll('.tip');
    body.addEventListener('click', (e) => {
        if(!e.target.classList.contains('tip__question')) {
            questions.forEach(q => q.classList.add('tip_disable'))
        }

    });

}

module.exports = {addQuestionToggling, hideOnBodyClick};

