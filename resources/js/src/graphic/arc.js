import constants from "./constants";

const arcDrow = () => {
    const {redColor, greenColor, yellowColor, darkGrayColor} = constants;
    const canvasDiv = document.querySelectorAll('.graphic-arc')
    canvasDiv.forEach(div => {
        const division = div.dataset.division;
        const attention = div.dataset.attention;
        let color;
        if(attention) {
            color = redColor;
        } else if (division > 80) {
            color = greenColor;
        } else if(division > 60) {
            color = yellowColor;
        } else {
            color = redColor;
        }

        const percent = div.querySelector('.graphic-arc__percent');
        const text = div.querySelector('.graphic-arc__text');

        text.style.color = color;
        percent.style.color = color;

        const canvas = div.querySelector('.graphic-arc__canvas');
        const ctx= canvas.getContext("2d");
        ctx.beginPath();
        ctx.arc(50,50,35,(-0.5)*Math.PI,(division/50 - 0.5)*Math.PI);
        ctx.strokeStyle = color;
        ctx.lineWidth = 2;
        ctx.stroke()
    })


}

export {
    arcDrow
}
