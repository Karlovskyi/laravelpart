import constants from "./constants";

const lineDrow = () => {
    const {darkRedColor, greenColor, yellowColor, darkGrayColor} = constants;
    const canvasDiv = document.querySelectorAll('.graphic-line');

    canvasDiv.forEach(div => {
        const height = div.clientHeight;
        const width = div.clientWidth;
        const division = div.dataset.division;
        const attention = div.dataset.attention;
        let color;
        if (attention) {
            color = darkRedColor;
        } else if (division > 80) {
            color = greenColor;
        } else if (division > 60) {
            color = yellowColor;
        } else {
            color = darkRedColor;
        }

        const percent = div.querySelector('.graphic-line__percent');
        const text = div.querySelector('.graphic-line__text');

        text.style.color = color;
        percent.style.color = color;

        const canvas = div.querySelector('.graphic-line__canvas');
        const ctx = canvas.getContext("2d");
        ctx.fillStyle = darkGrayColor;
        ctx.fillRect(0,0, width, height)
        ctx.fillStyle = color;
        ctx.fillRect(0,0, division / 100 * width, height);
    })
}

export {
    lineDrow
}
