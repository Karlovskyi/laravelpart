const constants = {
    redColor: '#ff0000',
    darkRedColor: '#870000',
    yellowColor: '#ffb800',
    greenColor: '#138700',
    darkGrayColor: '#dedede'
}

export default constants;
