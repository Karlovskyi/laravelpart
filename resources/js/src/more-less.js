const moreLessRely = () => {
    const links = document.querySelector('.reliability__links_rely');
    const more = links.querySelector('#more-1');
    const less = links.querySelector('#less-1');
    const content = document.querySelector('#resizeable')

    more.addEventListener('click', () => {
        less.style.display = 'block';
        more.style.display = 'none';
        content.classList.toggle('reliability__content_short');
    });
    less.addEventListener('click', () => {
        more.style.display = 'block';
        less.style.display = 'none';
        content.classList.toggle('reliability__content_short');
    });
}

moreLessRely();

const moreLessDescription = () => {
    const links = document.querySelector('.reliability__links_description');
    const more = links.querySelector('#more-2');
    const less = links.querySelector('#less-2');
    const content = document.querySelector('.portfolio__description')

    more.addEventListener('click', () => {
        less.style.display = 'block';
        more.style.display = 'none';
        content.classList.toggle('portfolio__description-full');
    });
    less.addEventListener('click', () => {
        more.style.display = 'block';
        less.style.display = 'none';
        content.classList.toggle('portfolio__description-full');
    });
}

moreLessDescription()
