const questionToggle = () => {
    const questions = document.querySelectorAll('.reliability__question');
    const tips = document.querySelectorAll('.reliability__tip')

    questions.forEach((q, i) => {
        const tip = tips[i];
        q.addEventListener('click', () => {
            tip.classList.toggle('reliability__tip_disable')
        })
    })
    tips.forEach(tip => {
        tip.addEventListener('click', () => {
            tip.classList.toggle('reliability__tip_disable');
        })
    })
}

questionToggle();
