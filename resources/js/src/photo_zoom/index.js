const addZoomToPhotos = () => {
    const photos = document.querySelectorAll('.company-reviews__item-image');
    const zoom = document.querySelector('.photo-zoom');
    const close = zoom.querySelector('.photo-zoom__close');
    const image = zoom.querySelector('.photo-zoom__img');
    const wrapper = zoom.querySelector('.photo-zoom__img-wrapper');
    const body = document.querySelector('body');

    const closeF = () => {
        image.classList.toggle('photo-zoom__img_disable')
        zoom.classList.toggle('photo-zoom_disable');
        close.classList.remove('photo-zoom__close_enter');
        close.classList.add('photo-zoom__close_exit');
        body.style.overflow = '';
    }

    photos.forEach(photo => {
        photo.addEventListener('click', () => {
            image.setAttribute('src', photo.getAttribute('src'))
            image.classList.toggle('photo-zoom__img_disable')
            zoom.classList.toggle('photo-zoom_disable');
            close.classList.add('photo-zoom__close_enter');
            close.classList.remove('photo-zoom__close_exit');
            body.style.overflow = 'hidden';
        })
    })
    close.addEventListener('click', closeF)
    wrapper.addEventListener('click', ({target}) => {
        if(target === wrapper) {
            closeF()
        }
    })
}

module.exports = addZoomToPhotos;
