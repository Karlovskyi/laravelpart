const toggleCalculation = () => {
    const showButton = document.querySelector('.best-choice__btn');
    const hideButton = document.querySelector('.how-calculate__close');

    const doc = document.querySelector('.how-calculate');
    const body = document.querySelector('body')

    if(doc)
    doc.addEventListener('click', (e) => {
        if (e.target === doc) {
            doc.classList.add('how-calculate_disable');
            body.style.overflow = '';
        }
    })
    if (showButton && hideButton && doc) {
        showButton.addEventListener('click', () => {
            doc.classList.remove('how-calculate_disable');
            body.style.overflow = 'hidden';
        })
        hideButton.addEventListener('click', () => {
            doc.classList.add('how-calculate_disable');
            body.style.overflow = '';
        })
    }
}

// toggleCalculation();
