const redirect = () => {
    document.querySelectorAll('.rating-list__item, .rating-card')
        .forEach(item => {
            const dropdown = item.querySelector('.dropdown');
            const links = item.querySelectorAll('.text-dark-link[data-type="link"]');

            item.addEventListener('click', (e) => {
                if (e.path.indexOf(dropdown) !== -1) {
                    e.preventDefault();
                }
                links.forEach(link => {
                    if(e.path.indexOf(link) !== -1) {
                        e.preventDefault();
                        document.location.href = (`${item.getAttribute('href')}#${link.dataset.direction}`);
                    }
                })
            });

        })
}

redirect();


