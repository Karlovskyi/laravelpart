const starPainting = () => {
    document.querySelectorAll('.stars__gray')
        .forEach(item => {
            const rate = Number(item.dataset.content);
            item.querySelector('span').style.width =  `${rate*20}%`;
        })
};

module.exports = {
    starPainting
}
