const Data = {
    currentIndex: 0,
    photos: null,
    mainPhoto: null,
    backgroundPhoto: null,
    carousel: null,
    carouselWidth: null,
    carouselItemWidth: null,
    wrapper: null

}

const carousel = () => {
    Data.photos = Array.apply(null, document.querySelectorAll('.portfolio__item'));
    Data.mainPhoto = document.querySelector('.portfolio__img');
    Data.backgroundPhoto = document.querySelector('.portfolio__img-bg');
    Data.carousel = document.querySelector('.portfolio__carousel');
    Data.carouselItemWidth = window.innerWidth > 992 ? 160 : 140;
    Data.wrapper = document.querySelector('.portfolio__img-wrapper');

    const controlLeft = document.querySelector('.portfolio__controls_left');
    const controlRight = document.querySelector('.portfolio__controls_right');

    const left = document.querySelector('.portfolio__left');
    const right = document.querySelector('.portfolio__right');


    const reducer = (acc, curr) => acc + (curr.offsetWidth || Data.carouselItemWidth) + 15;

    const movement = window.innerWidth > 992 ? 300: 200;
    Data.carouselWidth = Data.photos.reduce(reducer, 0);


    Data.photos.forEach(photo => {
        photo.addEventListener('click', () => setAnotherMainPhoto(photo))
    })
    left.addEventListener('click', () => {
        carouselLeft(movement);
    })

    right.addEventListener('click', () => {
        carouselRight(movement)
    })
    if(controlLeft)
        controlLeft.addEventListener('click', swipeLeft);
    if(controlRight)
        controlRight.addEventListener('click', swipeRight);
    if(Data.mainPhoto)
        swipe();
    if(Data.mainPhoto && Data.wrapper)
        scaleImage();
}


const setAnotherMainPhoto = (photo) => {
    Data.currentIndex = Data.photos
        .findIndex(it => it.getAttribute('src') === photo.getAttribute('src'))
    Data.mainPhoto.setAttribute('src', photo.getAttribute('src'));
    Data.backgroundPhoto.setAttribute('src', photo.getAttribute('src'));
    Data.photos.forEach(item => item.classList.remove('portfolio__item_selected'));
    photo.classList.add('portfolio__item_selected');
    setTimeout(() => {
        Data.mainPhoto.classList.remove('portfolio__img_left');
        Data.mainPhoto.classList.remove('portfolio__img_right');
        Data.mainPhoto.classList.add('portfolio__img_appear')
        setTimeout(() => {
            Data.mainPhoto.classList.remove('portfolio__img_appear')
        }, 300)
        Data.mainPhoto.style.left = '0';
    }, 200)

}

module.exports = {carousel: carousel}

const scaleImage = () => {

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        Data.mainPhoto.addEventListener('click', () => {
            Data.wrapper.classList.toggle('portfolio__img-wrapper_animated');
            Data.wrapper.classList.remove('portfolio__img-wrapper_scaled');

            setTimeout(() => {
                if(Data.wrapper.classList.contains('portfolio__img-wrapper_animated')) {
                    Data.wrapper.classList.add('portfolio__img-wrapper_scaled');
                }
            }, 300)

            const body = document.querySelector('body');
            if(body.style.overflow === '') {
                body.style.overflow = 'hidden'
            } else {
                body.style.overflow = ''
            }
        })
    }

}

const swipe = () => {
    const data = {
        start: null
    }
    Data.mainPhoto.addEventListener('touchstart', (e) => {
        data.start = e.changedTouches[0].clientX;
    })

    Data.mainPhoto.addEventListener('touchend', (e) => {
        const end = e.changedTouches[0].clientX;
        const res = data.start - end;
        if(res > 100) {
            swipeRight();
        } else if(res < -100) {
            swipeLeft();
        } else {
            stay()
        }
    })

    Data.mainPhoto.addEventListener("touchmove", (e) => {
        const delta =  e.changedTouches[0].clientX - data.start;
        Data.mainPhoto.style.left = `${delta}px`;
    });
}

const swipeLeft = () => {
    const index = Data.currentIndex - 1 >= 0 ? Data.currentIndex - 1 : Data.photos.length - 1;
    Data.mainPhoto.classList.add('portfolio__img_left')
    setTimeout(() => {
        setAnotherMainPhoto(Data.photos[index])
        carouselLeft(Data.carouselItemWidth + 12)
    }, 200)


}

const swipeRight = () => {
    const index = Data.currentIndex + 1 <= Data.photos.length - 1? Data.currentIndex + 1 : 0;
    Data.mainPhoto.classList.add('portfolio__img_right')
    setTimeout(() => {
        setAnotherMainPhoto(Data.photos[index]);
        carouselRight(Data.carouselItemWidth + 12)
    }, 200)

}

const stay = () => {
    Data.mainPhoto.style.left = '0'
}

const carouselLeft = (movement) => {
    const left = parseInt(Data.carousel.style.left) || 0;

    if(left === 0) {
        Data.carousel.style.left = `-${Data.carouselWidth - Data.carousel.offsetWidth}px`;
        return;
    }

    const sum = left + movement > 0 ? 0 : left + movement;
    Data.carousel.style.left = `${sum}px`;
}

const carouselRight = (movement) => {
    const left = parseInt(Data.carousel.style.left) || 0;
    if(left <= Data.carousel.offsetWidth - Data.carouselWidth) {
        Data.carousel.style.left = '0px';
        return;
    }

    const sum = left - movement < Data.carousel.offsetWidth - Data.carouselWidth ?
        Data.carousel.offsetWidth - Data.carouselWidth :
        left - movement;
    Data.carousel.style.left = `${sum}px`;
}
