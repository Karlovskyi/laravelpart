const formSubmit = (form) => {
    form.addEventListener('submit', () => eventListener(form))
};

const linkStopper = () => {

    const link = document.querySelector('#form-link')
    if(link)
    link.addEventListener('click', (e) => {
        e.preventDefault()
        const form = document.forms.compare;
        if(form) {
            eventListener(form);
            form.submit()
        }
    })
}

linkStopper()

const mainFormSubmit = () => {
    const form = document.forms.compare;
    if(form)
        formSubmit(form);
}

mainFormSubmit();

const sideFormSubmit = () => {
    const form = document.forms.side;
    if(form)
        formSubmit(form);
}


sideFormSubmit();

const eventListener = (form) => {
    const choosen = document.querySelectorAll('.btn_dark.btn_compare')
    choosen.forEach((item, index) => {

        form.insertAdjacentHTML('beforeend',
            `<input value="${item.dataset.content}" name="slug[${index}]" type="hidden">`);
    })
}
