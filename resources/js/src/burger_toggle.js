const toggleBurger = () => {
    const burger = document.querySelector('.nav__burger');
    const wrapper = document.querySelector('.nav__list-wrapper');
    const list = document.querySelector('.nav__list');
    burger.addEventListener('click', () => {
        burger.classList.toggle('nav__burger_active')
        wrapper.classList.toggle('nav__list-wrapper_active');
        list.classList.toggle('nav__list_active');

    })
}

toggleBurger();
