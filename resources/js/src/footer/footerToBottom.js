const footerToBottomCore = () => {
    const footer = document.querySelector('.footer');
    const body = document.querySelector('body')
    const {height: footerHeight} = footer.getBoundingClientRect();
    const {bottom: height} = body.getBoundingClientRect();


    if(window.innerHeight > height) {
        footer.style.position = 'absolute';
        footer.style.bottom = 0;
        footer.style.left = 0;
        body.style.minHeight = '100vh'
        body.style.paddingBottom = footerHeight + 'px';
    }
}

const footerToBottom = () => window.onload = footerToBottomCore;


export default footerToBottom;
