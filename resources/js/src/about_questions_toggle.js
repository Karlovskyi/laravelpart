const ToggleFAQ = () => {
    const cards = document.querySelectorAll('.card');
    const answers = Array.from(document.querySelectorAll('[data-content="a"]'));
    answers.forEach(ans => {
        ans.classList.add('about__paragraph_disable');
        ans.style.maxHeight = 0;
    })
    cards.forEach((card, index) => {
        const arrow = card.querySelector('i');
        card.addEventListener('click', () => {
            const ans = answers[index];
            card.classList.toggle('card_open')
            arrow.classList.toggle('about__toggle-arrow_active')
            ans.classList.toggle('about__paragraph_disable');
            const newMaxHeight = `${ans.scrollHeight}px`;

            ans.style.maxHeight = answers[index].style.maxHeight !== newMaxHeight ? newMaxHeight: 0;
        })
    })
};

export {
    ToggleFAQ
}
