import {reliabilityPercent} from "../models/Company";

const alphabetIncSort = (first, second) => {
    const key = 'company_name';
    if(first[key] > second[key]) {
        return 1
    } else if(first[key] < second[key]) {
        return -1;
    } else return 0
}

const alphabetDecSort = (first, second) => {
    const key = 'company_name';
    if(first[key] > second[key]) {
        return -1
    } else if(first[key] < second[key]) {
        return 1;
    } else return 0
}

const relyDecSort = (appConstants, payAttentionConstants) => (first, second) => {
    if(reliabilityPercent(appConstants, payAttentionConstants, first) > reliabilityPercent(appConstants, payAttentionConstants, second)) {
        return -1
    } else if(reliabilityPercent(appConstants, payAttentionConstants, first) < reliabilityPercent(appConstants, payAttentionConstants, second)) {
        return 1;
    } else return 0
}

const relyIncSort = (appConstants, payAttentionConstants) => (first, second) => {
    if(reliabilityPercent(appConstants, payAttentionConstants, first) > reliabilityPercent(appConstants, payAttentionConstants, second)) {
        return 1
    } else if(reliabilityPercent(appConstants, payAttentionConstants, first) < reliabilityPercent(appConstants, payAttentionConstants, second)) {
        return -1;
    } else return 0
}

const companySorting = (appConstants, payAttentionConstants) => (sorting) => {
    switch (sorting) {
        case 'rely-dec': return relyDecSort(appConstants, payAttentionConstants);
        case 'rely-inc': return relyIncSort(appConstants, payAttentionConstants);
        case 'alphabet-inc': return alphabetIncSort;
        case 'alphabet-dec': return alphabetDecSort;
        default: return undefined;
    }
}

export default companySorting;
