const companyFilter = (filters) => (company) => {
    let pass = true

    for(const i in filters) {
        const filter = filters[i];
        const companyField = company[i];
        if(filterIsTurnedOn(filter))
            switch (filter.key) {
                case 'company_term_of_work': {
                    if(filter[0].state === false && companyField >= 4)
                        pass = false;
                    if(filter[1].state === false && companyField < 4)
                        pass = false;
                } break;

                case 'company_change_of_leader': {
                    if(filter[0].state === false && companyField === 2)
                        pass = false;
                    if(filter[1].state === false && companyField < 2)
                        pass = false;
                } break;
                case 'company_legal_address': {
                    if(filter[0].state === false && companyField === 2)
                        pass = false;
                    if([1].state === false && companyField === 0)
                        pass = false;
                } break;
                case 'company_search_for_colleagues': {
                    if(filter[0].state === false && companyField === 2)
                        pass = false;
                    if(filter[1].state === false && companyField < 2)
                        pass = false;
                } break;
                case 'company_staff_members': {
                    if(filter[0].state === false && companyField >= 20)
                        pass = false;
                    if(filter[1].state === false && companyField < 20)
                        pass = false;
                } break;
                case 'company_payment_of_wages': {
                    if(filter[0].state === false && companyField >= 1)
                        pass = false;
                    if(filter[1].state === false && companyField === 0)
                        pass = false;
                } break;
                case 'company_income_dynamics': {
                    if(filter[0].state === false && companyField >= 1)
                        pass = false;
                    if(filter[1].state === false && companyField === 0)
                        pass = false;
                } break;
                case 'company_pay_attention': {
                    if(filter[0].state === false && (companyField === null || companyField === ''))
                        pass = false;
                    if(filter[1].state === false && companyField !== null && companyField !== '')
                        pass = false;
                } break
                case 'company_amount_of_credit': {
                    if(filter[0].state === false && companyField >= 0 && companyField < 100000)
                        pass = false;
                    if(filter[1].state === false && companyField >= 100000 && companyField < 1000000)
                        pass = false;
                    if(filter[2].state === false  && companyField >= 1000000)
                        pass = false;
                } break;
                case 'company_profitability': {
                    if(filter[0].state === false && companyField >= 2000000)
                        pass = false;
                    if(filter[1].state === false && companyField >= 1000000 && companyField < 2000000)
                        pass = false;
                    if(filter[2].state === false && companyField <= 0)
                        pass = false;
                } break
                case 'company_enforcement_proceedings': {
                    if(filter[0].state === false && companyField === 2)
                        pass = false;
                    if(filter[1].state === false && companyField === 1)
                        pass = false;
                } break;

                case 'companies': {
                    if(findCompanyFilter(company.company_name, filter) === false)
                        pass = false
                } break;
            }
    }
    return pass
};

export default companyFilter;

const findCompanyFilter = (companyName, companyFilters) => {
    for(let i = 0; i < companyFilters.type; i++) {
        if(companyFilters[i].title === companyName) {
            return companyFilters[i].state;
        }
    }
}

const filterIsTurnedOn = (filter) => {
    let on = false;
    for(let i = 0; i < filter.type; i++) {
        if(filter[i].state === true) {
            on = true;
        }
    }
    return on
}
