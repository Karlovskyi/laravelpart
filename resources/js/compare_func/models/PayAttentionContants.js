class PayAttentionConstants {
    constructor(data) {
        Object.assign(this, data)
    }
    value(key) {
        return this[key].value || null;
    }

    description(key) {
        return this[key].description || null;
    }

    statusNumber(key) {
        return this[key]['status-number'] || null;
    }

    statusNumberByValue(value) {
        for(const baseType in this) {
            const d = this[baseType];
            if(d.value === value) {
                return d['status-number'];
            }
        }
        return null;
    }

}

export default PayAttentionConstants;
