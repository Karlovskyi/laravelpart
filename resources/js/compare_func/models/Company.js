const HIGH_RELIABILITY_FRONTIER = 80;
const AVERAGE_RELIABILITY_FRONTIER = 60;

const payAttentionTypeIntStatus = (payAttentionConstants, company) => {
    const type = company.company_pay_attention_type || 0;
    return payAttentionConstants?.statusNumberByValue(type) ?? 0;
}

const isAllBadCompany = (payAttentionConstants, company) => {
    return company.company_pay_attention_type === payAttentionConstants?.value('PAY_ATTENTION_ALL_IS_BAD');
}

const isCriticalCompany = (payAttentionConstants, company) => {
    return company.company_pay_attention_type === payAttentionConstants?.value('PAY_ATTENTION_ALL_IS_BAD');
}

const reliabilityIntStatus = (appConstants, payAttentionConstants, company) => {
    const totalRely = reliabilityPercent(appConstants, payAttentionConstants, company);
    if(isCriticalCompany(payAttentionConstants, company) || isAllBadCompany(payAttentionConstants, company)) {
        return 0
    } else if(totalRely > HIGH_RELIABILITY_FRONTIER) {
        return 2;
    } else if(totalRely > AVERAGE_RELIABILITY_FRONTIER) {
        return 1;
    } else {
        return 0;
    }
}

const reliabilityPercent = (appConstants, payAttentionConstants, company) => {
    const type = company.company_pay_attention_type || 0;
    let numerator = company.company_total_reliability || 0;
    let denominator = appConstants?.RELIABILITY_MAX || 26;
    switch (type) {
        case payAttentionConstants?.value('PAY_ATTENTION_BAD'): denominator += 2;break;
        case payAttentionConstants?.value('PAY_ATTENTION_AVERAGE'): denominator += 2; numerator += 1;break;
        case payAttentionConstants?.value('PAY_ATTENTION_GOOD'): denominator += 2; numerator += 2;break;
        case payAttentionConstants?.value('PAY_ATTENTION_NOT_MORE_THAN_AVERAGE'):
            return Math.min(Math.round(numerator / denominator * 100), HIGH_RELIABILITY_FRONTIER);
        case payAttentionConstants?.value('PAY_ATTENTION_ALL_IS_BAD'): return 0;
    }

    return Math.round(numerator / denominator * 100);
}

const companyFieldStatus = (payAttentionConstants, company, field) => {
    return isAllBadCompany(payAttentionConstants, company) ? 0 : company[field];
}

const reliabilityStringStatus = (appConstants, payAttentionConstants, company) => {
    const totalRely = reliabilityPercent(appConstants, payAttentionConstants, company);
    if(isCriticalCompany(payAttentionConstants, company)|| isAllBadCompany(payAttentionConstants, company)) {
        return 'Низкая';
    } else if(totalRely > HIGH_RELIABILITY_FRONTIER) {
        return 'Высокая';
    } else if(totalRely > AVERAGE_RELIABILITY_FRONTIER) {
        return 'Средняя';
    } else {
        return 'Низкая';
    }
}

export {
    reliabilityIntStatus,
    reliabilityPercent,
    reliabilityStringStatus,
    payAttentionTypeIntStatus,
    companyFieldStatus,
    isCriticalCompany,
    isAllBadCompany
}
