const markDataAttr = (num) => {
    if (num > 4) {
        return 2
    } else if (num > 3) {
        return 1
    } else return 0;

}

const leaderString = (num, status, objectData) => {
    if(num === 100) {
        return "Не менялся"
    } else if (status === 2) {
        return replaceValue(objectData, 'criteria_value_1', num)
    } else if(status === 1){
        return replaceValue(objectData, 'criteria_value_2', num)
    } else if (status === 0) {
        return replaceValue(objectData, 'criteria_value_3', num)
    } else {
        return ''
    }
}

const bulkAddressString = (num) => {
    if(num) {
        return "Не содержится";
    } else return "Содержится";
}

const checksString = (dataObj, payload) => {
    if(!payload) {
        return replaceValue(dataObj, 'criteria_value_1', payload)
    } else {
        return replaceValue(dataObj, 'criteria_value_2', payload)
    }
}

const getCriteriaName = (field, dataObj) => dataObj[field].criteria_name_ru;

const transformPayload = (payload) => {
    if(parseInt(payload)) {
        let str = String(payload);
        for(let i = str.length - 3; i >= 1; i -= 3) {
            str = str.slice(0, i) + ' ' + str.slice(i);
        }
        return str;
    }
    else return payload;
}

const replaceYear = (str, payload) => {
    const int = Number(payload) || 0;
    let word;
    if(int === 1) {
        word = 'год';
    } else if(int > 1 && int < 5) {
        word = 'года';
    } else {
        word = 'лет';
    }
    return str.replace('(year)', word);
}

const replaceValue = (dataObj, field, payload) => {
    const transformedPayload = transformPayload(payload);
    const withData = dataObj[field].replace('(data)', transformedPayload);
    return replaceYear(withData, payload);
}

const getBinaryCriteria = (dataObj, payload) => {
    if(payload) {
        return replaceValue(dataObj, 'criteria_value_1', payload)
    } else {
        return replaceValue(dataObj, 'criteria_value_2', payload)
    }
}

const getBinaryCriteriaRelations = (dataObj, payload) => {
    if(payload) {
        return replaceValue(dataObj, 'criteria_value_2', payload)
    } else {
        return replaceValue(dataObj, 'criteria_value_1', payload)
    }
}

const getTernaryCriteria = (status, objectData) => {
    if(status === 2) {
        return replaceValue(objectData, 'criteria_value_1', status)
    } else if(status === 1) {
        return replaceValue(objectData, 'criteria_value_2', status)
    } else {
        return replaceValue(objectData, 'criteria_value_3', status)
    }
}

const prepareLink = (link) => {
    if(link === null) {
        return null
    }
    link = link.slice(link.indexOf('//') + 2, link.indexOf('?') - 1)

    return linkException(link);
}

const linkException = (link) => link === 'xn----itbbtigglchjdag0o.xn--p1ai' ? 'ремонт-экспресс.рф' : link;

const stuffMembersSingular = (str, data) => {
    if(data === 1) {
        return str.slice(0, -2)
    }
    return  str
}

module.exports = {
    leaderString,
    bulkAddressString,
    checksString,
    getCriteriaName,
    getBinaryCriteriaRelations,
    replaceValue,
    getBinaryCriteria,
    getTernaryCriteria,
    markDataAttr,
    prepareLink,
    stuffMembersSingular
}
