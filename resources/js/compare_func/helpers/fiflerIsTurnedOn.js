const filterIsTurnedOn = (filter) => {
    let on = false;
    for(let i = 0; i < filter.type; i++) {
        if(filter[i].state === true) {
            on = true;
        }
    }
    return on
}

export default filterIsTurnedOn;
