import filterIsTurnedOn from "./fiflerIsTurnedOn";

const filterCheck = (company, filters) => {
    let pass = true


    for(const i in filters) {
        if(filterIsTurnedOn(filters[i]))
        switch (filters[i].key) {
            case 'company_term_of_work': {
                if(filters[i][0].state === false && company[i] >= 4)
                    pass = false;
                if(filters[i][1].state === false && company[i] < 4)
                    pass = false;
            } break;

            case 'company_change_of_leader': {
                if(filters[i][0].state === false && company[i] === 2)
                    pass = false;
                if(filters[i][1].state === false && company[i] < 2)
                    pass = false;
            } break;
            case 'company_legal_address': {
                if(filters[i][0].state === false && company[i] === 2)
                    pass = false;
                if(filters[i][1].state === false && company[i] === 0)
                    pass = false;
            } break;
            case 'company_search_for_colleagues': {
                if(filters[i][0].state === false && company[i] === 2)
                    pass = false;
                if(filters[i][1].state === false && company[i] < 2)
                    pass = false;
            } break;
            case 'company_staff_members': {
                if(filters[i][0].state === false && company[i] >= 20)
                    pass = false;
                if(filters[i][1].state === false && company[i] < 20)
                    pass = false;
            } break;
            case 'company_payment_of_wages': {
                if(filters[i][0].state === false && company[i] >= 1)
                    pass = false;
                if(filters[i][1].state === false && company[i] === 0)
                    pass = false;
            } break;
            case 'company_income_dynamics': {
                if(filters[i][0].state === false && company[i] >= 1)
                    pass = false;
                if(filters[i][1].state === false && company[i] === 0)
                    pass = false;
            } break;
            case 'company_pay_attention': {
                if(filters[i][0].state === false && (company[i] === null || company[i] === ''))
                    pass = false;
                if(filters[i][1].state === false && company[i] !== null && company[i] !== '')
                    pass = false;
            } break
            case 'company_amount_of_credit': {
                if(filters[i][0].state === false && company[i] >= 0 && company[i] < 100000)
                    pass = false;
                if(filters[i][1].state === false && company[i] >= 100000 && company[i] < 1000000)
                    pass = false;
                if(filters[i][2].state === false  && company[i] >= 1000000)
                    pass = false;
            } break;
            case 'company_profitability': {
                if(filters[i][0].state === false && company[i] >= 2000000)
                    pass = false;
                if(filters[i][1].state === false && company[i] >= 1000000 && company[i] < 2000000)
                    pass = false;
                if(filters[i][2].state === false && company[i] <= 0)
                    pass = false;
            } break
            case 'company_enforcement_proceedings': {
                if(filters[i][0].state === false && company[i] === 2)
                    pass = false;
                if(filters[i][1].state === false && company[i] === 1)
                    pass = false;
            } break;

            case 'companies': {
                if(findCompanyFilter(company.company_name, filters[i]) === false)
                    pass = false
            } break;
        }
    }
    return pass
}

export default filterCheck

const findCompanyFilter = (companyName, companyFilters) => {
    for(let i = 0; i < companyFilters.type; i++) {
        if(companyFilters[i].title === companyName) {
            return companyFilters[i].state;
        }
    }
}


