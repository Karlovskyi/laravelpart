const bodyOverflowEffect = (reason) => {
    return () => {
        if (reason) {
            document.querySelector('body').style.overflow = 'hidden';
        } else {
            document.querySelector('body').style.overflow = 'unset';
        }
        return () => document.querySelector('body').style.overflow = 'unset';
    }
}

export default bodyOverflowEffect;
