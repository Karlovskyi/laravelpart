const getApiData = (onSuccess, onError) => {
    // Можно делать параллельно
    const activeSlugs = getActiveArray();
   fetchData()
        .then(apiData => {
            if(apiData) {
                const companies = apiData.companies;
                companies.forEach(company => company.is_active = activeSlugs.indexOf(company.company_slug) !== -1)
                onSuccess(apiData);
            }
            else {
                onError(apiData);
            }
        })


}

const getActiveArray = () => {
    const slug = document.querySelector('.compare__main-wrapper').dataset.slug.split(';')
    slug.pop();
    return slug;
}

const fetchData = async () => {
    const url = '/api/rating/compare';

    try {
        const response = await fetch(url);
        return await response.json();
    } catch (e) {
        return null;
    }
}

export {
    getApiData
}
