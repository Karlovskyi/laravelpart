const filterUpdate = (category) => (index) => ({type: 'filter/update', category, index});

const filterAdd = (key, payload) => ({type: 'filter/add', key, payload});

const filterAddOption = (key, payload) => ({type: 'filter/addOption', key, payload});

const filterRemoveOption = (key, index) => ({type: 'filter/removeOption', key, index});

const filterReset = () => ({type: 'filter/reset'})

const filterReplace = (payload) => ({type: 'filter/replace', payload})

export {filterUpdate, filterAdd, filterAddOption, filterRemoveOption, filterReset, filterReplace}
