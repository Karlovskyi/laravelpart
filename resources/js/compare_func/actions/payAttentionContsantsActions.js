const updatePayAttentionConstants = (payload) => ({type: 'payAttentionConstants/update', payload});

export {
    updatePayAttentionConstants
}
