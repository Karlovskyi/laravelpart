const setSorting = (payload) => ({type: 'sorting/set', payload})

export {
    setSorting
}
