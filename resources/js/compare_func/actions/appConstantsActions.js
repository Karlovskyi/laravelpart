const updateAppConstants = (payload) => ({type: 'appConstantsActions/update', payload});

export {
    updateAppConstants
}
