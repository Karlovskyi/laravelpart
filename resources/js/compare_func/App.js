import React from 'react';
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {createStore} from "redux";
import reducers from "./reducers";
import ErrorBoundary from "./components/ErrorBoundary";
import ContentApp from "./components/ContentApp";

const App = () => {
    return (
        <Provider store={createStore(reducers)}>
            <ErrorBoundary>
                <ContentApp />
            </ErrorBoundary>
        </Provider>
    )
}

if (document.getElementById('compare')) {
    ReactDOM.render(<App />, document.getElementById('compare'));
}
