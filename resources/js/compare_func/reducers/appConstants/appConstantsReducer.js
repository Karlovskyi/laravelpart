const appConstantsReducer = (state = null, action) => {
    switch (action.type) {
        case 'appConstantsActions/update': return action.payload;
        default: return state;
    }
}

export default appConstantsReducer;
