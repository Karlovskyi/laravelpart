

const sortingReducer = (state = null, action) => {
    switch (action.type) {
        case 'sorting/set': {
            return action.payload;
        }
        case 'sorting/reset': return null;
        default: return state;
    }
}

export default sortingReducer;
