import PayAttentionConstants from "../../models/PayAttentionContants";

const payAttentionConstantsReducer = (state = null, action) => {
    switch (action.type) {
        case 'payAttentionConstants/update': return new PayAttentionConstants(action.payload);
        default: return state;
    }
}

export default payAttentionConstantsReducer;
