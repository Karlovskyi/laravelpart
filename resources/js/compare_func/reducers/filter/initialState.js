const initialState  = {
    'company_term_of_work_clear': {
        type: 2,
        label: 'Срок работы',
        key: 'company_term_of_work',
        0: {
            name: 'company_term_of_work_1',
            title: '4+ лет',
            state: false
        },
        1: {
            name: 'company_term_of_work_2',
            title: 'До 4 лет',
            state: false
        }

    },
    'company_change_of_leader': {
        type: 2,
        label: 'Смена руководителя',
        key: 'company_change_of_leader',
        0: {
            name: 'company_change_of_leader_1',
            title: 'Не менялся',
            state: false
        },
        1: {
            name: 'company_change_of_leader_2',
            title: 'Менялся недавно',
            state: false
        },

    },
    'company_legal_address': {
        type: 2,
        label: 'Юр. адрес',
        key: 'company_legal_address',
        0: {
            name: 'company_legal_address_1',
            title: 'Не менялся',
            state: false
        },
        1: {
            name: 'company_legal_address_2',
            title: 'Менялся недавно',
            state: false
        },

    },
    'company_search_for_colleagues': {
        type: 2,
        label: 'Поиск сотрудников',
        key: 'company_search_for_colleagues',
        0: {
            name: 'company_search_for_colleagues_1',
            title: 'Активно ищет',
            state: false
        },
        1: {
            name: 'company_search_for_colleagues_2',
            title: 'Не ищет',
            state: false
        },
    },
    'company_staff_members_clear': {
        type: 2,
        label: 'Сотрудников',
        key: 'company_staff_members',
        0: {
            name: 'company_staff_members_1',
            title: '20+',
            state: false
        },
        1: {
            name: 'company_staff_members_2',
            title: '1-20',
            state: false
        },

    },
    'company_payment_of_wages': {
        type: 2,
        label: 'Выплаты персоналу',
        key: 'company_payment_of_wages',
        0: {
            name: 'company_payment_of_wages_1',
            title: 'Есть',
            state: false
        },
        1: {
            name: 'company_payment_of_wages_2',
            title: 'Нет',
            state: false
        },
    },
    'company_income_dynamics': {
        type: 2,
        label: 'Динамика доходов',
        key: 'company_income_dynamics',
        0: {
            name: 'company_income_dynamics_1',
            title: 'Положительная',
            state: false
        },
        1: {
            name: 'company_income_dynamics_2',
            title: 'Отрицательная',
            state: false
        }

    },
    'company_pay_attention': {
        type: 2,
        label: 'Банкротство',
        key: 'company_pay_attention',
        0: {
            name: 'company_pay_attention_1',
            title: 'Нет',
            state: false
        },
        1: {
            name: 'company_pay_attention_2',
            title: 'Да',
            state: false
        },

    },
    'company_amount_of_credit_clear': {
        type: 3,
        label: 'Долги',
        key: 'company_amount_of_credit',
        0: {
            name: 'company_amount_of_credit_1',
            title: '0 - 100 000',
            state: false
        },
        1: {
            name: 'company_amount_of_credit_2',
            title: '100 000 - 1 000 000',
            state: false
        },
        2: {
            name: 'company_amount_of_credit_3',
            title: '1 000 000+',
            state: false
        },

    },
    'company_profitability_clear': {
        type: 3,
        label: 'Прибыльность',
        key: 'company_profitability',
        0: {
            name: 'company_profitability_1',
            title: '2 000 000+',
            state: false
        },
        1: {
            name: 'company_profitability_2',
            title: '1 000 000 - 2 000 000',
            state: false
        },
        2: {
            name: 'company_profitability_3',
            title: '0',
            state: false
        },

    },
    'company_enforcement_proceedings': {
        type: 2,
        label: 'Суды',
        key: 'company_enforcement_proceedings',
        0: {
            name: 'company_enforcement_proceedings_1',
            title: 'Нет',
            state: false
        },
        1: {
            name: 'company_enforcement_proceedings_2',
            title: 'Есть',
            state: false
        },

    },

}

export default initialState
