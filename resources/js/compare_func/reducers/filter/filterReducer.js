import initialState from "./initialState";

const filterReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'filter/update': {
            const {category, index} = action;
            const newState = {...state};
            newState[category][index].state = !newState[category][index].state;
            return newState;
        }
        case 'filter/add': {
            const {key, payload} = action;
            const preState = {...state};
            delete preState[key];
            const newState = {}
            newState[key] = payload
            Object.assign(newState, preState)

            return newState;
        }
        case 'filter/addOption': {
            const {key, payload} = action
            const newState = {...state};
            if (newState[key] !== undefined) {
                const oldLength = newState[key].type;
                newState[key].type++;
                newState[key][oldLength] = payload;

                return newState
            }
            throw 'call To undefined index at: filterReducer';
        }

        case 'filter/removeOption': {
            const {key, index} = action;
            const newState = {...state};
            if (newState[key] !== undefined) {
                if(newState[key][index] !== undefined) {

                    // красиво переписать
                    for(let i = index+1; i < newState[key].type; i++) {
                        newState[key][i - 1] = newState[key][i]
                    }

                    delete newState[key][newState[key].type - 1];
                    newState[key].type--;
                }
                return newState;
            }
            throw 'call To undefined index at: filterReducer';
        }
        case 'filter/reset': {
            const newState = {...state}

            for(const i in newState) {
                for(let j = 0; j < newState[i].type; j++) {
                    newState[i][j].state = false;
                }
            }
            if(newState['companies'] !== undefined) {
                for(let i = 0; i < newState['companies'].type; i++) {
                    newState['companies'][i].state = true;
                }
            }
            return newState;
        }
        case 'filter/replace': {
            return action.payload;
        }
        default: return state
    }
}

export default filterReducer;
