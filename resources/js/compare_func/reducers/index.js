import {combineReducers} from "redux";
import sortingReducer from "./sorting/sortingReducer";
import filterReducer from "./filter/filterReducer";
import appConstantsReducer from "./appConstants/appConstantsReducer";
import payAttentionConstantsReducer from "./payAttentionConstants/payAttentionConstantsReducer";

export default combineReducers({
    sorting: sortingReducer,
    filter: filterReducer,
    appConstants: appConstantsReducer,
    payAttentionConstants: payAttentionConstantsReducer,
})
