import React from 'react';

const TableScroll = React.createContext(null);
const {Provider: TableScrollProvider, Consumer: TableScrollConsumer} = TableScroll;
export {
    TableScrollProvider,
    TableScrollConsumer,
    TableScroll
}
