import {createContext} from 'react'



const QuestionContext = createContext(null);
const {Provider: QuestionProvider, Consumer: QuestionConsumer} = QuestionContext;
export {
    QuestionContext,
    QuestionProvider,
    QuestionConsumer
}

