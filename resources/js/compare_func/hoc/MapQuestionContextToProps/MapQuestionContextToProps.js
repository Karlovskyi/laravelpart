import React, {useContext} from 'react';
import {QuestionContext} from "../../contexts/questionContext";


const MapQuestionContextToProps = (Component) => (props) => {
    const questionContext = useContext(QuestionContext)
    return <Component {...props} questionContext={questionContext}/>
}

export default MapQuestionContextToProps;
