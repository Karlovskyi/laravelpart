import React, {useContext, useEffect, useState} from 'react';
import {QuestionProvider} from "../../contexts/questionContext";

const questions = new Set([0]);
const refs = new Map();
let counter = 0;



const subscribe = (ref) => {
    counter++;
    questions.add(counter);
    refs.set(counter, ref)
    return counter;
}

const unsubscribe = (index) => {
    refs.delete(index);
    questions.delete(index);
}

const WithQuestionContext = ({children}) => {
    const [active, setActive] = useState(0);
    const safeSetActive = (index) => {
        if(questions.has(index)) {
            if(index === active) {
                setActive(0);
            } else {
                setActive(index)
            }
        }
    }

    useEffect(() => {
        const resetActive = (e) => {
            for(const [, ref] of refs) {
                const {current} = ref;
                if(e.path.includes(current)) {
                    return;
                }
            }
            safeSetActive(0)
        };
        window.addEventListener('click', resetActive);
        return () => window.removeEventListener('click', resetActive);
    }, [])

    return (
        <QuestionProvider value={[active, safeSetActive]}>
            {children}
        </QuestionProvider>
    )
}

export default WithQuestionContext

export {
    subscribe, unsubscribe
}
