import {useEffect, useState} from 'react';

const useWindowDimensions = () => {
    const [windowDimensions, setWindowDimensions] = useState(getDimensions());
    const listener = () => setWindowDimensions(getDimensions());
    useEffect(() => {
        window.addEventListener('resize', listener)
        return () => window.removeEventListener('resize', listener)
    })

    return windowDimensions
}

const getDimensions = () => ({height: window.innerHeight, width: window.innerWidth})

export default useWindowDimensions;
