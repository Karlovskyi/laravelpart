import {useSelector} from "react-redux";
import {companyFieldStatus} from "../models/Company";

const useCompanyFieldStatus = (company, field) => {
    const {payAttentionConstants} = useSelector(select);
    return companyFieldStatus(payAttentionConstants, company, field);
}

const select = ({payAttentionConstants}) => ({payAttentionConstants});

export default useCompanyFieldStatus;
