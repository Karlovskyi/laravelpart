import {useMemo} from "react";
import companyFilter from "../services/companyFilter";
import companySorting from "../services/companySorting";
import {useSelector} from "react-redux";

const useActiveCompanies = (companies, sorting, filter) => {
    const {appConstants, payAttentionConstants} = useSelector(selector);
    return useMemo(() => {
        const baseSorted = companies
            .filter((company) => company.is_active)
            .filter(companyFilter(filter))
            .sort(companySorting(appConstants, payAttentionConstants)(sorting))

        const notLastAddedArray = baseSorted
            .filter(company => company.last_added === 0);
        const LastAddedArray = baseSorted
            .filter(company => company.last_added !== 0)
            .sort(company => company.last_added);

        return [...notLastAddedArray, ...LastAddedArray];
    },[companies, sorting, filter]);
}

const useInactiveCompanies = (companies, filter, sorting) => {
    const {appConstants, payAttentionConstants} = useSelector(selector)
    return useMemo(() => companies
            .filter((company) => !company.is_active)
            .filter(companyFilter(filter))
            .sort(companySorting(appConstants, payAttentionConstants)(sorting)),
        [companies, sorting, filter])
}

const selector = ({appConstants, payAttentionConstants}) => ({
    appConstants,
    payAttentionConstants
});

export {
    useActiveCompanies,
    useInactiveCompanies
}
