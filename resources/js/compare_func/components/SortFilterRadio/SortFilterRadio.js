import React, {Fragment, useState} from 'react';
import Sorting from "../Sorting";
import Filter from "../Filter";

const SortFilterRadio = () => {
    const [visibleFilter, setVisibleFilter] = useState(false)
    const [visibleSorting, setVisibleSorting] = useState(false)
    const setVisibleFilterTrue = () => setVisibleFilter(true);
    const toggleSorting = () => setVisibleSorting(!visibleSorting);
    return (
        <Fragment>
            <Filter visible={visibleFilter} setVisible={setVisibleFilter}/>
            <SortFilterRadioContent filterClick={setVisibleFilterTrue}
                                    sortingClick={toggleSorting}
                                    visibleSorting={visibleSorting}
                                    setVisibleSorting={setVisibleSorting}/>
        </Fragment>
    )

}

export default SortFilterRadio;

const SortFilterRadioContent = ({filterClick, sortingClick, visibleSorting, setVisibleSorting}) => {
    const classList = visibleSorting ? 'sort-filter-radio sort-filter-radio_active' : 'sort-filter-radio';

    return (
        <div className={classList}>
            <div className="sort-filter-radio__control">
                <button className='sort-filter-radio__btn' onClick={filterClick}>Фильтры</button>
                <button className='sort-filter-radio__btn' onClick={sortingClick}>Сортировка</button>
            </div>
            <Sorting visible={visibleSorting} setVisible={setVisibleSorting}/>
        </div>
    )
}
