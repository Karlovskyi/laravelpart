import React from 'react';

const Loading = () => {
    return (
        <div className='compare__loading'>
            <img src="/images/loading-screen.svg" alt="Спинер загрузки" width={100} height={100}/>
            <h2 className='header header-sub'>Загрузка...</h2>
        </div>
    )
}

export default Loading
