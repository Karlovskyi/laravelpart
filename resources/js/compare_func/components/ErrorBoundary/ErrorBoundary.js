import React, {Component} from 'react';

class ErrorBoundary extends Component {
    constructor(props) {
        super(props);
        this.state = { hasError: false };
    }

    setError() {
        this.setState({hasError: true})
    }

    componentDidCatch(error, errorInfo) {
        this.setError();
        // TODO: Create Fetch ErrorIndicator Logger
    }

    render() {
        if (this.state.hasError) {
            return (
                <div className='compare__error'>
                    <img src="images/error.svg" alt="Картинка ошибки" className='compare__error-img max-w-10'/>
                    <span className='text-center mt-3 blue-light-text '>Упс, что-то пошло не так!</span>
                </div>
            );
        }
        return this.props.children;
    }
}

export default ErrorBoundary;
