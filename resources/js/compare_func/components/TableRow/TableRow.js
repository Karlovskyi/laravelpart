import React, {Fragment} from 'react';

const TableRow = ({companies, children, ...toPass}) => {
    // The reason to use JSX syntax instead of function call is hooks counting error
    const Component = children;
    return (
        <Fragment>
            {
                companies.map((company, index) => {
                    return <Component key={company.company_slug} company={company} index={index} length={companies.length} {...toPass}/>;
                })
            }
        </Fragment>
    )
}

export default TableRow;
