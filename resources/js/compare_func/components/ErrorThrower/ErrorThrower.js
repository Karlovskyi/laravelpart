import React, {useState} from 'react';

const ErrorThrower = () => {
    const [needError, setNeedError] = useState(false);

    const toggleNeedError = () => {
        setNeedError(!needError);
    }
    if(needError) {
        this.foo.bar = 0
    }

    return (
        <button onClick={toggleNeedError} className='btn btn_dark btn_bold'>Вызвать ошибку</button>
    )
}

export default ErrorThrower;
