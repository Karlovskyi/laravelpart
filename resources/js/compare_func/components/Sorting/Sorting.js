import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {setSorting} from '../../actions/sortingActions'

const Sorting = ({visible, setVisible}) => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(setSorting(sortingData[0].name))
    }, [])


    const toggleVisible = () => {
        setVisible(!visible);
    }
    const platStyle = visible ? "sorting__plat" : "sorting__plat sorting__plat_disable";
    const visibleStyle = visible ? "sorting__visible sorting__visible_arrow" : "sorting__visible";

    const {sorting} = useSelector(selector);

    const onChange = (e) => {
        dispatch(setSorting(e.currentTarget.value))
        setVisible(false);
    }

    const chosenName = () => {
        const ch = sortingData.find(item => item.name === sorting);
        if (ch) {
            return ch.title
        } else {
            return null
        }
    }

    return (
        <section className="sorting">
            <div className={visibleStyle} onClick={toggleVisible}>
                <h2 className="sorting__header">Сортировать:</h2>
                <small className="sorting__chosen">{chosenName()}</small>
            </div>
            <div className={platStyle}>
                {
                    sortingData.map(({name, title}, index) => {
                        return (
                            <div className="sorting__radio-wrapper" key={title}>
                                <input type="radio" id={name}
                                       name="sort" value={name} defaultChecked={index === 0} onChange={onChange} />
                                <label htmlFor={name} className='sorting__label'>{title}</label>
                            </div>
                        )
                    })
                }
            </div>
        </section>
    )
}

const selector = ({sorting}) => ({sorting});

export default Sorting;

const sortingData = [
    {
        name: "rely-dec",
        title: "По убыванию надёжности"
    },
    {
        name: "rely-inc",
        title: "По возрастанию надёжности"
    },
    {
        name: "alphabet-inc",
        title: "По алфавиту А-Я"
    },
    {
        name: "alphabet-dec",
        title: "По алфавиту Я-А"
    }
]

