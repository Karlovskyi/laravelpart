import React, {useState, useEffect} from 'react';

const ChooseBox = ({companiesList, addCompany}) => {


    const [scrollBox, setScrollBox] = useState(null);
    useEffect(() => {
        setScrollBox(document.querySelector('.compare__main'))
    }, [])
    return (
        <div className='compare__choose-box'>
            <h4 className="compare__label">
                Выберите компанию
            </h4>
            <ul className="compare__choose-item-wrapper">
                {
                    companiesList.map(({company_slug, company_logo_path, company_name}) => {
                        return (
                            <li className="compare__choose-item" data-id={company_slug} key={company_slug}
                                onClick={() => {
                                    addCompany(company_slug);
                                    if(scrollBox){
                                        setTimeout(() => {
                                            scrollBox.scrollTo({
                                                left: scrollBox.scrollWidth - scrollBox.offsetWidth,
                                                behavior: "smooth"
                                            })
                                        }, 50)
                                    }
                                }}>
                                <img src={`/logos/${company_logo_path}`}
                                     alt={`Логотип компании ${company_name}`}
                                     title={`Логотип компании ${company_name}`}
                                     className="compare__choose-logo"/>
                                <div className="compare__choose-name">{company_name}</div>
                            </li>
                        )
                    })
                }

            </ul>
        </div>
    )
}
export default ChooseBox;
