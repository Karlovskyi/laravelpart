import React, {useState} from 'react';
import Plus from "./Plus";
import ChooseBox from "./ChooseBox";

const AddCompany = ({companiesList, addCompany}) => {
    const [chooseBoxActive, setChooseBoxActive] = useState(true);
    const setChooseBoxVisible = () => {
        setChooseBoxActive(true);
    }
    const setChooseBoxHidden = () => {
        setChooseBoxActive(false)
    }
    const onAddCompany = (...argc) => {
        addCompany(...argc);
        setChooseBoxHidden();
    }
    if(companiesList.length === 0) {
        return null;
    }

    if(chooseBoxActive) {
        return <ChooseBox companiesList={companiesList}
                          addCompany={onAddCompany} />
    }
    return <Plus setChooseBoxVisible={setChooseBoxVisible} />
}

export default AddCompany;
