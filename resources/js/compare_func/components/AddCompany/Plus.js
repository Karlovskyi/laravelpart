import React from 'react';

const Plus = ({setChooseBoxVisible}) => {
    return (
        <div className='compare__plus' onClick={setChooseBoxVisible}>
            <span className="compare__plus-line compare__plus-line-1"></span>
            <span className="compare__plus-line compare__plus-line-2"></span>
            <div className="compare__plus-text">Выбрать еще одну компанию</div>
        </div>
    )
}

export default Plus
