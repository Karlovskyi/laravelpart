import React, {useEffect, useRef, useState} from 'react'
import MapQuestionContextToProps from "../../hoc/MapQuestionContextToProps";
import {subscribe, unsubscribe} from "../../hoc/WithQuestionContext";

const Description = ({content, questionContext, isLast}) => {
    const [activeIndex, setActiveIndex] = questionContext;
    const [ownIndex, setOwnIndex] = useState(null);
    const ref = useRef(null);

    useEffect(() => {
        setOwnIndex(subscribe(ref));
        return () => unsubscribe(ownIndex)
    }, [])


    const toggleQuestion = () => {
        setActiveIndex(ownIndex);
    }

    const classList = ownIndex === activeIndex ? 'tip tip_disappear' : 'tip tip_disable tip_disappear'
    const paragraphClass = isLast ? "tip__content tip__content--left-offset" : "tip__content"
    if(content) {
        return (
            <div className={classList} ref={ref} onClick={toggleQuestion}>
                <span className="tip__question"></span>
                <p className={paragraphClass}>{content}</p>
            </div>
        )
    } else {
        return null
    }

}

export default MapQuestionContextToProps(Description);
