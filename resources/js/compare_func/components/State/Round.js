import React from 'react';

const Round = ({content, binary = false,}) => {
    const multiplier = binary ? 2 : 1;
    return  content === null ? null : <div className="company-node__round" data-content={content * multiplier}/>;
}

export default Round;
