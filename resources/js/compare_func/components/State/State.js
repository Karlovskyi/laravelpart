import React from 'react';
import Description from "./Description";
import Round from "./Round";

const State = ({field, criteria, content, binary = false, isLast}) => {
    const description = criteria[field].criteria_description;
    return (
        <div className="company-node__state">
            <Description content={description} isLast={isLast}/>
            <Round content={content} binary={binary}/>
        </div>
    )
}


export default State;
