import React, {useEffect, useRef, useState} from 'react';

import {useDispatch, useSelector} from "react-redux";
import {updatePayAttentionConstants} from "../../actions/payAttentionContsantsActions";
import {updateAppConstants} from "../../actions/appConstantsActions";

import {getApiData} from "../../api/companies-api";

import Loading from "../Loading";
import ErrorIndicator from "../ErrorIndicator";
import TableMain from "../TableMain";
import AddCompany from "../AddCompany";
import SortFilterRadio from "../SortFilterRadio";
import Arrows from "../Arrows";
import {TableScrollProvider} from "../../contexts/tableScrollContext";

import {useActiveCompanies, useInactiveCompanies} from "../../hooks/useCompanies";
import WithQuestionContext from "../../hoc/WithQuestionContext";


const ContentApp = () => {
    const dispatch = useDispatch();
    const {sorting, filter} = useSelector(selector);

    const [isLoading, setIsLoading] = useState(true);
    const [isError, setIsError] = useState(false);
    const [compareCriteria, setCompareCriteria] = useState(null);
    const [companies, setCompanies] = useState([]);
    const [lastAdded, setLastAdded] = useState(1);

    const tableScrollRef = useRef(null);

    const inactiveCompanies = useInactiveCompanies(companies, sorting, filter);
    const activeCompanies = useActiveCompanies(companies, sorting, filter);

    const removeActive = (slug) => {
        const index = companies.findIndex(({company_slug}) => slug === company_slug)
        const company = companies[index];
        company.is_active = false;
        const newCompanies = [...companies.slice(0, index), company, ...companies.slice(index + 1)];
        setCompanies(newCompanies);
    }
    const addActive = (slug) => {
        const index = companies.findIndex(({company_slug}) => slug === company_slug)
        const company = companies[index];
        company.is_active = true;
        setLastAdded(lastAdded + 1)
        company.last_added = lastAdded;
        const newCompanies = [...companies.slice(0, index), company, ...companies.slice(index + 1)];
        setCompanies(newCompanies);
    }

    const onApiSuccess = ({criterion, companies: newCompanies, pay_attention_constants, app_constants}) => {
        setIsLoading(false);
        setIsError(false);
        dispatch(updatePayAttentionConstants(pay_attention_constants));
        dispatch(updateAppConstants(app_constants));
        setCompareCriteria(criterion);
        setCompanies(newCompanies.map(company => {
            company.last_added = 0;
            return company;
        }));
    }

    const onApiError = () => {
        setIsLoading(false);
        setIsError(true);
    }

    const subscribeOnResponse = () => {
        setIsLoading(true);
        setIsError(false);
        getApiData(onApiSuccess, onApiError)
    }

    useEffect(() => {
        subscribeOnResponse()
    }, []);

    useEffect(() => {
        setCompanies(companies.map(company => {
            company.last_added = 0;
            return company;
        }))
    }, [sorting, filter]);

    if (isLoading) {
        return <Loading />
    }
    if (isError) {
        return <ErrorIndicator onClick={subscribeOnResponse} />
    }

    const {current: currentTableScrollRef} = tableScrollRef;

    return (
        <WithQuestionContext>
            <TableScrollProvider value={currentTableScrollRef}>
                <SortFilterRadio />
                <Arrows scroll={500} />
                <div className="compare__main" ref={tableScrollRef}>
                    <TableMain companiesList={activeCompanies}
                               criteria={compareCriteria}
                               removeActive={removeActive} />
                    <AddCompany companiesList={inactiveCompanies} addCompany={addActive} />
                </div>
            </TableScrollProvider>
        </WithQuestionContext>
    );
}

const selector = ({sorting, filter}) => ({
    sorting,
    filter
});

export default ContentApp
