import React, {useState} from 'react';

import Checkbox from "./Checkbox";

const ListItem = ({data, k, companies, filterUpdate}) => {

    const {label} = data;
    const content = [];
    const fnc = filterUpdate(k);
    for(let i = 0; i < data.type; i++) {
        content.push(<Checkbox data={data[i]} inputName={k} key={data[i].name} fnc={fnc} index={i}/>)
    }
    if(companies && data.type === 0) {
        return null
    }

    if(companies) {
        return <ToggleableListItem label={label} content={content}/>
    }

    return (
        <li className='filter__category'>
            <h3 className='filter__category-label'>{label}</h3>
            {
                content.map(it => it)
            }
        </li>
    )
}

export default ListItem;

const ToggleableListItem = ({label, content}) => {
    const [on, setOn] = useState(false);

    const onClick = () => setOn(!on);

    const classList = on ? 'filter__category filter__category_large-device-hidden filter__category_toggleable' :
        'filter__category filter__category_large-device-hidden filter__category_rolled-up filter__category_toggleable'
    return (
        <li className={classList}>
            <h3 className='filter__category-label' onClick={onClick}>
                {label}
            </h3>
            {
                content.map(it => it)
            }
        </li>
    )
}
