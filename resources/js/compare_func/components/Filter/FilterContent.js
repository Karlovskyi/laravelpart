import React from 'react';
import ListItem from "./ListItem";

const FilterContent = ({filterData, updateFunction, visible, toggleVisible, controls}) => {
    const visibleStyle = visible ? "filter__visible filter__visible_arrow" : "filter__visible";
    const filterStyle = visible ? 'filter' : 'filter filter_disable';

    const listItems = [];

    for (const i in filterData) {
        listItems.push(<ListItem data={filterData[i]} k={i} key={i} companies={i === 'companies'}
                                 filterUpdate={updateFunction} />)
    }

    return (
        <section className={filterStyle}>
            <div className={visibleStyle} onClick={toggleVisible}>
                <h2 className="filter__header">Фильтры</h2>
            </div>
            <div className='filter__list-wrapper'>
                <ul className="filter__list">
                    {listItems.map(it => it)}
                </ul>
            </div>
            {controls}
        </section>
    )
}

export default FilterContent
