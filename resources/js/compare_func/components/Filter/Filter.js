import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import useWindowDimensions from "../../hooks/useWindowDimensions";
import FilterControls from "./FilterControls";
import {filterReplace} from "../../actions/filterActions";
import {createClearNewFilter, globalFilterUpdate, localFilterUpdate} from "./helpers";
import FilterContent from "./FilterContent";
import bodyOverflowEffect from "../../helpers/bodyOverflowEffect";

const Filter = ({visible, setVisible}) => {
    const {width: windowWidth} = useWindowDimensions();
    const dispatch = useDispatch();
    const limitWidth = 576;
    const mobile = windowWidth <= limitWidth;

    const {filter} = useSelector(selector);
    const [localFilter, setLocalFilter] = useState(createClearNewFilter(filter));

    useEffect(() => setLocalFilter(createClearNewFilter(filter)), [filter]);
    useEffect(bodyOverflowEffect(visible && mobile), [visible, windowWidth])

    const synchronizeFilter = () => dispatch(filterReplace(localFilter));
    const toggleVisible = () => setVisible(!visible);

    const filterData = mobile ? localFilter : filter;
    const updateFunction = mobile ? localFilterUpdate(localFilter, setLocalFilter) : globalFilterUpdate(dispatch)
    const controls = mobile ? <FilterControls apply={synchronizeFilter}
                                              toggleVisible={toggleVisible} /> : null;

    return <FilterContent visible={visible}
                          controls={controls}
                          filterData={filterData}
                          updateFunction={updateFunction}
                          toggleVisible={toggleVisible} />
}

const selector = ({filter}) => ({filter});

export default Filter;
