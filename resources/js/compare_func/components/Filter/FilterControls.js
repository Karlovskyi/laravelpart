import React from 'react';
import {filterReset} from "../../actions/filterActions";
import {useDispatch} from "react-redux";

const FilterControls = ({apply, toggleVisible}) => {
    const dispatch = useDispatch();

    const reset = () => dispatch(filterReset());

    const onClick = (func) => () => {
        toggleVisible();
        func();
    }
    return (
        <div className='filter__controls'>
            <button className="btn btn_compare-controls" onClick={onClick(reset)}>Сбросить</button>
            <button className="btn btn_compare-controls btn_dark-st" onClick={onClick(apply)}>Применить</button>
        </div>
    )
}

export default FilterControls;
