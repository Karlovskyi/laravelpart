import React, {useEffect, useRef} from 'react';

const Checkbox = ({data, inputName, fnc, index}) => {
    const checkBoxRef = useRef(null)
    const {name, title, state} = data;
    useEffect(() => {
        checkBoxRef.current.checked = state
    }, [state])
    const onChange = () => {
        fnc(index)
    }

    return (
        <div className="filter__checkbox-wrapper">
            <input type="checkbox" id={name} name={inputName} value={name}
                   className='filter__input'
                   onChange={onChange} ref={checkBoxRef} />
            <label htmlFor={name} className='filter__label'>
                <span>{title}</span>
            </label>
        </div>
    )
}


export default Checkbox
