import {filterUpdate} from "../../actions/filterActions";

const localFilterUpdate = (localFilter, setLocalFilter) => (category) => (index) => {
    const newFilter = {...localFilter}
    newFilter[category][index].state = !newFilter[category][index].state;
    setLocalFilter(newFilter)
}

const globalFilterUpdate = (dispatch) => (category) => (index) => {
    dispatch(filterUpdate(category)(index))
}

const createClearNewFilter = (filter) => {
    const newFilter = {...filter}
    for(const i in newFilter) {
        newFilter[i] = {...newFilter[i]}

        for(let j = 0; j < newFilter[i].type; j++) {
            newFilter[i][j] = {...newFilter[i][j]}
        }
    }
    return newFilter;
}

export  {
    localFilterUpdate,
    globalFilterUpdate,
    createClearNewFilter
}
