import React from 'react';

const ErrorIndicator = ({onClick}) => {
    return (
        <div className='compare__error'>
            <h2 className='header header-sub'>Ошибка</h2>
            <button onClick={onClick} className='btn btn_dark'>Перезагрузить</button>
        </div>
    )
}

export default ErrorIndicator
