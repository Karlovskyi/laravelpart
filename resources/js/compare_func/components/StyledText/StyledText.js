import React from 'react';

const StyledText = ({text, style}) => {
    return (
        <span className={style}>{text}</span>
    )
}

export default StyledText;
