import React from 'react';
import CellContent from "../../CellContent";
import {getBinaryCriteria, getCriteriaName} from "../../../helpers/content_helper";
import {State} from "../../State";
import useCompanyFieldStatus from "../../../hooks/useCompanyFieldStatus";

const PaymentCell = ({company, criteria, index, length}) => {
    const {company_payment_of_wages, company_name} = company;
    const state = useCompanyFieldStatus(company, 'company_payment_of_wages')
    const isLast = index === length - 1;
    return (
        <div className="company-node__item company-node__col company-node__col_bp"
             key={`company_payment_of_wages${company_name}`}>
            <CellContent small={getCriteriaName('company_payment_of_wages', criteria)}
                         regular={getBinaryCriteria(criteria.company_payment_of_wages, company_payment_of_wages)} />
            <State field='company_payment_of_wages'
                   criteria={criteria}
                   content={state}
                   binary isLast={isLast}/>
        </div>
    )
}

export default PaymentCell;
