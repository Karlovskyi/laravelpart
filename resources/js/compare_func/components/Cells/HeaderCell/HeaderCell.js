import React, {useEffect, useRef, useState} from 'react';
import {prepareLink} from "../../../helpers/content_helper";
import {isAllBadCompany} from "../../../models/Company";
import {useSelector} from "react-redux";
import {subscribe, unsubscribe} from "../../../hoc/WithQuestionContext";
import MapQuestionContextToProps from "../../../hoc/MapQuestionContextToProps";

const HeaderCell = ({company, removeActive, questionContext, index, length}) => {
    const {company_name, company_slug, company_link} = company;
    const {payAttentionConstants} = useSelector(select)
    const isBadCompany = isAllBadCompany(payAttentionConstants, company);
    const [activeIndex, setActiveIndex] = questionContext;
    const [ownIndex, setOwnIndex] = useState(null);
    const ref = useRef(null);

    useEffect(() => {
        setOwnIndex(subscribe(ref));
        return () => unsubscribe(ownIndex)
    }, [])

    const toggleQuestion = () => {
        setActiveIndex(ownIndex);
    }

    const tipClass = activeIndex === ownIndex ? 'tip tip_disappear ml-2' : 'tip tip_disable tip_disappear ml-2';
    const tipContent = index === length - 1 ? 'tip__content tip__content--bottom tip__content--left-offset' : 'tip__content tip__content--bottom';

    return (
        <div
            className="company-node__col company-node__col_lp compare__item-header-wrapper company-node__col_flex-between"
            key={`header${company_name}`}>
            <div className='compare__item-links-wrapper'>
                <div className="company-node__link">
                    <a className="compare__item-name" href={`/company/${company_slug}`}>{company_name}</a>
                    {isBadCompany ? (
                        <div className={tipClass} onClick={toggleQuestion} ref={ref}>
                            <button className="company-node__exclamation"></button>
                            <p className={tipContent}>{company.company_pay_attention}</p>
                        </div>

                    ) : null}
                </div>
                <a className="compare__item-site"
                   href={company_link}>
                    {prepareLink(company_link)}
                </a>
            </div>
            <div className="compare__item-close" onClick={() => removeActive(company_slug)}>
                <img src="/images/close.png" alt="Закрыть" />
            </div>
        </div>
    )
}

const select = ({payAttentionConstants}) => ({payAttentionConstants})

export default MapQuestionContextToProps(HeaderCell);
