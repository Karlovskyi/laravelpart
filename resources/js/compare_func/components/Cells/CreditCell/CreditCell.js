import React from 'react';
import StyledText from "../../StyledText";
import {getCriteriaName, replaceValue} from "../../../helpers/content_helper";
import CellContent from "../../CellContent";
import {State} from "../../State";
import {useSelector} from "react-redux";
import {companyFieldStatus} from "../../../models/Company";
import useCompanyFieldStatus from "../../../hooks/useCompanyFieldStatus";

const CreditCell = ({company, criteria, index, length}) => {
    const {company_amount_of_credit_clear, company_name} = company;
    const status = useCompanyFieldStatus(company, 'company_amount_of_credit')

    const isLast = index === length - 1;
    const regular = company_amount_of_credit_clear >= 10000 ?
        <StyledText style={'c-red'} text={replaceValue(criteria.company_amount_of_credit, 'criteria_value_1', company_amount_of_credit_clear)}/> :
        replaceValue(criteria.company_amount_of_credit, 'criteria_value_1', company_amount_of_credit_clear);
    return (
        <div className="company-node__item company-node__col company-node__col_bp"
             key={`company_amount_of_credit${company_name}`}>
            <CellContent small={getCriteriaName('company_amount_of_credit', criteria)}
                         regular={regular}/>
            <State field='company_amount_of_credit'
                   criteria={criteria}
                   content={status} isLast={isLast}/>
        </div>
    )
}



export default CreditCell;
