import React from 'react';
import CellContent from "../../CellContent";
import {getCriteriaName, leaderString} from "../../../helpers/content_helper";
import {State} from "../../State";
import useCompanyFieldStatus from "../../../hooks/useCompanyFieldStatus";

const LeaderCell = ({company, criteria, index, length}) => {
    const {company_change_of_leader, company_change_of_leader_clear, company_name} = company;
    const state = useCompanyFieldStatus(company, 'company_change_of_leader')
    const isLast = index === length - 1;
    return (
        <div className="company-node__item company-node__col company-node__col_bp"
             key={`company_change_of_leader${company_name}`}>
            <CellContent small={getCriteriaName('company_change_of_leader', criteria)}
                         regular={leaderString(company_change_of_leader_clear, company_change_of_leader, criteria.company_change_of_leader)} />
            <State field={'company_change_of_leader'}
                   criteria={criteria}
                   content={state} isLast={isLast}/>
        </div>
    )
}

export default LeaderCell;
