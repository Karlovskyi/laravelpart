import React from 'react';
import CellContent from "../../CellContent";
import {getBinaryCriteria, getCriteriaName} from "../../../helpers/content_helper";
import {State} from "../../State";
import useCompanyFieldStatus from "../../../hooks/useCompanyFieldStatus";

const IncomeCell = ({company, criteria, index, length}) => {
    const {company_income_dynamics, company_name} = company;
    const state = useCompanyFieldStatus(company, 'company_income_dynamics')
    const isLast = index === length - 1;
    return (
        <div className="company-node__item company-node__col company-node__col_bp"
             key={`company_income_dynamics${company_name}`}>
            <CellContent small={getCriteriaName('company_income_dynamics', criteria)}
                         regular={getBinaryCriteria(criteria.company_income_dynamics, company_income_dynamics)} />
            <State field='company_income_dynamics'
                   criteria={criteria}
                   content={state}
                   binary isLast={isLast}/>
        </div>
    )
}

export default IncomeCell;
