import React from 'react';
import CellContent from "../../CellContent";
import {getCriteriaName, replaceValue} from "../../../helpers/content_helper";
import {State} from "../../State";
import useCompanyFieldStatus from "../../../hooks/useCompanyFieldStatus";

const WorkTermCell = ({company, criteria, index, length}) => {
    const {company_term_of_work_clear, company_name} = company;
    const state = useCompanyFieldStatus(company, 'company_term_of_work');
    const isLast = index === length - 1;
    return (
        <div className="company-node__item company-node__col company-node__col_bp"
             key={`company_term_of_work${company_name}`}>
            <CellContent small={getCriteriaName('company_term_of_work', criteria)}
                         regular={replaceValue(criteria.company_term_of_work, 'criteria_value_1', company_term_of_work_clear)}/>
            <State field='company_term_of_work'
                   criteria={criteria}
                   content={state} isLast={isLast}/>
        </div>
    )
}

export default WorkTermCell;
