import React from 'react';
import CellContent from "../../CellContent";
import {getBinaryCriteria, getCriteriaName} from "../../../helpers/content_helper";
import {State} from "../../State";
import useCompanyFieldStatus from "../../../hooks/useCompanyFieldStatus";

const AddressCell = ({company, criteria, index, length}) => {
    const {company_legal_address, company_name} = company;
    const state = useCompanyFieldStatus(company, 'company_legal_address');
    const isLast = index === length - 1;
    return (
        <div className="company-node__item company-node__col company-node__col_bp"
             key={`company_legal_address${company_name}`}>
            <CellContent small={getCriteriaName('company_legal_address', criteria)}
                         regular={getBinaryCriteria(criteria.company_legal_address, company_legal_address)} />
            <State field='company_legal_address'
                   criteria={criteria}
                   content={state} isLast={isLast}/>
        </div>
    )
}

export default AddressCell;
