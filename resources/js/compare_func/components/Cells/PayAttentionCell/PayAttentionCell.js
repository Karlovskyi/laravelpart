import React from 'react';
import CellContent from "../../CellContent";
import {Round} from "../../State";
import {payAttentionTypeIntStatus} from "../../../models/Company";
import {useSelector} from "react-redux";

const PayAttentionCell = ({company}) => {
    const {payAttentionConstants} = useSelector(selector);
    const {company_pay_attention, company_name} = company;
    return (
        <div className="company-node__item company-node__col company-node__col_bp"
             key={`company_pay_attention${company_name}`}>
            <CellContent small='Обратите внимание:'
                         regular={company_pay_attention ? company_pay_attention : 'Данные отсутствуют'} />
            {
                company_pay_attention ? <Round content={payAttentionTypeIntStatus(payAttentionConstants, company)} /> : null
            }
        </div>
    )
}

const selector = ({payAttentionConstants}) => ({
    payAttentionConstants
});


export default PayAttentionCell;
