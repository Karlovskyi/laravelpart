import React from 'react';
import StyledText from "../../StyledText";
import {useSelector} from "react-redux";
import {reliabilityIntStatus, reliabilityStringStatus} from '../../../models/Company';

const Reliability = ({company, onClick, isOpen}) => {
    const {appConstants, payAttentionConstants} = useSelector(selector);
    const className = isOpen ? 'company-node__label company-node__label_arrow company-node__col' :
        'company-node__label company-node__col'
    let style;
    switch (reliabilityIntStatus(appConstants, payAttentionConstants, company)) {
        case 0:
            style = 'c-red';
            break;
        case 1:
            style = 'c-yellow';
            break;
        case 2:
            style = 'c-green';
            break;
    }
    return (
        <p className={className} data-content="rely" onClick={onClick}>
            <span className="company-node__label-content">
                Надёжность:&nbsp;
                <StyledText text={reliabilityStringStatus(appConstants, payAttentionConstants, company)} style={`text-bold ${style}`} />
            </span>
        </p>
    )
}

const selector = ({appConstants, payAttentionConstants}) => ({
    appConstants,
    payAttentionConstants
});

export default Reliability;
