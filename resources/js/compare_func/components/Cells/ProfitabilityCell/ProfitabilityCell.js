import React from 'react';
import CellContent from "../../CellContent";
import {getCriteriaName, replaceValue} from "../../../helpers/content_helper";
import {State} from "../../State";
import useCompanyFieldStatus from "../../../hooks/useCompanyFieldStatus";

const ProfitabilityCell = ({company, criteria}) => {
    const {company_profitability_clear, company_name} = company;
    const state = useCompanyFieldStatus(company, 'company_profitability')
    return (
        <div className="company-node__item company-node__col company-node__col_bp"
             key={`company_profitability${company_name}`}>
            <CellContent small={getCriteriaName('company_profitability', criteria)}
                         regular={replaceValue(criteria.company_profitability, 'criteria_value_1', company_profitability_clear)} />
            <State field='company_profitability'
                   criteria={criteria}
                   content={state}/>
        </div>
    )
}

export default ProfitabilityCell;
