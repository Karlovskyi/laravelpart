import React from 'react';
import CellContent from "../../CellContent";
import {getCriteriaName, replaceValue, stuffMembersSingular} from "../../../helpers/content_helper";
import {State} from "../../State";
import useCompanyFieldStatus from "../../../hooks/useCompanyFieldStatus";

const StaffCell = ({company, criteria, index, length}) => {
    const {company_staff_members_clear, company_name} = company;
    const state = useCompanyFieldStatus(company, 'company_staff_members')
    const isLast = index === length - 1;
    return (
        <div className="company-node__item company-node__col company-node__col_bp"
             key={`company_staff_members${company_name}`}>
            <CellContent small={getCriteriaName('company_staff_members', criteria)}
                         regular={stuffMembersSingular(replaceValue(criteria.company_staff_members, 'criteria_value_1', company_staff_members_clear), company_staff_members_clear)} />
            <State field='company_staff_members'
                   criteria={criteria}
                   content={state} isLast={isLast}/>
        </div>
    )
}

export default StaffCell;
