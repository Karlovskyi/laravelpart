import React from 'react';
import CellContent from "../../CellContent";
import {getCriteriaName, getTernaryCriteria} from "../../../helpers/content_helper";
import {State} from "../../State";
import useCompanyFieldStatus from "../../../hooks/useCompanyFieldStatus";

const ProceedingsCell = ({company, criteria, index, length}) => {
    const {company_enforcement_proceedings, company_name} = company;
    const state = useCompanyFieldStatus(company, 'company_enforcement_proceedings');
    const isLast = index === length - 1;
    return (
        <div className="company-node__item company-node__col company-node__col_bp"
             key={`company_enforcement_proceedings${company_name}`}>
            <CellContent small={getCriteriaName('company_enforcement_proceedings', criteria)}
                         regular={getTernaryCriteria(company_enforcement_proceedings, criteria.company_enforcement_proceedings)} />
            <State field='company_enforcement_proceedings'
                   criteria={criteria}
                   content={state} isLast={isLast}/>
        </div>
    )
}

export default ProceedingsCell;
