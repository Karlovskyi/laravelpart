import React from 'react';
import CellContent from "../../CellContent";
import {getCriteriaName, getTernaryCriteria} from "../../../helpers/content_helper";
import {State} from "../../State";
import useCompanyFieldStatus from "../../../hooks/useCompanyFieldStatus";

const ColleaguesCell = ({company, criteria, index, length}) => {
    const {company_search_for_colleagues, company_name} = company;
    const state = useCompanyFieldStatus(company, 'company_search_for_colleagues')
    const isLast = index === length - 1;
    return (
        <div className="company-node__item company-node__col company-node__col_bp"
             key={`company_search_for_colleagues${company_name}`}>
            <CellContent small={getCriteriaName('company_search_for_colleagues', criteria)}
                         regular={getTernaryCriteria(company_search_for_colleagues, criteria.company_search_for_colleagues)} />
            <State field='company_search_for_colleagues'
                   criteria={criteria}
                   content={state} isLast={isLast}/>
        </div>
    )
}

export default ColleaguesCell;
