import React from 'react';
import CellContent from "../../CellContent";
import {getCriteriaName, replaceValue} from "../../../helpers/content_helper";
import {State} from "../../State";
import useCompanyFieldStatus from "../../../hooks/useCompanyFieldStatus";

const AutonomyCell = ({company, criteria, index, length}) => {
    const {company_financial_autonomy, company_financial_autonomy_clear, company_name} = company;
    const state = useCompanyFieldStatus(company, 'company_financial_autonomy')
    const isLast = index === length - 1;
    return (
        <div className="company-node__item company-node__col company-node__col_bp"
             key={`company_financial_autonomy${company_name}`}>
            <CellContent small={getCriteriaName('company_financial_autonomy', criteria)}
                         regular={replaceValue(criteria.company_financial_autonomy, 'criteria_value_1', company_financial_autonomy_clear)} />
            <State field='company_financial_autonomy'
                   content={state}
                   criteria={criteria} isLast={isLast}/>
        </div>
    )
}

export default AutonomyCell;
