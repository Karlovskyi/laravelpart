import React from 'react';
import CellContent from "../../CellContent";
import {getBinaryCriteriaRelations, getCriteriaName} from "../../../helpers/content_helper";
import {State} from "../../State";
import useCompanyFieldStatus from "../../../hooks/useCompanyFieldStatus";

const RelationCell = ({company, criteria, index, length}) => {
    const {company_relation_clear, company_name} = company;
    const state = useCompanyFieldStatus(company, 'company_relation')
    const isLast = index === length - 1;
    return (
        <div className="company-node__item company-node__col company-node__col_bp"
             key={`company_relation${company_name}`}>
            <CellContent small={getCriteriaName('company_relation', criteria)}
                         regular={getBinaryCriteriaRelations(criteria.company_relation, company_relation_clear)} />
            <State field='company_relation'
                   criteria={criteria}
                   content={state + 1} isLast={isLast}/>
        </div>
    )
}

export default RelationCell;
