import React from 'react';
import CellContent from "../../CellContent";
import {getBinaryCriteria, getCriteriaName} from "../../../helpers/content_helper";
import {State} from "../../State";
import useCompanyFieldStatus from "../../../hooks/useCompanyFieldStatus";

const BulkRegisterCell = ({company, criteria, index, length}) => {
    const {company_bulk_address_register, company_name} = company;
    const state = useCompanyFieldStatus(company, 'company_bulk_address_register');
    const isLast = index === length - 1;
    return (
        <div className="company-node__item company-node__col company-node__col_bp"
             key={`company_bulk_address_register${company_name}`}>
            <CellContent small={getCriteriaName('company_bulk_address_register', criteria)}
                         regular={getBinaryCriteria(criteria.company_bulk_address_register, company_bulk_address_register)} />
            <State field='company_bulk_address_register'
                   criteria={criteria}
                   content={state}
                   binary isLast={isLast}/>
        </div>
    )
}

export default BulkRegisterCell;
