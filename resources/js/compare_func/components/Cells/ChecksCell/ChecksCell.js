import React from 'react';
import CellContent from "../../CellContent";
import {checksString, getCriteriaName} from "../../../helpers/content_helper";
import {State} from "../../State";

const ChecksCell = ({company, criteria, index, length}) => {
    const {company_checks, company_name} = company;
    const isLast = index === length - 1;
    return (
        <div className="company-node__item company-node__col company-node__col_bp"
             key={`company_checks${company_name}`}>
            <CellContent small={getCriteriaName('company_checks', criteria)}
                         regular={checksString(criteria.company_checks, company_checks)} />
            <State field='company_checks'
                   criteria={criteria}
                   content={null} isLast={isLast}/>
        </div>
    )
}

export default ChecksCell;
