import React from 'react';
import CellContent from "../CellContent/CellContent";

const RateCell = ({rate, k, value, last = false}) => {
    let style;
    if(rate < 3) {
        style = 'c-red'
    } else if(rate < 4) {
        style = 'c-yellow'
    } else {
        style = 'c-green'
    }
    const classList = last ? 'company-node__col_last' : '';
    return (
        <div className={`company-node__item company-node__col company-node__col_bp ${classList}`}>
            <CellContent small={k} regular={`${value} отзывов`} regularStyle='company-node__item-regular_review'/>
            <div className={`company-node__item-mark ${style}`}>
                {Math.round(rate * 100) / 100}
            </div>
        </div>
    )
}
export default RateCell;
