import React, {useContext} from 'react'
import {TableScroll} from "../../contexts/tableScrollContext";

const Arrows = ({scroll = 500}) => {
    const refLink = useContext(TableScroll)
    if(refLink === null) {
        return null;
    }
    return (
        <div className="compare__arrows">
            <i className="compare__arrows-item" onClick={scrollRight(refLink, scroll)}></i>
            <i className="compare__arrows-item" onClick={scrollLeft(refLink, scroll)}></i>
        </div>
    )
}

export default Arrows;

const scrollLeft = (elem, scroll) => () => {
    elem.scrollLeft = elem.scrollLeft + scroll
}
const scrollRight = (elem, scroll) => () => {
    elem.scrollLeft = elem.scrollLeft - scroll
}
