import React, {useState} from 'react';
import Reliability from "../Cells/Reliability";
import Rating from "../Rating";
import RateCell from "../RateCell";
import FirstTableLine from "../FirstTableLine";
import ErrorBoundary from "../ErrorBoundary";
import TableRow from "../TableRow";
import CreditCell from "../Cells/CreditCell";
import WorkTermCell from "../Cells/WorkTermCell";
import StaffCell from "../Cells/StaffCell";
import LeaderCell from "../Cells/LeaderCell";
import PaymentCell from "../Cells/PaymentCell";
import ProfitabilityCell from "../Cells/ProfitabilityCell";
import AddressCell from "../Cells/AddressCell";
import ChecksCell from "../Cells/ChecksCell";
import AutonomyCell from "../Cells/AutonomyCell";
import IncomeCell from "../Cells/IncomeCell";
import ProceedingsCell from "../Cells/ProceedingsCell";
import BulkRegisterCell from "../Cells/BulkRegisterCell";
import ColleaguesCell from "../Cells/ColleaguesCell";
import RelationCell from "../Cells/RelationCell";
import PayAttentionCell from "../Cells/PayAttentionCell";

const TableMain = ({companiesList, criteria, removeActive}) => {
    const [openReliability, setOpenReliability] = useState(true);
    const [openRating, setOpenRating] = useState(true);

    if (companiesList.length === 0) {
        return null
    }

    const toggleReliability = () => {
        setOpenReliability(!openReliability)
    }
    const toggleRating = () => {
        setOpenRating(!openRating)
    }

    const reliabilityClass = openReliability ? 'company-node__line' : 'company-node__line company-node__line_hidden';
    const ratingClass = openRating ? 'company-node__line' : 'company-node__line company-node__line_hidden';
    return (
        <ErrorBoundary>
            <ul className='company-node__main'>
                <FirstTableLine companiesList={companiesList} removeActive={removeActive} />
                <li className="company-node__line">
                    <TableRow companies={companiesList} onClick={toggleReliability} isOpen={openReliability}
                              children={Reliability} />
                </li>
                <li className={reliabilityClass}>
                    <TableRow companies={companiesList} children={CreditCell} criteria={criteria} />
                </li>
                <li className={reliabilityClass}>
                    <TableRow companies={companiesList} children={WorkTermCell} criteria={criteria} />
                </li>
                <li className={reliabilityClass}>
                    <TableRow companies={companiesList} children={StaffCell} criteria={criteria} />
                </li>
                <li className={reliabilityClass}>
                    <TableRow companies={companiesList} children={LeaderCell} criteria={criteria} />
                </li>
                <li className={reliabilityClass}>
                    <TableRow companies={companiesList} children={PaymentCell} criteria={criteria} />
                </li>
                <li className={reliabilityClass}>
                    <TableRow companies={companiesList} children={ProfitabilityCell} criteria={criteria} />
                </li>
                <li className={reliabilityClass}>
                    <TableRow companies={companiesList} children={AddressCell} criteria={criteria} />
                </li>
                <li className={reliabilityClass}>
                    <TableRow companies={companiesList} children={ChecksCell} criteria={criteria} />
                </li>
                <li className={reliabilityClass}>
                    <TableRow companies={companiesList} children={AutonomyCell} criteria={criteria} />
                </li>
                <li className={reliabilityClass}>
                    <TableRow companies={companiesList} children={IncomeCell} criteria={criteria} />
                </li>
                <li className={reliabilityClass}>
                    <TableRow companies={companiesList} children={ProceedingsCell} criteria={criteria} />
                </li>
                <li className={reliabilityClass}>
                    <TableRow companies={companiesList} children={BulkRegisterCell} criteria={criteria} />
                </li>
                <li className={reliabilityClass}>
                    <TableRow companies={companiesList} children={ColleaguesCell} criteria={criteria} />
                </li>
                <li className={reliabilityClass}>
                    <TableRow companies={companiesList} children={RelationCell} criteria={criteria} />
                </li>
                <li className={reliabilityClass}>
                    <TableRow companies={companiesList} children={PayAttentionCell} />
                </li>
                <li className="company-node__line">
                    {
                        companiesList.map((company) => {
                            const {company_mark_average, company_name} = company;
                            let style;
                            if (company_mark_average < 3) {
                                style = 'c-red'
                            } else if (company_mark_average < 4) {
                                style = 'c-yellow'
                            } else {
                                style = 'c-green'
                            }
                            return (
                                <Rating onClick={toggleRating} style={style} content={company_mark_average}
                                        rating={openRating} key={`rate${company_name}`} />
                            )
                        })
                    }
                </li>
                <li className={ratingClass}>
                    {
                        companiesList.map((company) => {
                            const {company_review_quantity_yandex, company_mark_average_yandex, company_name} = company;
                            return (
                                <RateCell k='Яндекс:' key={`yandex${company_name}`}
                                          value={company_review_quantity_yandex} rate={company_mark_average_yandex} />
                            )
                        })
                    }
                </li>
                <li className={ratingClass}>
                    {
                        companiesList.map((company) => {
                            const {company_review_quantity_google, company_mark_average_google, company_name} = company;
                            return (
                                <RateCell k='Google:' key={`google${company_name}`}
                                          value={company_review_quantity_google} rate={company_mark_average_google} />
                            )
                        })
                    }
                </li>
                <li className={ratingClass}>
                    {
                        companiesList.map((company) => {
                            const {company_review_quantity_otzovick, company_mark_average_otzovick, company_name} = company;
                            return (
                                <RateCell k='Отзовик:' key={`otzovick${company_name}`} last
                                          value={company_review_quantity_otzovick}
                                          rate={company_mark_average_otzovick} />
                            )
                        })
                    }
                </li>
            </ul>
        </ErrorBoundary>

    )
}
export default TableMain;
