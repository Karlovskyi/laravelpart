import React, {useState, useEffect, useRef, Fragment, useContext} from 'react';
import {prepareLink} from '../../helpers/content_helper'
import {TableScroll} from "../../contexts/tableScrollContext";
import {isAllBadCompany} from "../../models/Company";
import TableRow from "../TableRow";
import HeaderCell from "../Cells/HeaderCell";

const FirstTableLine = ({companiesList, removeActive}) => {
    const [top, setTop] = useState(1);
    const refLink = useContext(TableScroll)
    const firstLine = useRef(null);
    const secondLine = useRef(null);

    const eventListener = (e) => {
        if(firstLine) {
            setTop(firstLine.current.getBoundingClientRect().top)
        }
    }

    const refEventListener = (e) => {
        if(secondLine) {
            secondLine.current.style.left = `${-refLink.scrollLeft + 20 + refLink.getBoundingClientRect().left}px`
        }
    }

    useEffect(() => {
        window.addEventListener('scroll', eventListener)
        return () => window.removeEventListener('scroll', eventListener);
    }, [])

    useEffect(() => {
        if(refLink) {
            refLink.addEventListener('scroll', refEventListener)
        }
    }, [refLink])

    const className = top > 0 ? 'company-node__line company-node__line_hidden' : 'company-node__line company-node__line_fixed'
    return (
        <Fragment>
            <li className='company-node__line' ref={firstLine}>
                <TableRow companies={companiesList} children={HeaderCell} removeActive={removeActive}/>
            </li>
            <li className={className} ref={secondLine}>
                <TableRow companies={companiesList} children={HeaderCell} removeActive={removeActive}/>
            </li>
        </Fragment>
    )
}

export default FirstTableLine;


