import React from 'react';

const Rating = ({rating, onClick, content, style}) => {
    const classList = rating ? 'company-node__label company-node__label_arrow company-node__col company-node__col_rating' :
        'company-node__label company-node__col company-node__col_rating'
    return (
        <p className={classList} data-content="review" onClick={onClick}>
            <span className="company-node__label-content company-node__label-content_review">
                Рейтинг:&nbsp;
                <span className={`company-node__label-rate company-node__item-mark ${style}`}>
                        {Math.round(content * 100) / 100}
                </span>
            </span>
        </p>
    )
}

export default Rating
