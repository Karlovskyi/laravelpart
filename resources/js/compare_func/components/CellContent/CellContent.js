import React from 'react';

const CellContent = ({small, regular, regularStyle = ''}) => {
    return (
        <div className="company-node__item-wrapper">
            <Small content={small}/>
            <Regular content={regular} regularStyle={regularStyle}/>
        </div>
    )
}

export default CellContent;

const Small = ({content}) => {
    return (
        <div className="company-node__item-small">
            {content}
        </div>
    )
}

const Regular = ({content, regularStyle}) => {
    return (
        <div className={`company-node__item-regular ${regularStyle}`}>
            {content}
        </div>
    )
}
