class DeleteRedirection {
    constructor() {
        this.deleteButton = document.querySelector('#delete')
        this.submitDeleteForm()
    }

    submitDeleteForm() {
        if (this.deleteButton) {
            this.deleteButton.addEventListener('click', () => {
                const form = document.forms[0];
                const input = form.querySelector('input[name="_method"]');
                input.value = "DELETE";
                form.submit()
            })
        }
    }
}

new DeleteRedirection();

