const starPrinting = require('../src/star_painting').starPainting;
const more = document.querySelector('.company-reviews__more');
const spinner = document.querySelector('.company-reviews__spinner');
const error = document.querySelector('.company-reviews__error');

const GetReviews = (page = 1) => {
    const container = document.querySelector('.company-reviews__container');
    const company_id = container.dataset.company_id;

    more.style.display = 'none';
    spinner.style.display = 'flex';

    fetch(`/api/rating/companies/${company_id}/reviews?page=${page}`)
        .then(data => data.json())
        .then(({ data }) => {
            const nodeList = data.map(({review_date, review_mark, reviewer_name, review_link, review_text, images}) => {
                return MakeHtml(review_date, review_mark, reviewer_name, review_link, review_text, images);
            })
            nodeList.forEach(node => {
                container.insertAdjacentHTML('beforeend', node);
            })
            starPrinting();
            more.style.display = 'block';
            spinner.style.display = 'none';
        })
        .catch(err => {
            spinner.style.display = 'none';
            error.style.display = 'block';
        })
}


const getImages = (images) => {
    if(images.length !== 0)
    {
        let str = `<div class="company-reviews__item-image-wrapper">`;

        images.forEach(({image_path}) => {
            str+=`<img src="${image_path}" alt="review photo" class="company-reviews__item-image">`
        })
        str+=`</div>`;
        return str;
    } else return '';
}


const MakeHtml = (reviewDate, mark, name, link, text, images) => {
    const star = `<div class="stars">
                        <div class="stars__gray" data-content="${mark}">
                            <img src="/images/star.svg" alt="star">
                            <img src="/images/star.svg" alt="star">
                            <img src="/images/star.svg" alt="star">
                            <img src="/images/star.svg" alt="star">
                            <img src="/images/star.svg" alt="star">
                            <span class="stars__yellow">
                                <img src="/images/star_orange.svg" alt="star">
                                <img src="/images/star_orange.svg" alt="star">
                                <img src="/images/star_orange.svg" alt="star">
                                <img src="/images/star_orange.svg" alt="star">
                                <img src="/images/star_orange.svg" alt="star">
                            </span>
                        </div>
                        <div class="stars__mark-average">
                            ${mark}
                        </div>
                    </div>`
    return (
        `
        <div class="company-reviews__item">
            <div class="company-reviews__item-info-line">
                <div class="company-reviews__item-mark">
                   <a href="${link}" class="company-reviews__item-source">${name}</a>
                    ${star}
                </div>
                <div class="company-reviews__item-date">
                    ${reviewDate}
                </div>
            </div>
            <div class="company-reviews__item-text">${text}</div>
            ${getImages(images)}

        </div>
        `
    )
}
let PAGE = 2;

GetReviews();

const buttonMore = () => {
    const button = document.querySelector('.company-reviews__more')
    button.addEventListener('click', () => {
        GetReviews(PAGE);
        PAGE++;
    })
}

buttonMore();
